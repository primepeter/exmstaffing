library universal_ui;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_quill/flutter_quill.dart';
import 'package:Loopin/AppEngine.dart' hide CachedNetworkImage;
import 'package:Loopin/assets.dart';
import 'package:universal_html/html.dart' as html;

import 'fake_ui.dart' if (dart.library.html) 'real_ui.dart' as ui_instance;
import 'responsive_widget.dart';

class PlatformViewRegistryFix {
  void registerViewFactory(dynamic x, dynamic y) {
    if (kIsWeb) {
      ui_instance.PlatformViewRegistry.registerViewFactory(
        x,
        y,
      );
    }
  }
}

class UniversalUI {
  PlatformViewRegistryFix platformViewRegistry = PlatformViewRegistryFix();
}

var ui = UniversalUI();

Widget defaultEmbedBuilderWebX(BuildContext context, Embed node) {
  switch (node.value.type) {
    case 'image':
      final String imageUrl = node.value.data;
      final size = MediaQuery.of(context).size;
      UniversalUI().platformViewRegistry.registerViewFactory(
          imageUrl, (viewId) => html.ImageElement()..src = imageUrl);
      return Padding(
        padding: EdgeInsets.only(
          right: ResponsiveWidget.isMediumScreen(context)
              ? size.width * 0.5
              : (ResponsiveWidget.isLargeScreen(context))
                  ? size.width * 0.75
                  : size.width * 0.2,
        ),
        child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.45,
          // child: HtmlElementView(
          //   viewType: imageUrl,
          // ),
          child: CachedNetworkImage(
            // imageUrl: imageUrl,
            imageUrl:
                "https://firebasestorage.googleapis.com/v0/b/liamanah.appspot.com/o/9d900780-dcb7-11eb-9e62-6b8d2f5d3efe?alt=media&token=7874a8ad-0612-46eb-99e2-d988d55f7c53",
            height: 250,
            width: double.infinity,
            fit: BoxFit.cover,
            alignment: Alignment.topCenter,
            placeholder: (c, link) {
              return loadingLayout();
            },
          ),
        ),
      );

    default:
      throw UnimplementedError(
        'Embeddable type "${node.value.type}" is not supported by default '
        'embed builder of QuillEditor. You must pass your own builder function '
        'to embedBuilder property of QuillEditor or QuillField widgets.',
      );
  }
}

Widget defaultEmbedBuilderWeb(
    BuildContext context, Embed embed, bool readOnly) {
  switch (embed.value.type) {
    case BlockEmbed.imageType:
      return _buildImage(context, embed);
    case BlockEmbed.horizontalRuleType:
      return const Divider();
    default:
      throw UnimplementedError('Embed "${embed.value.type}" is not supported.');
  }
}

Widget _buildImage(BuildContext context, Embed embed) {
  final imageUrl = embed.value.data;
  return Container(
    height: 300,
    alignment: Alignment.center,
    child: Builder(
      builder: (c) {
        if (imageUrl.isEmpty) return loadingLayout();

        return ClipRRect(
          borderRadius: BorderRadius.circular(5),
          child: Image.network(
            imageUrl,
            // courseCoverImage,
            fit: BoxFit.cover,
            height: 300,
            width: 300,
            alignment: Alignment.topCenter,
          ),
        );
      },
    ),
    decoration: BoxDecoration(
        color: Colors.black.withOpacity(.05),
        borderRadius: BorderRadius.circular(5),
        border: Border.all(color: black.withOpacity(.09))),
  );
}
