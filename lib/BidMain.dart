import 'dart:io';

import 'package:Loopin/basemodel.dart';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/assets.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class BidMain extends StatefulWidget {
  BaseModel produce;
  BidMain({this.produce});
  @override
  _BidMainState createState() => _BidMainState();
}

class _BidMainState extends State<BidMain> {
  MoneyMaskedTextController priceController = new MoneyMaskedTextController(
      decimalSeparator: ".", thousandSeparator: ",");

  int clickBack = 0;
  BaseModel produce;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    produce = widget.produce;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext c) {
    return WillPopScope(
        onWillPop: () {
          int now = DateTime.now().millisecondsSinceEpoch;
          if ((now - clickBack) > 5000) {
            clickBack = now;
            showError("Click back again to exit");
            return;
          }
          Navigator.pop(context);
          return;
        },
        child: SafeArea(
          child: Scaffold(
              resizeToAvoidBottomInset: true,
              backgroundColor: transparent,
              body: page()),
        ));
  }

  ScrollController mainScroll = ScrollController();
  page() {
    return pageBuilder(context, pageChild(), (DragUpdateDetails dragUpdate) {
      mainScroll.position.moveTo(dragUpdate.globalPosition.dy * 3.5);
      setState(() {});
    });
  }

  pageChild() {
    String country = produce.getString(COUNTRY);
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        addSpace(10),
        new Container(
          width: double.infinity,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: black,
                      size: 25,
                    )),
                  )),
              Flexible(
                fit: FlexFit.tight,
                flex: 1,
                child: Text(
                  "",
                  style: textStyle(true, 20, black),
                ),
              ),
              Text(
                "Initial Price",
                style: textStyle(false, 18, dark_green0.withOpacity(.5)),
              ),
              addSpaceWidth(10),
              priceBox(produce.getString(COUNTRY), produce.getDouble(PRICE)),
              addSpaceWidth(15),
            ],
          ),
        ),
        //addLine(1, black.withOpacity(.2), 0, 5, 0, 0),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          height: errorText.isEmpty ? 0 : 40,
          color: red0,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Center(
              child: Text(
            errorText,
            style: textStyle(true, 16, white),
          )),
        ),
        Expanded(
          flex: 1,
          child: Center(
            child: SingleChildScrollView(
                controller: mainScroll,
                padding: EdgeInsets.all(20),
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "I will buy it for",
                        style: textStyle(false, 40, black.withOpacity(.5)),
                      ),
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 2),
                            child: CachedNetworkImage(
                              imageUrl: getCurrencyLogo(country),
//                              width: 25,
                              height: 25, fit: BoxFit.cover,
                              color: black.withOpacity(.3),
                            ),
                          ),
                          Flexible(
                            child: TextField(
                              onSubmitted: (_) {
                                //postHeadline();
                              },
//                textInputAction: maxLine==1?TextInputAction.done:TextInputAction.newline,
                              textCapitalization: TextCapitalization.sentences,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                contentPadding:
                                    EdgeInsets.fromLTRB(10, 10, 15, 10),
                                counter: null,
                              ),
                              style: textStyle(
                                false,
                                35,
                                black,
                              ),
                              controller: priceController,
                              autofocus: true,
                              cursorColor: black,
                              cursorWidth: 1,
//                          maxLength: 50,
                              maxLines: 1,
                              keyboardType: (TextInputType.number),
                              scrollPadding: EdgeInsets.all(0),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        height: 50,
//               margin: EdgeInsets.fromLTRB(20, 0, 20, 20),
                        width: double.infinity,
                        child: FlatButton(
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
//                                padding: EdgeInsets.all(0),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(25),
                                side: BorderSide(color: dark_green0, width: 2)),
                            color: transparent,
                            onPressed: () {
                              post();
                            },
                            child: Text(
                              "Send Offer",
                              style: textStyle(true, 20, dark_green0),
                            )),
                      ),
                      addSpace(50),
                    ])),
          ),
        ),
      ],
    );
  }

  void post() {
    double defPrice = produce.getDouble(PRICE);
    double currencyValue = priceController.numberValue;

    if (currencyValue > defPrice) {
      showErrorDialog(
          context, "Your bid cannot be more than the initial price");
      return;
    }
    if (currencyValue < (defPrice / 2)) {
      showErrorDialog(
          context, "Your bid cannot be less than half the initial price");
      return;
    }

    Navigator.pop(context,
        "I will buy it for ${getCurrencyText(produce.getString(COUNTRY))}${formatAmount(currencyValue.toStringAsFixed(2))}");
  }

  String errorText = "";
  showError(String text) {
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 1), () {
      errorText = "";
      setState(() {});
    });
  }
}
