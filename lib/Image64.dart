

 import 'dart:convert';

import 'package:flutter/material.dart';

class Image64 extends StatefulWidget {
  String passportPhoto;
  Image64(this.passportPhoto);
  @override
  _Image64State createState() => _Image64State();
}

class _Image64State extends State<Image64> {
  String passportPhoto;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    passportPhoto=widget.passportPhoto;
  }
  @override
  Widget build(BuildContext context) {
    return child();
  }

  child(){

    return passportPhoto==null?Container():
    Image.memory(base64.decode(passportPhoto),width: double.infinity,height: 150,fit: BoxFit.cover);
  }
}

