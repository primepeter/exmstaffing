import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io' as io;
import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/auth/SignUp.dart';
import 'package:Loopin/main_pages/Dashboard.dart';
import 'package:Loopin/widgets/shimmerListLoader.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'basemodel.dart';
import 'forms/AddEmployeeForm.dart';
import 'forms/AddSkillsForm.dart';
import 'forms/AddUserForm.dart';
import 'forms/FilterForm.dart';
import 'main_pages/EmployeePage.dart';
import 'main_pages/JobPage.dart';
import 'main_pages/NursingPage.dart';
import 'main_pages/RecruitmentPage.dart';
import 'main_pages/RequestPage.dart';
import 'main_pages/WorkPage.dart';

import 'main_pages/InstructionPage.dart';
import 'main_pages/UsersPage.dart';

final offerReloadController = StreamController<bool>.broadcast();
final pageChangeController = StreamController<int>.broadcast();

List<BaseModel> instructionsList = [];
bool instructionsBusy = true;

class PageTabs {
  Widget page;
  List privileges;
  String title;
  IconData icon;
  int count = 0;

  PageTabs(
      {this.page,
      this.count,
      @required this.privileges,
      @required this.title,
      @required this.icon});
}

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<StreamSubscription> subs = [];
  bool setup = false;

  List<PageTabs> get pageTabs {
    return [
      PageTabs(
        // page: RecruitmentPage(),
        page: Dashboard(),
        privileges: [0, 1],
        title: "DASHBOARD",
        icon: Icons.dashboard,
      ),
      PageTabs(
          page: UsersPage(),
          privileges: [0],
          title: "USERS",
          icon: Icons.person,
          count: appSettingsModel.getList(USER_BASE_COUNT).length),
      PageTabs(
          page: WorkPage(),
          privileges: [0, 1],
          title: "WORK ORDERS",
          icon: Icons.group_work,
          count: appSettingsModel.getList(WORK_BASE_COUNT).length),
      PageTabs(
          page: EmployeePage(),
          privileges: [0, 1],
          title: "EMPLOYEES",
          icon: Icons.people_alt,
          count: appSettingsModel.getList(EMPLOYEE_BASE_COUNT).length),
      PageTabs(
          page: RecruitmentPage(),
          privileges: [0, 1],
          title: "RECRUITMENT",
          icon: Icons.repeat),
      PageTabs(
          page: JobPage(),
          privileges: [0, 1],
          title: "WORK ASSIGNMENT",
          icon: Icons.home_work),
      PageTabs(
          page: InstructionPage(),
          privileges: [0, 1],
          title: "INSTRUCTIONS",
          icon: Icons.message),
      PageTabs(
          page: RequestPage(),
          privileges: [0, 2],
          title: "CLIENT REQUESTS",
          icon: Icons.format_quote),
      PageTabs(
          page: NursingPage(),
          privileges: [0, 1],
          title: "NURSING HOMES",
          icon: Icons.healing),
      PageTabs(
          page: Container(),
          privileges: [0],
          title: "JOB SKILLS",
          icon: Icons.handyman,
          count: appSettingsModel.getList(SKILLS).length),
    ];
  }

  List<PageTabs> get privilegeTabs => pageTabs
      .where((e) => e.privileges.contains(userModel.getInt(PRIVILEGE_INDEX)))
      .toList();

  List<Widget> get privilegePages => pageTabs
      .where((e) => e.privileges.contains(userModel.getInt(PRIVILEGE_INDEX)))
      .toList()
      .where((e) => null != e.page)
      .toList()
      .map((e) => e.page)
      .toList();

  int currentPage = 0;
  PageController pageController = PageController();
  final scaffoldKey = GlobalKey(); // Create a key

  @override
  initState() {
    super.initState();
    var sub = offerReloadController.stream.listen((event) async {});
    var sub1 = pageChangeController.stream.listen((p) async {
      pageController.jumpToPage(p);
    });
    subs.add(sub);
    subs.add(sub1);
    Future.delayed(Duration(seconds: 1), () {
      loadSettings();
      createUserListener();
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  @override
  didChangeDependencies() {
    super.didChangeDependencies();
  }

  loadSettings() async {
    var settingsSub = FirebaseFirestore.instance
        .collection(APP_SETTINGS_BASE)
        .doc(APP_SETTINGS)
        .snapshots()
        .listen((shot) {
      if (shot != null) {
        appSettingsModel = BaseModel(doc: shot);
        List banned = appSettingsModel.getList(BANNED);
        if (banned.contains(userModel.getObjectId()) ||
            banned.contains(userModel.getString(DEVICE_ID)) ||
            banned.contains(userModel.getEmail())) {
          io.exit(0);
        }
        setState(() {});
      }
    });
    subs.add(settingsSub);
  }

  void createUserListener() async {
    // return;
    // if (userModel.getObjectId().isEmpty) return;

    // final _auth = FirebaseAuth.instance;
    // final user = _auth.currentUser;

    final pref = await SharedPreferences.getInstance();
    String user = pref.getString("user");

    if (null == user) {
      Future.delayed(Duration(milliseconds: 500), () {
        Navigator.pushReplacementNamed(context, "/setter");

        // pushAndResult(context, SignUp(), fade: true, clear: true);
      });
      return;
    }
    var userSub = FirebaseFirestore.instance
        .collection(USER_BASE)
        // .doc(userModel.getObjectId())
        .doc(user)
        // .doc(user.uid)
        // .doc("9c1PR9nYuYHuTle0wZiK")
        .snapshots()
        .listen((shot) async {
      if (!shot.exists) {
        Future.delayed(Duration(milliseconds: 500), () {
          Navigator.pushReplacementNamed(context, "/setter");
        });

        return;
      }
      if (shot != null) {
        userModel = BaseModel(doc: shot);
        isAdmin = userModel.getBoolean(IS_ADMIN);
        for (String s in userModel.getList(BLOCKED)) {
          if (!blockedIds.contains(s)) blockedIds.add(s);
        }
        if (!setup) {
          setup = true;
          listenToInstructions();
        }
        if (mounted) setState(() {});
      }
    });
    subs.add(userSub);
  }

  int unreadCount = 0;

  listenToInstructions() async {
    print(userModel.getObjectId());

    Query query = FirebaseFirestore.instance.collection(INSTRUCTION_BASE);

    if (userModel.getInt(PRIVILEGE_INDEX) != 0) {
      query = FirebaseFirestore.instance
          .collection(INSTRUCTION_BASE)
          .where(PARTIES, arrayContainsAny: ["All", userModel.getObjectId()]);
    }

    query //.limit(1)
        .orderBy(TIME)
        .snapshots()
        .listen((value) {
      for (var doc in value.docs) {
        final bm = BaseModel(doc: doc);
        bool unread = !bm.getList(READ_BY).contains(userModel.getObjectId());

        if (unread) unreadCount = unreadCount + 1;

        int p = instructionsList
            .indexWhere((e) => e.getObjectId() == bm.getObjectId());
        if (p == -1) {
          instructionsList.add(bm);
        } else {
          instructionsList[p] = bm;
        }
      }
      instructionsBusy = false;
      setState(() {});
    });
  }

  menuPage([bool useDrawer = false]) {
    if (useDrawer)
      return Drawer(
        child: Container(
            width: 200,
            color: appColor,
            child: Column(
              children: [
                Container(
                  height: 80,
                  width: 170,
                  padding: EdgeInsets.only(left: 20, right: 20),
                  // color: black,
                  alignment: Alignment.centerLeft,
                  // child: Image.asset(
                  //   "assets/icons/st_logo.png",
                  //   scale: 1.6,
                  // ),
                ),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    // mainAxisAlignment: MainAxisAlignment.center,
                    children: List.generate(privilegeTabs.length, (p) {
                      bool active = p == currentPage;
                      final tabs = privilegeTabs[p];
                      String title = tabs.title;
                      var icon = tabs.icon;
                      int count = tabs.count;
                      return InkWell(
                        onTap: () {
                          if (title == "JOB SKILLS") {
                            pushAndResult(context, AddSkillsForm(), fade: true,
                                result: (_) {
                              setState(() {});
                            });
                            return;
                          }
                          if (title == "INSTRUCTIONS") {
                            unreadCount = 0;
                            for (var bm in instructionsList) {
                              bm
                                ..putInList(READ_BY, userModel.getObjectId())
                                ..updateItems(delaySeconds: 1);
                            }
                            setState(() {});
                          }

                          pageController.jumpToPage(p);
                          Navigator.pop(context);
                        },
                        child: Container(
                          height: 70,
                          color: transparent,
                          alignment: Alignment.centerLeft,
                          child: Row(
                            children: [
                              Container(
                                width: 5,
                                height: 50,
                                decoration: BoxDecoration(
                                    color: Colors.orange
                                        .withOpacity(active ? 1 : 0),
                                    borderRadius: BorderRadius.circular(10)),
                              ),
                              addSpaceWidth(5),
                              Icon(
                                icon,
                                color: white.withOpacity(active ? 1 : 0.8),
                                size: 20,
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child: Text(
                                  title,
                                  style: TextStyle(
                                      fontSize: active ? 15 : 12,
                                      fontWeight:
                                          active ? FontWeight.bold : null,
                                      color:
                                          white.withOpacity(active ? 1 : 0.8)),
                                ),
                              ),
                              if (p == 5 && unreadCount > 0)
                                Container(
                                  margin: EdgeInsets.only(left: 10),
                                  height: 10,
                                  width: 10,
                                  decoration: BoxDecoration(
                                      color: red, shape: BoxShape.circle),
                                )
                            ],
                          ),
                        ),
                      );
                    }),
                  ),
                ),
              ],
            )),
      );

    return Container(
        width: 200,
        color: appColor,
        child: Column(
          children: [
            Container(
              height: 80,
              width: 200,
              padding: EdgeInsets.only(left: 20, right: 20),
              color: black,
              alignment: Alignment.centerLeft,
              // child: Image.asset(
              //   "assets/icons/st_logo.png",
              //   scale: 1.6,
              // ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: List.generate(privilegeTabs.length, (p) {
                    bool active = p == currentPage;

                    final tabs = privilegeTabs[p];
                    String title = tabs.title;
                    var icon = tabs.icon;
                    int count = tabs.count;

                    return InkWell(
                      onTap: () {
                        if (title == "JOB SKILLS") {
                          pushAndResult(context, AddSkillsForm(), fade: true,
                              result: (_) {
                            setState(() {});
                          });
                          return;
                        }
                        if (title == "INSTRUCTIONS") {
                          unreadCount = 0;
                          for (var bm in instructionsList) {
                            bm
                              ..putInList(READ_BY, userModel.getObjectId())
                              ..updateItems(delaySeconds: 1);
                          }
                          setState(() {});
                        }

                        pageController.jumpToPage(p);
                      },
                      child: Container(
                        height: 65,
                        color: transparent,
                        alignment: Alignment.centerLeft,
                        child: Row(
                          children: [
                            Container(
                              width: 5,
                              height: 50,
                              decoration: BoxDecoration(
                                  color:
                                      Colors.orange.withOpacity(active ? 1 : 0),
                                  borderRadius: BorderRadius.circular(10)),
                            ),
                            addSpaceWidth(5),
                            Icon(
                              icon,
                              color: white.withOpacity(active ? 1 : 0.8),
                              size: 20,
                            ),
                            addSpaceWidth(10),
                            if (p == 5 && unreadCount > 0)
                              Container(
                                margin: EdgeInsets.only(right: 5),
                                height: 10,
                                width: 10,
                                decoration: BoxDecoration(
                                    border: Border.all(color: white),
                                    color: red,
                                    shape: BoxShape.circle),
                              ),
                            Expanded(
                              child: Text(
                                title,
                                style: TextStyle(
                                    fontSize: active ? 15 : 12,
                                    fontWeight: active ? FontWeight.bold : null,
                                    color: white.withOpacity(active ? 1 : 0.8)),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }),
                ),
              ),
            ),
          ],
        ));
  }

  appBar(
    BuildContext context,
    bool isLargeScreen,
  ) {
    return Container(
      height: 100,
      padding: EdgeInsets.only(left: 0, right: 5),
      color: appColor,
      alignment: Alignment.centerLeft,
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(4),
            child: Image.asset(
              "assets/icons/txt_logo.jpg",
              scale: 5,
            ),
          ),
          // addSpaceWidth(10),
          // Text(
          //   "Exmstaffing",
          //   style: TextStyle(
          //       fontSize: 25, color: white, fontWeight: FontWeight.bold),
          // ),
          Spacer(),
          Container(
            margin: EdgeInsets.only(right: isLargeScreen ? 0 : 5),
            child: TextButton(
              onPressed: () {
                yesNoDialog(
                    context, "Logout?", "Are you sure you want to logout?",
                    () async {
                  final pref = await SharedPreferences.getInstance();
                  pref.clear();
                  userModel = BaseModel();
                  for (var s in subs) s?.cancel();
                  Navigator.pushReplacementNamed(context, "/setter");
                });
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Logout",
                    style: TextStyle(fontSize: 12),
                  ),
                  addSpaceWidth(4),
                  Icon(
                    Icons.exit_to_app,
                    size: 18,
                  ),
                ],
              ),
              style: TextButton.styleFrom(
                  padding: EdgeInsets.zero,
                  primary: black,
                  backgroundColor: white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                  minimumSize: Size(120, 50)),
            ),
          ),
          if (!isLargeScreen)
            IconButton(
                onPressed: () {
                  // scaffoldKey.currentState.openDrawer();
                  Scaffold.of(context).openDrawer();
                },
                icon: Icon(
                  Icons.menu,
                  color: white,
                ))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      //backgroundColor: appColor,
      drawer: menuPage(true),
      body: Builder(
        builder: (con) {
          return LayoutBuilder(builder: (c, box) {
            bool isLargeScreen = box.maxWidth > 850;
            final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;
            return Stack(
              children: [
                Row(
                  children: [
                    if (isLargeScreen) menuPage(),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(top: 110),
                        child: PageView(
                          controller: pageController,
                          onPageChanged: (p) {
                            currentPage = p;
                            setState(() {});
                          },
                          physics: NeverScrollableScrollPhysics(),
                          children: privilegePages,
                        ),
                      ),
                      //oluwatoyinoguneso@gmail.com
                    )
                  ],
                ),
                appBar(
                  con,
                  isLargeScreen,
                )
              ],
            );
          });
        },
      ),
    );
  }
}

appBarHeader(BuildContext context,
    {bool isLargeScreen = true, bool showLogout = false}) {
  return Container(
    height: 100,
    padding: EdgeInsets.only(left: 20, right: 5),
    color: appColor,
    alignment: Alignment.centerLeft,
    child: Row(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(4),
          child: Image.asset(
            "assets/icons/logo.png",
            height: 50,
            width: 50,
          ),
        ),
        addSpaceWidth(10),
        Text(
          "Exmstaffing",
          style: TextStyle(
              fontSize: 25, color: white, fontWeight: FontWeight.bold),
        ),
        Spacer(),
        if (showLogout)
          Container(
            margin: EdgeInsets.only(right: isLargeScreen ? 0 : 5),
            child: TextButton(
              onPressed: () {
                yesNoDialog(
                    context, "Logout?", "Are you sure you want to logout?",
                    () async {
                  final pref = await SharedPreferences.getInstance();
                  pref.clear();
                  userModel = BaseModel();
                  Navigator.pushReplacementNamed(context, "/setter");
                });
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Logout",
                    style: TextStyle(fontSize: 12),
                  ),
                  addSpaceWidth(4),
                  Icon(
                    Icons.exit_to_app,
                    size: 18,
                  ),
                ],
              ),
              style: TextButton.styleFrom(
                  padding: EdgeInsets.zero,
                  primary: black,
                  backgroundColor: white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                  minimumSize: Size(120, 50)),
            ),
          ),
        if (!isLargeScreen)
          IconButton(
              onPressed: () {
                // scaffoldKey.currentState.openDrawer();
                Scaffold.of(context).openDrawer();
              },
              icon: Icon(
                Icons.menu,
                color: white,
              ))
      ],
    ),
  );
}
