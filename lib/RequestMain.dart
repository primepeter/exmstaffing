import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:timeago/timeago.dart';

class RequestMain extends StatefulWidget {
  String id;
  BaseModel model;
  RequestMain({this.model, this.id});
  @override
  _RequestMainState createState() => _RequestMainState();
}

class _RequestMainState extends State<RequestMain> {
  bool setup = false;
  bool fadeBack = false;
  BaseModel model;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.model != null) {
      setup = true;
      model = widget.model;
    } else {
      loadItem(widget.id);
    }
    Timer.periodic(Duration(milliseconds: 800), (timer) {
      if (!mounted) {
        timer.cancel();
        return;
      }
      fadeBack = !fadeBack;
      setState(() {});
    });
  }

  loadItem(String id) async {
    DocumentSnapshot shot =
        await FirebaseFirestore.instance.collection(REQUEST_BASE).doc(id).get();

    if (shot == null) return;

    model = BaseModel(doc: shot);
    setup = true;
    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getWhiteColor(),
      body: page(),
    );
  }

  page() {
    return Column(
      children: [
        addSpace(50),
        Row(
          children: [
            GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  width: 50,
                  height: 30,
                  child: Center(
                      child: Icon(
                    Icons.arrow_back_ios,
                    color: getBlackColor(),
                    //                    size: 20,
                  )),
                )),
            Flexible(
              child: Text(
                "",
                style: textStyle(true, 20, getBlackColor()),
              ),
            )
          ],
        ),
        addSpace(10),
        Expanded(
          child: !setup
              ? loadingLayout()
              : Builder(
                  builder: (c) {
                    double amount = model.getDouble(AMOUNT);
                    int status = model.getInt(STATUS);
                    int type = model.getInt(TYPE);
                    String cryptoIcon = model.getString(CRYPTO_ICON);
                    String cryptoName = model.getString(CRYPTO_NAME);
                    String cryptoId = model.getString(CRYPTO_ID);
                    return SingleChildScrollView(
                      child: Column(
                        children: [
                          Container(
                            margin:
                                EdgeInsets.only(bottom: 5, left: 20, right: 20),
                            child: Stack(
                              children: [
                                Card(
                                  color: status == STATUS_OPEN
                                      ? white
                                      : default_white,
                                  elevation: 5,
                                  shadowColor: black.withOpacity(.1),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      side: BorderSide(
                                          color: black.withOpacity(.1),
                                          width: .5)),
                                  child: Padding(
                                    padding: const EdgeInsets.all(15),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        // addSpace(3),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Icon(
                                              Icons.format_quote,
                                              color: black.withOpacity(.3),
                                            ),
                                            addSpaceWidth(10),
                                            Flexible(
                                              child: RichText(
                                                text: TextSpan(children: [
                                                  TextSpan(
                                                      text:
                                                          "I want to ${type == BUYING ? "buy" : "sell"} ",
                                                      style: textStyle(
                                                          false, 18, black,
                                                          height: 1.5)),
                                                  WidgetSpan(
                                                      child: Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  bottom: 4),
                                                          child: Image.asset(
                                                            naira,
                                                            width: 12,
                                                            height: 12,
                                                            color: red0,
                                                          ))),
                                                  TextSpan(
                                                      text: formatAmount(amount,
                                                          decimal: 0),
                                                      style: textStyle(
                                                          true, 18, red0,
                                                          height: 1.5)),
                                                  TextSpan(
                                                      text: " worth of ",
                                                      style: textStyle(
                                                          false, 18, black,
                                                          height: 1.5)),
                                                  TextSpan(
                                                      text: "$cryptoName ",
                                                      style: textStyle(
                                                          true,
                                                          18,
                                                          type == BUYING
                                                              ? blue0
                                                              : light_green3,
                                                          height: 1.5)),
                                                ]),
                                              ),
                                            ),
                                          ],
                                        ),
                                        addSpace(5),
                                        Text(
                                          format(
                                              DateTime
                                                  .fromMillisecondsSinceEpoch(
                                                      model.getTime()),
                                              locale: "en"),
                                          style: textStyle(
                                              false, 14, black.withOpacity(.3)),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.topRight,
                                  child: Container(
                                    margin: EdgeInsets.all(15),
                                    child: CachedNetworkImage(
                                        imageUrl: cryptoIcon,
                                        width: 20,
                                        height: 20),
                                  ),
                                )
                              ],
                            ),
                          ),
                          levelItem("Seller sends crypto to Loopin", 2),
                          levelItem("Loopin confirms receipt of crypto", 2),
                          levelItem(
                              "Waiting for an interested ${type == BUYING ? "seller" : "buyer"}",
                              model.getString(INTERESTED_ID).isEmpty ? 1 : 2),
                          levelItem("Buyer sends payment to seller", 0),
                          levelItem("Seller confirms receipt of payment", 0),
                          levelItem("Loopin sends crypto to buyer", 0),
                          addSpace(20),
                          Icon(Icons.emoji_emotions_outlined,
                              size: 50, color: blue0),
                          addSpace(100)
                        ],
                      ),
                    );
                  },
                ),
        )
      ],
    );
  }

  levelItem(String text, int status) {
    bool pending = status == 0;
    bool current = status == 1;
    bool completed = status == 2;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          width: 1,
          height: 50,
          color: blue0,
        ),
        Opacity(
          opacity: completed ? (1) : (.3),
          child: Stack(
            children: [
              Container(
                width: double.infinity,
                margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: blue0, width: 1)),
                padding: EdgeInsets.all(5),
                child: AnimatedContainer(
                  duration: Duration(milliseconds: 500),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: current && fadeBack
                          ? red0
                          : completed
                              ? blue0
                              : transparent),
                  padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                  child: Center(
                    child: Text(
                      text,
                      style: textStyle(true, 18, completed ? white : black),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  resetRequest() async {}
}
