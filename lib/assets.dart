import 'package:flutter/material.dart';
import 'package:Loopin/basemodel.dart';

// e58f7910-2c00-11ec-a387-a579b260ead9
// 0e1c6780-20fd-11ec-8188-0fd645392479

const String OFFER_STATUS = "offerStatus";

const String EMAIL_BASE = "emailBase";
const String DRIVERS_LICENCE_URL = "driversLicenceUrl";
const String SSN_CARD_URL = "ssnCardUrl";
const String BLS_URL = "blsUrl";
const String TB_URL = "tbUrl";
const String STATE_LICENCE_URL = "stateLicenceUrl";
const String COVID_URL = "covidUrl";
const String RESIDENT_URL = "residentUrl";

const String DRIVERS_LICENCE_EXPIRY = "driversLicenceExpiry";
const String SSN_CARD_EXPIRY = "ssnCardExpiry";
const String BLS_EXPIRY = "blsExpiry";
const String TB_EXPIRY = "tbExpiry";
const String STATE_LICENCE_EXPIRY = "stateLicenceExpiry";
const String COVID_EXPIRY = "covidExpiry";
const String RESIDENT_EXPIRY = "residentExpiry";

const String DRIVERS_LICENCE_EXT = "driversLicenceExt";
const String SSN_CARD_EXT = "ssnCardExt";
const String BLS_EXT = "blsExt";
const String TB_EXT = "tbExt";
const String STATE_LICENCE_EXT = "stateLicenceExt";
const String COVID_EXT = "covidExt";
const String RESIDENT_EXT = "residentExt";

const String CLOCKING_INFO = "clockingInfo";
const String CLOCKED_IN = "clockedIn";
const String CLOCKED_OUT = "clockedOut";

const int TYPE_OLD_EMPLOYEES = 0;
const int TYPE_NEW_EMPLOYEES = 1;
const String APT_NUMBER = "aptNumber";

const String CITIZEN_INDEX = "citizenIndex";
const String TRANS_INDEX = "transIndex";
const String TRANS_ADDRESS = "transAddress";
const String TRANS_ZIP = "transZip";
const String TRANS_STATE = "transCity";
const String TRANS_CITY = "transState";

const String CONTACT_NAME = "contactName";
const String CONTACT_TELEPHONE = "contactTelephone";
const String CONTACT_RELATIONSHIP = "contactRelationship";
const String CONTACT_ADDRESS = "contactAddress";
const String CONTACT_ZIP = "contactZip";
const String CONTACT_STATE = "contactCity";
const String CONTACT_CITY = "contactState";
const String RELATIONSHIP = "relationship";

const String COORDINATORS_LIST = "coordinatorsList";
const String JOB_PENDING_LIST = "jobPendingList";
const String JOB_COMPLETED_LIST = "jobCompletedList";
const String JOB_ASSIGNED_LIST = "jobAssignedList";
const String AGREEMENT_DATE = "agreementDate";
const String SKILL_CHECK = "skillCheck";

const String SALARY_DESIRED = "salaryDesired";
const String PRESENT_ADDRESS = "presentAddress";
const String PRESENT_STATE = "presentState";
const String PRESENT_CITY = "presentCity";
const String PRESENT_ZIP = "presentZipCode";
const String PERMANENT_ADDRESS = "permanentAddress";
const String PERMANENT_STATE = "permanentState";
const String PERMANENT_CITY = "permanentCity";
const String PERMANENT_ZIP = "permanentZipCode";
const String REFERRED_BY = "referredBy";

const String POSITION = "position";
const String EMPLOYMENT_START_DATE = "employmentStartDate";
const String EMPLOYMENT_INDEX = "employmentIndex";
const String PRESENT_EMPLOYER = "presentEmployer";

const String APPLIED_INDEX = "appliedIndex";
const String APPLIED_WHERE = "appliedWhere";
const String APPLIED_WHEN = "appliedWhen";

const String NIGHT_INDEX = "nightIndex";
const String OVERTIME_INDEX = "overTimeIndex";
const String WEEKEND_INDEX = "weekendIndex";
const String FULL_TIME_INDEX = "fullTimeIndex";
const String TERMINATED_INDEX = "terminatedIndex";
const String PART_TIME_REASON = "partTimeReason";
const String TERMINATED_TIME_REASON = "terminatedReason";
const String LIMITATION_REASON = "limitationReason";

const String OFFER_AMOUNT = "offerAmount";
const String FORM_KEY = "formKey";
const String FORMS_TO_FILL = "formsToFill";
const String FORMS_EXPIRY = "formsExpiry";
const String FORMS_SENT = "formsSent";
const String FORMS_SECURED = "formsSecured";
const String FORMS_PASSWORD = "formsPassword";
const String FORMS_UNLOCKED = "formsUnLocked";
const String JOB_ID = "jobId";

const String SEARCH = "search";
const String ASSIGNED_TO = "assignedTo";
const String ASSIGNED_BY = "assignedBy";
const String COMPANY_NAME = "companyName";
const String COMPANY_IPI_NUMBER = "companyIpiNumber";
const String COMPANY_AFFILIATION = "companyAffiliation";
const String AFFILIATIONS = "affiliations";
const String NEGATIVE_INDEX = "negativeIndex";
const String VACCINATION_INDEX = "vaccinationIndex";
const String PUBLISHING_INDEX = "publishingIndex";
const String PRIVILEGE_INDEX = "privilegeIndex";
const String PROCEED_INDEX = "proceedIndex";

const String LOCATION = "location";
const String DATE_OF_VACCINATION = "dateOfVaccination";
const String IPI_NUMBER = "ipiNumber";
const String LYRICS = "lyrics";
const String CHORUS = "chorus";
const String LANGUAGE = "language";
const String SONG = "language";
const String ISWC = "iswc";
const String WRITER = "writer";
const String COWRITER = "coWriter";
const String TITLES = "titles";

const String INSTRUCTION_BASE = "instructionBase";
const String NURSING_BASE = "nursingBase";
const String NURSING_BASE_COUNT = "nursingBaseCount";
const String WORK_BASE = "workBase";
const String WORK_BASE_COUNT = "workBaseCount";
const String JOB_BASE_COUNT_P = "jobBaseCountP";
const String JOB_BASE_COUNT_C = "jobBaseCountC";
const String EMPLOYEE_BASE = "employeeBase";
const String EMPLOYEE_BASE_COUNT = "employeeBaseCount";
const String EMPLOYEE_BASE_COUNT_P = "employeeBaseCountP";
const String USER_BASE = "userBase";
const String USER_BASE_COUNT = "userBaseCount";
const String JOB_BASE_COUNT = "jobBaseCount";
const String REQUEST_BASE_COUNT = "requestBaseCount";

const String FLW_DEBUG = "flwDebug";
const String FLW_SEC_KEY = "flwSecKey";
const String FLW_PUB_KEY = "flwPubKey";
const String FLW_SEC_KEY_TEST = "flwSecKeyT";
const String FLW_PUB_KEY_TEST = "flwPubKeyT";

const String PAYOUT_INFO = "payoutInfo";
const String BANK_NAME = "bankName";
const String BANK_BRANCH = "bankBranch";
const String BANK_SWIFT_CODE = "bankSwiftCode";
const String ACCOUNT_NAME = "accountName";
const String ACCOUNT_NUMBER = "accountNumber";
const String ROUTING_NUMBER = "routingNumber";
const String SS_NUMBER = "ssNumber";

//keys for wallet....
const Color appColor = Color(0xff5daddc);
// const Color appColor = Color(0xff741CA4);
const Color appColorD = Color(0xff451062);
const Color bronze = Color(0xffcd7f32);
const Color gold = Color(0xffd4af37);

const String CALLBACK_PENDING =
    "https://us-central1-cheetah-2021.cloudfunctions.net/transaction_pending";
const String CALLBACK_INCOMING =
    "https://us-central1-cheetah-2021.cloudfunctions.net/transaction_incoming";

// const String ACTIVE = "active";
const String ACCOUNT_ID = "accountId";
const String EXTERNAL_ID = "externalId";
const String MNEMONIC = "mnemonic";
const String XPUB = "xpub";
const String SYMBOL = "symbol";
const String NAME = "name";
const String WALLETS = "wallets";
const String ADDRESS_LIST = "addressList";
const String BALANCE = "balance";
const String RATES = "rates";
const String SUBSCRIPTION_ID = "subscriptionId";
const String WALLET_IDS = "walletIds";

// const FLUTTER_PUBLIC_KEY = "FLWPUBK-27a6cca1a5928c5826a50d20c2284a08-X";
// const FLUTTER_SECRET_KEY = "FLWSECK-d9ae6c4b35adc505aba82966c7571365-X";
// const FLUTTER_ENC_KEY = "d9ae6c4b35adde2797550cef";

// const FLUTTER_PUBLIC_KEY_TEST =
//     "FLWPUBK_TEST-13b0d5a178b246e77f40a17c7767dc44-X";
// const FLUTTER_SECRET_KEY_TEST =
//     "FLWSECK_TEST-cc0fde19ecf8b9eb56b58028057e86c6-X";
// const FLUTTER_ENC_KEY_TEST = "FLWSECK_TEST35ed468bced8";

const String RAVE_ENC_KEY = "raveEncKey";
const String RAVE_PUB_KEY = "ravePubKey";
const String RAVE_SECRET_KEY = "raveSecretKey";
const String RAVE_TEST_MODE = "raveTestMode";
bool TEST_MODE = true;

bool raveTestMode = userModel.getBoolean(RAVE_TEST_MODE);
String raveEncKey = raveTestMode
    ? "FLWPUBK-9768585b355bc716fd3343688a0df49b-X"
    : appSettingsModel.getString(RAVE_ENC_KEY);
String ravePublicKey = raveTestMode
    ? "FLWPUBK-9768585b355bc716fd3343688a0df49b-X"
    : appSettingsModel.getString(RAVE_PUB_KEY);
String raveSecretKey = raveTestMode
    ? "FLWSECK-fd4172f2b7fc2935adbf67e8e60d6dbc-X"
    : appSettingsModel.getString(RAVE_SECRET_KEY);
String ravePayBaseUrl = raveTestMode
    ? "https://ravesandboxapi.flutterwave.com/v3/"
    : "https://api.ravepay.co/v3/";

const String plain = 'assets/icons/plain.png';
const String white_launcher = 'assets/icons/white.png';
const String txt_logo = 'assets/icons/txt_logo.png';
const String top_back = 'assets/icons/top_back.png';
const String money_back = 'assets/icons/money_back.png';
const String naira = 'assets/icons/naira.png';
const String naira1 = 'assets/icons/naira1.png';

const String ic_like = 'assets/icons/ic_like.png';
const String ic_comment = 'assets/icons/ic_comment.png';
const String ic_send = 'assets/icons/ic_send.png';
const String zenith = 'assets/icons/zenith.png';
const String ic_thumb_down = 'assets/icons/ic_thumb_down.png';
const String ic_thumb_up = 'assets/icons/ic_thumb_up.png';
const String ic_people = 'assets/icons/ic_people.png';
const String ic_people1 = 'assets/icons/ic_people1.png';
const String ic_chat_private = 'assets/icons/ic_chat_private.png';
const String ic_comment1 = 'assets/icons/ic_comment1.png';
const String kiss = 'assets/icons/kiss.png';
const String ic_kiss = 'assets/icons/ic_kiss.png';
const String ic_kiss1 = 'assets/icons/ic_kiss1.png';
const String ic_kiss2 = 'assets/icons/ic_kiss2.png';
const String ic_user = 'assets/icons/ic_user.png';
const String ic_user1 = 'assets/icons/ic_user1.png';
const String ic_bell = 'assets/icons/ic_bell.png';
const String ic_bell1 = 'assets/icons/ic_bell1.png';
const String ic_chat = 'assets/icons/ic_chat.png';
const String ic_chat1 = 'assets/icons/ic_chat1.png';
const String ic_person = 'assets/icons/ic_person.png';
const String ic_person1 = 'assets/icons/ic_person1.png';
const String ic_home = 'assets/icons/ic_home.png';
const String ic_home1 = 'assets/icons/ic_home1.png';
const String lip = 'assets/icons/lip.jpg';
const String lip1 = 'assets/icons/lip1.jpg';
const String ic_female = 'assets/icons/ic_female.png';
const String ic_male = 'assets/icons/ic_male.png';
const String ic_arrow_down = 'assets/icons/ic_arrow_down.png';
const String google_icon = 'assets/icons/google_icon.png';
const String apple_icon = 'assets/icons/apple_icon.png';
const String group_image = 'assets/icons/group_image.png';
const String lock_image = 'assets/icons/lock_image.png';
const String map_image = 'assets/icons/map_image.png';
const String globe_image = 'assets/icons/globe.png';
const String phone_home = 'assets/icons/phone_home.png';
const String phone_add_farm = 'assets/icons/phone_add_farm.png';
const String phone_signup = 'assets/icons/phone_signup.png';
const String farm_scene = 'assets/icons/farm_scene.jpg';
const String ic_play_store = 'assets/icons/ic_play_store.png';
const String ic_apple_store = 'assets/icons/ic_apple_store.png';
const String web_banner = 'assets/icons/web_banner.png';
const String emoji_perfect = 'assets/icons/perfect.png';
const String emoji_normal = 'assets/icons/normal.png';
const String emoji_terrible = 'assets/icons/terrible.png';
const String watermark = 'assets/icons/watermark.png';
const String ic_exit_group = 'assets/icons/ic_exit_group.png';
const String sub_back = 'assets/icons/sub_back.jpg';
const String arrow_up = 'assets/icons/arrow_up.png';
const String ic_sort = 'assets/icons/ic_sort.png';
const String ic_person_marker = 'assets/icons/ic_person_marker.png';
//const String ic_store_marker = 'assets/icons/ic_store_marker.png';
const String ic_farm_marker = 'assets/icons/ic_farm_marker.png';
const String map_background = 'assets/icons/map_background.jpg';
const String ic_whatsapp = 'assets/icons/ic_whatsapp.png';
const String farm_back = 'assets/icons/farm.jpg';
const String ic_verified = 'assets/icons/ic_verified.png';
const String ic_farm = 'assets/icons/ic_farm.png';
const String ic_google = 'assets/icons/ic_google.png';
const String ic_facebook = 'assets/icons/ic_facebook.png';
const String ic_insta = 'assets/icons/ic_insta.png';
const String ic_twitter = 'assets/icons/ic_twitter.png';
const String page5 = 'assets/icons/page5.gif';
//const String naira = 'assets/icons/naira.png';
const String ic_buy = 'assets/icons/ic_buy.png';
const String ic_sell = 'assets/icons/ic_sell.png';
const String ic_split = 'assets/icons/ic_split.png';
const String ic_spend = 'assets/icons/ic_spend.png';
const String perfect = 'assets/icons/perfect.png';
const String swipe_right = 'assets/icons/swipe_right.png';
const String ic_launcher = 'assets/icons/ic_launcher.png';
const String ic_launcher1 = 'assets/icons/ic_launcher1.png';
const String ic_launcher2 = 'assets/icons/ic_launcher2.png';
const String launch_image = 'assets/icons/launch_image.jpg';
//const String back1 = 'assets/icons/back1.jpg';
const String soul_back1 = 'assets/icons/soul_back1.jpg';
const String soul_back2 = 'assets/icons/soul_back2.png';
const String atlas = 'assets/icons/atlas.png';
const String ic_knock = 'assets/icons/ic_knock.png';
const String ic_team = 'assets/icons/ic_team.png';
const String ic_plain = 'assets/icons/ic_plain.png';
const String ic_plain_small = 'assets/icons/ic_plain_small.png';
const String map_sample = 'assets/icons/map_sample.jpg';
const String ic_filter = 'assets/icons/ic_filter.png';
const String back = 'assets/icons/back.jpeg';
const String banner = 'assets/icons/banner.png';
const String banner2 = 'assets/icons/banner2.png';
const String bride = 'assets/icons/bride.png';
const String bride_large = 'assets/icons/bride_large.png';
const String bride_small = 'assets/icons/bride_small.png';
const String groom = 'assets/icons/groom.png';
const String groom_large = 'assets/icons/groom_large.png';
const String groom_small = 'assets/icons/groom_small.png';
const String heart = 'assets/icons/heart.png';
const String heart_border = 'assets/icons/heart_border.png';
const String heart_half = 'assets/icons/heart_half.png';
const String ic_down = 'assets/icons/ic_down.png';
const String pretty = 'assets/icons/pretty.jpg';
const String pretty1 = 'assets/icons/pretty1.png';
const String pretty2 = 'assets/icons/pretty2.jpg';
const String ic_couple = 'assets/icons/ic_couple.png';

const String calc = 'assets/icons/calc.png';
const String classroom = 'assets/icons/classroom.png';
const String ic_broadcast = 'assets/icons/ic_broadcast.png';
const String ic_cam_add = 'assets/icons/ic_cam_add.png';
const String ic_poly = 'assets/icons/ic_poly.png';
const String ic_college = 'assets/icons/ic_college.png';
const String ic_school = 'assets/icons/ic_school.png';
const String ic_staff = 'assets/icons/ic_staff.png';
const String ic_student = 'assets/icons/ic_student.png';
const String ic_study = 'assets/icons/ic_study.png';
const String ic_study1 = 'assets/icons/ic_study1.png';
const String ic_uni = 'assets/icons/ic_uni.png';
const String lib = 'assets/icons/lib.png';
const String library = 'assets/icons/library.png';
const String news = 'assets/icons/news.png';
const String pre1 = 'assets/icons/pre1.png';
const String shop = 'assets/icons/shop.png';
const String timetable = 'assets/icons/timetable.png';
const String ic_level = 'assets/icons/ic_level.png';
const String ic_book = 'assets/icons/ic_book.png';
const String ic_coin = 'assets/icons/ic_coin.png';
const String sample = 'assets/icons/sample.jpg';
const String icon_file_doc = 'assets/icons/icon_file_doc.png';
const String icon_file_pdf = 'assets/icons/icon_file_pdf.png';
const String icon_file_text = 'assets/icons/icon_file_text.png';
const String icon_file_video = 'assets/icons/icon_file_video.png';
const String icon_file_xls = 'assets/icons/icon_file_xls.png';
const String icon_file_ppt = 'assets/icons/icon_file_ppt.png';
const String ic_mic = 'assets/icons/ic_mic.png';
const String ic_mute = 'assets/icons/ic_mute.png';
const String ic_unmute = 'assets/icons/ic_unmute.png';
const String game5 = 'assets/icons/game5.png';
const String game7 = 'assets/icons/game7.png';

const String an11 = 'assets/icons/an11.png';
const String an12 = 'assets/icons/an12.png';
const String fl1 = 'assets/icons/fl1.png';
const String fl2 = 'assets/icons/fl2.png';
const String fl3 = 'assets/icons/fl3.png';
const String fl4 = 'assets/icons/fl4.png';
const String fl5 = 'assets/icons/fl5.png';
const String fl6 = 'assets/icons/fl6.png';
const String fl7 = 'assets/icons/fl7.png';

const String c37 = 'assets/icons/c37.png';
const String ic_meal = 'assets/icons/ic_meal.png';
const String ic_world = 'assets/icons/ic_world.png';
const String icon_file_unknown = 'assets/icons/icon_file_unknown.png';
const String icon_file_photo = 'assets/icons/icon_file_photo.png';
const String icon_file_zip = 'assets/icons/icon_file_zip.png';
const String icon_file_xml = 'assets/icons/icon_file_xml.png';
const String icon_file_audio = 'assets/icons/icon_file_audio.png';
const String store_large = 'assets/icons/store_large.png';
const String ic_sad = 'assets/icons/ic_sad.png';
const String ic_champ = 'assets/icons/ic_champ.png';
const String qback = 'assets/icons/qback.png';
const String ic_winner = 'assets/icons/ic_winner.png';
const String ic_winner2 = 'assets/icons/ic_winner2.png';
const String q1 = 'assets/icons/q1.png';
const String q2 = 'assets/icons/q2.png';
const String q3 = 'assets/icons/q3.png';
const String q4 = 'assets/icons/q4.png';
const String q5 = 'assets/icons/q5.png';
const String ic_gpa = 'assets/icons/ic_gpa.png';
const String ic_point_left = 'assets/icons/ic_point_left.png';
const String ic_point_right = 'assets/icons/ic_point_right.png';
const String ic_divide = 'assets/icons/ic_divide.png';
const String back1 = 'assets/icons/back1.jpg';
const String back2 = 'assets/icons/back2.jpg';
const String back3 = 'assets/icons/back3.jpg';
const String p1 = 'assets/icons/p11.jpg';
const String p2 = 'assets/icons/p12.jpg';
const String p3 = 'assets/icons/p13.jpg';
const String p4 = 'assets/icons/p14.jpg';
const String p5 = 'assets/icons/p15.jpg';
const String p6 = 'assets/icons/p16.jpg';
const String p7 = 'assets/icons/p17.jpg';
const String p8 = 'assets/icons/p18.jpg';
const String p9 = 'assets/icons/p19.jpg';
const String p10 = 'assets/icons/p20.jpg';

var actionTexts = [
  "Shop Now",
  CONTACT_US,
  "Download App",
  "Sign Up",
  "Watch Video",
  "Play Game",
  "Apply Now",
  "Learn More"
];
var actionIcons = [
  Icons.shopping_basket,
  Icons.email,
  Icons.get_app,
  Icons.edit,
  Icons.ondemand_video,
  Icons.games,
  Icons.edit,
  Icons.more_horiz,
];

const Color brown0 = Color(0xffa52a2a);
const Color brown1 = Color(0xff942525);
const Color brown1b = Color(0xfff3842121);
const Color brown2 = Color(0xff842121);
const Color brown3 = Color(0xff731d1d);
const Color brown4 = Color(0xff631919);
const Color brown5 = Color(0xff521515);
const Color brown6 = Color(0xff421010);
const Color brown7 = Color(0xff310c0c);
const Color brown8 = Color(0xff210808);
const Color brown9 = Color(0xff100404);

const Color brown01 = Color(0xffae3f3f);
const Color brown02 = Color(0xffb75454);
const Color brown03 = Color(0xffc06969);
const Color brown04 = Color(0xffc97f7f);
const Color brown05 = Color(0xffd29494);
const Color brown06 = Color(0xffdba9a9);
const Color brown07 = Color(0xffe4bfbf);
const Color brown08 = Color(0xffedd4d4);
const Color brown09 = Color(0xff1ef6e9e9);

const Color blue0 = Color(0xff8470ff);
const Color blue1 = Color(0xff7664e5);
const Color blue2 = Color(0xff6959cc);
const Color blue3 = Color(0xff5c4eb2);
const Color blue4 = Color(0xff4f4399);
const Color blue5 = Color(0xff42387f);
const Color blue6 = Color(0xff342c66);
const Color blue7 = Color(0xff27214c);
const Color blue8 = Color(0xff1a1633);
const Color blue9 = Color(0xff0d0b19);

const Color blue01 = Color(0xff8470ff);
const Color blue02 = Color(0xff907eff);
const Color blue03 = Color(0xff928cff);
const Color blue04 = Color(0xffa89aff);
const Color blue05 = Color(0xffb5a9ff);
const Color blue06 = Color(0xffc1b7ff);
const Color blue07 = Color(0xffcdc5ff);
const Color blue08 = Color(0xff08534949);
const Color blue09 = Color(0xff0f534949);

const Color black = Color(0xff000000);
const Color white = Color(0xffffffff);
const Color white1 = Color(0xfffafafa);
const Color transparent = Color(0xff00000000);
const Color default_white = Color(0xfffff3f3f3);

const Color tang0 = Color(0xffffa500);
// const Color tang0 = Color(0xff66D7D1);

const Color orange0 = Color(0xffffa500);
const Color orange1 = Color(0xffe59400);
const Color orange2 = Color(0xffcc8400);
const Color orange3 = Color(0xffb27300);
const Color orange4 = Color(0xff996300);
const Color orange5 = Color(0xff7f5200);
const Color orange6 = Color(0xff664200);
const Color orange7 = Color(0xff4c3100);
const Color orange8 = Color(0xff332100);
const Color orange9 = Color(0xff191000);

const Color orange01 = Color(0xffffa500);
const Color orange02 = Color(0xffffae19);
const Color orange03 = Color(0xffffb732);
const Color orange04 = Color(0xffffc04c);
const Color orange05 = Color(0xffffc966);
const Color orange06 = Color(0xffffd27f);
const Color orange07 = Color(0xffffdb99);
const Color orange08 = Color(0xffffe4b2);
const Color orange09 = Color(0xffffedcc);
const Color orange010 = Color(0xfffff6e5);

const Color yellow0 = Color(0xffffff00);
const Color yellow1 = Color(0xffe5e500);
const Color yellow2 = Color(0xffcccc00);
const Color yellow3 = Color(0xffb2b200);
const Color yellow4 = Color(0xff999900);
const Color yellow5 = Color(0xff7f7f00);
const Color yellow6 = Color(0xff666600);
const Color yellow7 = Color(0xff4c4c00);
const Color yellow8 = Color(0xff333300);
const Color yellow9 = Color(0xff191900);

const Color yellow01 = Color(0xffffff00);
const Color yellow02 = Color(0xffffff19);
const Color yellow03 = Color(0xffffff32);
const Color yellow04 = Color(0xffffff4c);
const Color yellow05 = Color(0xffffff66);
const Color yellow06 = Color(0xffffff7f);
const Color yellow07 = Color(0xffffff99);
const Color yellow08 = Color(0xffffffb2);
const Color yellow09 = Color(0xffffffcc);
const Color yellow010 = Color(0xffffffe5);

const Color red0 = Color(0xffff0000);
const Color red1 = Color(0xffe50000);
const Color red2 = Color(0xffcc0000);
const Color red3 = Color(0xffb20000);
const Color red4 = Color(0xff990000);
const Color red5 = Color(0xff7f0000);
const Color red6 = Color(0xff660000);
const Color red7 = Color(0xff4c0000);
const Color red8 = Color(0xff330000);
const Color red9 = Color(0xff190000);

const Color red00 = Color(0xffff0000);
const Color red01 = Color(0xffff1919);
const Color red02 = Color(0xffff3232);
const Color red03 = Color(0xffff4c4c);
const Color red04 = Color(0xffff6666);
const Color red05 = Color(0xffff7f7f);
const Color red06 = Color(0xffff9999);
const Color red07 = Color(0xffffb2b2);
const Color red08 = Color(0xffffcccc);
const Color red09 = Color(0xffffe5e5);

const Color dark_green0 = Color(0xff006400);
const Color dark_green1 = Color(0xff005a00);
const Color dark_green2 = Color(0xff005000);
const Color dark_green3 = Color(0xff004600);
const Color dark_green4 = Color(0xff003c00);
const Color dark_green5 = Color(0xff003200);
const Color dark_green6 = Color(0xff002800);
const Color dark_green7 = Color(0xff001e00);
const Color dark_green8 = Color(0xff001400);
const Color dark_green9 = Color(0xff000a00);
const Color dark_green10 = Color(0xff000000);

const Color dark_green01 = Color(0xff006400);
const Color dark_green02 = Color(0xff197319);
const Color dark_green03 = Color(0xff328332);
const Color dark_green04 = Color(0xff4c924c);
const Color dark_green05 = Color(0xff66a266);
const Color dark_green06 = Color(0xff7fb17f);
const Color dark_green07 = Color(0xff99c199);
const Color dark_green08 = Color(0xffb2d0b2);
const Color dark_green09 = Color(0xffcce0cc);
const Color dark_green010 = Color(0xffe5efe5);

const Color light_green0 = Color(0xff00ff00);
const Color light_green1 = Color(0xff00e500);
const Color light_green2 = Color(0xff00cc00);
const Color light_green3 = Color(0xff00b200);
const Color light_green4 = Color(0xff009900);
const Color light_green5 = Color(0xff007f00);
const Color light_green6 = Color(0xff006600);
const Color light_green7 = Color(0xff004c00);
const Color light_green8 = Color(0xff003300);
const Color light_green9 = Color(0xff001900);
const Color light_green10 = Color(0xff000000);

const Color light_green00 = Color(0xff00ff00);
const Color light_green01 = Color(0xff19ff19);
const Color light_green02 = Color(0xff32ff32);
const Color light_green03 = Color(0xff4cff4c);
const Color light_green04 = Color(0xff66ff66);
const Color light_green05 = Color(0xff7fff7f);
const Color light_green06 = Color(0xff99ff99);
const Color light_green07 = Color(0xffb2ffb2);
const Color light_green08 = Color(0xffccffcc);
const Color light_green09 = Color(0xffe5ffef);
const Color light_green010 = Color(0xffffffff);

const Color pink0 = Color(0xffff69b4);
const Color pink1 = Color(0xffe55ea2);
const Color pink2 = Color(0xffcc5490);
const Color pink3 = Color(0xffb2497d);
const Color pink4 = Color(0xff993f6c);
const Color pink5 = Color(0xff7f345a);
const Color pink6 = Color(0xff662a48);
const Color pink7 = Color(0xff4c1f36);
const Color pink8 = Color(0xff331524);
const Color pink9 = Color(0xff190a12);
const Color pink10 = Color(0xff000000);

const Color pink01 = Color(0xffff78bb);
const Color pink02 = Color(0xffff87c3);
const Color pink03 = Color(0xffff96ca);
const Color pink04 = Color(0xffffa5d2);
const Color pink05 = Color(0xffffb4d9);
const Color pink06 = Color(0xffffc3e1);
const Color pink07 = Color(0xffffd2e8);
const Color pink08 = Color(0xffffe1f0);
const Color pink09 = Color(0xfffff0f7);
const Color pink010 = Color(0xffffffff);

const Color app_blue = Color(0xff0072e5);

const Color blue010 = Color(0xff0f534949);

const Color m_blue0 = Color(0xff0000ff);
const Color m_blue1 = Color(0xff0000e5);
const Color m_blue2 = Color(0xff0000cc);
const Color m_blue3 = Color(0xff0000b2);
const Color m_blue4 = Color(0xff000099);
const Color m_blue5 = Color(0xff00007f);
const Color m_blue6 = Color(0xff000066);
const Color m_blue7 = Color(0xff00004c);
const Color m_blue8 = Color(0xff000033);
const Color m_blue9 = Color(0xff000019);

const Color plain_blue = Color(0xff000064);
const Color m_blue01 = Color(0xff1919ff);
const Color m_blue02 = Color(0xff4c4cff);
const Color m_blue03 = Color(0xff6666ff);
const Color m_blue04 = Color(0xff7f7fff);
const Color m_blue05 = Color(0xff9999ff);
const Color m_blue06 = Color(0xffb2b2ff);
const Color m_blue07 = Color(0xffccccff);
const Color m_blue08 = Color(0xffe5e5ff);

const Color azure_blue00 = Color(0xff007fff);
const Color azure_blue01 = Color(0xff198bff);
const Color azure_blue02 = Color(0xff3298ff);
const Color azure_blue03 = Color(0xff4ca5ff);
const Color azure_blue04 = Color(0xff66b2ff);
const Color azure_blue05 = Color(0xff7fbfff);
const Color azure_blue06 = Color(0xff99cbff);
const Color azure_blue07 = Color(0xffb2d8ff);
const Color azure_blue08 = Color(0xffcce5ff);
const Color azure_blue09 = Color(0xffe5f2ff);

const Color azure_blue0 = Color(0xff007fff);
const Color azure_blue1 = Color(0xff0072e5);
const Color azure_blue2 = Color(0xff0065cc);
const Color azure_blue3 = Color(0xff0058b2);
const Color azure_blue4 = Color(0xff004c99);
const Color azure_blue5 = Color(0xff003f7f);
const Color azure_blue6 = Color(0xff003266);
const Color azure_blue7 = Color(0xff00264c);
const Color azure_blue8 = Color(0xff001933);
const Color azure_blue9 = Color(0xff000c19);

const Color light_grey = Color(0xff14000000);
const Color light_white = Color(0xffc7ffffff);
const Color dark_grey = Color(0xff96000000);

const Color brown00 = Color(0xffa52a2a);

const Color brown010 = Color(0xffffffff);

const Color black1 = Color(0xffcd000000);

const Color brown10 = Color(0xff000000);

const Color white60 = Color(0xffa0ffffff);
const Color white_two = Color(0xffefefef);
const Color white_three = Color(0xffebebeb);
const Color white_four = Color(0xffe0e0e0);
const Color white_five = Color(0xffdadada);
const Color blue = Color(0xff031cd7);
const Color gray = Color(0xff333333);
const Color dark_gray = Color(0xff282a2b);
const Color dark_grey_two = Color(0xff191a1b);
const Color warm_grey = Color(0xff7f7f7f);
const Color warm_grey_two = Color(0xff9c9c9c);
const Color warm_grey_three = Color(0xff8b8b8b);
const Color warm_grey_four = Color(0xff979797);
const Color dark_mint = Color(0xff51c05c);
const Color cornflower_blue_two = Color(0xff4f62d7);
const Color cornflower_blue_two_24 = Color(0xff3d4f62d7);
const Color cornflower_blue_two_dark = Color(0xff475bd4);
const Color cornflower_blue_light_40 = Color(0xff64bec5f7);

const Color cornflower_blue = Color(0xff6274e2);
const Color cornflower_blue_dark = Color(0xff303F9F);
const Color cornflower_blue_darkest = Color(0xff2d3a93);
const Color gray_light = Color(0xffe8e8e8);
const Color gray_transparent = Color(0xffa6efefef);
const Color gray_dark = Color(0xff858585);
const Color gray_dark_transparent = Color(0xffae858585);
const Color gray_darkest = Color(0xffae282828);
const Color black_10 = Color(0xff19000000);
const Color ivory = Color(0xfff8efe6);
const Color ivory_dark = Color(0xfff7e8d9);
const Color green = Color(0xff38be55);
const Color green_dark = Color(0xff2da346);
const Color red = Color(0xffe94f4f);
const Color brown = Color(0xff4e342e);

const Color silver = Color(0xffaaa9ad);

const String REQUEST_BASE = "requestBase";

const String FIAT_LIST = "fiatList";
const String CRYPTO_LIST = "cryptoList";
const String DEPOSIT_LIST = "depositList";
const String PROVIDER_LIST = "providerList";

const String ITEM_NAME = "itemName";
const String ITEM_DESCRIPTION = "itemDescription";

const String STATUS = "status";
const String EMPLOYEE_STATUS = "employeestatus";

const int BUYING = 0;
const int SELLING = 1;

const int FLOATING = 0;
const int FIXED = 1;

const int STATUS_OPEN = 0;
const int STATUS_CLOSED = 1;
const int STATUS_CANCELLED = 2;

const String INTERESTED_ID = "interestedId";
const String AMOUNT = "amount";
const String MIN_AMOUNT = "minAmount";
const String MAX_AMOUNT = "maxAmount";
const String CRYPTO_NAME = "cryptoName";
const String CRYPTO_ID = "cryptoId";
const String CRYPTO_ICON = "cryptoIcon";

const String FIAT_NAME = "fiatName";
const String FIAT_ID = "fiatId";
const String FIAT_ICON = "fiatIcon";

////Remove.......

const String LOAN_BASE = "loanBase"; //db
const String B_BASE = "bBase"; //db
const String DATE_OF_BIRTH = "dateOfBirth";

const String MIN_LOAN = "minLoan";
const String MAX_LOAN = "maxLoan";
const String MIN_LOAN_DAYS = "minLoanDays";
const String MAX_LOAN_DAYS = "maxLoanDays";
const String INSURANCE_RATE = "insuranceRate";
const String MUST_INSURE = "mustInsure";

const String JOB_TYPE = "jobType";
const String MONTHLY_SALARY = "monthlySalary";
const String PAY_DAY = "payDay";
const String LAST_PAY_DAY = "lastPayDay";
const String WORK_ADDRESS = "workAddress";
const String WORK_START = "workStart";
const String ACCOUNT_BANK = "accountBank";
const String B_DETAILS = "bDetails";
const String ACCOUNT_BANK_CODE = "accountBankCode";
const String ACCOUNT_STATEMENT = "accountStatement";
const String ACCOUNT_STATEMENT_META = "accountStatementMeta";
const String ACCOUNT_STATEMENT_PASS = "accountStatementPass";
const String DOC_PASS = "docPass";
const String PRIMARY = "primary";

const String EMPLOYER_NAME = "employerName";
const String EMPLOYER_EMAIL = "employerEmail";
const String EMPLOYER_PHONE = "employerPhone";

const String VERIFY_STATUS = "verifyStatus";
const String FUNDS = "funds";
const String TRANS_FEE = "transFee";
const String TRANS_FEE_MAX = "transFeeMax";
const String WITHDRAW_FEE = "withdrawFee";
const String WITHDRAW_FEE_MAX = "withdrawFeeMax";

const String LOAN_INSURANCE = "loanInsurance";
const String LOAN_STATUS = "loanStatus";
const String LOAN_SENT_BY = "loanSentBy";
const String LOAN_SENT_BY_NAME = "loanSentByName";
const String LOAN_SENT_TIME = "loanSentTime";
const String LOAN_RECEIVED_TIME = "loanReceivedTime";
const String LOAN_REPAID_TIME = "loanRepaidTime";
// const String AMOUNT_SENT = "amountSent";

const int LOAN_STATUS_OPEN = 0;
const int LOAN_STATUS_CLOSED = 1;
const int LOAN_STATUS_RECEIVED = 2;
const int LOAN_STATUS_REPAID = 3;

const int PROFILE_STATUS_EMPLOYED = 1;
const int PROFILE_STATUS_UNEMPLOYED = 2;

const int VERIFY_STATUS_EMPTY = 0;
const int VERIFY_STATUS_PENDING = 1;
const int VERIFY_STATUS_VERIFIED = 2;
const int VERIFY_STATUS_ERROR = 3;

//remove............................
const String POST_BASE = "postBase"; //db
const String KISS_BASE = "kissBase"; //db
const String TRANS_BASE = "transBase"; //db
const String FARM_VERIFY_BASE = "farmVerifyBase"; //db
const String REVIEW_BASE = "reviewBase"; //db
const String PLAN_BASE = "planBase1"; //db
const String COUNTRY_BASE = "countryBase"; //db
const String COUNTER_BASE = "counterBase"; //db
const String FARM_BASE = "farmBase"; //db
const String PRODUCT_BASE = "productBase"; //db
//const String PRODUCT_BASE_LIVESTOCK = "productBaseLivestock"; //db
//const String PRODUCT_BASE_CROPS = "productBaseCrops"; //db
const String LOGO = "Logo";
const String FARM_LOGO = "FarmLogo";
const String SUBJECT = "subject";

const String STATUS_PENDING = "pending";
const String STATUS_FAILED = "failed";
const String STATUS_CANCELED = "cancelled";
const String STATUS_SUCCESS = "success";

const String STATS_BASE = "statsBase"; //db
const String CURRENCY_BASE = "currencyBase"; //db
// const String CRYPTO_BASE = "cryptoBase"; //db
const String CONNECTS_BASE = "connectsBase"; //db

const String BROADCAST_BASE = "broadcastBase"; //db
const String SCHOOL_BASE = "schoolBase"; //db
const String STUDY_BASE = "studyBase1";
const String LEVEL_BASE = "levelBase1";
const String VOICE_BASE = "voiceBase";
//const String VOICE_BASE = "voiceBase";
const String QUIZ_BASE = "quizBase4";
const String QUIZ_TIME = "quizTime";
const String SESSION_BASE = "sessionBase2";
const String ADMIN_QUIZ_BASE = "adminQuizBase";
const String ADMIN_QUOTE_BASE = "adminQuoteBase";
const String ADMIN_KNOW_BASE = "adminKnowBase";
const String ADMIN_ITEM_POSITION = "adminItemPosition";
const String ADMIN_QUIZ_PRIZE = "adminQuizPrize";
const String GAME_POSITION = "gamePosition";

const String STAT_VIEWS = "views";
const String STAT_VISITS = "visits";
const String STAT_PHOTO_VIEWS = "photoViews";
const String STAT_CONTACT_CLICKS = "contactClicks";
const String STAT_CHAT_CLICKS = "chatClicks";
//const String STAT_AVG_TIME = "avgTime";
//const String SEEN_PEOPLE = "seenPeople";
//const String LOVE_LIST = "loveList";

const String MAX_POST_TIME = "maxPostTime";
const String MAX_QUIZ_TIME = "maxQuizTime";
const String QUIZ_TAP_TIP = "quizTapTip";

const String FOOD_LIST = "foodList";
//const String CITY = "city";
const String EXPLANATION = "explanation";
const String STORY_IMAGE = "storyImage";
const String STORY_TEXT = "storyText";
const String MORE_INFO = "moreInfo";

//const String NAME = "name";
const String MY_PROVIDERS = "myProviders";
const String MY_BANKS = "myBanks";
const String FIRST_NAME = "firstName";
const String MIDDLE_NAME = "middleName";
const String LAST_NAME = "lastName";
const String OTHER_NAME = "otherName";
const String DECLINE_MESSAGE = "declineMessage";
const String STOPPED_MESSAGE = "stoppedMessage";
const String DOB = "dobTime";
const String PRIVATE_DOB = "privateDob";
//const String DOB_TEXT = "dobText";
const String CAN_CHAT = "canChat";
const String CUSTOM_BACK = "customBack";
const String CAN_SEX = "canSex";
const String CAN_COOK = "canCook";
const String SEX_DRIVE = "sexDrive";
const String IDEAL = "ideal";
//const String COOK_LIST = "cookList";

const String COIN_MAP = "coinList";
const String ALL_SEARCHES = "allSearches";
//const String SEARCH_HISTORY = "searchHistory";

const String QUESTION_BASE = "questionBase";
const String QUESTION_INDEX = "questionIndex";
const String CORRECT = "correct";
const String SCORE = "score";
const String HIGH_SCORE = "highScore";
const String SUMMED = "summed";
const String TOKEN = "token";
const String TOPICS = "topics";
const String GPA_BASE = "gpaBase";
const String MY_GPA_SYSTEM = "myGpaSystem";

const String USER_BASE_ADMIN = "userBaseAdmin";
const String VERIFY_BASE = "verifyBase";
const String SKILLS_BASE = "skillsBase";
const String CATEGORIES_BASE = "categoriesBase3";
const String APP_SETTINGS_BASE = "appSettingsBase";
const String WORD_LIST = "wordList";
const String WORDS = "words";
const String APP_SETTINGS = "appSettings";
const String HIRE_BASE = "hireBase";
const String JOB_BASE = "jobBase";
const String BID_BASE = "bidBase";
const String REFERENCE_BASE = "referenceBase";
const String ACCOM_BASE = "accomBase";
const String CHAT_BASE = "chatBase";
const String ORDER_BASE = "orderBase";
const String CHAT_IDS_BASE = "chatIdsBase";
const String CONVERSATIONS = "conversations";
const String REMOVED_IDS = "removedIds";
const String CONVERSATIONS_INFO = "conversationsInfo";
//const String STORY_BASE = "storyBase";
const String REPORT_BASE = "reportBase";
const String REPORTS = "reports";
//const String REPORT_STATUS = "reportStatus";
//const String REPORT_COUNT = "reportCount";
//const String REPORT_TIME = "reportTime";
const String NOTIFY_BASE = "notifyBase";
//const String MIN_IDEAL_DESC = "minDesc";

const String LIKED = "liked";
const String COLOR_KEY = "colorKey";

const String EXPERT_BASE = "expertBase1";
const String RATING_BASE = "ratingBase1";
const String HEADLINE_BASE = "headlineBase";
const String COMMENT_BASE = "commentBase";
//const String POSTS_BASE = "postsBase";
const String GROUPS_BASE = "groupsBase";
const String LIBRARY_BASE = "libraryBase";
const String MARKET_BASE = "marketBase";
const String ADVERT_BASE = "NewAdvertBase1";
const String ADVERT_BASE_OLD = "advertBase";
const String ADVERT_BASE_OLD1 = "advertBase1";
const String WITHDRAW_BASE = "withdrawBase";
const String WINNER_BASE = "winnerBase";

const String LOVE_QUOTE = "loveQuote";
const String PLATFORM = "platform";
const String CASH_TAKEN = "cashTaken";
const String HIDE_VOICE = "hideVoice";
const String VOICE_TEXT = "voiceText";
const String PRIZE_WON = "prizeWon";
const String CASH_EARNED = "cashEarned";
const String LIGHTER = "lighter";
const String PROFILE_UPDATED = "profileUpdated";

const String POSTS_AD_SPACING = "postsAdSpacing";
const String LIB_AD_SPACING = "libAdSpacing";
const String LIB_CROSS_AD_SPACING = "libCrossAdSpacing";
const String MARKET_AD_SPACING = "marketAdSpacing";

//const String DELETED = "deleted";
const String DELETED_CHATS = "deletedChatsIds";
const String NEW_APP = "newApp3";

const String REC_ID = "recId";
const String SPACE = "____";
const String SHOWN = "shown";
const String CLICKS = "clicks";

const String COURSE_CODE = "courseCode";
const String CREDIT_LOAD = "creditLoad";
const String GRADE = "grade";

const String CARD_COLOR_INDEX = "cardColorIndex";

const String GROUP_ID = "groupId";
const String GROUP_NAME = "groupName";
const String GROUP_DESCRIPTION = "groupDescription";
const String GROUP_PRIVACY = "groupPrivacy";
const String GROUP_ADMIN = "groupAdmin";
const String GROUP_MEMBERS = "groupMembers";
//const String GROUP_MEMBERS_IDS = "groupMembersIds";

const String CHAT_COST = "chatCost";
const String TIMETABLE_SPLIT = ":::";
const String TIMETABLE_BASE = "timetableBase";
const String TIMETABLE = "timetable";
//const String TIME_TABLE_PUBLIC = "timetablePublic";
const String TIMETABLE_MUTED = "timetableMuted";
const String INDEX = "index";
const String ACTION_URL = "actionUrl";
const String PENDING_NOTIFY = "pendingNotify";

const String CLOG = "cLog";
const String NEW_MESSAGE_AVAILALBE = "newAvailable";

const String PAYMENT_REF = "paymentRef";
const String PRICE_BASE = "priceDBase";
const String NOTIFY_TYPE = "notifyType";

const String DELETED = "deleted";
const String HIDE_PROFILE = "hideProfile";
//const String CREDITS = "credits";
const String VALUE_TO_ONE_DOLLAR = "valueToOneDollar";
const String IN_USD = "inUsd";
const String IN_NAIRA = "inNaira";
const String EXTERNAL = "external";

const String PRIVATE_NOTE = "privateNote";
const String MAP_NAME = "mapName";
const String MAP_POINTS = "mapPoints";
const String MAP_ID = "mapId";
//const String MAP_ZOOM = "mapZoom";
const String MAP_BASE = "mapBase";
const String BRANCH_BASE = "branchBase";
const String OUTREACH_BASE = "outreachBase";
//const String OUTREACH = "outreach";
const String IS_LEADER = "isLeader";

const String EMPLOYEES_NEEDED = "employeesNeeded";
const String START_DATE = "startDate";
const String START_TIME = "startTime";
const String END_TIME = "endTime";
const String END_DATE = "endDate";
const String SHIFTS = "shifts";
const String SKILLS = "skills";
const String CONDITION = "condition";
const String TELEPHONE = "telephone";
const String SOCIAL_SECURITY_NUMBER = "socialSecurityNumber";
const String LICENCE_INFO = "addressInfo";
const String ADDRESS_INFO = "addressInfo";
const String BANKING_INFO = "bankingInfo";
const String TAX_INFO = "taxInfo";
const String EDUCATION_INFO = "educationInfo";
const String REFERENCES_INFO = "referencesInfo";
const String EMPLOYERS_INFO = "employersInfo";
const String SHIFTS_INFO = "shiftsInfo";
const String NOTES_INFO = "notesInfo";
const String COORDINATOR_INFO = "coordinatorInfo";
const String TIME_CARD = "timeCard";
// const String POSITION = "position";
const String DEPARTMENT = "department";
const String HIRE_DATE = "hireDate";
const String EMPLOYMENT_TYPE = "employmentType";
const String EMPLOYMENT_PAY_RATE = "employmentPayRate";
const String PAYMENT_TYPE = "paymentType";
const String ASSIGNED_INFO = "assignedInfo";
const String ASSIGNED_ID = "assignedId";

const String SHARE_TEXT = "shareText";

const String SOULS_WON = "SoulsWon";
const String SOULS_REQUIRED = "SoulsRequired";
const String CURRENT_SOULS = "currentSouls";

const String NOTE = "note";
const String NOTE_IDS = "noteIds";
const String SOUL_IDS = "soulIds";

const String SINGLE = "Single";
const String MARRIED = "Married";
const String DIVORCED = "Divorced";
const String WIDOW = "Widow";
const String WIDOWER = "Widower";

const String AGE = "age";

const String SOUL_BASE = "soulBase";
const String NOTE_BASE = "noteBase";
const String BRANCH_NAME = "branchName";
const String BRANCH_ID = "branchId";

const String OPINION_STRING = "opinion";
const String CHAT_STRING = "chat";
const String SESSION_STRING = "session";
const String CONTACT_PHONE = "cPhone";
const String CONTACT_EMAIL = "cEmail";
const String CONTACT_WHATS = "cWhats";

const String ACTION_LIST = "actionList";
const String ACTION_LIST_NAMES = "actionListNames";
const String ACTION_TEXT = "actionText";
const String ACTION_LINK = "actionLink";
const String ACTION_TEXT_CLICKED = "actionTextClicked";

const String BLOCKED = "blocked";

const String ANSWERED_CORRECTLY = "answeredCorrectly";
const String ANSWERED_WRONGLY = "answeredWrongly";
const String ALL_ANSWER = "allAnswers";

const String DEFAULT_COUNTRY = "defaultCountry";
const String DEFAULT_STATE = "defaultState";
const String DEFAULT_CITY = "defaultCity";
const String DEFAULT_LIST = "defaultList";
//const String SHARE_TYPE = "shareType";
const String EXCLUDED = "excluded";
const String COUNTRY_NAME = "countryName";
const String CURRENCY = "currency";
const String CURRENCY_LOGO = "currencyLogo";
const String WHATS_NEW = "whatsNew";
const String COST_PER_REACH = "costPerReach";
const String MIN_BUDGET = "minBudget";
const String MAX_REACH = "maxReach";
const String QUIZ_ID = "quizId";
const String MY_PREF = "myPref";
const String SHOW_BY_PREF = "showByPref";
const String ITEM_PREF = "ItemPref";
const String PREF_SET = "prefSet";

const String ADMIN_QUOTE_TITLES = "adminQuoteTitles";

const int PREF_ANY = 0;
const int PREF_SCIENCE = 1;
const int PREF_ART = 2;

const int GROUP_JOINED = 0;
const int GROUP_LEAVE = 1;
const int GROUP_DELETE = 2;

const int POST_TYPE_PHOTO = 0;
const int POST_TYPE_QUIZ = 1;
const int POST_TYPE_TEXT = 2;
const int POST_TYPE_VIDEO = 3;
const int POST_TYPE_HEADLINE = 4;
const int POST_TYPE_GROUP = 5;
const int POST_TYPE_ADS = 6;
const int POST_TYPE_RIDDLE = 7;

const int NOTIFY_TYPE_NORMAL = 0;
const int NOTIFY_TYPE_OPINION = 1;

const int ACCOUNT_TYPE_PRE = 0;
const int ACCOUNT_TYPE_STUDENT = 1;
const int ACCOUNT_TYPE_STAFF = 2;

const int CHAT_TYPE_TEXT = 0;
const int CHAT_TYPE_IMAGE = 1;
const int CHAT_TYPE_DOC = 2;
const int CHAT_TYPE_VIDEO = 3;
const int CHAT_TYPE_REC = 4;

const int REPORT_TYPE_PROFILE = 0;

const int EXPERT_TYPE_TOP = 0;
const int EXPERT_TYPE_REVIEW = 1;

const int STORY_TYPE_TOP = 0;
const int STORY_TYPE_OPINION = 1;

const int ANDROID = 0;
const int IOS = 1;
const int WEB = 2;

const int MALE = 1;
const int FEMALE = 2;
//const String MALE = "Male";
//const String FEMALE = "Female";

const String MILE_CHURCH_SERVICE = "mChurchService";
const String MILE_CHURCH_SERVICE_BY = "mChurchServiceBy";

const String MILE_ALTER_CALL = "mAfterCall";
const String MILE_ALTER_CALL_BY = "mAfterCallBy";

const String MILE_FOUNDATION_CLASS = "mFoundationClass";
const String MILE_FOUNDATION_CLASS_BY = "mFoundationClassBy";

const String MILE_BAPTISM = "mBaptism";
const String MILE_BAPTISM_BY = "mBaptismBy";

const String MILE_JOINED_GROUP = "mJoinedGroup";
const String MILE_JOINED_GROUP_BY = "mJoinedGroupBy";

const String MILE_BIBLE_SCHOOL = "mBibleSchool";
const String MILE_BIBLE_SCHOOL_BY = "mBibleSchoolBy";

const String PHONE_NUMBER = "phoneNumber";
const String PHONE_PREF = "phonePref";
const String WHATSAPP_NUMBER = "whatsappNumber";
const String WHATSAPP_PREF = "whatsappPref";
const String IMPRESSIONS = "impressions";

const String BY = "by";
const String STATE = "state";
// const String LGA = "lga";
const String PASSPORT = "passport";
const String CITY = "city";
const String ZIPCODE = "zipCode";

const String PERSONAL_INFO_COMPLETED = "personalInfoCompleted";
const String WORK_INFO_COMPLETED = "workInfoCompleted";
const String ACCOUNT_INFO_COMPLETED = "accountInfoCompleted";
const String EMAIL_VERIFIED = "emailVerified";
const String BVN_VERIFIED = "bvnVerified";

const String BANNED = "banned";
const String DISABLED = "disabledList";
//const String ACCOUNT_DISABLED = "disabled";

const String FAILED_ATTEMPTS = "failedAttempt";

const String MALE_GENDER = "maleGender";
const String FEMALE_GENDER = "femaleGender";

const String NEWS = "news";
const String NEWS_BUT_TEXT = "newsBut";
const String NEWS_LINK = "newsLink";

//const String START_TIME = "startTime";
const String GRAND_PRIZE = "grandPrize";
const String FEE = "fee";
const String QUESTION_DURATION = "qDuration";
const String QUESTION = "question";
const String ANSWER = "answer";
const String OPTIONS = "options";
const String STARS = "stars";
const String REPLIES = "replies";
const String OPINION_ID = "opinionId";
const String OPINION = "opinion";

const String AUDIO_DURATION = "audioDuration";
const String AUDIO_URL = "audioUrl";
const String AUDIO_SIZE = "audioSize";
const String AUDIO_PATH = "audioPath";
const String AUDIO_LENGTH = "audioLength";
const String REPLY_DATA = "replyData";
const String VOICE_TYPE = "voiceType";

const String SHORT_TITLE = "shortTitle";
const String MAIN_TITLE = "mainTItle";

const String SHOW_ADS = "showAds";
const String FAILED_TIME = "failedTime";
const String FIRST_LAUNCH = "firstLaunch";
const String SETTINGS_PREF = "settingsPref";
const String REPORT_TYPE = "reportType";
const String ISLIST = "isList";

const String STORED_NOTIFY_IDS = "storeNotifyIds";

const String NATION_WIDE = "__nationWide";
const String WORLD_WIDE = "__worldWide";

const String MY_SCHOOL = "My School";
const String SCHOOLS_NATIONWIDE = "Schools Nationwide";
const String SCHOOLS_WORLDWIDE = "Schools Worldwide";
const String SELECTED_SCHOOLS = "Selected Schools";
const String ANY_STUDY = "Any Study";
const String SELECTED_STUDY = "Selected Study";
const String ANY_LEVEL = "Any Level";
const String SELECTED_LEVEL = "Selected Level";
const String ANY_GENDER = "Any Gender";
const String MALE_ONLY = "Male Only";
const String FEMALE_ONLY = "Female Only";

const String TARGET_SCHOOL = "targetSchool";
const String TARGET_STUDY = "targetStudy";
const String TARGET_LEVEL = "targetLevel";
const String TARGET_COUNTRY = "targetCountry";
const String TARGET_GENDER = "targetGender";

const String BY_ADMIN = "byAdmin";
const String SCHOOL = "school";
const String SCHOOL_ID = "schoolId";
const String STUDY_ID = "studyId";
const String STUDY = "study";
const String LEVEL = "level";
const String COUNTRY = "country";
const String COUNTRY_LIST = "countryList";
const String PLAN_LIST = "planList";
const String CHAT_SETTINGS = "chatSettings";
const String COUNTRY_CODE = "countryCode";

const String DISPLAY_NAME = "displayName";
const String QUOTES = "quotes";
const String ITEM_TITLE = "itemTitle";

const String VISIBILITY = "visibility";
const String REFERENCE = "reference";
const String CURRENT_POSITION = "currentPosition";
const String EMAIL = "email";

const String BVN = "bvn";
const String BUSINESS_NAME = "businessName";
const String BUSINESS_EMAIL = "businessEmail";
const String BUSINESS_PHONE = "businessPhone";
const String BUSINESS_ADDRESS = "businessAddress";
const String BUSINESS_CAC = "businessCac";

const String PASSWORD = "password";
const String MY_CODE = "myCode";
const String CREATED_AT = "createdAt";
const String TIME = "time";
//const String PRIORITY = "priority";
const String TIME_OF_OPERATION = "timeOfOperation";
const String TIME_UPDATED = "timeUpdated";
const String TIME_MODIFIED = "timeModified";
const String TIME_FILE_UPDATED = "timeFileUpdated";
const String SHOW_DATE = "showDate";
const String UPDATED_AT = "updatedAt";
//const String STOP_SPLIT = "stopSplit";
const String OBJECT_ID = "objectId";
const String MAX_LENGTH = "maxLength";
const String MY_SENT_CHAT = "mySentChat";
const String FARM_USERNAME = "farmUsername";
const String USERNAME = "username";
const String RATINGS = "ratings1";
const String RATINGS_TEXT = "ratingText";
const String EXPERT_ID = "expertId";
const String USER_ID = "userId";
const String LOOPIN_ID = "loopinId";
const String READ = "read";
//const String ID_DOC = "idDoc";
const String INTEREST = "interest";
const String FARM_ID = "farmId";
//const String FARM_ADMIN = "farmAdmin";
const String PRODUCE_ID = "produceId";
//const String PRODUCE_DB = "producedb";
const String FARM_NAME = "farmName";
//const String FARM_TYPE = "farmType";
//const String SUB_CATEGORIES = "subCategories";
const String SUB_CATEGORY = "subCategory";
const String DETAILS = "details";
const String TIME_LIMIT = "timeLimit";
const String PRICE_TYPE = "priceType";
const String FIXED_PRICE = "fixedPrice";
const String FLOAT_PRICE = "floatPrice";
const String RATE_PRICE = "ratePrice";
const String FLOAT_PERCENT = "floatPercent";
const String TERMS_HAS_KYC = "termsHasKYC";
const String TERMS_HAS_DAYS = "termsHasDays";
const String REGISTERED_DAYS = "registeredDays";
//const String NEGO = "nego";
const String SPLITTABLE = "splittable";
const String SPLIT_COUNT = "splitCount";
const String USER_IMAGE = "userImage";
const String BACK_IMAGE = "backImage";
const String IS_HIRING = "isHiring";
const String DEVICE_ID = "deviceId";
const String DEVICE_TYPE = "deviceType";
const String AVATAR_POSITION = "avatarPosition";
const String GENDER = "gender";
const String MOOD = "mood";
const String PLACE_NAME = "placeName";
const String ADDRESS = "address";
const String JOB_LOCATION = "jobLocation";
const String JOB_NAME = "jobName";
const String JOB_SKILLS = "jobSkills";
const String REPORT_TO = "reportTo";
const String CLIENT_ID = "clientId";
const String CLIENT_NAME = "clientName";
const String CLIENT_EMAIL = "clientEmail";
const String MY_SKILLS = "mySkills";
const String LATITUDE = "latitude1";
const String LONGITUDE = "longitude1";
const String CATEGORY = "category";
const String REGIONS = "regions";
const String MAIN_CATEGORY = "mainCategory";
//const String CATEGORY_LIST = "categoryList";
//const String MAIN_CATEGORY = "mainCategory";
const String STORY_TAGS = "storyTags";
const String TAG = "tag";
const String ID_CARD = "idCard";
const String ADD = "add";
const String VALUE = "value";

const String LAST_PEOPLE_TIME = "lastPeopleTime";
//const String VERIFIED = "verified";

const String VIEWS_IDS = "viewsIds";
const String SEEN_PEOPLE = "seenPeople";
const String LOVE_IDS = "loveIds";
const String LOVE_LIST = "loveList";

const String LAST_SEEN_STORY = "lastSeenStory";

const String QUIZ_TIP = "quizTip1";
const String STORY = "story";
const String OPINION_COUNT = "opinionCount";
const String SHOW_UNREAD = "showUnread";
const String BUDGET = "budget";

const String PAYBACK_AMOUNT = "paybackAmount";
const String PAYBACK_DAYS = "paybackDays";
// const String DURATION_DAYS = "durationDays";
const String NAIRA_TO_USD = "nairaToUsd1";
const String DAYS = "days";
const String MONTHS = "months";
const String STORY_BASE = "storyBase";
//const String DISABLED = "disabled";
const String SHORT_DESCRIPTION = "shortDescription";
const String DESCRIPTION = "description";
const String DYNAMIC_LINK = "dynamicLink";
const String PRICE_HISTORY = "priceHistory";
const String PRICE = "price";
const String PRICE2 = "price2";
//const String SELL_PRICE = "sellPrice";
const String IMAGES = "images2";
const String BACK_IMAGES = "backImages";
const String CONTACTS = "contacts";
const String LOCATION_DETAILS = "locationDetails";
const String LOCATION_DATA = "locationData";
const String VIDEO_LENGTH = "videoLength";
const String VIDEO_URL = "videoUrl";
const String THUMBNAIL_URL = "thumbUrl";
const String THUMBNAIL_PATH = "thumbPath";
const String PROCESSING = "processing";
const String VIDEO_PATH = "videoPath";
const String VIDEO_PATH_COMPRESSED = "videoPathCompressed";
const String EXPERT_IMAGES = "expertImages";
const String PLACE = "place";
const String EMPTY = "empty";
const String SIZE = "size";
const String READ_BY = "readBy";
const String HIDDEN = "hidden";
const String MUTED = "muted";
const String NOTIFY_ME_IDS = "notifyMeIds";
const String HAS_RATED = "hasRated1";
const String SILENCED = "slienced";
const String KICKED_OUT = "kickedOut";
const String ENDED = "ended";
const String OPENED = "opened";

const String LIB_SAVED_IDS = "libSavedIds";

const String FILTER_LIST = "filterList";
const String SEEN_QUOTES = "seenQuotes";

const String DEFAULT_POSITION = "defPosition";
const String FROM_SIGNUP = "fromSignup";
const String TITLE = "title";
const String PAGE_COUNT = "pageCount1";
const String SESSION_CLOSED = "sessionClosed";
const String CONTENT = "content";
const String COUNT = "count";
const String ABOUT = "about";

const String MATERIAL_TYPE = "materialType";
const String COURSE_NAME = "courseName";
const String COURSE_ID = "courseId";

const String SESSIONS_FEE = "sessionFee";
const String SESSIONS_COUNT = "sessionCount";
const String DISABLE_SUB = "disableSub";
//const String REVIEW_COUNT = "reviewCount";

const String FROM_CHAT = "fromChat";
const String SUB_TITLE = "subTitle";
const String TYPE = "type";
const String PEOPLE = "people";
const String DATABASE_NAME = "databaseName";

const String READ_ITEMS = "readItems";
const String RECEIVER_ID = "receiverId";
const String ICONS = "icons";
const String COLORS = "colors";
const String ITEMS = "items";
const String ICON = "icon";
const String MULTIPLE = "multiple";
const String HAS_SUB_ITEMS = "hasSubItems";

const String NEW_FEATURE = "newFeature";
//const String MUST_UPDATE = "mustUpdate";
const String STORIES_COUNT = "storiesCount";
const String TIPS_COUNT = "tipsCount";

//const String DUMMY_USER = "dummyUser";
const String DUMMY_IDS = "dummyIds";
const String CONTACT_US = "Contact Us";
//const String CONTACT_LINK = "ContactLink";

const String NEW_UPDATE = "newUpdate";
const String NEW_NOTIFICATION = "newNotification";

const String IMAGE_CHAT_STRING = "@@image";
const String DOCUMENT_CHAT_STRING = "@@document";

const String FILTER_KEY = "filterKey";
const String FILTER_TEXT = "filterText";
const String TIME_DOWNLOADED = "timeDownloaded";

const String WIDTH = "width";
const String HEIGHT = "height";
const String DOWNLOADED_BY = "downloadedBy1";
const String DOWNLOADED_BY_IDS = "downloadedByIds";
const String DUMMY_TEXT = "dummyText";
const String LAST_CHAT_BY = "lastChatBy";
const String LAST_CHAT = "lastChat";
//const String SAVED_PRODUCE = "savedProduce";
//const String SAVED_FARMS = "savedFarms";
const String SAVED_POSTS = "savedPosts";
const String USER_STATUS_IN_JOB = "userStatusInJob";
const String BIDS_BY = "bidsBy";
const String ACTION = "action";
const String FILE_URL = "fileUrl";
const String FILE_PATH = "filePath";
const String FILE_EXTENSION = "fileExtension";
const String FILE_ORIGINAL_PATH = "fileOriginalPath";
const String FILE_NAME = "fileName";
const String FILE_SIZE = "fileSize";
const String FILE_DATA = "fileData";
const String VIDEO_SIZE = "videoSize";
const String ERROR = "error";
//const String ACTION_NEW_OPINION = "actionNewOpinion";
const String ACTION_NEW_CHAT_REMOVE = "actionNewChatRemove";
const String ACTION_NEW_CHAT_ADD = "actionNewChatADD";
const String ACTION_LEAVE = "actionLeave";
const String ACTION_JOIN = "actionJoin";
const String ACTION_DELETE = "actionDelete";
const String ACTION_UNREAD = "actionUnread";
const String ACTION_REFRESH = "actionRefresh";
const String ACTION_DOWNLOAD = "actionDownload";
const String ACTION_DOWNLOAD_FAILED = "actionDownloadFailed";
const String ACTION_UPLOAD = "actionUpload";
const String ACTION_UPLOAD_FAILED = "actionUploadFailed";

const String SEARCH_TYPE = "searchType";
const String SEARCH_LIST = "searchList";
const String DRAFT_POST = "draftPost";
//const String DRAFT_FILTER = "draftFilter";

const String BROADCAST_GROUP = "com.maugost.group";
const String BROADCAST_CONVERSATION = "com.maugost.conv";
const String BROADCAST_CATEGORY = "com.maugost.caty";
const String BROADCAST_STORY = "com.maugost.story";

const String BROADCAST_SCHOOLS = "com.maugost.schools";
const String BROADCAST_STUDY = "com.maugost.study";
const String BROADCAST_LEVELS = "com.maugost.levels";
const String BROADCAST_HEADLINE = "com.maugost.headline";
const String BROADCAST_COMMENTS = "com.maugost.comments";
const String BROADCAST_POSTS = "com.maugost.posts";

const String SCHOOL_NAME = "schoolName";
const String SCHOOL_TYPE = "schoolType";
const String SCHOOL_LOCATION = "schoolLocation";
//const String SCHOOL_LAT_LONG = "schoolLatLong";
const String SCHOOL_PHONE = "schoolPhone";
const String SCHOOL_EMAIL = "schoolEmail";
const String SCHOOL_WEBSITE = "schoolWebsite";
const String SCHOOL_ABBRV = "schoolAbbreviation";

const String PREVIOUS_FACES = "prevFaces";
const String FACE_ITEM = "faceItem";
const String FACE_SETTINGS = "faceSettings";
const String FACE_TYPE = "face Type";
const String FACE_FREQ = "face Frequency";

const String UPDATED = "updated";

const String IS_ONLINE = "isOnline1";
const String TIME_ONLINE = "timeOnline";

const String PARTIES = "parties";
//const String PACKAGE_NAME = "packageName1";
//const String WEBSITE = "website1";
const String ABOUT_LINK = "aboutLink";
const String PRIVACY_LINK = "privacyLink";
const String TERMS_LINK = "termsLink";
const String AGREEMENT_LINK = "agreementLink";
//const String DEFAULT_WEBSITE = "https://agrobasedmarket.com";
//const String VERSION_CODE = "versionCode";
//const String GEN_MESSAGE = "genMessage";
//const String GEN_MESSAGE_TIME = "genMessageTime";

//const String PLAN_POSITION = "planPosition";
//const String FARM_COUNT = "farmCount";
//const String PRODUCE_COUNT = "produceCount";
//const String INTERNATIONAL = "international";
//const String SPECIAL_FORMAT = "specialFormat";
//const String TOP_FARM = "topFarm";
//const String IN_PAGE_PROMO = "inPagePromo";
//const String RECOMMENDED = "recommended";
const String FEATURES = "features";

const String PERCENT_OFF_3 = "percentOff3";
const String PERCENT_OFF_6 = "percentOff6";

const String EVERYONE = "everyone";
const String AD_LOADED = "adLoaded";
const String AD_KEY = "adKey";
const String AD_KEY_VIDEO = "adKeyVideo";
const String AD_KEY_INTER = "adKeyInter";

//const String EXPERT_TITLE = "expertTitle";
const int MODE_EASY = 2;
const int MODE_MEDIUM = 3;
const int MODE_HARD = 4;
const int MODE_DIFFICULT = 5;
const String TIMED_GAME = "timedGame";
const String GAME_MODE = "gameMode";
//const String QUESTION_COUNT = "questionCount";
const String GAME_SOUND = "gameSound";
const String RANKING = "ranking";

const String TYPING_ID = "typingId";
const String TYPING_BY = "typingBy";

const String CHAT_ID = "chatId";
const String PUSH_HUBBY = "pushHubby";
//const String MY_CHATS = "myChatsList13";
const String MY_OPINIONS = "myOpinions1";
const String MY_SESSIONS = "mySessions";

//const String IS_ONLINE = "isOnline";
//const String ACTIVATE_CHAT_ID = "activeChatId";
const String ACTIVATE = "active";
const String CODE_ENABLED = "codeEnabled";
const String PUBLIC_STORIES = "publicStories";
const String PRIVATE_CHAT = "privateChat";
const String DONT_PUSH = "dontPush";
const String DONT_CHAT = "dontChat";

const String CHAT_SOUND = "chatSound";
const String KISS_SOUND = "kissSound";

const String PUBLIC_EMAIL = "publicEmail";
const String DISABLE_VOICE_PUSH = "disableVoicePush";

const String COMMENT_ID = "commentId";
const String ITEM_ID = "itemId";
const String ITEM_DB = "itemDb";
const String MESSAGE = "message";
const String VERIFY_MESSAGE = "verifyMessage";
const String KEY = "key";
const String IMAGE_URL = "imageUrl";
const String IMAGE_PATH = "imagePath";
const String ADMIN_IMAGES = "adminImages";
const String HOME_IMAGES = "homeImages";
const String SELECT_MOOD_IMAGES = "selectMoodImages";
const String CARD_BACKGROUND_IMAGES = "cardBackImages";
const String PROFILE_PHOTO_IMAGES_MALE = "profileImagesMale";
const String PROFILE_PHOTO_IMAGES_FEMALE = "profileImagesFemale";
const String PROFILE_BACK_IMAGES_MALE = "profileBackImagesMale";
const String PROFILE_BACK_IMAGES_FEMALE = "profileBackImagesFemale";
//const String DB_NAME = "dbName";

const String MESSAGE_TYPE = "messageType";

const String ACCOUNT_TYPE = "accountType";

const String REJECTED_MESSAGE = "rejectedMessage";

const String FILES_TO_UPLOAD = "filesToUpload";
//const String LOVE_FINDER = "Love Finder";

const String PATH = "path";
const String IS_VIDEO = "isVideo";
const String BOY = "boy";
const String IS_ADMIN = "isAdmin";
const String IS_DUMMY = "isDummy";
const String IS_EXPERT = "isExpert1";
const String FULL_MODE = "fullMode";

const String CONVERSATION_TYPE = "convType";

const String THE_MODEL = "theModel";
const String AUTHOR = "AUTHOR";
// const String ITEM_NAME = "ItemName";
const String MCR = "mcr";
const String MCR_FREE = "mcrFree";
const String MCR_IDS = "mcrIds";
const String PROFILE_STATUS = "profileStatus";

const String SUPPORT_EMAIL = "supportEmail";
const String BLOCKED_VOICE = "blockedVoice1";
const String PLAYER_STATUS = "playerStatus";
const String QUIZ_PLAYERS = "quizPlayers";
const String PLAN_EXPIRY_TIME = "planExpiryTime";
const String PLAN_NAME = "planName";
const String PLAN_SETTINGS = "planSettings";
//const String PAID_CHAT = "paidChat";
//const String PLAN_DURATION_DAYS = "planDurationDays";
const String LIB_ACTIVATE = "libActive";
const String CURRENT_PLAN = "currentPlan";
const String PLAN_COUNT = "planCount";
const String BRONZE_COUNT = "bronzeCount";
const String SILVER_COUNT = "silverCount";
const String GOLD_COUNT = "goldCount";
const String BRONZE_COST = "bronzeCost";
const String SILVER_COST = "silverCost";
const String GOLD_COST = "goldCost";
const String BRONZE_COST_USD = "bronzeCostUsd";
const String SILVER_COST_USD = "silverCostUsd";
const String GOLD_COST_USD = "goldCostUsd";

const String DUMMY = "dummy";

const int PLAYER_FREE = 0;
const int PLAYER_WIN = 1;
const int PLAYER_WATCH = 2;

const int VOICE_TYPE_ALL = 0;
const int VOICE_TYPE_FOLLOWERS = 1;

const int MARKET_TYPE_ITEM = 0;
const int MARKET_TYPE_FOODSTUFF = 1;
const int MARKET_TYPE_SERVICES = 2;

const int IN_PROGRESS = 0;
const int SUCCESS = 1;
const int FAILED = 2;

const int NOT_MEMBER = 0;
const int MEMBER = 1;
const int ADMIN_MEMBER = 2;

const int FACE_TYPE_AUTO = 0;
const int FACE_TYPE_MANUAL = 1;

const int TYPE_USER_ONLINE = 0;
const int TYPE_USER_NORMAL = 1;

const int TYPE_POST_HEADLINE = 0;
const int TYPE_HEADLINE_NEWS = 1;

const int PUBLIC = 0;
const int PRIVATE = 1;
const int REMOVED = 2;

const int CONV_CHAT = 1;
const int CONV_OPINION = 2;

const int PENDING = 0;
const int APPROVED = 1;
const int PAUSED = 2;
const int STOPPED = 3;
const int DECLINED = 4;
const int SENT = 5;
const int ASSIGNED = 1000;
const int COMPLETED = 2000;

const int ACTIVE = 0;
const int IN_ACTIVE = 1;

const int TYPE_SAVED = 1;
const int TYPE_TIP = 2;
const int TYPE_HIDDEN = 3;

const int TYPE_OPINION = 0;
const int TYPE_INCOMING = 1;
const int TYPE_OUTGOING = 2;

const int STATUS_SENT = 0;
const int STATUS_READ = 1;

const int MESSAGE_TYPE_TEXT = 0;
const int MESSAGE_TYPE_IMAGE = 1;
const int MESSAGE_TYPE_FILE = 2;

const int LAUNCH_CHAT = 0;
const int LAUNCH_NOTIFICATION = 1;
const int LAUNCH_PENDING = 2;
const int LAUNCH_REPORTS = 3;

const int TYPE_ABOUT = 0;
const int TYPE_TERMS = 1;
const int TYPE_POLICY = 2;

const int SEARCH_SCHOOL = 0;
const int SEARCH_STUDY = 1;
const int SEARCH_LEVEL = 2;

const int SCHOOL_TYPE_UNI = 0;
const int SCHOOL_TYPE_POLY = 1;
const int SCHOOL_TYPE_COLLEGE = 2;

const int LOAD_CURRENT = 0;
const int LOAD_REFRESH = 1;
const int LOAD_PREVIOUS = 2;

const int ADD_CATEGORY = 0;
const int ADD_SKILL = 1;

const int STATUS_UNDONE = 0;
const int STATUS_COMPLETED = 1;
const int STATUS_IN_PROGRESS = 3;

const int PAY_LOCAL_BANK = 0;
const int PAY_PAYPAL = 1;

const int TYPE_FEEDS_ITEM = 0;
const int TYPE_JOB_FRAG_ITEM = 1;
const int TYPE_MY_BID_ITEM = 2;
const int TYPE_EMPLOYER_BID_ITEM = 3;

// const int VERIFY_STATUS_PENDING = 0;
// const int VERIFY_STATUS_VERIFIED = 1;
// const int VERIFY_STATUS_TRUSTED = 2;

const int ITEM_TYPE_POST = 0;
const int ITEM_TYPE_PROFILE = 1;
const int ITEM_TYPE_LIBRARY = 2;
const int ITEM_TYPE_MARKET = 3;
const int ITEM_TYPE_GROUP = 4;
const int ITEM_TYPE_HEADLINE = 5;
const int ITEM_TYPE_COMMENT = 6;
const int ITEM_TYPE_ADVERT = 7;
const int ITEM_TYPE_MCR = 8;
const int ITEM_TYPE_SCHOOL = 9;
const int ITEM_TYPE_STUDY = 10;
const int ITEM_TYPE_BROADCAST = 11;
const int ITEM_TYPE_WITHDRAW = 12;
const int ITEM_TYPE_PLAN = 13;

const String QUOTE_BY = "quoteBy";

const String PUSH_TYPE_CHAT = '1';
const String PUSH_TYPE_LIB = '2';
const String PUSH_TYPE_FOLLOW = '3';

//    const int PROFILE_START_BIDDER = 0;
//    const int PROFILE_START_BIDDER = 0;

const String DOWNLOAD_PATH_SAVE_PHOTO = "/maugost/savePhotos";
const String DOWNLOAD_FILE_PATH = "/maugost/savedFiles";

const String COMMENTS_COUNT = "commentCounts";
const String LIKED_BY = "likedBy";
//const String FOLLOWERS = "followers";
const String FOLLOWING = "following";
const String NOTIFYING = "notifying";
const String UNREAD_COUNT = "unreadCount";

const String SAVE_PHOTO_PATH = "";
const String SAVE_FILE_PATH = "";

const String SHOW_ALL_POSTS = "showAllPosts";
const String BROAD_COST = "broadCost";

const String AD_SECTION_HOME = "adSectionHome";
const String AD_SECTION_LIB_MAIN = "adSectionLibMain";
const String AD_SECTION_LIB_BOOKS = "adSectionLibBooks";
const String AD_SECTION_MARKET = "adSectionMarket";
const String MY_DOWNLOADS = "myDownloads";
const String MY_DOWNLOAD_COUNT = "myDownloadCount";
const String LAST_DOWNLOAD_TIME = "lastDownloadTime";
const String PAID_CHATS = "paidChats";
const String SUMMARY = "summary";
const String DOWNLOAD_TIME = "downloadTime";

/*
*   TextEditingController downloadLinksController = TextEditingController();
  TextEditingController playStoreLinkController = TextEditingController();
  TextEditingController appleStoreLinkController = TextEditingController();
  TextEditingController webLinkController = TextEditingController();
  TextEditingController supportEmailController = TextEditingController();
  TextEditingController supportPhoneController = TextEditingController();
  TextEditingController warningMessageController = TextEditingController();
  TextEditingController androidVersionController = TextEditingController();
  List chatSamples = [];*/

const String FACEBOOK_LINK = "facebookLink";
const String INSTA_LINK = "instaLink";
const String TWITTER_LINK = "twitterLink";
const String DOWNLOAD_LINKS = "downloadLinks";
const String PLAYSTORE_LINK = "playStoreLink";
const String APPLESTORE_LINK = "appleStoreLink";
const String WEB_LINK = "webLink";
const String SUPPORT_ID = "supportId";
const String SUPPORT_PHONE = "supportPhone";
const String WARNING_MESSAGE_POST = "warningMessagePost";
const String WARNING_MESSAGE_PROFILE = "warningMessageProfile";
const String ANDROID_VERSION = "androidVersion";
const String ANDROID_VERSION_TEXT = "androidVersionText";
const String APPLE_VERSION = "appleVersion";
const String APPLE_VERSION_TEXT = "appleVersionText";
const String CHAT_SAMPLES = "chatSamplesMessages";
const String POST_REPORT_SAMPLES = "postReports";
const String USER_REPORT_SAMPLES = "userReports";
const String DISAPPROVE_SAMPLES = "disapproveSamples";
const String MOOD_TYPES = "moods";
const String GENDER_KISS = "genderKiss";
const String KISS_COUNT = "kissCount";
const String KISS_TYPE = "kissType";
const String GENDER_COMMENT = "genderComment";
const String STORY_PREFIX = "storyPrefix";
//const String PREMIUM_USER = "premiumUser";

const int KISS_TYPE_POST = 0;
const int KISS_TYPE_PHOTO = 1;
const int KISS_TYPE_PROFILE = 2;

const int SORT_TYPE_RELEVANT = 0;
//const int SORT_TYPE_NEWEST = 1;
const int SORT_TYPE_LOWEST_PRICE = 1;
const int SORT_TYPE_HIGHEST_PRICE = 2;

const String SORT_TYPE = "sortType";
const String MIN = "min";
const String MAX = "max";

List sortOrders = ["Relevant", /*"Newest",*/ "Lowest Price", "Highest Price"];

const List MARITAL_LIST = [SINGLE, MARRIED, DIVORCED, WIDOW, WIDOWER];
const List AGE_RANGE_LIST = ["13-20", "21-25", "26-35", "35-54", "55+"];
const List MILESTONE_LIST_KEYS = [
  MILE_CHURCH_SERVICE,
  MILE_ALTER_CALL,
  MILE_FOUNDATION_CLASS,
  MILE_BAPTISM,
  MILE_JOINED_GROUP,
  MILE_BIBLE_SCHOOL
];
const List MILESTONE_LIST_TEXT = [
  "Church Service Attendance",
  "Alter Call",
  "Attended Foundation Class",
  "Baptized",
  "Joined Service Group",
  "Attended Bible School",
];

const String ANY = "Any";
const String TODAY = "Today";
const String THIS_WEEK = "This Week";
const String THIS_MONTH = "This Month";
const String THIS_YEAR = "This Year";
const List TIME_RANGES = [
  ANY,
  TODAY,
  THIS_WEEK,
  THIS_MONTH,
  THIS_YEAR,
];

var accountTypes = ["Individual", "Business"];

const String RED = "red";
const String BLUE = "blue";
const String GREEN = "pink";
const String BROWN = "brown";
const String DARK_GREEN = "darkGreen";
const String DARK_BLUE = "darkBlue";
const String ORANGE = "orange";

List blockedIds = [];
BaseModel userModel = BaseModel();
bool isLoggedIn = false;
BaseModel appSettingsModel = BaseModel();
bool lighter = false;
bool darkMode = false;
BuildContext baseContext;

const states = [
  {
    "state": {
      "name": "Abia State",
      "id": 1,
      "locals": [
        {"name": "Aba South", "id": 1},
        {"name": "Arochukwu", "id": 2},
        {"name": "Bende", "id": 3},
        {"name": "Ikwuano", "id": 4},
        {"name": "Isiala Ngwa North", "id": 5},
        {"name": "Isiala Ngwa South", "id": 6},
        {"name": "Isuikwuato", "id": 7},
        {"name": "Obi Ngwa", "id": 8},
        {"name": "Ohafia", "id": 9},
        {"name": "Osisioma", "id": 10},
        {"name": "Ugwunagbo", "id": 11},
        {"name": "Ukwa East", "id": 12},
        {"name": "Ukwa West", "id": 13},
        {"name": "Umuahia North", "id": 14},
        {"name": "Umuahia South", "id": 15},
        {"name": "Umu Nneochi", "id": 16}
      ]
    }
  },
  {
    "state": {
      "name": "Adamawa State",
      "id": 2,
      "locals": [
        {"name": "Fufure", "id": 1},
        {"name": "Ganye", "id": 2},
        {"name": "Gayuk", "id": 3},
        {"name": "Gombi", "id": 4},
        {"name": "Grie", "id": 5},
        {"name": "Hong", "id": 6},
        {"name": "Jada", "id": 7},
        {"name": "Lamurde", "id": 8},
        {"name": "Madagali", "id": 9},
        {"name": "Maiha", "id": 10},
        {"name": "Mayo Belwa", "id": 11},
        {"name": "Michika", "id": 12},
        {"name": "Mubi North", "id": 13},
        {"name": "Mubi South", "id": 14},
        {"name": "Numan", "id": 15},
        {"name": "Shelleng", "id": 16},
        {"name": "Song", "id": 17},
        {"name": "Toungo", "id": 18},
        {"name": "Yola North", "id": 19},
        {"name": "Yola South", "id": 20}
      ]
    }
  },
  {
    "state": {
      "name": "Akwa Ibom State",
      "id": 3,
      "locals": [
        {"name": "Eastern Obolo", "id": 1},
        {"name": "Eket", "id": 2},
        {"name": "Esit Eket", "id": 3},
        {"name": "Essien Udim", "id": 4},
        {"name": "Etim Ekpo", "id": 5},
        {"name": "Etinan", "id": 6},
        {"name": "Ibeno", "id": 7},
        {"name": "Ibesikpo Asutan", "id": 8},
        {"name": "Ibiono-Ibom", "id": 9},
        {"name": "Ika", "id": 10},
        {"name": "Ikono", "id": 11},
        {"name": "Ikot Abasi", "id": 12},
        {"name": "Ikot Ekpene", "id": 13},
        {"name": "Ini", "id": 14},
        {"name": "Itu", "id": 15},
        {"name": "Mbo", "id": 16},
        {"name": "Mkpat-Enin", "id": 17},
        {"name": "Nsit-Atai", "id": 18},
        {"name": "Nsit-Ibom", "id": 19},
        {"name": "Nsit-Ubium", "id": 20},
        {"name": "Obot Akara", "id": 21},
        {"name": "Okobo", "id": 22},
        {"name": "Onna", "id": 23},
        {"name": "Oron", "id": 24},
        {"name": "Oruk Anam", "id": 25},
        {"name": "Udung-Uko", "id": 26},
        {"name": "Ukanafun", "id": 27},
        {"name": "Uruan", "id": 28},
        {"name": "Urue-Offong\/Oruko", "id": 29},
        {"name": "Uyo", "id": 30}
      ]
    }
  },
  {
    "state": {
      "name": "Anambra State",
      "id": 4,
      "locals": [
        {"name": "Anambra East", "id": 1},
        {"name": "Anambra West", "id": 2},
        {"name": "Anaocha", "id": 3},
        {"name": "Awka North", "id": 4},
        {"name": "Awka South", "id": 5},
        {"name": "Ayamelum", "id": 6},
        {"name": "Dunukofia", "id": 7},
        {"name": "Ekwusigo", "id": 8},
        {"name": "Idemili North", "id": 9},
        {"name": "Idemili South", "id": 10},
        {"name": "Ihiala", "id": 11},
        {"name": "Njikoka", "id": 12},
        {"name": "Nnewi North", "id": 13},
        {"name": "Nnewi South", "id": 14},
        {"name": "Ogbaru", "id": 15},
        {"name": "Onitsha North", "id": 16},
        {"name": "Onitsha South", "id": 17},
        {"name": "Orumba North", "id": 18},
        {"name": "Orumba South", "id": 19},
        {"name": "Oyi", "id": 20}
      ]
    }
  },
  {
    "state": {
      "name": "Bauchi State",
      "id": 5,
      "locals": [
        {"name": "Bauchi", "id": 1},
        {"name": "Bogoro", "id": 2},
        {"name": "Damban", "id": 3},
        {"name": "Darazo", "id": 4},
        {"name": "Dass", "id": 5},
        {"name": "Gamawa", "id": 6},
        {"name": "Ganjuwa", "id": 7},
        {"name": "Giade", "id": 8},
        {"name": "Itas\/Gadau", "id": 9},
        {"name": "Jama'are", "id": 10},
        {"name": "Katagum", "id": 11},
        {"name": "Kirfi", "id": 12},
        {"name": "Misau", "id": 13},
        {"name": "Ningi", "id": 14},
        {"name": "Shira", "id": 15},
        {"name": "Tafawa Balewa", "id": 16},
        {"name": "Toro", "id": 17},
        {"name": "Warji", "id": 18},
        {"name": "Zaki", "id": 19}
      ]
    }
  },
  {
    "state": {
      "name": "Bayelsa State",
      "id": 6,
      "locals": [
        {"name": "Ekeremor", "id": 1},
        {"name": "Kolokuma\/Opokuma", "id": 2},
        {"name": "Nembe", "id": 3},
        {"name": "Ogbia", "id": 4},
        {"name": "Sagbama", "id": 5},
        {"name": "Southern Ijaw", "id": 6},
        {"name": "Yenagoa", "id": 7}
      ]
    }
  },
  {
    "state": {
      "name": "Benue State",
      "id": 7,
      "locals": [
        {"name": "Apa", "id": 1},
        {"name": "Ado", "id": 2},
        {"name": "Buruku", "id": 3},
        {"name": "Gboko", "id": 4},
        {"name": "Guma", "id": 5},
        {"name": "Gwer East", "id": 6},
        {"name": "Gwer West", "id": 7},
        {"name": "Katsina-Ala", "id": 8},
        {"name": "Konshisha", "id": 9},
        {"name": "Kwande", "id": 10},
        {"name": "Logo", "id": 11},
        {"name": "Makurdi", "id": 12},
        {"name": "Obi", "id": 13},
        {"name": "Ogbadibo", "id": 14},
        {"name": "Ohimini", "id": 15},
        {"name": "Oju", "id": 16},
        {"name": "Okpokwu", "id": 17},
        {"name": "Oturkpo", "id": 18},
        {"name": "Tarka", "id": 19},
        {"name": "Ukum", "id": 20},
        {"name": "Ushongo", "id": 21},
        {"name": "Vandeikya", "id": 22}
      ]
    }
  },
  {
    "state": {
      "name": "Borno State",
      "id": 8,
      "locals": [
        {"name": "Askira\/Uba", "id": 1},
        {"name": "Bama", "id": 2},
        {"name": "Bayo", "id": 3},
        {"name": "Biu", "id": 4},
        {"name": "Chibok", "id": 5},
        {"name": "Damboa", "id": 6},
        {"name": "Dikwa", "id": 7},
        {"name": "Gubio", "id": 8},
        {"name": "Guzamala", "id": 9},
        {"name": "Gwoza", "id": 10},
        {"name": "Hawul", "id": 11},
        {"name": "Jere", "id": 12},
        {"name": "Kaga", "id": 13},
        {"name": "Kala\/Balge", "id": 14},
        {"name": "Konduga", "id": 15},
        {"name": "Kukawa", "id": 16},
        {"name": "Kwaya Kusar", "id": 17},
        {"name": "Mafa", "id": 18},
        {"name": "Magumeri", "id": 19},
        {"name": "Maiduguri", "id": 20},
        {"name": "Marte", "id": 21},
        {"name": "Mobbar", "id": 22},
        {"name": "Monguno", "id": 23},
        {"name": "Ngala", "id": 24},
        {"name": "Nganzai", "id": 25},
        {"name": "Shani", "id": 26}
      ]
    }
  },
  {
    "state": {
      "name": "Cross River State",
      "id": 9,
      "locals": [
        {"name": "Akamkpa", "id": 1},
        {"name": "Akpabuyo", "id": 2},
        {"name": "Bakassi", "id": 3},
        {"name": "Bekwarra", "id": 4},
        {"name": "Biase", "id": 5},
        {"name": "Boki", "id": 6},
        {"name": "Calabar Municipal", "id": 7},
        {"name": "Calabar South", "id": 8},
        {"name": "Etung", "id": 9},
        {"name": "Ikom", "id": 10},
        {"name": "Obanliku", "id": 11},
        {"name": "Obubra", "id": 12},
        {"name": "Obudu", "id": 13},
        {"name": "Odukpani", "id": 14},
        {"name": "Ogoja", "id": 15},
        {"name": "Yakuur", "id": 16},
        {"name": "Yala", "id": 17}
      ]
    }
  },
  {
    "state": {
      "name": "Delta State",
      "id": 10,
      "locals": [
        {"name": "Aniocha South", "id": 1},
        {"name": "Bomadi", "id": 2},
        {"name": "Burutu", "id": 3},
        {"name": "Ethiope East", "id": 4},
        {"name": "Ethiope West", "id": 5},
        {"name": "Ika North East", "id": 6},
        {"name": "Ika South", "id": 7},
        {"name": "Isoko North", "id": 8},
        {"name": "Isoko South", "id": 9},
        {"name": "Ndokwa East", "id": 10},
        {"name": "Ndokwa West", "id": 11},
        {"name": "Okpe", "id": 12},
        {"name": "Oshimili North", "id": 13},
        {"name": "Oshimili South", "id": 14},
        {"name": "Patani", "id": 15},
        {"name": "Sapele", "id": 16},
        {"name": "Udu", "id": 17},
        {"name": "Ughelli North", "id": 18},
        {"name": "Ughelli South", "id": 19},
        {"name": "Ukwuani", "id": 20},
        {"name": "Uvwie", "id": 21},
        {"name": "Warri North", "id": 22},
        {"name": "Warri South", "id": 23},
        {"name": "Warri South West", "id": 24}
      ]
    }
  },
  {
    "state": {
      "name": "Ebonyi State",
      "id": 11,
      "locals": [
        {"name": "Afikpo North", "id": 1},
        {"name": "Afikpo South", "id": 2},
        {"name": "Ebonyi", "id": 3},
        {"name": "Ezza North", "id": 4},
        {"name": "Ezza South", "id": 5},
        {"name": "Ikwo", "id": 6},
        {"name": "Ishielu", "id": 7},
        {"name": "Ivo", "id": 8},
        {"name": "Izzi", "id": 9},
        {"name": "Ohaozara", "id": 10},
        {"name": "Ohaukwu", "id": 11},
        {"name": "Onicha", "id": 12}
      ]
    }
  },
  {
    "state": {
      "name": "Edo State",
      "id": 12,
      "locals": [
        {"name": "Egor", "id": 1},
        {"name": "Esan Central", "id": 2},
        {"name": "Esan North-East", "id": 3},
        {"name": "Esan South-East", "id": 4},
        {"name": "Esan West", "id": 5},
        {"name": "Etsako Central", "id": 6},
        {"name": "Etsako East", "id": 7},
        {"name": "Etsako West", "id": 8},
        {"name": "Igueben", "id": 9},
        {"name": "Ikpoba Okha", "id": 10},
        {"name": "Orhionmwon", "id": 11},
        {"name": "Oredo", "id": 12},
        {"name": "Ovia North-East", "id": 13},
        {"name": "Ovia South-West", "id": 14},
        {"name": "Owan East", "id": 15},
        {"name": "Owan West", "id": 16},
        {"name": "Uhunmwonde", "id": 17}
      ]
    }
  },
  {
    "state": {
      "name": "Ekiti State",
      "id": 13,
      "locals": [
        {"name": "Efon", "id": 1},
        {"name": "Ekiti East", "id": 2},
        {"name": "Ekiti South-West", "id": 3},
        {"name": "Ekiti West", "id": 4},
        {"name": "Emure", "id": 5},
        {"name": "Gbonyin", "id": 6},
        {"name": "Ido Osi", "id": 7},
        {"name": "Ijero", "id": 8},
        {"name": "Ikere", "id": 9},
        {"name": "Ikole", "id": 10},
        {"name": "Ilejemeje", "id": 11},
        {"name": "Irepodun\/Ifelodun", "id": 12},
        {"name": "Ise\/Orun", "id": 13},
        {"name": "Moba", "id": 14},
        {"name": "Oye", "id": 15}
      ]
    }
  },
  {
    "state": {
      "name": "Enugu State",
      "id": 14,
      "locals": [
        {"name": "Awgu", "id": 1},
        {"name": "Enugu East", "id": 2},
        {"name": "Enugu North", "id": 3},
        {"name": "Enugu South", "id": 4},
        {"name": "Ezeagu", "id": 5},
        {"name": "Igbo Etiti", "id": 6},
        {"name": "Igbo Eze North", "id": 7},
        {"name": "Igbo Eze South", "id": 8},
        {"name": "Isi Uzo", "id": 9},
        {"name": "Nkanu East", "id": 10},
        {"name": "Nkanu West", "id": 11},
        {"name": "Nsukka", "id": 12},
        {"name": "Oji River", "id": 13},
        {"name": "Udenu", "id": 14},
        {"name": "Udi", "id": 15},
        {"name": "Uzo Uwani", "id": 16}
      ]
    }
  },
  {
    "state": {
      "name": "FCT",
      "id": 15,
      "locals": [
        {"name": "Bwari", "id": 1},
        {"name": "Gwagwalada", "id": 2},
        {"name": "Kuje", "id": 3},
        {"name": "Kwali", "id": 4},
        {"name": "Municipal Area Council", "id": 5}
      ]
    }
  },
  {
    "state": {
      "name": "Gombe State",
      "id": 16,
      "locals": [
        {"name": "Balanga", "id": 1},
        {"name": "Billiri", "id": 2},
        {"name": "Dukku", "id": 3},
        {"name": "Funakaye", "id": 4},
        {"name": "Gombe", "id": 5},
        {"name": "Kaltungo", "id": 6},
        {"name": "Kwami", "id": 7},
        {"name": "Nafada", "id": 8},
        {"name": "Shongom", "id": 9},
        {"name": "Yamaltu\/Deba", "id": 10}
      ]
    }
  },
  {
    "state": {
      "name": "Imo State",
      "id": 17,
      "locals": [
        {"name": "Ahiazu Mbaise", "id": 1},
        {"name": "Ehime Mbano", "id": 2},
        {"name": "Ezinihitte", "id": 3},
        {"name": "Ideato North", "id": 4},
        {"name": "Ideato South", "id": 5},
        {"name": "Ihitte\/Uboma", "id": 6},
        {"name": "Ikeduru", "id": 7},
        {"name": "Isiala Mbano", "id": 8},
        {"name": "Isu", "id": 9},
        {"name": "Mbaitoli", "id": 10},
        {"name": "Ngor Okpala", "id": 11},
        {"name": "Njaba", "id": 12},
        {"name": "Nkwerre", "id": 13},
        {"name": "Nwangele", "id": 14},
        {"name": "Obowo", "id": 15},
        {"name": "Oguta", "id": 16},
        {"name": "Ohaji\/Egbema", "id": 17},
        {"name": "Okigwe", "id": 18},
        {"name": "Orlu", "id": 19},
        {"name": "Orsu", "id": 20},
        {"name": "Oru East", "id": 21},
        {"name": "Oru West", "id": 22},
        {"name": "Owerri Municipal", "id": 23},
        {"name": "Owerri North", "id": 24},
        {"name": "Owerri West", "id": 25},
        {"name": "Unuimo", "id": 26}
      ]
    }
  },
  {
    "state": {
      "name": "Jigawa State",
      "id": 18,
      "locals": [
        {"name": "Babura", "id": 1},
        {"name": "Biriniwa", "id": 2},
        {"name": "Birnin Kudu", "id": 3},
        {"name": "Buji", "id": 4},
        {"name": "Dutse", "id": 5},
        {"name": "Gagarawa", "id": 6},
        {"name": "Garki", "id": 7},
        {"name": "Gumel", "id": 8},
        {"name": "Guri", "id": 9},
        {"name": "Gwaram", "id": 10},
        {"name": "Gwiwa", "id": 11},
        {"name": "Hadejia", "id": 12},
        {"name": "Jahun", "id": 13},
        {"name": "Kafin Hausa", "id": 14},
        {"name": "Kazaure", "id": 15},
        {"name": "Kiri Kasama", "id": 16},
        {"name": "Kiyawa", "id": 17},
        {"name": "Kaugama", "id": 18},
        {"name": "Maigatari", "id": 19},
        {"name": "Malam Madori", "id": 20},
        {"name": "Miga", "id": 21},
        {"name": "Ringim", "id": 22},
        {"name": "Roni", "id": 23},
        {"name": "Sule Tankarkar", "id": 24},
        {"name": "Taura", "id": 25},
        {"name": "Yankwashi", "id": 26}
      ]
    }
  },
  {
    "state": {
      "name": "Kaduna State",
      "id": 19,
      "locals": [
        {"name": "Chikun", "id": 1},
        {"name": "Giwa", "id": 2},
        {"name": "Igabi", "id": 3},
        {"name": "Ikara", "id": 4},
        {"name": "Jaba", "id": 5},
        {"name": "Jema'a", "id": 6},
        {"name": "Kachia", "id": 7},
        {"name": "Kaduna North", "id": 8},
        {"name": "Kaduna South", "id": 9},
        {"name": "Kagarko", "id": 10},
        {"name": "Kajuru", "id": 11},
        {"name": "Kaura", "id": 12},
        {"name": "Kauru", "id": 13},
        {"name": "Kubau", "id": 14},
        {"name": "Kudan", "id": 15},
        {"name": "Lere", "id": 16},
        {"name": "Makarfi", "id": 17},
        {"name": "Sabon Gari", "id": 18},
        {"name": "Sanga", "id": 19},
        {"name": "Soba", "id": 20},
        {"name": "Zangon Kataf", "id": 21},
        {"name": "Zaria", "id": 22}
      ]
    }
  },
  {
    "state": {
      "name": "Kano State",
      "id": 20,
      "locals": [
        {"name": "Albasu", "id": 1},
        {"name": "Bagwai", "id": 2},
        {"name": "Bebeji", "id": 3},
        {"name": "Bichi", "id": 4},
        {"name": "Bunkure", "id": 5},
        {"name": "Dala", "id": 6},
        {"name": "Dambatta", "id": 7},
        {"name": "Dawakin Kudu", "id": 8},
        {"name": "Dawakin Tofa", "id": 9},
        {"name": "Doguwa", "id": 10},
        {"name": "Fagge", "id": 11},
        {"name": "Gabasawa", "id": 12},
        {"name": "Garko", "id": 13},
        {"name": "Garun Mallam", "id": 14},
        {"name": "Gaya", "id": 15},
        {"name": "Gezawa", "id": 16},
        {"name": "Gwale", "id": 17},
        {"name": "Gwarzo", "id": 18},
        {"name": "Kabo", "id": 19},
        {"name": "Kano Municipal", "id": 20},
        {"name": "Karaye", "id": 21},
        {"name": "Kibiya", "id": 22},
        {"name": "Kiru", "id": 23},
        {"name": "Kumbotso", "id": 24},
        {"name": "Kunchi", "id": 25},
        {"name": "Kura", "id": 26},
        {"name": "Madobi", "id": 27},
        {"name": "Makoda", "id": 28},
        {"name": "Minjibir", "id": 29},
        {"name": "Nasarawa", "id": 30},
        {"name": "Rano", "id": 31},
        {"name": "Rimin Gado", "id": 32},
        {"name": "Rogo", "id": 33},
        {"name": "Shanono", "id": 34},
        {"name": "Sumaila", "id": 35},
        {"name": "Takai", "id": 36},
        {"name": "Tarauni", "id": 37},
        {"name": "Tofa", "id": 38},
        {"name": "Tsanyawa", "id": 39},
        {"name": "Tudun Wada", "id": 40},
        {"name": "Ungogo", "id": 41},
        {"name": "Warawa", "id": 42},
        {"name": "Wudil", "id": 43}
      ]
    }
  },
  {
    "state": {
      "name": "Katsina State",
      "id": 21,
      "locals": [
        {"name": "Batagarawa", "id": 1},
        {"name": "Batsari", "id": 2},
        {"name": "Baure", "id": 3},
        {"name": "Bindawa", "id": 4},
        {"name": "Charanchi", "id": 5},
        {"name": "Dandume", "id": 6},
        {"name": "Danja", "id": 7},
        {"name": "Dan Musa", "id": 8},
        {"name": "Daura", "id": 9},
        {"name": "Dutsi", "id": 10},
        {"name": "Dutsin Ma", "id": 11},
        {"name": "Faskari", "id": 12},
        {"name": "Funtua", "id": 13},
        {"name": "Ingawa", "id": 14},
        {"name": "Jibia", "id": 15},
        {"name": "Kafur", "id": 16},
        {"name": "Kaita", "id": 17},
        {"name": "Kankara", "id": 18},
        {"name": "Kankia", "id": 19},
        {"name": "Katsina", "id": 20},
        {"name": "Kurfi", "id": 21},
        {"name": "Kusada", "id": 22},
        {"name": "Mai'Adua", "id": 23},
        {"name": "Malumfashi", "id": 24},
        {"name": "Mani", "id": 25},
        {"name": "Mashi", "id": 26},
        {"name": "Matazu", "id": 27},
        {"name": "Musawa", "id": 28},
        {"name": "Rimi", "id": 29},
        {"name": "Sabuwa", "id": 30},
        {"name": "Safana", "id": 31},
        {"name": "Sandamu", "id": 32},
        {"name": "Zango", "id": 33}
      ]
    }
  },
  {
    "state": {
      "name": "Kebbi State",
      "id": 22,
      "locals": [
        {"name": "Arewa Dandi", "id": 1},
        {"name": "Argungu", "id": 2},
        {"name": "Augie", "id": 3},
        {"name": "Bagudo", "id": 4},
        {"name": "Birnin Kebbi", "id": 5},
        {"name": "Bunza", "id": 6},
        {"name": "Dandi", "id": 7},
        {"name": "Fakai", "id": 8},
        {"name": "Gwandu", "id": 9},
        {"name": "Jega", "id": 10},
        {"name": "Kalgo", "id": 11},
        {"name": "Koko\/Besse", "id": 12},
        {"name": "Maiyama", "id": 13},
        {"name": "Ngaski", "id": 14},
        {"name": "Sakaba", "id": 15},
        {"name": "Shanga", "id": 16},
        {"name": "Suru", "id": 17},
        {"name": "Wasagu\/Danko", "id": 18},
        {"name": "Yauri", "id": 19},
        {"name": "Zuru", "id": 20}
      ]
    }
  },
  {
    "state": {
      "name": "Kogi State",
      "id": 23,
      "locals": [
        {"name": "Ajaokuta", "id": 1},
        {"name": "Ankpa", "id": 2},
        {"name": "Bassa", "id": 3},
        {"name": "Dekina", "id": 4},
        {"name": "Ibaji", "id": 5},
        {"name": "Idah", "id": 6},
        {"name": "Igalamela Odolu", "id": 7},
        {"name": "Ijumu", "id": 8},
        {"name": "Kabba\/Bunu", "id": 9},
        {"name": "Kogi", "id": 10},
        {"name": "Lokoja", "id": 11},
        {"name": "Mopa Muro", "id": 12},
        {"name": "Ofu", "id": 13},
        {"name": "Ogori\/Magongo", "id": 14},
        {"name": "Okehi", "id": 15},
        {"name": "Okene", "id": 16},
        {"name": "Olamaboro", "id": 17},
        {"name": "Omala", "id": 18},
        {"name": "Yagba East", "id": 19},
        {"name": "Yagba West", "id": 20}
      ]
    }
  },
  {
    "state": {
      "name": "Kwara State",
      "id": 24,
      "locals": [
        {"name": "Baruten", "id": 1},
        {"name": "Edu", "id": 2},
        {"name": "Ekiti", "id": 3},
        {"name": "Ifelodun", "id": 4},
        {"name": "Ilorin East", "id": 5},
        {"name": "Ilorin South", "id": 6},
        {"name": "Ilorin West", "id": 7},
        {"name": "Irepodun", "id": 8},
        {"name": "Isin", "id": 9},
        {"name": "Kaiama", "id": 10},
        {"name": "Moro", "id": 11},
        {"name": "Offa", "id": 12},
        {"name": "Oke Ero", "id": 13},
        {"name": "Oyun", "id": 14},
        {"name": "Pategi", "id": 15}
      ]
    }
  },
  {
    "state": {
      "name": "Lagos State",
      "id": 25,
      "locals": [
        {"name": "Ajeromi-Ifelodun", "id": 1},
        {"name": "Alimosho", "id": 2},
        {"name": "Amuwo-Odofin", "id": 3},
        {"name": "Apapa", "id": 4},
        {"name": "Badagry", "id": 5},
        {"name": "Epe", "id": 6},
        {"name": "Eti Osa", "id": 7},
        {"name": "Ibeju-Lekki", "id": 8},
        {"name": "Ifako-Ijaiye", "id": 9},
        {"name": "Ikeja", "id": 10},
        {"name": "Ikorodu", "id": 11},
        {"name": "Kosofe", "id": 12},
        {"name": "Lagos Island", "id": 13},
        {"name": "Lagos Mainland", "id": 14},
        {"name": "Mushin", "id": 15},
        {"name": "Ojo", "id": 16},
        {"name": "Oshodi-Isolo", "id": 17},
        {"name": "Shomolu", "id": 18},
        {"name": "Surulere", "id": 19}
      ]
    }
  },
  {
    "state": {
      "name": "Nasarawa State",
      "id": 26,
      "locals": [
        {"name": "Awe", "id": 1},
        {"name": "Doma", "id": 2},
        {"name": "Karu", "id": 3},
        {"name": "Keana", "id": 4},
        {"name": "Keffi", "id": 5},
        {"name": "Kokona", "id": 6},
        {"name": "Lafia", "id": 7},
        {"name": "Nasarawa", "id": 8},
        {"name": "Nasarawa Egon", "id": 9},
        {"name": "Obi", "id": 10},
        {"name": "Toto", "id": 11},
        {"name": "Wamba", "id": 12}
      ]
    }
  },
  {
    "state": {
      "name": "Niger State",
      "id": 27,
      "locals": [
        {"name": "Agwara", "id": 1},
        {"name": "Bida", "id": 2},
        {"name": "Borgu", "id": 3},
        {"name": "Bosso", "id": 4},
        {"name": "Chanchaga", "id": 5},
        {"name": "Edati", "id": 6},
        {"name": "Gbako", "id": 7},
        {"name": "Gurara", "id": 8},
        {"name": "Katcha", "id": 9},
        {"name": "Kontagora", "id": 10},
        {"name": "Lapai", "id": 11},
        {"name": "Lavun", "id": 12},
        {"name": "Magama", "id": 13},
        {"name": "Mariga", "id": 14},
        {"name": "Mashegu", "id": 15},
        {"name": "Mokwa", "id": 16},
        {"name": "Moya", "id": 17},
        {"name": "Paikoro", "id": 18},
        {"name": "Rafi", "id": 19},
        {"name": "Rijau", "id": 20},
        {"name": "Shiroro", "id": 21},
        {"name": "Suleja", "id": 22},
        {"name": "Tafa", "id": 23},
        {"name": "Wushishi", "id": 24}
      ]
    }
  },
  {
    "state": {
      "name": "Ogun State",
      "id": 28,
      "locals": [
        {"name": "Abeokuta South", "id": 1},
        {"name": "Ado-Odo\/Ota", "id": 2},
        {"name": "Egbado North", "id": 3},
        {"name": "Egbado South", "id": 4},
        {"name": "Ewekoro", "id": 5},
        {"name": "Ifo", "id": 6},
        {"name": "Ijebu East", "id": 7},
        {"name": "Ijebu North", "id": 8},
        {"name": "Ijebu North East", "id": 9},
        {"name": "Ijebu Ode", "id": 10},
        {"name": "Ikenne", "id": 11},
        {"name": "Imeko Afon", "id": 12},
        {"name": "Ipokia", "id": 13},
        {"name": "Obafemi Owode", "id": 14},
        {"name": "Odeda", "id": 15},
        {"name": "Odogbolu", "id": 16},
        {"name": "Ogun Waterside", "id": 17},
        {"name": "Remo North", "id": 18},
        {"name": "Shagamu", "id": 19}
      ]
    }
  },
  {
    "state": {
      "name": "Ondo State",
      "id": 29,
      "locals": [
        {"name": "Akoko North-West", "id": 1},
        {"name": "Akoko South-West", "id": 2},
        {"name": "Akoko South-East", "id": 3},
        {"name": "Akure North", "id": 4},
        {"name": "Akure South", "id": 5},
        {"name": "Ese Odo", "id": 6},
        {"name": "Idanre", "id": 7},
        {"name": "Ifedore", "id": 8},
        {"name": "Ilaje", "id": 9},
        {"name": "Ile Oluji\/Okeigbo", "id": 10},
        {"name": "Irele", "id": 11},
        {"name": "Odigbo", "id": 12},
        {"name": "Okitipupa", "id": 13},
        {"name": "Ondo East", "id": 14},
        {"name": "Ondo West", "id": 15},
        {"name": "Ose", "id": 16},
        {"name": "Owo", "id": 17}
      ]
    }
  },
  {
    "state": {
      "name": "Osun State",
      "id": 30,
      "locals": [
        {"name": "Atakunmosa West", "id": 1},
        {"name": "Aiyedaade", "id": 2},
        {"name": "Aiyedire", "id": 3},
        {"name": "Boluwaduro", "id": 4},
        {"name": "Boripe", "id": 5},
        {"name": "Ede North", "id": 6},
        {"name": "Ede South", "id": 7},
        {"name": "Ife Central", "id": 8},
        {"name": "Ife East", "id": 9},
        {"name": "Ife North", "id": 10},
        {"name": "Ife South", "id": 11},
        {"name": "Egbedore", "id": 12},
        {"name": "Ejigbo", "id": 13},
        {"name": "Ifedayo", "id": 14},
        {"name": "Ifelodun", "id": 15},
        {"name": "Ila", "id": 16},
        {"name": "Ilesa East", "id": 17},
        {"name": "Ilesa West", "id": 18},
        {"name": "Irepodun", "id": 19},
        {"name": "Irewole", "id": 20},
        {"name": "Isokan", "id": 21},
        {"name": "Iwo", "id": 22},
        {"name": "Obokun", "id": 23},
        {"name": "Odo Otin", "id": 24},
        {"name": "Ola Oluwa", "id": 25},
        {"name": "Olorunda", "id": 26},
        {"name": "Oriade", "id": 27},
        {"name": "Orolu", "id": 28},
        {"name": "Osogbo", "id": 29}
      ]
    }
  },
  {
    "state": {
      "name": "Oyo State",
      "id": 31,
      "locals": [
        {"name": "Akinyele", "id": 1},
        {"name": "Atiba", "id": 2},
        {"name": "Atisbo", "id": 3},
        {"name": "Egbeda", "id": 4},
        {"name": "Ibadan North", "id": 5},
        {"name": "Ibadan North-East", "id": 6},
        {"name": "Ibadan North-West", "id": 7},
        {"name": "Ibadan South-East", "id": 8},
        {"name": "Ibadan South-West", "id": 9},
        {"name": "Ibarapa Central", "id": 10},
        {"name": "Ibarapa East", "id": 11},
        {"name": "Ibarapa North", "id": 12},
        {"name": "Ido", "id": 13},
        {"name": "Irepo", "id": 14},
        {"name": "Iseyin", "id": 15},
        {"name": "Itesiwaju", "id": 16},
        {"name": "Iwajowa", "id": 17},
        {"name": "Kajola", "id": 18},
        {"name": "Lagelu", "id": 19},
        {"name": "Ogbomosho North", "id": 20},
        {"name": "Ogbomosho South", "id": 21},
        {"name": "Ogo Oluwa", "id": 22},
        {"name": "Olorunsogo", "id": 23},
        {"name": "Oluyole", "id": 24},
        {"name": "Ona Ara", "id": 25},
        {"name": "Orelope", "id": 26},
        {"name": "Ori Ire", "id": 27},
        {"name": "Oyo", "id": 28},
        {"name": "Oyo East", "id": 29},
        {"name": "Saki East", "id": 30},
        {"name": "Saki West", "id": 31},
        {"name": "Surulere", "id": 32}
      ]
    }
  },
  {
    "state": {
      "name": "Plateau State",
      "id": 32,
      "locals": [
        {"name": "Barkin Ladi", "id": 1},
        {"name": "Bassa", "id": 2},
        {"name": "Jos East", "id": 3},
        {"name": "Jos North", "id": 4},
        {"name": "Jos South", "id": 5},
        {"name": "Kanam", "id": 6},
        {"name": "Kanke", "id": 7},
        {"name": "Langtang South", "id": 8},
        {"name": "Langtang North", "id": 9},
        {"name": "Mangu", "id": 10},
        {"name": "Mikang", "id": 11},
        {"name": "Pankshin", "id": 12},
        {"name": "Qua'an Pan", "id": 13},
        {"name": "Riyom", "id": 14},
        {"name": "Shendam", "id": 15},
        {"name": "Wase", "id": 16}
      ]
    }
  },
  {
    "state": {
      "name": "Rivers State",
      "id": 33,
      "locals": [
        {"name": "Ahoada East", "id": 1},
        {"name": "Ahoada West", "id": 2},
        {"name": "Akuku-Toru", "id": 3},
        {"name": "Andoni", "id": 4},
        {"name": "Asari-Toru", "id": 5},
        {"name": "Bonny", "id": 6},
        {"name": "Degema", "id": 7},
        {"name": "Eleme", "id": 8},
        {"name": "Emuoha", "id": 9},
        {"name": "Etche", "id": 10},
        {"name": "Gokana", "id": 11},
        {"name": "Ikwerre", "id": 12},
        {"name": "Khana", "id": 13},
        {"name": "Obio\/Akpor", "id": 14},
        {"name": "Ogba\/Egbema\/Ndoni", "id": 15},
        {"name": "Ogu\/Bolo", "id": 16},
        {"name": "Okrika", "id": 17},
        {"name": "Omuma", "id": 18},
        {"name": "Opobo\/Nkoro", "id": 19},
        {"name": "Oyigbo", "id": 20},
        {"name": "Port Harcourt", "id": 21},
        {"name": "Tai", "id": 22}
      ]
    }
  },
  {
    "state": {
      "name": "Sokoto State",
      "id": 34,
      "locals": [
        {"name": "Bodinga", "id": 1},
        {"name": "Dange Shuni", "id": 2},
        {"name": "Gada", "id": 3},
        {"name": "Goronyo", "id": 4},
        {"name": "Gudu", "id": 5},
        {"name": "Gwadabawa", "id": 6},
        {"name": "Illela", "id": 7},
        {"name": "Isa", "id": 8},
        {"name": "Kebbe", "id": 9},
        {"name": "Kware", "id": 10},
        {"name": "Rabah", "id": 11},
        {"name": "Sabon Birni", "id": 12},
        {"name": "Shagari", "id": 13},
        {"name": "Silame", "id": 14},
        {"name": "Sokoto North", "id": 15},
        {"name": "Sokoto South", "id": 16},
        {"name": "Tambuwal", "id": 17},
        {"name": "Tangaza", "id": 18},
        {"name": "Tureta", "id": 19},
        {"name": "Wamako", "id": 20},
        {"name": "Wurno", "id": 21},
        {"name": "Yabo", "id": 22}
      ]
    }
  },
  {
    "state": {
      "name": "Taraba State",
      "id": 35,
      "locals": [
        {"name": "Bali", "id": 1},
        {"name": "Donga", "id": 2},
        {"name": "Gashaka", "id": 3},
        {"name": "Gassol", "id": 4},
        {"name": "Ibi", "id": 5},
        {"name": "Jalingo", "id": 6},
        {"name": "Karim Lamido", "id": 7},
        {"name": "Kumi", "id": 8},
        {"name": "Lau", "id": 9},
        {"name": "Sardauna", "id": 10},
        {"name": "Takum", "id": 11},
        {"name": "Ussa", "id": 12},
        {"name": "Wukari", "id": 13},
        {"name": "Yorro", "id": 14},
        {"name": "Zing", "id": 15}
      ]
    }
  },
  {
    "state": {
      "name": "Yobe State",
      "id": 36,
      "locals": [
        {"name": "Bursari", "id": 1},
        {"name": "Damaturu", "id": 2},
        {"name": "Fika", "id": 3},
        {"name": "Fune", "id": 4},
        {"name": "Geidam", "id": 5},
        {"name": "Gujba", "id": 6},
        {"name": "Gulani", "id": 7},
        {"name": "Jakusko", "id": 8},
        {"name": "Karasuwa", "id": 9},
        {"name": "Machina", "id": 10},
        {"name": "Nangere", "id": 11},
        {"name": "Nguru", "id": 12},
        {"name": "Potiskum", "id": 13},
        {"name": "Tarmuwa", "id": 14},
        {"name": "Yunusari", "id": 15},
        {"name": "Yusufari", "id": 16}
      ]
    }
  },
  {
    "state": {
      "name": "Zamfara State",
      "id": 37,
      "locals": [
        {"name": "Bakura", "id": 1},
        {"name": "Birnin Magaji\/Kiyaw", "id": 2},
        {"name": "Bukkuyum", "id": 3},
        {"name": "Bungudu", "id": 4},
        {"name": "Gummi", "id": 5},
        {"name": "Gusau", "id": 6},
        {"name": "Kaura Namoda", "id": 7},
        {"name": "Maradun", "id": 8},
        {"name": "Maru", "id": 9},
        {"name": "Shinkafi", "id": 10},
        {"name": "Talata Mafara", "id": 11},
        {"name": "Chafe", "id": 12},
        {"name": "Zurmi", "id": 13}
      ]
    }
  }
];
//code for auto creating states...
/*for(Map map in states){
      String stateName = map["state"]["name"];
//      Map<String, Map<String, Object>> map = states[allItems.indexOf(selectedState)];
      Map<String, Object> placeInfo = map['state'];
      List<Map<String, Object>> locals = placeInfo['locals'];
      List localNames = List.generate(locals.length, (p) => locals[p]['name']);

      BaseModel model = BaseModel();
      model.put(COUNTRY, "Nigeria");
      model.put(STATE, stateName);
      model.put(CITY, localNames);
      model.saveItem(COUNTRY_BASE, false,document: "${"Nigeria"}$stateName");
    }*/

bool isAdmin = false;
String currentProgress = "";
//BaseModel currentUser = new BaseModel();
String currentCountry = "Nigeria";
String defaultCountry = "Nigeria";
String defaultState = "";
//String defaultCity = "";
//String FILTER_CATEGORY = "";
//bool FILTER_SPLITTABLE = false;
//int FILTER_SORT_TYPE = 0;
int FILTER_MIN_AGE = 0;
int FILTER_MAX_AGE = 0;
String FILTER_GENDER = "";

List topCategories = ["Livestock", "Crops", "Others"];

//const List STATUS_KEYS = ["Pending Approval","Active","Paused","Stopped","Declined"];
const List STATUS_KEYS = ["Pending", "Active", "Paused", "Stopped", "Declined"];

const List STATUS_TEXT = [
  "Your farm produce that are currently under review are shown here. Approval usually takes less than 24 hours",
  "Your farm produce that have been approved and are active are shown here",
  "Your farm produce that are currently not active are shown here. You can reactivate them anytime",
  "If there is any issue with your farm, your farm produce automatically stops showing. They will reactivate when the issues are resolved",
  "Your farm produce that have been disapproved are shown here. Click on them and find out why they were rejected",
];

const List STATUS_ICONS = [
  Icons.access_time,
  Icons.check_circle_outline,
  Icons.pause_circle_outline,
  Icons.not_interested,
  Icons.cancel
];

const List STATUS_COLORS = [light_green3, blue0, brown01, red6, red0];

List ghanaRegions = [
  {
    "state": "Northern Region",
    "regions": [
      "Gushegu",
      "Karaga",
      "Kpandai",
      "Kumbungu",
      "Mion",
      "Nanton",
      "Nanumba North",
      "Nanumba South",
      "Saboba",
      "Sagnarigu",
      "Savelugu",
      "Tamale Metropolitan",
      "Tatale Sangule",
      "Tolon",
      "Yendi Municipal District",
      "	Zabzugu",
    ]
  },
  {
    "state": "Ashanti Region",
    "regions": [
      "Adansi North",
      "Adansi South",
      "Afigya-Kwabre",
      "Ahafo Ano North",
      "Ahafo Ano South",
      "Amansie Central",
      "Amansie West",
      "Asante Akim Central Municipal",
      "Asante Akim North",
      "Asante Akim South",
      "Asokore Mampong Municipal",
      "Atwima Kwanwoma",
      "Atwima Mponua",
      "Atwima Nwabiagya",
      "Bekwai Municipal",
      "Bosome Freho",
      "Bosomtwe",
      "Ejisu-Juaben Municipal",
      "Ejura/Sekyedumase",
      "Kumasi Metropolitan",
      "Kwabre",
      "Mampong Municipal",
      "Obuasi Municipal",
      "Offinso Municipal",
      "Offinso North",
      "Sekyere Afram Plains",
      "Sekyere Central",
      "Sekyere East",
      "Sekyere Kumawu",
      "Sekyere South",
    ]
  },
  {
    "state": "Western Region",
    "regions": [
      "Ahanta West",
      "Aowin",
      "Bia East",
      "Bia West",
      "Bodi",
      "Ellembelle",
      "Jomoro",
      "Juabeso",
      "Mpohor",
      "Nzema East",
      "Prestea-Huni Valley",
      "Sefwi Akontombra",
      "Bibiani-Anhwiaso-Bekwai",
      "Sefwi-Wiawso",
      "Sekondi-Takoradi Metropolis(Metropolitan)",
      "Effia-Kwesimintsim",
      "Shama",
      "Suaman",
      "Tarkwa-Nsuaem",
      "Wassa Amenfi Central",
      "Wassa Amenfi East",
      "Wassa Amenfi West",
      "Wassa East",
    ]
  },
  {
    "state": "Volta Region",
    "regions": [
      "Adaklu District",
      "Afadjato South District",
      "Agotime Ziope District",
      "Akatsi North District",
      "Akatsi South District",
      "Anloga District",
      "Central Tongu District",
      "Ho Municipal",
      "Ho West District",
      "Hohoe Municipal",
      "Keta Municipal",
      "Ketu North Municipal District",
      "Ketu South Municipal",
      "Kpando Municipal District",
      "North Dayi District",
      "North Tongu District",
      "South Dayi District",
      "South Tongu District",
    ]
  },
  {
    "state": "Bono region",
    "regions": [
      "Banda",
      "Berekum East Municipal",
      "Berekum West",
      "Dormaa Central Municipal",
      "Dormaa East",
      "Dormaa West",
      "Jaman North",
      "Jaman South Municipal",
      "Sunyani Municipal",
      "Sunyani West",
      "Tain",
      "Wenchi Municipal",
    ]
  },
  {
    "state": "Bono East region",
    "regions": [
      "Atebubu-Amanten Municipal	",
      "Kintampo North Municipal",
      "Kintampo South District",
      "Nkoranza North District",
      "Nkoranza South Municipal",
      "Pru East District",
      "Pru West District",
      "Sene East District",
      "Sene West District",
      "Techiman Municipal",
      "Techiman North District",
    ]
  },
  {
    "state": "Ahafo Region",
    "regions": [
      "Asunafo North Municipal",
      "Asunafo South District",
      "Asutifi North District",
      "Asutifi South District",
      "Tano North Municipal",
      "Tano South Municipal",
    ]
  },
  {
    "state": "Central Region",
    "regions": [
      "Abura/Asebu/Kwamankese",
      "Agona East",
      "Agona West Municipal",
      "Ajumako/Enyan/Essiam",
      "Asikuma/Odoben/Brakwa",
      "Assin North Municipal",
      "Assin South",
      "Awutu Senya East Municipal",
      "Awutu Senya West",
      "Cape Coast Metropolitan",
      "Effutu Municipal",
      "Ekumfi",
      "Gomoa East",
      "Gomoa West",
      "Komenda/Edina/Eguafo/Abirem Municipal",
      "Mfantsiman Municipal",
      "Twifo-Ati Morkwa",
      "Twifo/Heman/Lower Denkyira",
      "Upper Denkyira East Municipal",
      "Upper Denkyira West",
    ]
  },
  {
    "state": "Eastern Region",
    "regions": [
      "Akuapim North Municipal",
      "Akuapim South",
      "Akyemansa",
      "Asuogyaman",
      "Atiwa",
      "Ayensuano",
      "Birim Central Municipal",
      "Birim North",
      "Birim South",
      "Denkyembour",
      "East Akim Municipal",
      "Fanteakwa",
      "Kwaebibirem",
      "Kwahu Afram Plains North",
      "Kwahu Afram Plains South",
      "Kwahu East",
      "Kwahu South",
      "Kwahu West Municipal",
      "Lower Manya Krobo",
      "New-Juaben Municipal",
      "Nsawam Adoagyire Municipal",
      "Suhum Municipal",
      "Upper Manya Krobo",
      "Upper West Akim",
      "West Akim Municipal",
      "Yilo Krobo",
    ]
  },
  {
    "state": "Greater Accra Region",
    "regions": [
      "Accra Metropolitan",
      "Ada East",
      "Ada West",
      "Adentan Municipal",
      "Ashaiman Municipal",
      "Ga Central Municipal",
      "Ga East Municipal",
      "Ga South Municipal",
      "Ga West Municipal",
      "Kpone Katamanso",
      "La Dade Kotopon Municipal",
      "La Nkwantanang Madina Municipal",
      "Ledzokuku-Krowor Municipal",
      "Ningo Prampram",
      "Shai Osudoku",
      "Tema Metropolitan",
    ]
  },
  {
    "state": "Savannah Region",
    "regions": [
      "Bole",
      "Central Gonja",
      "North Gonja",
      "East Gonja",
      "North East Gonja",
      "West Gonja",
      "Sawla-Tuna-Kalba",
    ]
  },
  {
    "state": "North East Region",
    "regions": [
      "Bunkpurugu-Nyakpanduri",
      "Chereponi",
      "East Mamprusi",
      "Mamprugu Moagduri",
      "West Mamprusi",
      "Yunyoo-Nasuan",
    ]
  },
  {
    "state": "Upper East Region",
    "regions": [
      "Bawku Municipal",
      "Bawku West",
      "Binduri",
      "Bolgatanga Municipal",
      "Bongo",
      "Builsa North",
      "Builsa South",
      "Garu",
      "Kassena Nankana East",
      "Kassena Nankana West",
      "Nabdam",
      "Pusiga",
      "Talensi",
      "Bolgatanga East District",
      "Tempane District",
    ]
  },
  {
    "state": "Upper West Region",
    "regions": [
      "Daffiama Bussie Issa",
      "Jirapa/Lambussie",
      "Lambussie Karni",
      "Lawra",
      "Nadowli",
      "Nandom",
      "Sissala East",
      "Sissala West",
      "Wa East",
      "Wa Municipal",
      "Wa West",
    ]
  },
  {
    "state": "Oti Region",
    "regions": [
      "Biakoye District",
      "Jasikan District",
      "Kadjebi District",
      "Santrokofi, Akpafu, Lolobi and Likpe",
      "Krachi East Municipal",
      "Krachi Nchumuru District",
      "Krachi West District",
      "Nkwanta North District",
      "Nkwanta South Municipal",
    ]
  },
  {
    "state": "Western North Region",
    "regions": [
      "Aowin",
      "Bia East",
      "Bia West",
      "Bodi",
      "Juabeso",
      "Sefwi Akontombra",
      "Sefwi Bibiani-Anhwiaso-Bekwai",
      "Sefwi-Wiawso",
      "Suaman",
    ]
  },
];

List cardColors = [
  [blue0, blue3],
  [pink0, pink3],
  [brown0, brown3],
  [dark_green0, dark_green3],
  [azure_blue0, azure_blue3],
  [orange0, orange3],
  [light_green3, light_green5],
  [red0, red3],
  [m_blue0, m_blue3],
];

var currencies = [
  "btg",
  "xmr",
  "btc",
  "zec",
  "ada",
  "ltc",
  "bch",
  "dash",
  "xlm",
  "xrp",
  "dgb",
  "trx",
  "kmd",
  "bat",
  "eos",
  "waves",
  "bnb",
  "nano",
  "tusd",
  "zen",
  "grs",
  "fun",
  "gas",
  "pax",
  "usdc",
  "rvn",
  "bnbmainnet",
  "bcd",
  "usdterc20",
  "cro",
  "wabi",
  "busd",
  "stpt",
  "eth"
];

printToConsole(s) {
  // print(s);
}
