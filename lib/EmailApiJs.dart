// import 'dart:convert';
// import 'dart:io';
//
// import 'package:http/http.dart' as http;
//
// class EmailApiJs {
//   static String baseUrl = "https://api.emailjs.com/api/v1.0/email/send";
//   static String serviceId = "service_houf554";
//   static String templateAdmin = "template_33j5m3m";
//   static String templateIdOrder = "template_6edzae1";
//   static String userId = "user_cE6CtUNFiamuOXmCEqg76";
//
//   static sendEmail(
//       {bool isAdmin = false,
//       String toName,
//       String toEmail,
//       String message}) async {
//     var data = {
//       "service_id": serviceId,
//       "template_id": isAdmin ? templateAdmin : templateIdOrder,
//       "user_id": userId,
//       "template_params": {
//         "to_email": toEmail,
//         "to_name": toName,
//         "message": message
//       }
//     };
//
//     // ContentType.json;
//     // HeaderValue.parse(value)
//     var response = await http.post(Uri.parse(baseUrl),
//         headers: {HttpHeaders.contentTypeHeader: 'application/json'},
//         body: jsonEncode(data));
//
//     print(response.body);
//     return response.body;
//   }
// }
