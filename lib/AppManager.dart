import 'package:Loopin/AppEngine.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'assets.dart';

class AppManager extends StatefulWidget {
  @override
  _AppManagerstate createState() => _AppManagerstate();
}

class _AppManagerstate extends State<AppManager> {
  TextEditingController shareTextController = TextEditingController();
  TextEditingController downloadLinksController = TextEditingController();
  TextEditingController playStoreLinkController = TextEditingController();
  TextEditingController appleStoreLinkController = TextEditingController();
  TextEditingController webLinkController = TextEditingController();
  TextEditingController aboutLinkController = TextEditingController();
  TextEditingController privacyLinkController = TextEditingController();
  TextEditingController termsLinkController = TextEditingController();
  TextEditingController supportEmailController = TextEditingController();
  TextEditingController supportPhoneController = TextEditingController();
  TextEditingController warningMessagePostController = TextEditingController();
  TextEditingController warningMessageProfileController =
      TextEditingController();
  TextEditingController androidVersionController = TextEditingController();
  TextEditingController androidVersionTextController = TextEditingController();
  TextEditingController appleVersionController = TextEditingController();
  TextEditingController appleVersionTextController = TextEditingController();
  TextEditingController latestFeatureController = TextEditingController();
  TextEditingController postReportController = TextEditingController();
  TextEditingController disapproveController = TextEditingController();
  TextEditingController userReportController = TextEditingController();
  TextEditingController raveController = TextEditingController();
  TextEditingController facebookController = TextEditingController();
  TextEditingController instaController = TextEditingController();
  TextEditingController twitterController = TextEditingController();
  TextEditingController moodsController = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    shareTextController.text = appSettingsModel.getString(SHARE_TEXT);
    downloadLinksController.text = appSettingsModel.getString(DOWNLOAD_LINKS);
    playStoreLinkController.text = appSettingsModel.getString(PLAYSTORE_LINK);
    appleStoreLinkController.text = appSettingsModel.getString(APPLESTORE_LINK);
    webLinkController.text = appSettingsModel.getString(WEB_LINK);
    aboutLinkController.text = appSettingsModel.getString(ABOUT_LINK);
    privacyLinkController.text = appSettingsModel.getString(PRIVACY_LINK);
    termsLinkController.text = appSettingsModel.getString(TERMS_LINK);
    supportEmailController.text = appSettingsModel.getString(SUPPORT_EMAIL);
    supportPhoneController.text = appSettingsModel.getString(SUPPORT_PHONE);
    warningMessagePostController.text =
        appSettingsModel.getString(WARNING_MESSAGE_POST);
    warningMessageProfileController.text =
        appSettingsModel.getString(WARNING_MESSAGE_PROFILE);
    androidVersionController.text =
        appSettingsModel.getInt(ANDROID_VERSION).toString();
    androidVersionTextController.text =
        appSettingsModel.getString(ANDROID_VERSION_TEXT);
    appleVersionController.text =
        appSettingsModel.getInt(APPLE_VERSION).toString();
    appleVersionTextController.text =
        appSettingsModel.getString(APPLE_VERSION_TEXT);
    latestFeatureController.text = appSettingsModel.getString(NEW_FEATURE);
    postReportController.text = appSettingsModel.getString(POST_REPORT_SAMPLES);
    userReportController.text = appSettingsModel.getString(USER_REPORT_SAMPLES);
    disapproveController.text = appSettingsModel.getString(DISAPPROVE_SAMPLES);
    raveController.text = appSettingsModel.getString(RAVE_PUB_KEY);
    facebookController.text = appSettingsModel.getString(FACEBOOK_LINK);
    instaController.text = appSettingsModel.getString(INSTA_LINK);
    twitterController.text = appSettingsModel.getString(TWITTER_LINK);
    moodsController.text = appSettingsModel.getString(MOOD_TYPES);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: page(),
      backgroundColor: getWhiteColor(),
    );
  }

  page() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        addSpace(50),
        Row(
          children: <Widget>[
            InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  width: 50,
                  height: 50,
                  child: Center(
                      child: Icon(
                    Icons.keyboard_backspace,
                    color: getBlackColor(),
                    size: 20,
                  )),
                )),
            Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: Text(
                  "App Manager",
                  style: textStyle(true, 25, getBlackColor()),
                )),
            addSpaceWidth(10),
            Container(
              height: 40,
              child: FlatButton(
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
//                                padding: EdgeInsets.all(0),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25),
                      side: BorderSide(color: red0, width: 2)),
                  color: transparent,
                  onPressed: () {
                    save();
                  },
                  child: Text(
                    "Update",
                    style: textStyle(true, 16, pink0),
                  )),
            ),
            addSpaceWidth(10),
          ],
        ),
        addSpace(10),
        Expanded(
            child: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              inputTextView(
                  "Warning Message Post", warningMessagePostController,
                  isNum: false, maxLine: 3),
              inputTextView(
                  "Warning Message Profile", warningMessageProfileController,
                  isNum: false, maxLine: 3),
              inputTextView(
                  "Download Links (Separator (,))", downloadLinksController,
                  isNum: false),
              inputTextView("Share Text", shareTextController, isNum: false),
              inputTextView("PlayStore Link", playStoreLinkController,
                  isNum: false),
              inputTextView("AppleStore Link", appleStoreLinkController,
                  isNum: false),
              inputTextView("Web Link", webLinkController, isNum: false),
              inputTextView("About Link", aboutLinkController, isNum: false),
              inputTextView("Terms Link", termsLinkController, isNum: false),
              inputTextView("Privacy Link", privacyLinkController,
                  isNum: false),
              inputTextView("Rave Key (public + encrypt)", raveController,
                  isNum: false),
              inputTextView("Facebook Link", facebookController, isNum: false),
              inputTextView("Insta Link", instaController, isNum: false),
              inputTextView("Twitter Link", twitterController, isNum: false),
              inputTextView("Support Email", supportEmailController,
                  isNum: false),
              inputTextView("Support Phone", supportPhoneController,
                  isNum: false),
              inputTextView("Android Version", androidVersionController,
                  isNum: true),
              inputTextView(
                  "Android Version Text", androidVersionTextController,
                  isNum: false),
              inputTextView("Apple Version", appleVersionController,
                  isNum: true),
              inputTextView("Apple Version Text", appleVersionTextController,
                  isNum: false),
              inputTextView("Latest Features Text", latestFeatureController,
                  isNum: false, maxLine: 3),
              inputTextView("Post Reports (Separator (,)", postReportController,
                  isNum: false, maxLine: 3),
              inputTextView("User Reports (Separator (,)", userReportController,
                  isNum: false, maxLine: 3),
              inputTextView("Moods (Separator (,)", moodsController,
                  isNum: false, maxLine: 3),
              inputTextView(
                  "Disapprove Samples (Separator (+)", disapproveController,
                  isNum: false, maxLine: 3),
            ],
          ),
        ))
      ],
    );
  }

  save() {
    appSettingsModel.put(SHARE_TEXT, shareTextController.text.trim());
    appSettingsModel.put(DOWNLOAD_LINKS, downloadLinksController.text.trim());
    appSettingsModel.put(PLAYSTORE_LINK, playStoreLinkController.text.trim());
    appSettingsModel.put(APPLESTORE_LINK, appleStoreLinkController.text.trim());
    appSettingsModel.put(WEB_LINK, webLinkController.text.trim());
    appSettingsModel.put(ABOUT_LINK, aboutLinkController.text.trim());
    appSettingsModel.put(PRIVACY_LINK, privacyLinkController.text.trim());
    appSettingsModel.put(TERMS_LINK, termsLinkController.text.trim());
    appSettingsModel.put(SUPPORT_EMAIL, supportEmailController.text.trim());
    appSettingsModel.put(SUPPORT_PHONE, supportPhoneController.text.trim());
    appSettingsModel.put(
        WARNING_MESSAGE_POST, warningMessagePostController.text.trim());
    appSettingsModel.put(
        WARNING_MESSAGE_PROFILE, warningMessageProfileController.text.trim());
    appSettingsModel.put(
        ANDROID_VERSION,
        androidVersionController.text.isEmpty
            ? 0
            : int.parse(androidVersionController.text.trim()));
    appSettingsModel.put(
        ANDROID_VERSION_TEXT, androidVersionTextController.text.trim());
    appSettingsModel.put(
        APPLE_VERSION,
        appleVersionController.text.isEmpty
            ? 0
            : int.parse(appleVersionController.text.trim()));
    appSettingsModel.put(
        APPLE_VERSION_TEXT, appleVersionTextController.text.trim());
    appSettingsModel.put(NEW_FEATURE, latestFeatureController.text.trim());
    appSettingsModel.put(POST_REPORT_SAMPLES, postReportController.text.trim());
    appSettingsModel.put(DISAPPROVE_SAMPLES, disapproveController.text.trim());
    appSettingsModel.put(USER_REPORT_SAMPLES, userReportController.text.trim());
    appSettingsModel.put(MOOD_TYPES, moodsController.text.trim());
    appSettingsModel.put(RAVE_PUB_KEY, raveController.text.trim());
    appSettingsModel.put(FACEBOOK_LINK, facebookController.text.trim());
    appSettingsModel.put(INSTA_LINK, instaController.text.trim());
    appSettingsModel.put(TWITTER_LINK, twitterController.text.trim());
    appSettingsModel.updateItems(context: context);
    Navigator.pop(context);
  }
}
