import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/assets.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class Services extends StatefulWidget {
  @override
  _ServicesState createState() => _ServicesState();
}

class _ServicesState extends State<Services> {
  bool viewCreated = false;

  String message = "";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadAsset();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      appBar: AppBar(
        backgroundColor: black,
        title: Text(
          "Services",
          style: textStyle(true, 22, white),
        ),
      ),
      body:SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: <Widget>[
              Text(
                message,
                style: textStyle(false, 16, black),
              )
            ],
          ),
        ),
      ),
    );
  }

  void loadAsset() async {
    message = await DefaultAssetBundle.of(context).loadString("assets/services.txt");
    setState(() {});
  }
}
