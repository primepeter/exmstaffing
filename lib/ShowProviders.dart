import 'package:Loopin/AppEngine.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'assets.dart';

class ShowProviders extends StatefulWidget {
  @override
  _ShowProvidersState createState() => _ShowProvidersState();
}

class _ShowProvidersState extends State<ShowProviders> {
  TextEditingController searchController = TextEditingController();
  bool setup = false;
  bool showCancel = false;
  FocusNode focusSearch = FocusNode();
  List allCryptoList = [];
  List categoryList = [];

  ScrollController downScrollController = ScrollController();

  Map selectedItem;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //bitcoin.
    focusSearch.addListener(() {
      setState(() {});
    });
    allCryptoList = List.from(appSettingsModel.getList(PROVIDER_LIST));
    reload();
  }

  reload() {
    String search = searchController.text.trim().toLowerCase();
    categoryList.clear();
    for (Map item in allCryptoList) {
      String text = item[ITEM_NAME];
      if (search.isNotEmpty) {
        bool exist = text.toLowerCase().trim().contains(search);
        if (!exist) {
          continue;
        }
      }

      categoryList.add(item);
    }

    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: page(),
        backgroundColor: white,
      ),
    );
  }

  ScrollController mainScroll = ScrollController();

  page() {
    return pageBuilder(context, pageChild(), (DragUpdateDetails dragUpdate) {
      mainScroll.position.moveTo(dragUpdate.globalPosition.dy * 3.5);
      setState(() {});
    });
  }

  pageChild() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        addSpace(10),
        Row(
          children: <Widget>[
            InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  width: 50,
                  height: 50,
                  child: Center(
                      child: Icon(
                    Icons.arrow_back_ios,
                    color: black,
                    size: 18,
                  )),
                )),
            Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: Text(
                "Choose Provider",
                style: textStyle(true, 18, black),
              ),
            ),
            addSpaceWidth(30),
          ],
        ),
        Container(
          height: 45,
          margin: EdgeInsets.fromLTRB(30, 0, 30, 10),
          decoration: BoxDecoration(
              color: white.withOpacity(.8),
              borderRadius: BorderRadius.circular(10),
              border: Border.all(
                  color: focusSearch.hasFocus
                      ? light_green3
                      : black.withOpacity(.1),
                  width: focusSearch.hasFocus ? 2 : 1)),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            //mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              addSpaceWidth(10),
              Icon(
                Icons.search,
                color: black.withOpacity(.5),
                size: 17,
              ),
              addSpaceWidth(10),
              new Flexible(
                flex: 1,
                child: new TextField(
                  textInputAction: TextInputAction.search,
                  textCapitalization: TextCapitalization.sentences,
                  autofocus: false,
                  onSubmitted: (_) {
                    //reload();
                  },
                  decoration: InputDecoration(
                      hintText: "Search",
                      hintStyle: textStyle(
                        false,
                        18,
                        black.withOpacity(.5),
                      ),
                      border: InputBorder.none,
                      isDense: true),
                  style: textStyle(false, 16, black),
                  controller: searchController,
                  cursorColor: black,
                  cursorWidth: 1,
                  focusNode: focusSearch,
                  keyboardType: TextInputType.text,
                  onChanged: (s) {
                    showCancel = s.trim().isNotEmpty;
                    setState(() {});
                    reload();
                  },
                ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    focusSearch.unfocus();
                    showCancel = false;
                    searchController.text = "";
                  });
                  reload();
                },
                child: showCancel
                    ? Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                        child: Icon(
                          Icons.close,
                          color: black,
                          size: 20,
                        ),
                      )
                    : new Container(),
              )
            ],
          ),
        ),
        Expanded(
          child: Builder(
            builder: (c) {
              return ListView.builder(
                controller: mainScroll,
                itemBuilder: (c, p) {
                  Map item = categoryList[p];
                  String name = item[ITEM_NAME];
                  String currenyCode = item[ITEM_ID];
                  String icon = item[ICON];

                  return GestureDetector(
                    onTap: () async {
                      FocusScope.of(context).requestFocus();
                      Navigator.pop(context, item);
                    },
                    child: Column(
                      children: [
                        Container(
                          color: transparent,
                          padding:
                              EdgeInsets.fromLTRB(20, p == 0 ? 10 : 20, 20, 20),
                          child: Row(
                            children: <Widget>[
                              addSpaceWidth(5),
                              Container(
                                  padding: EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      // shape: BoxShape.circle,
                                      borderRadius: BorderRadius.circular(10),
                                      border:
                                          Border.all(color: gold, width: 1)),
                                  child: icon.isEmpty
                                      ? Icon(Icons.more_horiz)
                                      : CachedNetworkImage(
                                          imageUrl: icon,
                                          width: 10,
                                          height: 10,
                                        )),
                              addSpaceWidth(10),
                              Flexible(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(
                                      name + "-($currenyCode)",
                                      style: textStyle(true, 16, black),
                                    ),
                                  ],
                                ),
                                flex: 1,
                                fit: FlexFit.tight,
                              ),
                              addSpaceWidth(10),

//                 addSpaceWidth(10)
                            ],
                          ),
                        ),
                        addLine(.5, black.withOpacity(.1), 80, 0, 20, 0)
                      ],
                    ),
                  );
                },
                physics: BouncingScrollPhysics(),
                padding: EdgeInsets.all(0),
                itemCount: categoryList.length,
              );
            },
          ),
        ),
      ],
    );
  }
}
