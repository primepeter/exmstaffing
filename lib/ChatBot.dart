import 'dart:convert';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/assets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:cached_network_image/cached_network_image.dart' as cac;
import 'package:shimmer/shimmer.dart';

class Message {
  String sender;
  String message;
  String recipientId;
  String text;
  String image;
  bool myItem;
  int timestamp;

  Message(
      {this.sender,
      this.message,
      this.recipientId,
      this.text,
      this.image,
      this.timestamp,
      this.myItem = false});

  Message.json(
    Map json,
  ) {
    this.sender = json["sender"];
    this.message = json["message"];
    this.recipientId = json["recipient_id"] ?? "";
    this.text = json["text"] ?? "";
    this.image = json["image"] ?? "";
    this.myItem = json["myItem"] ?? false;
    this.timestamp = json["timestamp"] ?? DateTime.now().millisecondsSinceEpoch;
  }

  Map get toJson => {
        "sender": this.sender,
        "message": this.message,
        "recipient_id": this.recipientId,
        "image": this.image
      };
}

class ChatBot extends StatefulWidget {
  const ChatBot({Key key}) : super(key: key);

  @override
  _ChatBotState createState() => _ChatBotState();
}

class _ChatBotState extends State<ChatBot> {
  String urlEndpoint = "http://178.62.87.99:5005/webhooks/rest/webhook";
  List<Message> messageList = [];
  bool hasMessage = false;
  FocusNode messageNode = FocusNode();
  final messageController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  postMessage() async {
    String text = messageController.text;
    final chat = Message(
        sender: "Lekan",
        message: text,
        myItem: true,
        timestamp: DateTime.now().millisecondsSinceEpoch);
    messageList.insert(0, chat);
    messageController.clear();
    setState(() {});

    final response =
        await http.post(Uri.parse(urlEndpoint), body: jsonEncode(chat.toJson));

    print(response.request.url);
    print(response.body);
    print(chat.toJson);

    if (response.statusCode != 200) return;

    final items = List.from(jsonDecode(response.body))
        .map((e) => Message.json(
              e,
            ))
        .toList();
    if (items.isEmpty) return;

    for (var item in items) {
      messageList.insert(0, item);
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            padding: EdgeInsets.only(top: 100),
            child: Column(
              children: [
                Expanded(
                  child: ListView.builder(
                      itemCount: messageList.length,
                      reverse: true,
                      itemBuilder: (c, p) {
                        final item = messageList[p];
                        bool myItem = item.myItem;
                        String msg = item.message;
                        return Container(
                          margin:
                              EdgeInsets.only(left: 15, right: 15, bottom: 10),
                          child: Column(
                            crossAxisAlignment: myItem
                                ? CrossAxisAlignment.end
                                : CrossAxisAlignment.start,
                            children: [
                              if (myItem)
                                Builder(builder: (c) {
                                  return outgoingChatText(
                                      context, item, p == 0);
                                })
                              else
                                Builder(builder: (c) {
                                  return incomingChatText(context, item);
                                })
                            ],
                          ),
                        );
                      }),
                ),
                Container(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  decoration: BoxDecoration(
                      color: white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      ),
                      boxShadow: [
                        BoxShadow(
                            color: black.withOpacity(.05),
                            spreadRadius: 4,
                            blurRadius: 2)
                      ]),
                  constraints: BoxConstraints(maxHeight: 120, minHeight: 55),
                  child: Row(
                    children: [
                      TextButton(
                        onPressed: () {},
                        child: Icon(Icons.add),
                        style: TextButton.styleFrom(
                            primary: appColor,
                            shape: CircleBorder(),
                            minimumSize: Size(30, 30)),
                      ),
                      Expanded(
                          child: TextField(
                        controller: messageController,
                        onChanged: (String text) {
//                               lastTyped = DateTime.now()
//                                   .millisecondsSinceEpoch;
                          bool empty = text.trim().isEmpty;
                          if (empty && hasMessage) {
                            hasMessage = false;
                            setState(() {});
                          }
                          if (!empty && !hasMessage) {
                            hasMessage = true;
                            setState(() {});
                          }
//                                        checking=false;
                        },
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "Type a message"),
                      )),
                      TextButton(
                        onPressed: () {
                          if (!hasMessage) return;
                          postMessage();
                        },
                        child: Icon(
                          Icons.send,
                          size: 18,
                        ),
                        style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: appColor,
                            shape: CircleBorder(),
                            minimumSize: Size(40, 40)),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            //height: 50,
            decoration: BoxDecoration(
                color: appColor,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(40),
                  bottomRight: Radius.circular(40),
                )),
            padding: EdgeInsets.only(top: 40, right: 20, left: 20, bottom: 15),
            child: Row(
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 30,
                  width: 30,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, border: Border.all(color: white)),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(50),
                    child: Image.asset(
                      ic_launcher,
                    ),
                  ),
                ),
                addSpaceWidth(10),
                Text(
                  "Lekan",
                  style: TextStyle(fontSize: 18, color: white),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  incomingChatText(context, Message chat) {
    String message = chat.text;
    String imageUrl = chat.image ?? "ok";

    return new Stack(
      children: <Widget>[
        new GestureDetector(
          onLongPress: () {},
          child: Container(
              margin: EdgeInsets.fromLTRB(10, 0, 60, 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Card(
                    clipBehavior: Clip.antiAlias,
                    color: default_white,
                    elevation: 5,
                    shadowColor: black.withOpacity(.1),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      topLeft: Radius.circular(25),
                      bottomLeft: Radius.circular(0),
                      bottomRight: Radius.circular(25),
                    )),
                    child: new Container(
                      padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                      decoration: BoxDecoration(
//                    color: white,
                          borderRadius: BorderRadius.circular(10)),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          if (message.isNotEmpty)
                            Text(
                              message,
                              style: textStyle(false, 17, black),
                            ),
                          if (imageUrl.isNotEmpty)
                            Container(
                              margin: EdgeInsets.only(top: 5),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10),
                                child: cac.CachedNetworkImage(
                                  imageUrl: imageUrl,
                                  height: 200,
                                  width: 300,
                                  fit: BoxFit.cover,
                                  placeholder: (c, s) => Shimmer.fromColors(
                                    baseColor: black.withOpacity(.15),
                                    highlightColor: black.withOpacity(.1),
                                    child: Container(
                                      height: 200,
                                      width: 300,
                                      color: white,
                                    ),
                                  ),
                                  errorWidget: (c, s, a) => Shimmer.fromColors(
                                    baseColor: black.withOpacity(.15),
                                    highlightColor: black.withOpacity(.1),
                                    child: Container(
                                      height: 200,
                                      width: 300,
                                      color: white,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          addSpace(3),
                          Text(
                            /*timeAgo.format(
                            DateTime.fromMillisecondsSinceEpoch(
                                chat.getTime()),
                            locale: "en_short")*/
                            getChatTime(chat.timestamp),
                            style: textStyle(false, 12, black.withOpacity(.3)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )),
        ),
        //otherPersonImage(context, chat.getBoolean(IS_ADMIN))
      ],
    );
  }

  outgoingChatText(context, Message chat, bool firstChat) {
    String message = chat.message;

    return new GestureDetector(
      onLongPress: () {},
      child: Container(
        margin: EdgeInsets.fromLTRB(60, 0, 15, 15),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(
                  child: Card(
                    elevation: 5,
                    shadowColor: black.withOpacity(.1),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      topLeft: Radius.circular(25),
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(0),
                    )),
                    child: new Container(
                      margin: const EdgeInsets.fromLTRB(15, 5, 10, 5),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            message,
                            style: textStyle(false, 17, black),
                          ),
//              addSpace(3),
//              Text(
//                getChatTime(chat.getInt(TIME)),
//                style: textStyle(false, 12, black.withOpacity(.3)),
//              ),
                        ],
                      ),
                    ),
                  ),
                ),

                // if (read && firstChat)
                //   Icon(
                //     Icons.remove_red_eye,
                //     size: 12,
                //     color: appColor,
                //   )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
