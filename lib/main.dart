import 'dart:convert';

import 'package:Loopin/forms/ClockInPage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:Loopin/forms/ApplicationForms.dart';
import 'package:Loopin/forms/DepositApplication.dart';
import 'package:flutter/foundation.dart';
import 'package:rainbow_color/rainbow_color.dart';
import 'package:square_percent_indicater/square_percent_indicater.dart';
// import 'package:Loopin/api/Lucro.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:url_strategy/url_strategy.dart';

import 'forms/AddRequest.dart';
import 'forms/ApplyJob.dart';
import 'AppEngine.dart';
import 'dart:async';

import 'package:flutter/widgets.dart';

import 'forms/BBpForm.dart';
import 'forms/CareAgreement.dart';
import 'forms/ClinicalSkillCheck.dart';
import 'forms/CnaAgreement.dart';
import 'forms/CnaSkillCheck.dart';
import 'forms/CompanyPolicy.dart';
import 'forms/CookAgreement.dart';
import 'forms/DinningAgreement.dart';
import 'forms/DisclosureForm.dart';
import 'forms/DocUploads.dart';
import 'forms/EligibilityForm.dart';
import 'forms/EmployeeProfile.dart';
import 'HomePage.dart';
import 'auth/SignUp.dart';
import 'forms/EmploymentApplication.dart';
import 'forms/HireApplication.dart';
import 'forms/InfectionControlTraining.dart';
import 'forms/JobListings.dart';
import 'forms/LinkForms.dart';
import 'forms/LpnApplication.dart';
import 'forms/MedAgreement.dart';
import 'forms/OtherAgreement.dart';
import 'forms/PostJob.dart';
import 'forms/PrnApplication.dart';
import 'forms/ReceptionAgreement.dart';
import 'forms/RnApplication.dart';
import 'forms/TrainingProgram.dart';

//peter@exmstaffing.com
bool webApp = false;
// npm install -g firebase-tools

GoogleSignIn googleSignIn = GoogleSignIn(
  scopes: [
    'email',
//    'https://www.googleapis.com/auth/contacts.readonly',
  ],
);

Map<String, dynamic> routeDataHolder = {};

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: FirebaseOptions(
          apiKey: "AIzaSyCr8Z3a9SLBQuRN91RN9oVvN5vgBLEyTgc",
          authDomain: "exmstaffing.firebaseapp.com",
          projectId: "exmstaffing",
          storageBucket: "exmstaffing.appspot.com",
          messagingSenderId: "60797976644",
          appId: "1:60797976644:web:12d8ef22ccf1d93dfa3c11"));
  await FirebaseAuth.instance.setPersistence(Persistence.LOCAL);
  // if (kIsWeb) setPathUrlStrategy();
  runApp(MyApp());
}

Map<LogicalKeySet, Intent> ignoredNavigationShortcuts() {
  Map<LogicalKeySet, Intent> shortcuts = Map.from(WidgetsApp.defaultShortcuts);
  if (kIsWeb) {
    shortcuts[LogicalKeySet(LogicalKeyboardKey.space)] = ActivateIntent();
    shortcuts[LogicalKeySet(LogicalKeyboardKey.arrowUp)] = ActivateIntent();
    shortcuts[LogicalKeySet(LogicalKeyboardKey.arrowDown)] = ActivateIntent();
    shortcuts[LogicalKeySet(LogicalKeyboardKey.arrowLeft)] = ActivateIntent();
    shortcuts[LogicalKeySet(LogicalKeyboardKey.arrowRight)] = ActivateIntent();
  }
  return shortcuts;
}

class MyApp extends StatelessWidget {
  final Map<String, Widget> routeMap = {
    '/setter': AppSetter(),
    '/home': HomePage(),
    '/signUp': SignUp(),
    '/applyJob': ApplyJob(),
    '/postJob': PostJob(),
    '/jobListings': JobListings(),
    '/empProfile': EmployeeProfile(),
    '/depositApp': DepositApplication(),
    '/employmentApp': EmploymentApplication(),
    '/hireApp': HireApplication(),

    //agreements
    '/lpnAgreement': LpnAgreement(), //done ..
    '/rnAgreement': RnAgreement(), //done
    '/cnaAgreement': CnaAgreement(), //done
    '/careAgreement': CareAgreement(), //done
    '/medAgreement': MedAgreement(), //done
    '/cookAgreement': CookAgreement(), //done
    '/receptionAgreement': ReceptionAgreement(), //done
    '/dinningAgreement': DinningAgreement(), //done
    '/otherAgreement': OtherAgreement(),

    '/bbpForm': BBpForm(),
    '/trainingProgram': TrainingProgram(),
    '/cnaSkillCheck': CnaSkillCheck(),
    '/clinicalSkillCheck': ClinicalSkillCheck(),
    '/eligibilityForm': EligibilityForm(),
    '/applicationForm': ApplicationForms(),
    '/linkForm': LinkForms(),
    '/clockIn': ClockInPage(),
    '/docUploads': DocUploads(),
    '/consentForm': DisclosureForm(),

    '/companyPolicy': CompanyPolicy(),
    '/infectionControl': InfectionControlTraining(),
  };

  @override
  Widget build(BuildContext c) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // shortcuts: ignoredNavigationShortcuts(),
      shortcuts: {
        LogicalKeySet(LogicalKeyboardKey.space): ActivateIntent(),
        LogicalKeySet(LogicalKeyboardKey.arrowUp): ActivateIntent(),
        LogicalKeySet(LogicalKeyboardKey.arrowDown): ActivateIntent(),
        LogicalKeySet(LogicalKeyboardKey.arrowLeft): ActivateIntent(),
        LogicalKeySet(LogicalKeyboardKey.arrowRight): ActivateIntent()
      },
      title: "ExmStaffing",
      color: white,
      theme: ThemeData(
          fontFamily: GoogleFonts.overpass().fontFamily,
          scrollbarTheme: ScrollbarThemeData(
              thickness: MaterialStateProperty.all(15), isAlwaysShown: true),
          primaryColor: appColor,
          textSelectionTheme: TextSelectionThemeData(cursorColor: appColor)),

      // routes: routes,
      initialRoute: "/setter",
      // initialRoute: "/home",
      onGenerateRoute: onGenerateRoute,
      onUnknownRoute: (s) => pageRoute(s, null, true),
    );
  }

  //git remote set-url origin https://mtellect@bitbucket.org/primepeter/exmstaffing.git
  //git remote add origin https://mtellect@bitbucket.org/primepeter/exmstaffing.git
  static pageRoute(RouteSettings settings, Widget page, [bool error = false]) {
    return PageRouteBuilder(
        transitionsBuilder: fadeTransition,
        transitionDuration: Duration(milliseconds: 5),
        opaque: false,
        pageBuilder: (BuildContext context, _, __) {
          if (error)
            return Material(
              color: Colors.white,
              child: Container(
                alignment: Alignment.center,
                child: emptyLayout(Icons.sentiment_satisfied, "Page not found",
                    "Ops looks like the page your looking for doesn't exit or has been removed!.",
                    textColor: black),
              ),
            );
          return page;
        },
        settings: settings);
  }

  Route onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    final urlPath = settings.name;

    print("urlPath >> $urlPath");
    List items = urlPath.split("??");
    String path = items[0];

    print("path >> $path");

    if (items.length > 1) {
      String pathId = items[1];
      if (items.length == 4) {
        bool admin = items.last;
        isAdmin = admin;
        print("pathId admin>> $admin isAdmin $isAdmin");
      }

      if (null == pathId || pathId.isEmpty) return null;
      routeDataHolder[path] = pathId;
    }

    if (null == routeMap[path]) return null;
    return pageRoute(settings, routeMap[path]);
  }
}

class AppSetter extends StatefulWidget {
  @override
  _AppSetterState createState() => _AppSetterState();
}

class _AppSetterState extends State<AppSetter> with TickerProviderStateMixin {
  bool showLogin = false;
  double opacity = .3;

  AnimationController progressDController;
  Animation<Color> progressDAnimation;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    progressDController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 1200));
    progressDAnimation = RainbowColorTween([
      Colors.white,
      Color(0XFFFF20E9),
      // Colors.blue,
      Colors.green,
      Colors.red,
      Colors.purple,
    ]).animate(progressDController)
      ..addListener(() {})
      ..addStatusListener((status) {});
    progressDController.repeat(reverse: false);

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      checkUser();
    });

    // checkUser();
    loadSettings();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    progressDController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: ClipRRect(
      borderRadius: BorderRadius.circular(60),
      child: Image.asset(
        "assets/icons/logo.png",
        fit: BoxFit.cover,
        height: 60,
        width: 60,
      ),
    ));
  }

  checkUser() async {
    final _auth = FirebaseAuth.instance;
    await Future.delayed(Duration(milliseconds: 10));
    final pref = await SharedPreferences.getInstance();
    String user = pref.getString("user");

    if (user != null) {
      loadLocalUser(user);
      return;
    }

    pushAndResult(context, SignUp(), clear: true, fade: true);
  }

  loadSettings() {
    FirebaseFirestore.instance
        .collection(APP_SETTINGS_BASE)
        .doc(APP_SETTINGS)
        .get(/*source: Source.cache*/)
        .then((doc) {
      if (!doc.exists) {
        appSettingsModel = new BaseModel();
        appSettingsModel.saveItem(APP_SETTINGS_BASE, false,
            document: APP_SETTINGS);
        return;
      }
      appSettingsModel = BaseModel(doc: doc);
    });
  }

  loadLocalUser(String userId) {
    FirebaseFirestore.instance
        .collection(USER_BASE)
        .doc(userId)
        .get(
            // GetOptions(source: Source.cache)
            )
        .then((doc) {
      if (!doc.exists) {
        print("No User Here");
        pushAndResult(
          context,
          SignUp(),
        );
        return;
      }
      userModel = BaseModel(doc: doc);
      isAdmin = userModel.getBoolean(IS_ADMIN) ||
          userModel.getString(EMAIL) == "johnebere@gmail.com";
      // pushAndResult(context, HomePage(), clear: true, fade: true);
      Navigator.pushReplacementNamed(context, "/home");
    });
  }
}

String footerTermsText =
    "By my signature below, I authorize Exceptional Medical Staffing Inc. (“EXM”) to contact "
    "any current and prior employers, educational institutions, licensing boards and personal "
    "references referenced in my resume or other materials provided by me to EXM. I authorize "
    "my current and prior employers, educational institutions, licensing board and personal "
    "references to supply any information concerning my background, character or qualifications "
    "for employment with EXM. I release any current or prior employer, educational institution, "
    "licensing board and individual who provides such information and I release EXM and any "
    "related subsidiaries, partnerships or affiliated companies and their owners, employees and "
    "agents from any and all liability and responsibility for damages or claims of any kind for "
    "revealing or receiving information concerning my background, character or qualifications for "
    "employment with EXM.";
