import 'dart:convert';
// import 'dart:io';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:universal_io/io.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class EmailApi {
  // static String baseUrl = "https://api.sendinblue.com/v3/smtp/email";
  // static String baseUrl = "https://cors-anywhere.herokuapp.com/https://api.sendinblue.com/v3/smtp/email";

  static String apiKey = appSettingsModel.getString("apiKeySendInBlue");
  static String baseUrl =
      "https://us-central1-exmstaffing.cloudfunctions.net/helloWorld";
  // static Map<String, String> headers = {
  //   // 'api-key': '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',
  //   'api-key': apiKey,
  //   'content-type': 'application/json',
  //   "accept": "application/json",
  // };

  //xsmtpsib-f6cebce1e124902166a1b5f74cf30d85476aec59b651ddb2c7ecd13d2ae7329d-6aGvCtrBhg7OIb0Z

  static const int TYPE_INSTRUCTIONS = 6;
  static const int TYPE_REQUEST = 5;
  static const int TYPE_EMPLOYEE = 4;
  static const int TYPE_ORDER = 3;
  static const int TYPE_WELCOME = 7;
  static const int TYPE_CLOCKING = 8;
  static const int TYPE_ASSIGNED_ORDER = 10;
  static const int TYPE_EMPLOYEE_APPLY = 11;
  static const int TYPE_AGREEMENT_LPN = 12;
  static const int TYPE_AGREEMENT_RN = 13;
  static const int TYPE_AGREEMENT_CNA = 14;
  static const int TYPE_AGREEMENT_CARE = 15;
  static const int TYPE_AGREEMENT_MED = 16;
  static const int TYPE_AGREEMENT_COOK = 17;
  static const int TYPE_AGREEMENT_RECEPTION = 18;
  static const int TYPE_AGREEMENT_DINNING = 19;
  static const int TYPE_AGREEMENT_OTHER = 20;

  // '/lpnAgreement': LpnAgreement(),
  // '/rnAgreement': RnAgreement(), //done
  // '/cnaAgreement': CnaAgreement(), //done
  // '/careAgreement': CareAgreement(), //done
  // '/medAgreement': MedAgreement(), //done
  // '/cookAgreement': CookAgreement(), //done
  // '/receptionAgreement': ReceptionAgreement(), //done
  // '/dinningAgreement': DinningAgreement(), //done
  // '/otherAgreement': OtherAgreement(),

  static sendEmail({
    @required int type,
    @required String toName,
    @required String toEmail,
    String message,
    String url = "",
    String firstShift,
    String secondShift,
    String thirdShift,
    String firstShiftTime,
    String secondShiftTime,
    String thirdShiftTime,
    String orderDate,
    String amount,
  }) async {
    var payload = {
      "sender": {"name": "Exmstaffing", "email": "support@exstaffing.com"},
      "params": {
        if (null != url) "URL": url,
        if (null != message) "BODY": message,
        if (null != firstShift) "FIRST_SHIFT": firstShift,
        if (null != secondShift) "SECOND_SHIFT": secondShift,
        if (null != thirdShift) "THIRD_SHIFT": thirdShift,
        if (null != firstShiftTime) "FIRST_SHIFT_TIME": firstShiftTime,
        if (null != secondShiftTime) "SECOND_SHIFT_TIME": secondShiftTime,
        if (null != thirdShiftTime) "THIRD_SHIFT_TIME": thirdShiftTime,
        if (null != orderDate) "ORDER_DATE": orderDate,
        if (null != amount) "AMOUNT": amount,
        if (null != toName) "FIRSTNAME": toName,
      },
      "to": [
        {
          if (null != toEmail) "email": toEmail,
          if (null != toName) "name": toName
        }
      ],
      "templateId": type,
      // "subject": message,
    };

    var data = {
      "baseURL": "https://api.sendinblue.com/v3",
      "reqPath": "/smtp/email",
      "reqHeader": {
        // 'api-key': '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',
        'api-key': apiKey,
        'content-type': 'application/json',
        "accept": "application/json",
      },
      "payload": payload
    };

    BaseModel model = BaseModel(items: data);
    model.saveItem(EMAIL_BASE, true);

    // print(jsonEncode(payload));
    //
    // final httpClient = HttpClient();
    // final request = await httpClient.postUrl(Uri.parse(baseUrl));
    // request.write(jsonEncode(data));
    //
    // // Enable credentials mode
    // if (request is BrowserHttpClientRequest) {
    //   request.browserCredentialsMode = true;
    // }
    //
    // final response = await request.close();
    //
    // // var response = await http.post(Uri.parse(baseUrl),
    // //     headers: header, body: jsonEncode(data));
    //
    // response.transform(utf8.decoder).listen((contents) {
    //   print(contents);
    // });
  }

  static sendEmailAll({
    @required int type,
    List items,
    String date,
    String request,
    String message,
    String url,
    String firstName,
    String lastName,
    String tel,
    String email,
    String state,
    String city,
    String skillSet,
    String covid,
  }) async {
    var payload = {
      "sender": {"name": "Exmstaffing", "email": "support@exstaffing.com"},

      "params": {
        if (null != url) "URL": url,
        if (null != message) "BODY": message,
        if (null != date) "DATE": date,
        if (null != request) "REQUEST": request,
        if (null != firstName) "FIRSTNAME": firstName,
        if (null != lastName) "LASTNAME": lastName,
        if (null != tel) "TEL": tel,
        if (null != email) "email": email,
        if (null != state) "STATE": state,
        if (null != city) "CITY": city,
        if (null != skillSet) "SKILLSET": skillSet,
        if (null != covid) "COVID": covid,
      },
      "to": items,

      "templateId": type,
      // "subject": message,
    };

    var data = {
      "baseURL": "https://api.sendinblue.com/v3",
      "reqPath": "/smtp/email",
      "reqHeader": {
        // 'api-key': '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',
        'api-key': apiKey,
        'content-type': 'application/json',
        "accept": "application/json",
      },
      "payload": payload
    };

    BaseModel model = BaseModel(items: data);
    model.saveItem(EMAIL_BASE, true);

    // print(jsonEncode(payload));
    //
    // final httpClient = HttpClient();
    // final request = await httpClient.postUrl(Uri.parse(baseUrl));
    // request.write(jsonEncode(data));
    //
    // // Enable credentials mode
    // if (request is BrowserHttpClientRequest) {
    //   request.browserCredentialsMode = true;
    // }
    //
    // final response = await request.close();
    //
    // // var response = await http.post(Uri.parse(baseUrl),
    // //     headers: header, body: jsonEncode(data));
    //
    // response.transform(utf8.decoder).listen((contents) {
    //   print(contents);
    // });
  }
}
