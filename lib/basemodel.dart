import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:Loopin/assets.dart';

import 'AppEngine.dart';

class BaseModel {
  Map<String, Object> items = new Map();
  Map<String, Object> itemsToUpdate = new Map();
  // Map<String, Map> itemsToUpdateList = new Map();
  // Map<String, Map> itemsToUpdateMap = new Map();

  BaseModel({Map items, DocumentSnapshot doc}) {
    if (items != null) {
      Map<String, Object> theItems = Map.from(items);
      this.items = theItems;
    }
    if (doc != null && doc.exists) {
      this.items = doc.data();
      this.items[OBJECT_ID] = doc.id;
    }
  }

  void put(String key, Object value) {
    items[key] = value;
    itemsToUpdate[key] = value;
  }

  void putInList(String key, Object value, {bool add = true}) {
    items[key] = value;
    itemsToUpdate[key] =
        add ? FieldValue.arrayUnion([value]) : FieldValue.arrayRemove([value]);
  }

  void remove(String key) {
    items.remove(key);
    itemsToUpdate[key] = null;
  }

  String getObjectId() {
    Object value = items[OBJECT_ID];
    return value == null || !(value is String) ? "" : value.toString();
  }

  List getList(String key) {
    Object value = items[key];
    return value == null || !(value is List) ? new List() : List.from(value);
  }

  List<BaseModel> getListModel(String key) {
    return getList(key).map((e) => BaseModel(items: e)).toList();
  }

/*  List<Object> addToList(String key, Object value, bool add) {
    List<Object> list = items[key];
    list = list == null ? new List<Object>() : list;
    if (add) {
      if (!list.contains(value)) list.add(value);
    } else {
      list.remove(value);
    }
    put(key, list);
    return list;
  }*/

  /* List<Map<String, Object>> addOnceToMap(
      String mapName, BaseModel bm, bool add) {
    List<Map<String, Object>> maps = items[mapName];
    maps = maps == null ? new List<Map<String, Object>>() : maps;
    bool canAdd = true;
    for (Map<String, Object> theMap in maps) {
      BaseModel model = new BaseModel(items: theMap);
      if (model.getString(OBJECT_ID) == (bm.getString(OBJECT_ID))) {
        canAdd = false;
        if (!add) maps.remove(theMap);
        break;
      }
    }
    if (canAdd && add) {
      maps.add(bm.items);
    }

    put(mapName, maps);
    return maps;
  }

  bool hasMap(String mapName, BaseModel bm) {
    List<Map<String, Object>> maps = items[mapName];
    maps = maps == null ? new List<Map<String, Object>>() : maps;
    for (Map<String, Object> theMap in maps) {
      BaseModel model = new BaseModel(items: theMap);
      if (model.getString(OBJECT_ID) == (bm.getString(OBJECT_ID))) {
        return true;
      }
    }
    return false;
  }*/

  Map getMap(String key) {
    Object value = items[key];
    return value == null || !(value is Map) ? new Map() : Map.from(value);
  }

  Object get(String key) {
    return items[key];
  }

  BaseModel getModel(String key) {
    return BaseModel(items: getMap(key));
  }

  String getUserId() {
    Object value = items[USER_ID];

    return value == null || !(value is String) ? "" : value.toString();
  }

  String getFullName() {
    return "${getString(FIRST_NAME)} ${getString(LAST_NAME)}";
  }

  String getUserName() {
    Object value = items[USERNAME];
    String name = value == null || !(value is String) ? "" : value.toString();
    if (name.length > 2) {
      name = name.substring(0, 1).toUpperCase() + name.substring(1);
    }
    return name;
  }

  String getString(String key) {
    Object value = items[key];

    return value == null || !(value is String) ? "" : value.toString();
  }

  String getEmail() {
    Object value = items[EMAIL];
    return value == null || !(value is String) ? "" : value.toString();
  }

  String getPassword() {
    Object value = items[PASSWORD];
    return value == null || !(value is String) ? "" : value.toString();
  }

  /*int getCreatedAt() {
    Object value = items[CREATED_AT];
    return value == null || !(value is DateTime)
        ? 0
        : (value as DateTime).millisecond;
  }

  DateTime getCreatedAtDate() {
    Object value = items[CREATED_AT];
    return value == null || !(value is DateTime) ? new DateTime.now() : (value);
  }

  DateTime getUpdatedAtDate() {
    Object value = items[UPDATED_AT];
    return value == null || !(value is DateTime) ? new DateTime.now() : (value);
  }

  int getUpdatedAt() {
    Object value = items[UPDATED_AT];
    return value == null || !(value is Timestamp)
        ? 0
        : (value as Timestamp).millisecondsSinceEpoch;
  }*/

  /*bool isRead({String userId}) {
    List<String> readBy = List.from(getList(READ_BY));
    return readBy.contains(userId != null ? userId : userModel.getObjectId());
  }*/

  bool isMuted(String chatId) {
    List<String> readBy = getList(MUTED);
    return readBy.contains(chatId);
  }

  bool isRated(String chatId) {
    List<String> readBy = getList(HAS_RATED);
    return readBy.contains(chatId);
  }

  bool isSilenced() {
    List<String> silence = getList(SILENCED);
    return silence.contains(userModel.getObjectId());
  }

  bool isKicked() {
    List<String> readBy = getList(KICKED_OUT);
    return readBy.contains(userModel.getObjectId());
  }

  bool isMale() {
    return getInt(GENDER) == MALE;
  }

  /*void setRead() {
    List<String> readBy = getList(READ_BY);
    if (!readBy.contains(userModel.getObjectId())) {
      readBy.add(userModel.getObjectId());
      put(READ_BY, readBy);
    }
    updateItem();
  }*/

  /*void setUnRead() {
    List<String> readBy = getList(READ_BY);
    readBy.remove(userModel.getObjectId());
    updateItem();
  }*/

  bool hasItem(String key) {
    return items[key] != null;
  }

  bool myItem() {
    return getUserId() == (userModel.getObjectId());
  }

  bool mySentChat() {
    return getBoolean(MY_SENT_CHAT);
  }

  bool isHidden() {
    List<String> readBy = getList(HIDDEN);
    return readBy.contains(userModel.getObjectId());
  }

  // int getInt(String key,[]) {
  //   Object value = items[key];
  //   return value == null || !(value is num) ? 0 : int.parse(value.toString());
  // }

  int getInt(String key, [bool inNegative = false]) {
    Object value = items[key];
    return value == null || !(value is int)
        ? inNegative
            ? -1
            : 0
        : (value);
  }

  int getType() {
    Object value = items[TYPE];
    return value == null || !(value is int) ? 0 : value;
  }

  double getDouble(String key) {
    Object value = items[key];
    return value == null || !(value is num)
        ? 0
        : double.parse(value.toString());
  }

  int getTime() {
    Object value = items[TIME];
    return value == null || !(value is int) ? 0 : value;
  }

  /*int getLong(String key) {
    Object value = items[key];
    return value == null || !(value is int) ? 0 : value;
  }*/

  bool getBoolean(String key) {
    Object value = items[key];
    return value == null || !(value is bool) ? false : value;
  }

  bool isAdminItem() {
    return getBoolean(IS_ADMIN);
  }

  bool isLeader() {
    return getBoolean(IS_LEADER);
  }

  bool isJohn() {
    return getEmail() == ("johnebere58@gmail.com");
  }

  void updateItems(
      {context,
      bool updateTime = true,
      int delaySeconds = 0,
      String db,
      String objectId,
      int retryMax = 6,
      bool silently = true,
      progressMessage,
      onComplete(error)}) async {
    if (!silently) showProgress(true, context, msg: progressMessage);

    if ((!(await isConnected()))) {
      if (!silently) await Future.delayed(Duration(milliseconds: 600));
      if (!silently) showProgress(false, context);
      if (!silently) await Future.delayed(Duration(milliseconds: 600));
      if (onComplete != null) onComplete("No internet connectivity");
      return;
    }

    Future.delayed(Duration(seconds: delaySeconds), () async {
      String dName = db ?? items[DATABASE_NAME];
      String id = objectId ?? items[OBJECT_ID];

      try {
        var _firestore = FirebaseFirestore.instance;
        await _firestore.runTransaction((transaction) async {
          DocumentReference itemRef = _firestore.collection(dName).doc(id);

          DocumentSnapshot snapshot = await transaction.get(itemRef);
          if (!snapshot.exists) return;

          if (updateTime) {
            itemsToUpdate[UPDATED_AT] = FieldValue.serverTimestamp();
            itemsToUpdate[TIME_UPDATED] = DateTime.now().millisecondsSinceEpoch;
          }
          transaction.update(itemRef, itemsToUpdate);
        }, timeout: Duration(seconds: 10));
        print("Finished Transaction");
        if (!silently) await Future.delayed(Duration(milliseconds: 600));
        if (!silently) showProgress(false, context);
        if (!silently) await Future.delayed(Duration(milliseconds: 600));
        if (onComplete != null) onComplete(null);
      } catch (e) {
        print("Finished with error: $e");
        delaySeconds += 5;
        delaySeconds = delaySeconds >= 60 ? 0 : delaySeconds;
        print("$e... retrying in $delaySeconds seconds");
        retryMax--;
        if (retryMax > 0 && !silently) {
          updateItems(
              context: context,
              updateTime: updateTime,
              delaySeconds: delaySeconds,
              db: db,
              onComplete: onComplete,
              silently: silently,
              retryMax: retryMax,
              progressMessage: progressMessage,
              objectId: objectId);
        } else {
          if (!silently) await Future.delayed(Duration(milliseconds: 600));
          if (!silently) showProgress(false, context);
          if (!silently) await Future.delayed(Duration(milliseconds: 600));
          if (onComplete != null)
            onComplete(/*e.toString()*/ "Error occurred, try again");
        }
      }
    });
  }

  void updateItemOncex(context, onComplete(error), Map<String, Object> map,
      {bool updateTime = true,
      String progressMessage = "Please wait",
      bool silently = false}) async {
    if (!silently) showProgress(true, context, msg: progressMessage);
    // await Future.delayed(Duration(milliseconds: 600));

    if ((!(await isConnected()))) {
      if (!silently) await Future.delayed(Duration(milliseconds: 600));
      if (!silently) showProgress(false, context);
      if (!silently) await Future.delayed(Duration(milliseconds: 600));
      if (onComplete != null) onComplete("No internet connectivity");
      return;
    }

    String dName = items[DATABASE_NAME];
    String id = items[OBJECT_ID];
    if (updateTime) {
      map[UPDATED_AT] = FieldValue.serverTimestamp();
      map[TIME_UPDATED] = DateTime.now().millisecondsSinceEpoch;
    }

    print("Updating");

    try {
      var _firestore = FirebaseFirestore.instance;
      await _firestore.runTransaction((transaction) async {
        DocumentReference itemRef = _firestore.collection(dName).doc(id);
        //DocumentSnapshot snapshot = await transaction.get(itemRef);
        await transaction.update(
          itemRef,
          map,
        );
      }, timeout: Duration(seconds: 10));
      print("Finished Transaction");
      if (!silently) await Future.delayed(Duration(milliseconds: 600));
      if (!silently) showProgress(false, context);
      if (!silently) await Future.delayed(Duration(milliseconds: 600));
      try {
        items.addAll(map);
      } catch (e) {}
      onComplete(null);
    } catch (e) {
      print("Finished with error: $e");
      if (!silently) await Future.delayed(Duration(milliseconds: 600));
      if (!silently) showProgress(false, context);
      if (!silently) await Future.delayed(Duration(milliseconds: 600));
      onComplete(/*e.toString()*/ "Error occurred, try again");
    }
  }

  void updateCountItem(String key, bool increase,
      {bool updateTime = true, int delaySeconds = 0}) async {
    Future.delayed(Duration(seconds: 1), () async {
      /*bool connected = await isConnected();
      if (!connected) {
        delaySeconds = delaySeconds + 10;
        delaySeconds = delaySeconds >= 60 ? 0 : delaySeconds;
        updateCountItem(key, increase,
            updateTime: updateTime, delaySeconds: delaySeconds);
        return;
      }*/

      String dName = items[DATABASE_NAME];
      String id = items[OBJECT_ID];

      DocumentSnapshot doc = await FirebaseFirestore.instance
          .collection(dName)
          .doc(id)
          .get(GetOptions(source: Source.server))
          .catchError((error) {
        delaySeconds = delaySeconds + 10;
        delaySeconds = delaySeconds >= 60 ? 0 : delaySeconds;
        updateCountItem(key, increase,
            updateTime: updateTime, delaySeconds: delaySeconds);
        return null;
      });
      if (doc == null) return;
      if (!doc.exists) return;

      Map data = doc.data();
      var item = data[key] ?? 0;
      if (increase) {
        item = item + 1;
      } else {
        item = item - 1;
      }
      data[key] = item;

      if (updateTime) {
        data[UPDATED_AT] = FieldValue.serverTimestamp();
        data[TIME_UPDATED] = DateTime.now().millisecondsSinceEpoch;
      }

      doc.reference.set(data);
    });
  }

  void deleteItem() {
    String dName = items[DATABASE_NAME];
    String id = items[OBJECT_ID];

//    Firestore db = FirebaseFirestore.instance;
    FirebaseFirestore.instance.collection(dName).doc(id).delete();
  }

  processSave(String name, bool addMyInfo) {
    items[VISIBILITY] = PUBLIC;
    items[DATABASE_NAME] = name;
    items[UPDATED_AT] = FieldValue.serverTimestamp();
    items[CREATED_AT] = FieldValue.serverTimestamp();
    items[TIME] = DateTime.now().millisecondsSinceEpoch;
    items[TIME_UPDATED] = DateTime.now().millisecondsSinceEpoch;
    if (name != (USER_BASE) &&
        name != (APP_SETTINGS_BASE) &&
        name != (NOTIFY_BASE)) {
      if (addMyInfo) addMyDetails(addMyInfo);
    }
  }

  void addMyDetails(bool addMyInfo) {
    items[USER_ID] = userModel.getObjectId();
    items[PHONE_NUMBER] = userModel.getString(PHONE_NUMBER);
    items[USER_IMAGE] = userModel.getString(USER_IMAGE);
    items[FIRST_NAME] = userModel.getString(FIRST_NAME);
    items[LAST_NAME] = userModel.getString(LAST_NAME);
    items[DEVICE_ID] = userModel.getString(DEVICE_ID);
  }

  void saveItem(String name, bool addMyInfo,
      {String document, onComplete(error), bool merge = true}) async {
    if (onComplete != null && !(await isConnected())) {
      onComplete("No internet connectivity");
      return;
    }

    processSave(name, addMyInfo);
    if (document == null) {
      FirebaseFirestore.instance
          .collection(name)
          .add(
            items,
          )
          .then((value) {
        if (onComplete != null) onComplete(null);
      }, onError: (e) {
        if (onComplete != null) onComplete(e.toString());
      }).timeout(Duration(seconds: 10));
    } else {
      items[OBJECT_ID] = document;
      FirebaseFirestore.instance
          .collection(name)
          .doc(document)
          .set(items, SetOptions(merge: merge))
          .then((value) {
        if (onComplete != null) onComplete(null);
      }, onError: (e) {
        if (onComplete != null) onComplete(e.toString());
      }).timeout(Duration(seconds: 10));
    }
  }

  bool get isCoordinator {
    return getInt(PRIVILEGE_INDEX, true) == 1;
  }

  // void listen(){
  //   FirebaseFirestore.instance
  //       .collection(name).doc()
  // }
}
