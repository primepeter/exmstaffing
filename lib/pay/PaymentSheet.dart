import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/pay/ChooseBankSheet.dart';

import 'package:Loopin/assets.dart';
import 'package:Loopin/pay/widgets.dart';

import 'PinOrOTPSheet.dart';
import 'WebAuthSheet.dart';
import 'payEngineV2.dart';

class PaymentSheet extends StatefulWidget {
  final String title;
  final int type;
  final double amount;

  const PaymentSheet({
    Key key,
    this.title,
    this.type,
    this.amount,
  }) : super(key: key);

  @override
  _PaymentSheetState createState() => _PaymentSheetState();
}

class _PaymentSheetState extends State<PaymentSheet> {
  Charge charge;

  double amount = 0;
  FocusNode _focusNode = FocusNode();

  final cardNumberController =
      new MaskedTextController(mask: '0000 **** **** 0000');
  final cardExpController = new MaskedTextController(mask: '00/00');
  final cardNameController = TextEditingController();
  final cardCvvController = TextEditingController();
  bool billingRequired = false;

  final phoneController = TextEditingController();
  final accountController = TextEditingController();
  String selectedBank = "";
  Bank bank;
  String accountName = "";
  bool accountResolved = false;
  bool isBusy = false;

  int currentTransfer = 0;
  List get paymentItems => [
        {
          TITLE: "REGULAR",
          BANK_NAME: "Zenith Bank",
          ACCOUNT_NAME: "Streams of Joy Fellowship",
          ACCOUNT_NUMBER: "1012108290"
        },
        {
          TITLE: "PARTNERSHIP",
          BANK_NAME: "Zenith Bank",
          ACCOUNT_NAME: "Streams of Joy Partnership",
          ACCOUNT_NUMBER: "1015465765"
        },
        {
          TITLE: "INTERNATIONAL",
          BANK_NAME: "Zenith Bank",
          BANK_BRANCH: "Okigwe",
          BANK_SWIFT_CODE: "ZEIBNGLA",
          ACCOUNT_NAME: "Streams of Joy",
          ACCOUNT_NUMBER: "5070668310"
        }
      ];

  String errorText = "";
  showError(
    String text,
  ) {
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 4), () {
      errorText = "";
      setState(() {});
    });
  }

  @override
  initState() {
    super.initState();
    amount = widget.amount;
    charge = Charge(
        amount: amount.toInt(),
        redirectUrl: redirectLink,
        email: "ammaugost@gmail.com",
        phoneNumber: "08132733836",
        fullName: "Maugost Okore",
        narration: "Just a test Payment",
        txRef: getRandomId());
    accountController.addListener(() {
      String text = accountController.text;
      if (text.isEmpty || text.length != 10) {
        accountResolved = false;
        isBusy = false;
        if (mounted) setState(() {});
      }
      // loadDetails();
    });
  }

  resolveAccount() async {
    String accountNumber = accountController.text;
    setState(() {
      charge.bankCode = bank.code;
      charge.accountNumber = accountNumber;
    });
    showProgress(true, context, msg: "Fetching Account...");
    try {
      accountName = await revolveAccount(charge);
      accountResolved = true;
      isBusy = false;
      showProgress(
        false,
        context,
      );
      if (mounted) setState(() {});
    } catch (e) {
      showProgress(
        false,
        context,
      );
      showErrorDialog(context, e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 90),
      padding: const EdgeInsets.only(top: 0, right: 0, left: 0, bottom: 20),
      decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        ),
      ),
      child: Column(
        children: [
          Container(
            height: 60,
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: pink3.withOpacity(.2),
                border:
                    Border(bottom: BorderSide(color: black.withOpacity(.1)))),
            child: Row(
              children: [
                //if (widget.type == 0)
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      "assets/icons/naira.png",
                      height: 15,
                      width: 15,
                      color: black,
                    ),
                    addSpaceWidth(2),
                    Text(
                      formatAmount(amount.toString()),
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: black,
                          fontSize: 20),
                    ),
                    addSpaceWidth(10),
                  ],
                ),
                Expanded(
                  child: Text.rich(
                    TextSpan(children: [
                      TextSpan(text: "Pay with "),
                      TextSpan(
                        text: "${widget.title}",
                        style: TextStyle(
                            fontSize: 16,
                            color: black,
                            fontWeight: FontWeight.bold),
                      ),
                    ]),
                    style: TextStyle(
                        fontSize: 16,
                        color: black.withOpacity(.6),
                        fontWeight: FontWeight.bold),
                  ),
                ),
                IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.keyboard_arrow_down)),
              ],
            ),
          ),
          AnimatedContainer(
            duration: Duration(milliseconds: 500),
            width: double.infinity,
            height: errorText.isEmpty ? 0 : 40,
            decoration: BoxDecoration(
                color: red0, borderRadius: BorderRadius.circular(0)),
            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            //margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
            child: Center(
                child: Text(
              errorText,
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            )),
          ),
          Expanded(
              child: Container(
            padding: EdgeInsets.all(15),
            child: Builder(
              builder: (c) {
                if (widget.type == 0) return accountPage();
                if (widget.type == 1) return cardPage();
                return transferPage();
              },
            ),
          ))
        ],
      ),
    );
  }

  Widget cardPage() => Column(
        children: [
          addSpace(20),
          inputItem("CARD NUMBER ", cardNumberController, Icons.credit_card,
              hint: "0000 1234 1234 0000",
              inputType: TextInputType.number, onChanged: (s) {
            // cardNumber = s;
            setState(() {});
          }),
          Row(
            children: [
              Expanded(
                  child: inputItem(
                      "CARD EXPIRY ", cardExpController, Icons.credit_card,
                      hint: "12/25",
                      inputType: TextInputType.number, onChanged: (s) {
                //expiryDate = s;
                setState(() {});
              })),
              addSpaceWidth(10),
              Expanded(
                  child: inputItem(
                      "CARD CVV ", cardCvvController, Icons.credit_card,
                      hint: "***",
                      inputType: TextInputType.number,
                      focusNode: _focusNode, onChanged: (s) {
                // cvv = s;
                setState(() {});
              })),
            ],
          ),
          addSpace(20),
          buttonItem("PAY", () async {
            FocusScope.of(context).requestFocus(FocusNode());
            String cardNumber = cardNumberController.text;
            String cardCVV = cardCvvController.text;
            String cardExpiry = cardExpController.text;
            if (cardNumber.isEmpty) {
              showError("Enter Credit Card Number");
              return;
            }

            if (cardExpiry.isEmpty) {
              showError("Enter Credit Card Expiry");
              return;
            }

            if (cardCVV.isEmpty) {
              showError("Enter Credit Card CVV");
              return;
            }
            String cardExpiryMonth = cardExpiry.split("/")[0] ?? "";
            String cardExpiryYear = cardExpiry.split("/")[1] ?? "";

            // charge.chargeType = ChargeType.card;
            // charge.cardCvv = "564";
            // charge.cardNumber = "5531886652142950";
            // charge.cardExpiryMonth = "09";
            // charge.cardExpiryYear = "22";

            print("debugMode $debugMode");
            setState(() {
              charge.chargeType = ChargeType.card;
              charge.cardCvv = cardCVV;
              charge.cardNumber = cardNumber.replaceAll(" ", "");
              charge.cardExpiryMonth = cardExpiryMonth;
              charge.cardExpiryYear = cardExpiryYear;
            });

            showProgress(true, context, msg: "Processing Payment...");
            performCharge();
          })
        ],
      );

  performCharge() async {
    try {
      await chargePayment(charge,
          onNoAuthUsed: () => performCharge(),
          onShowWebAuthorization: (authUrl, flwRef) async {
            print("showWebAuthorization");
            String val = await getWebAuthorization(authUrl);
            bool terminated = val.isEmpty;
            if (terminated) {
              showProgress(
                false,
                context,
              );
              showErrorDialog(context, "Transaction Terminated by You.");
              return;
            }

            final verResp = await verifyPayment(flwRef);
            print("flwMeta  ${verResp.get("flwMeta")}");
            print(verResp.get("flwMeta"));
            showProgress(
              false,
              context,
            );
            String msg = verResp.flwMetaVBVRESPONSEMESSAGE ?? "";
            String code = verResp.chargeCode ?? "";
            if (!verResp.isSuccessful) {
              showErrorDialog(context, "$code $msg.");
              return;
            }
            //c4c932d0-e177-11eb-9965-07c3047b66ae
            //KSJL1009692938517

            showMessage(
              context,
              Icons.check,
              green,
              "Successful!",
              "Great! Your Transaction was Successful!\n$msg",
              // cancellable: false,
              delayInMilli: 700,
              clickYesText: "Go Back",
              onClicked: (_) {
                Navigator.pop(context);
              },
            );
          },
          onBillingRequest: () {
            showProgress(
              false,
              context,
            );
            showErrorDialog(
                context, "Terminated no Support for this card Yet.");
          },
          onPinRequested: (flwRef) async {
            print("onPinRequested");
            String val = await getPinOrOTP();
            bool terminated = val.isEmpty;
            if (terminated) {
              showProgress(
                false,
                context,
              );
              showErrorDialog(context, "Transaction Terminated by You.");
              return;
            }
            setState(() {
              charge.pin = val;
              charge.suggestedAuth = "PIN";
            });
            performCharge();
          },
          onOtpRequested: (msg, flwRef) async {
            print("onOtpRequested");
            String val = await getPinOrOTP(msg);
            bool terminated = val.isEmpty;
            if (terminated) {
              showProgress(
                false,
                context,
              );
              showErrorDialog(context, "Transaction Terminated by You.");
              return;
            }
            bool isCard = charge.chargeType == ChargeType.card;
            final resp = await validateChargeOTP(flwRef, val, isCard);
            final verResp = await verifyPayment(flwRef);
            print("flwMeta  ${verResp.get("flwMeta")}");
            print(verResp.get("flwMeta"));
            showProgress(
              false,
              context,
            );
            String msgs = verResp.flwMetaVBVRESPONSEMESSAGE ?? "";
            String code = verResp.chargeCode ?? "";
            if (!verResp.isSuccessful) {
              showErrorDialog(context, "$code $msgs.");
              return;
            }
            showMessage(
              context,
              Icons.check,
              green,
              "Successful!",
              "Great! Your Transaction was Successful!\n$msgs",
              // cancellable: false,
              delayInMilli: 700,
              clickYesText: "Go Back",
              onClicked: (_) {
                Navigator.pop(context);
              },
            );
          });
    } catch (e) {
      showProgress(
        false,
        context,
      );
      showErrorDialog(context, e.toString());
    }
  }

  /// Remove all line feed, carriage return and whitespace characters
  String cleanUrl(String url) {
    return url.replaceAll(RegExp(r"[\n\r\s]+"), "");
  }

  Future<String> getWebAuthorization(String url) async {
    final val = await showModalBottomSheet<String>(
      context: context,
      isScrollControlled: true,
      isDismissible: false,
      enableDrag: false,
      backgroundColor: black.withOpacity(0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        ),
      ),
      builder: (context) {
        return WebAuthSheet(
          authUrl: url,
        );
      },
    );
    // if (null == val || val.isEmpty) throw "Transaction Terminated by You.";
    return val;
  }

  Future<String> getPinOrOTP([String message = ""]) async {
    final val = await showModalBottomSheet<String>(
      context: context,
      isScrollControlled: true,
      isDismissible: false,
      enableDrag: false,
      backgroundColor: black.withOpacity(0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        ),
      ),
      builder: (context) {
        return PinOrOTPSheet(
          message: message,
          isOTP: message.isNotEmpty,
        );
      },
    );
    // if (null == val || val.isEmpty) throw "Transaction Terminated by You.";
    return val;
  }

  Widget accountPage() => SingleChildScrollView(
        child: Column(
          children: [
            addSpace(20),
            if (accountResolved)
              ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  bottomLeft: Radius.circular(20),
                ),
                child: Container(
                  margin: EdgeInsets.only(bottom: 10),
                  decoration: BoxDecoration(
                      border: Border(
                          left:
                              BorderSide(color: gold.withOpacity(1), width: 4),
                          bottom: BorderSide(color: gold.withOpacity(.1)))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              child: Icon(
                                Icons.info_outline,
                                size: 15,
                                color: white.withOpacity(.6),
                              ),
                            ),
                            addSpaceWidth(5),
                            Text(
                              "ACCOUNT INFO",
                              style: textStyle(false, 11, white.withOpacity(1)),
                            ),
                          ],
                        ),
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            color: gold,
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(10),
                              bottomRight: Radius.circular(0),
                            )),
                      ),
                      //addSpace(10),
                      Container(
                        padding: EdgeInsets.all(10),
                        alignment: Alignment.centerLeft,
                        decoration: BoxDecoration(
                            color: gold.withOpacity(0.4),
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(10),
                              bottomRight: Radius.circular(0),
                              bottomLeft: Radius.circular(0),
                            ),
                            border: Border.all(color: gold, width: .5)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Bank Name",
                              style: TextStyle(
                                  color: black, fontWeight: FontWeight.bold),
                            ),
                            addSpace(5),
                            Text(
                              bank.name,
                              style: TextStyle(
                                color: black,
                                // fontWeight: FontWeight.bold
                              ),
                            ),
                            addSpace(20),
                            Text(
                              "Account Name",
                              style: TextStyle(
                                  color: black, fontWeight: FontWeight.bold),
                            ),
                            addSpace(5),
                            Text(
                              accountName,
                              style: TextStyle(
                                color: black,
                                // fontWeight: FontWeight.bold
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            inputItem("PHONE NUMBER", phoneController, Icons.phone_sharp,
                inputType: TextInputType.number, hint: "080123456789"),
            inputItem("ACCOUNT NUMBER", accountController, Icons.dialpad,
                inputType: TextInputType.number, hint: "Enter Account Number"),
            clickItem("ACCOUNT BANK", selectedBank, Icons.food_bank, () async {
              final resp = await showModalBottomSheet<Bank>(
                context: context,
                isScrollControlled: true,
                // isDismissible: false,
                // enableDrag: false,
                backgroundColor: black.withOpacity(0),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15),
                  ),
                ),
                builder: (context) {
                  return ChooseBankSheet(
                    debitBank: true,
                  );
                },
              );

              if (null == resp) return;
              bank = resp;
              selectedBank = resp.name;
              setState(() {});
            }, hint: "Choose your Bank"),
            addSpace(20),
            buttonItem(accountResolved ? "PAY" : "VERIFY ACCOUNT", () async {
              FocusScope.of(context).requestFocus(FocusNode());

              String phone = phoneController.text;
              String accountNumber = accountController.text;

              if (phone.isEmpty) {
                showError("What's your Phone Number?");
                return;
              }

              if (accountNumber.isEmpty) {
                showError("What's your Account Number?");
                return;
              }

              if (selectedBank.isEmpty) {
                showError("What's your Bank?");
                return;
              }

              if (!accountResolved) {
                resolveAccount();
                return;
              }

              setState(() {
                charge.chargeType = ChargeType.bankNGN;
                charge.accountNumber = accountNumber;
                charge.bankCode = bank.code;
              });

              showProgress(true, context, msg: "Processing Payment...");
              performCharge();
            })
          ],
        ),
      );
  Widget transferPage() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: List.generate(paymentItems.length, (index) {
                  bool active = currentTransfer == index;
                  Map item = paymentItems[index];
                  String title = item[TITLE];
                  String bankName = item[BANK_NAME];
                  String branch = item[BANK_BRANCH] ?? "";
                  String swift = item[BANK_SWIFT_CODE] ?? "";
                  String name = item[ACCOUNT_NAME];
                  String number = item[ACCOUNT_NUMBER];

                  Color color = black;
                  // if (active) color = black;

                  return ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(20),
                    ),
                    child: Container(
                      margin: EdgeInsets.only(bottom: 10),
                      decoration: BoxDecoration(
                          border: Border(
                              left: BorderSide(
                                  color: gold.withOpacity(1), width: 4),
                              bottom: BorderSide(color: gold.withOpacity(.1)))),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Container(
                                  child: Icon(
                                    Icons.info_outline,
                                    size: 15,
                                    color: white.withOpacity(.6),
                                  ),
                                ),
                                addSpaceWidth(5),
                                Text(
                                  title,
                                  style: textStyle(
                                      false, 11, white.withOpacity(1)),
                                ),
                              ],
                            ),
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                color: appColor,
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10),
                                  bottomRight: Radius.circular(0),
                                )),
                          ),
                          //addSpace(10),
                          Container(
                            padding: EdgeInsets.all(10),
                            alignment: Alignment.centerLeft,
                            decoration: BoxDecoration(
                                // color: pink3.withOpacity(active ? 0.2 : 0),
                                color: default_white.withOpacity(.5),
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10),
                                  bottomRight: Radius.circular(0),
                                  bottomLeft: Radius.circular(0),
                                ),
                                border: Border.all(
                                    color: black.withOpacity(.09), width: .5)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Bank Name",
                                  style: TextStyle(
                                      color: color,
                                      fontWeight: FontWeight.bold),
                                ),
                                addSpace(5),
                                Text(
                                  bankName,
                                  style: TextStyle(
                                    color: color,
                                    // fontWeight: FontWeight.bold
                                  ),
                                ),
                                addSpace(20),
                                Text(
                                  "Account Name",
                                  style: TextStyle(
                                      color: color,
                                      fontWeight: FontWeight.bold),
                                ),
                                addSpace(5),
                                Text(
                                  name,
                                  style: TextStyle(
                                    color: color,
                                    // fontWeight: FontWeight.bold
                                  ),
                                ),
                                addSpace(20),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "Account Number",
                                            style: TextStyle(
                                                color: color,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          addSpace(5),
                                          Text(
                                            number,
                                            style: TextStyle(
                                              color: color,
                                              // fontWeight: FontWeight.bold
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    TextButton(
                                        onPressed: () {},
                                        style: TextButton.styleFrom(
                                            primary: white,
                                            backgroundColor: appColor),
                                        child: Text("COPY"))
                                  ],
                                ),
                                if (branch.isNotEmpty) ...[
                                  addSpace(20),
                                  Text(
                                    "Bank Branch",
                                    style: TextStyle(
                                        color: color,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  addSpace(5),
                                  Text(
                                    branch,
                                    style: TextStyle(
                                      color: color,
                                      // fontWeight: FontWeight.bold
                                    ),
                                  ),
                                ],
                                if (swift.isNotEmpty) ...[
                                  addSpace(20),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "Swift Code",
                                              style: TextStyle(
                                                  color: color,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            addSpace(5),
                                            Text(
                                              swift,
                                              style: TextStyle(
                                                color: color,
                                                // fontWeight: FontWeight.bold
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      TextButton(
                                          onPressed: () {},
                                          style: TextButton.styleFrom(
                                              primary: white,
                                              backgroundColor: appColor),
                                          child: Text("COPY"))
                                    ],
                                  ),
                                ]
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }),
              ),
            ),
          ),
        ],
      );
}
