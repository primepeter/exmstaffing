import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:Loopin/assets.dart';

import 'package:Loopin/basemodel.dart';
import 'package:Loopin/pay/PayEncryption.dart';

String get redirectLink => "https://payment-status-page.firebaseapp.com/";
bool get debugMode => appSettingsModel.getBoolean(FLW_DEBUG);

String get baseApiUrlV2 => debugMode
    ? "https://ravesandboxapi.flutterwave.com/v2/"
    : "https://api.ravepay.co/v2/";

String get baseApiUrl => debugMode
    ? "https://ravesandboxapi.flutterwave.com/"
    : "https://api.ravepay.co/";

Map<String, String> get baseApiHeaders => {
      // 'Content-Type': 'application/json',
      'Authorization': 'Bearer $secretKey',
    };
String get secretKey =>
    appSettingsModel.getString(debugMode ? FLW_SEC_KEY_TEST : FLW_SEC_KEY);

String get publicKey =>
    appSettingsModel.getString(debugMode ? FLW_PUB_KEY_TEST : FLW_PUB_KEY);

// ***Main localKey
// String get secretKey => debugMode
//     ? "FLWSECK-fd4172f2b7fc2935adbf67e8e60d6dbc-X"
//     : "FLWSECK-d9ae6c4b35adc505aba82966c7571365-X";
// String get publicKey => debugMode
//     ? "FLWPUBK-9768585b355bc716fd3343688a0df49b-X"
//     : "FLWPUBK-27a6cca1a5928c5826a50d20c2284a08-X";

final String basePath = "/flwv3-pug/getpaidx/api";
final String _feeEndpoint = "$basePath/fee";
final String _chargeEndpoint = "$basePath/charge";
final String _validateChargeEndpoint = "$basePath/validatecharge";
final String _reQueryEndpoint = "$basePath/verify/mpesa";
final String _bankEndpoint = "$basePath/flwpbf-banks.js";

//******* Handle all issues with rave banking API *******
class Bank {
  final int id;
  final String code;
  final String name;
  final bool isMobileVerified;
  final bool internetbanking;
  final String swiftCode;
  final String branches;

  Bank(Map json, [bool debit = false])
      : this.id = debit ? num.parse(json["bankcode"]).toInt() : json["Id"],
        this.code = json[debit ? "bankcode" : "Code"],
        this.name = json[debit ? "bankname" : "Name"],
        this.isMobileVerified = debit ? false : json["IsMobileVerified"],
        this.internetbanking = debit ? json["internetbanking"] : false,
        this.swiftCode = debit ? "" : json["SwiftCode"],
        this.branches = debit ? "" : json["branches"];
}

class BankBranch {
  final int id;
  final String branchCode;
  final String branchName;
  final String swiftCode;
  final String bic;
  final String bankId;

  BankBranch(Map json)
      : this.id = json["id"],
        this.branchCode = json["branch_code"],
        this.branchName = json["branch_name"],
        this.swiftCode = json["swift_code"],
        this.bic = json["bic"],
        this.bankId = json["bank_id"];
}

/// country*
// string
// Pass either NG, GH, KE, UG, ZA or TZ to get list of banks in
// Nigeria, Ghana, Kenya, Uganda, South Africa or Tanzania respectively
// - https://developer.flutterwave.com/v3.0/reference#get-all-banks

Future<List<Bank>> getAllBanks([String country = "NG"]) async {
  //for direct debit
  //https://api.ravepay.co/flwv3-pug/getpaidx/api/flwpbf-banks.js?json=1

  //for bank transfer
  //https://api.ravepay.co/v2/banks/country
  final String _basePath =
      "${baseApiUrlV2}banks/$country?public_key=$publicKey";
  final url = Uri.parse(_basePath);
  final response = await http.get(url, headers: baseApiHeaders);
  print(_basePath);
  print(response.request.url);
  if (response.statusCode != HttpStatus.ok) {
    throw ("Sorry check payEngine getAllBanks code.");
  }

  Map item = jsonDecode(response.body);
  print(item);
  List data = List.from(item["data"]["Banks"] ?? []);
  data.sort(
      (a, b) => a['Name'].toLowerCase().compareTo(b['Name'].toLowerCase()));
  return data.map((e) => Bank(e)).toList();
}

Future<List<Bank>> getAllDebitBanks([String country = "NG"]) async {
  //for direct debit
  //https://api.ravepay.co/flwv3-pug/getpaidx/api/flwpbf-banks.js?json=1

  //for bank transfer
  //https://api.ravepay.co/v2/banks/country
  final String _basePath =
      "${baseApiUrlV2}banks/$country?public_key=$publicKey";
  final url = Uri.parse(
      "https://api.ravepay.co/flwv3-pug/getpaidx/api/flwpbf-banks.js?json=1");
  final response = await http.get(url, headers: baseApiHeaders);
  print(_basePath);
  print(response.request.url);
  if (response.statusCode != HttpStatus.ok) {
    throw ("Sorry check payEngine getAllDebitBanks code.");
  }

  // Map item = jsonDecode(response.body);
  // print(item);
  // List data = List.from(item["data"]["Banks"] ?? []);
  final data = List.from(jsonDecode(response.body));

  data.sort((a, b) =>
      a['bankname'].toLowerCase().compareTo(b['bankname'].toLowerCase()));
  return data.map((e) => Bank(e, true)).toList();
}

///id*
// int32
// Unique bank ID, it is returned in the call to fetch banks here
// - https://developer.flutterwave.com/v3.0/reference#get-all-banks
//  https://developer.flutterwave.com/reference#get-bank-branches

Future<List<BankBranch>> getBankBranches(Bank bank) async {
  // https://api.flutterwave.com/v3/banks/id/branches
  final url = Uri.parse("${baseApiUrlV2}banks/${bank.id}/branches");
  final response = await http.get(url, headers: baseApiHeaders);
  if (response.statusCode != HttpStatus.ok) {
    throw ("Sorry check payEngine getAllBanks code.");
  }
  Map item = jsonDecode(response.body);
  String status = item["status"];
  String message = item["message"];
  List data = List.from(item["data"] ?? []);
  return data.map((e) => BankBranch(e)).toList();
}

Future<String> revolveAccount(Charge charge) async {
  // https://api.ravepay.co/flwv3-pug/getpaidx/api/resolve_account
  final url = Uri.parse("${baseApiUrl}flwv3-pug/getpaidx/api/resolve_account");
  print(url);
  final response = await http.post(url, headers: baseApiHeaders, body: {
    "recipientaccount": charge.accountNumber,
    "destbankcode": charge.bankCode,
    "PBFPubKey": publicKey
  });
  print("response ${response.request.url}");
  print("response ${response.body}");

  if (response.statusCode != HttpStatus.ok) {
    throw ("Sorry check PayEngine getAllBanks code.");
  }
  Map item = jsonDecode(response.body);
  String message = item["message"];

  Map data = (item["data"] ?? {});
  String accountName = data["data"]["accountname"];
  if (null == accountName) {
    throw ("Could not fetch Account, Please Cross-Check Account Number");
  }
  return accountName;
}

// ******* Handle all issues with rave OTP API *******
//https://developer.flutterwave.com/reference#create-otp

class OTP {
  // {
  // "medium": "email",
  // "reference": "CF-BARTER-20200616015533756952",
  // "otp": "5378980",
  // "expiry": "2020-06-16T02:00:33.4426637+00:00"
  // }

  final String medium;
  final String reference;
  final String otp;
  final String expiry;

  OTP(Map json)
      : this.medium = json["medium"],
        this.reference = json["reference"],
        this.otp = json["otp"],
        this.expiry = json["expiry"];
}

// This document shows you how to create an otp
// - https://developer.flutterwave.com/reference#create-otp

Future<List<OTP>> createOTP(
    String customerName, String customerEmail, String customerPhone,
    {int length = 7, int expiry = 10}) async {
  final url = Uri.parse("${baseApiUrlV2}otps/");
  final load = {
    "length": length,
    "customer": {
      "name": customerName,
      "email": customerEmail,
      "phone": customerPhone
    },
    "sender": "Streams of joy Int'l",
    "send": true,
    "medium": ["email", "whatsapp", "sms"],
    "expiry": expiry
  };
  final response = await http.post(url, headers: baseApiHeaders, body: load);
  if (response.statusCode != HttpStatus.ok) {
    throw ("Sorry check PayEngine createOTP code.");
  }
  Map item = jsonDecode(response.body);
  String status = item["status"];
  String message = item["message"];
  List data = List.from(item["data"] ?? []);
  return data.map((e) => OTP(e)).toList();
}

//
// ******* Handle all issues with rave Charge API *******
//https://developer.flutterwave.com/reference#create-otp

enum ChargeType {
  card,
  transfer,
  ach,
  bankUK,

  mmGhana,
  mmRwanda,
  mmUganda,
  mmZambia,
  ussd,
  mpesa,
  voucher,
  mmFrancophone,
  bankNGN,
}

class Charge {
  ChargeType chargeType;
  String txRef;
  int amount;
  String bankCode;
  String accountNumber;
  String currency;
  String email;
  String phoneNumber;
  String fullName;
  String pin;
  String suggestedAuth;
  String orderId;
  String network;
  String redirectUrl;
  String cardNumber;
  String cardCvv;
  String cardExpiryMonth;
  String cardExpiryYear;
  String authorizationMode;
  String authorizationPin;
  String clientIp;
  String fingerPrint;
  String narration;
  bool isPermanent;

  static Charge card({
    String suggestedAuth = "",
    String currency = "NGN",
    String redirectUrl = "https://payment-status-page.firebaseapp.com/",
    ChargeType chargeType = ChargeType.card,
    // @required double amount,
    // @required String email,
    // @required String fullName,
    // @required String txRef,
    @required String cardNumber,
    @required String cardCvv,
    @required String cardExpiryMonth,
    @required String cardExpiryYear,
  }) {
    return Charge(
      suggestedAuth: suggestedAuth,
      currency: currency,
      redirectUrl: redirectUrl,
      chargeType: chargeType,
      cardNumber: cardNumber,
      cardCvv: cardCvv,
      cardExpiryMonth: cardExpiryMonth,
      cardExpiryYear: cardExpiryYear,
    );
  }

  Charge({
    this.suggestedAuth,
    this.chargeType,
    this.txRef,
    this.amount,
    this.bankCode,
    this.accountNumber,
    this.currency = "NGN",
    this.email,
    this.phoneNumber,
    this.fullName,
    this.pin,
    this.orderId,
    this.network,
    this.redirectUrl = "https://payment-status-page.firebaseapp.com/",
    this.cardNumber,
    this.cardCvv,
    this.cardExpiryMonth,
    this.cardExpiryYear,
    this.authorizationMode,
    this.authorizationPin,
    this.clientIp,
    this.fingerPrint,
    this.narration,
    this.isPermanent,
  });

  String get type {
    if (chargeType == ChargeType.card) return "card";
    if (chargeType == ChargeType.transfer) return "bank_transfer";
    if (chargeType == ChargeType.ach) return "ach_payment";
    if (chargeType == ChargeType.bankUK) return "debit_uk_account";
    if (chargeType == ChargeType.mmGhana) return "mobile_money_ghana";
    if (chargeType == ChargeType.mmRwanda) return "mobile_money_rwanda";
    if (chargeType == ChargeType.mmUganda) return "mobile_money_uganda";
    if (chargeType == ChargeType.mmZambia) return "mobile_money_zambia";
    if (chargeType == ChargeType.ussd) return "ussd";
    if (chargeType == ChargeType.mpesa) return "mpesa";
    if (chargeType == ChargeType.voucher) return "voucher_payment";
    if (chargeType == ChargeType.mmFrancophone) return "mobile_money_franco";
    if (chargeType == ChargeType.bankNGN) return "debit_ng_account";
    return "debit_ng_account";
  }

  Map<String, dynamic> get payload {
    if (null == amount) throw ("Amount is required!");
    Map<String, dynamic> item = {};
    item["PBFpublicKey"] = publicKey;
    item["txRef"] = txRef;
    item["currency"] = "NGN";
    item["phonenumber"] = "08143733836";
    item["firstname"] = fullName.split(" ")[0];
    item["lastname"] = fullName.split(" ")[1];
    item["amount"] = amount;
    item["email"] = email;
    item["phonenumber"] = phoneNumber;
    item["currency"] = currency;
    item["IP"] = clientIp;
    item["device_fingerprint"] = fingerPrint;
    // item["redirectUrl"] = redirectUrl;
    // item["redirect_url"] = redirectUrl;

    if (chargeType == ChargeType.card) {
      item["cardno"] = cardNumber;
      item["cvv"] = cardCvv;
      item["expirymonth"] = cardExpiryMonth;
      item["expiryyear"] = cardExpiryYear;
      // item["redirectUrl"] = redirectUrl;
      item["redirect_url"] = redirectUrl;
      putIfNotNull(map: item, key: "pin", value: pin);
      putIfNotNull(map: item, key: "suggested_auth", value: suggestedAuth);

      // if (null != item["authorization"]) {
      //   item["authorization"]["mode"] = authorizationMode;
      // }
      // if (null != item["authorization"]["pin"]) {
      //   item["authorization"]["pin"] = authorizationPin;
      // }
      item["type"] = "card";
      item["txRef"] = txRef;
    }

    if (chargeType == ChargeType.transfer) {
      item["currency"] = "NGN";
      item["narration"] = narration;
      item["is_permanent"] = isPermanent;
    }

    if (chargeType == ChargeType.ach) {
      item["country"] = "ZAR";
      item["phone_number"] = narration;
      item["is_permanent"] = isPermanent;
    }

    if (chargeType == ChargeType.bankUK) {
      item["type"] = "debit_uk_account";
      item["currency"] = "GBP";
    }

    if (chargeType == ChargeType.bankNGN) {
      item["accountbank"] = bankCode;
      item["accountnumber"] = accountNumber;
      item["payment_type"] = "account";
      item["country"] = "NG";
      // item["bvn"] = bvn;
    }

    if (chargeType == ChargeType.mmZambia) {
      item["order_id"] = orderId;
      item["network"] = network;
    }

    if (chargeType == ChargeType.ussd) {
      item["account_bank"] = bankCode;
    }

    if (chargeType == ChargeType.mpesa) {
      item["currency"] = "KES";
    }

    if (chargeType == ChargeType.voucher) {
      item["pin"] = pin;
    }

    if (chargeType == ChargeType.mmFrancophone) {
      item["country"] = "CM";
    }
    return item;
  }
}

putIfNotNull({@required Map map, @required key, @required value}) {
  if (value == null || (value is String && value.isEmpty)) return;
  map[key] = value;
}

class ChargeResponse extends BaseModel {
  bool get isSuccessful {
    return status.toUpperCase() == "successful".toUpperCase();
  }

  final String id;
  final String txRef;
  final String orderRef;
  final String flwRef;
  final String redirectUrl;
  final String fingerPrint;
  final String amount;
  final String chargeAmount;
  final String appFee;
  final String merchantFee;
  final String merchantBearsFee;
  final String chargeResponseCode;
  final String chargeResponseMessage;
  final String authModelUsed;
  final String acctValRespMsg;
  final String narration;
  final String status;

  ChargeResponse(Map json, [bool isCard = false])
      : this.id = jsonContent(json, "id", isCard).toString(),
        this.txRef = jsonContent(json, "txRef", isCard),
        this.orderRef = jsonContent(json, "orderRef", isCard),
        this.flwRef = jsonContent(json, "flwRef", isCard),
        this.redirectUrl = jsonContent(json, "redirectUrl", isCard),
        this.fingerPrint = jsonContent(json, "device_fingerprint", isCard),
        this.amount = jsonContent(json, "amount", isCard).toString(),
        this.chargeAmount =
            jsonContent(json, "charged_amount", isCard).toString(),
        this.appFee = jsonContent(json, "appfee", isCard).toString(),
        this.merchantFee = jsonContent(json, "merchantfee", isCard).toString(),
        this.merchantBearsFee =
            jsonContent(json, "merchantbearsfee", isCard).toString(),
        this.chargeResponseCode =
            jsonContent(json, "chargeResponseCode", isCard),
        this.chargeResponseMessage =
            jsonContent(json, "chargeResponseMessage", isCard),
        this.authModelUsed = jsonContent(json, "authModelUsed", isCard),
        this.acctValRespMsg = jsonContent(json, "acctvalrespmsg", isCard),
        this.narration = jsonContent(json, "narration", isCard),
        this.status = jsonContent(json, "status", isCard),
        // narration
        super(items: json);

  static dynamic jsonContent(Map json, String key, bool isCard) {
    return isCard ? json["tx"][key] : json[key];
  }
}

// This document shows you how to create an otp
// - https://developer.flutterwave.com/reference#create-otp

Future<void> chargePayment(Charge charge,
    {onNoAuthUsed,
    onPinRequested,
    onOtpRequested,
    onBillingRequest,
    onShowWebAuthorization}) async {
  final payload = PayEncryption(secretKey).encryptRequest(charge.payload);
  final url = Uri.parse("${baseApiUrl}flwv3-pug/getpaidx/api/charge");
  final response = await http.post(url, headers: baseApiHeaders, body: payload);

  Map item = jsonDecode(response.body);
  String status = item["status"];
  String message = item["message"] ?? "".toUpperCase();

  if (response.statusCode != HttpStatus.ok) {
    throw ("PayEngine Charge.Err $message");
  }
  Map data = item["data"];
  var suggestedAuth = data["suggested_auth"] ?? "".toUpperCase();
  var authModelUsed = data["authModelUsed"] ?? "".toUpperCase();
  var chargeResponseCode = data["chargeResponseCode"];
  var chargeResponseMessage = data["chargeResponseMessage"];
  var authUrl = data["authurl"];
  var flwRef = data["flwRef"];

  print(response.request.url);
  print(data);
  print({
    "message": message,
    "suggestedAuth": suggestedAuth,
    "authModelUsed": authModelUsed,
    "chargeResponseCode": chargeResponseCode,
    "chargeResponseMessage": chargeResponseMessage,
    "authUrl": authUrl,
    "flwRef": flwRef
  });

  if (message == "AUTH_SUGGESTION") {
    if (suggestedAuth == "PIN") {
      onPinRequested(flwRef);
      return;
    }

    if (suggestedAuth == "AVS_VBVSECURECODE" ||
        suggestedAuth == "NO_AUTH_INTERNATIONAL") {
      onBillingRequest(flwRef);
      return;
    }
  }

  if (message == "V-COMP") {
    if (chargeResponseCode == "02") {
      if (authModelUsed == "ACCESS_OTP" ||
          chargeResponseMessage.contains("OTP")) {
        onOtpRequested(chargeResponseMessage, flwRef);
        return;
      }

      if (authModelUsed == "PIN") {
        onPinRequested(flwRef);
        return;
      }

      if (authModelUsed == "VBVSECURECODE") {
        onShowWebAuthorization(authUrl, flwRef);
        return;
      }
      if (authModelUsed == "AUTH") {
        onShowWebAuthorization(authUrl, flwRef);
        return;
      }
    }

    if (chargeResponseCode == "00") {
      onNoAuthUsed();
      return;
    }
  }

  if (authModelUsed == "GTB_OTP" ||
      authModelUsed == "ACCESS_OTP" ||
      authModelUsed.contains("OTP")) {
    onOtpRequested(chargeResponseMessage);
    return;
  }
}

class VerifyResponse extends BaseModel {
  bool get isSuccessful {
    return status.toUpperCase() == "successful".toUpperCase();
  }

  final String id;
  final String txRef;
  final String flwRef;
  final String fingerPrint;
  final String amount;
  final String chargeAmount;
  final String appFee;
  final String merchantFee;
  final String merchantBearsFee;
  final String chargeCode;
  final String chargeMessage;
  final String authModel;
  final String acctValRespMsg;
  final String narration;
  final String status;
  final String flwMetaVBVRESPONSEMESSAGE;
  final String flwMetaVBVRESPONSECODE;
  final String flwMetaACCOUNTVALIDATIONRESPMESSAGE;
  final String flwMetaACCOUNTVALIDATIONRESPONSECODE;

  VerifyResponse(Map json, [bool isCard = false])
      : this.id = jsonContent(json, "txid", isCard).toString(),
        this.txRef = jsonContent(json, "txref", isCard),
        this.flwRef = jsonContent(json, "flwref", isCard),
        this.fingerPrint = jsonContent(json, "devicefingerprint", isCard),
        this.amount = jsonContent(json, "amount", isCard).toString(),
        this.chargeAmount =
            jsonContent(json, "chargedamount", isCard).toString(),
        this.appFee = jsonContent(json, "appfee", isCard).toString(),
        this.merchantFee = jsonContent(json, "merchantfee", isCard).toString(),
        this.merchantBearsFee =
            jsonContent(json, "merchantbearsfee", isCard).toString(),
        this.chargeCode = jsonContent(json, "chargecode", isCard),
        this.chargeMessage = jsonContent(json, "chargemessage", isCard),
        this.authModel = jsonContent(json, "authmodel", isCard),
        this.acctValRespMsg = jsonContent(json, "acctvalrespmsg", isCard),
        this.narration = jsonContent(json, "narration", isCard),
        this.status = jsonContent(json, "status", isCard),
        this.flwMetaVBVRESPONSEMESSAGE =
            jsonContent(json["flwMeta"], "VBVRESPONSEMESSAGE", isCard),
        this.flwMetaVBVRESPONSECODE =
            jsonContent(json["flwMeta"], "VBVRESPONSECODE", isCard),
        this.flwMetaACCOUNTVALIDATIONRESPMESSAGE = jsonContent(
            json["flwMeta"], "ACCOUNTVALIDATIONRESPMESSAGE", isCard),
        this.flwMetaACCOUNTVALIDATIONRESPONSECODE = jsonContent(
            json["flwMeta"], "ACCOUNTVALIDATIONRESPONSECODE", isCard),
        // narration
        super(items: json);

  static dynamic jsonContent(Map json, String key, bool isCard) {
    return isCard ? json["tx"][key] : json[key];
  }
}

Future<VerifyResponse> verifyPayment(
  String txRef,
) async {
  final load = {
    // "tx_ref": txRef,
    "flw_ref": txRef,
    "SECKEY": secretKey,
  };
  final url = Uri.parse("${baseApiUrl}flwv3-pug/getpaidx/api/verify");
  final response = await http.post(url, headers: baseApiHeaders, body: load);
  Map item = jsonDecode(response.body);
  String status = item["status"];
  String message = item["message"];
  print(response.request.url);
  print(response.body);
  if (response.statusCode != HttpStatus.ok) {
    throw ("PayEngine VerifyCharge.Err $message");
  }
  Map data = item["data"] ?? {};
  return VerifyResponse(data);
}

Future<ChargeResponse> validateChargeOTP(
    String txRef, String otp, bool isCard) async {
  final load = {
    "otp": otp,
    "transaction_reference": txRef,
    "transactionreference": txRef,
    "PBFPubKey": publicKey
  };
  String path = isCard ? "validatecharge" : "validate";
  final url = Uri.parse("${baseApiUrl}flwv3-pug/getpaidx/api/$path");
  final response = await http.post(url, headers: baseApiHeaders, body: load);
  Map item = jsonDecode(response.body);
  String status = item["status"];
  String message = item["message"];

  if (response.statusCode != HttpStatus.ok) {
    throw ("PayEngine ValidateChargeOTP.Err $message");
  }
  Map data = item["data"] ?? {};
  return ChargeResponse(data, isCard);
}
