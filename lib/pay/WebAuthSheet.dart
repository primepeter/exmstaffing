import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/pay/ChooseBankSheet.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/pay/widgets.dart';

import 'payEngineV2.dart';

class WebAuthSheet extends StatefulWidget {
  final String authUrl;
  const WebAuthSheet({
    Key key,
    this.authUrl,
  }) : super(key: key);

  @override
  _WebAuthSheetState createState() => _WebAuthSheetState();
}

class _WebAuthSheetState extends State<WebAuthSheet> {
  String errorText = "";
  showError(
    String text,
  ) {
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 4), () {
      errorText = "";
      setState(() {});
    });
  }

  bool loaded = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        yesNoDialog(
            context, "Terminate?", "Do you want to terminate this transaction?",
            () {
          Navigator.pop(context, "");
        });
        return false;
      },
      child: Container(
        margin: EdgeInsets.only(top: 160),
        padding: const EdgeInsets.only(top: 0, right: 0, left: 0, bottom: 20),
        decoration: BoxDecoration(
          color: white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15),
            topRight: Radius.circular(15),
          ),
        ),
        child: Column(
          children: [
            Container(
              height: 60,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: pink3.withOpacity(.2),
                  border:
                      Border(bottom: BorderSide(color: black.withOpacity(.1)))),
              child: Row(
                children: [
                  Expanded(
                      child: Text(
                    "Web Authorization",
                    style: TextStyle(
                        fontSize: 16,
                        color: black,
                        fontWeight: FontWeight.bold),
                  )),
                  IconButton(
                      onPressed: () {
                        yesNoDialog(context, "Terminate?",
                            "Do you want to terminate this transaction?", () {
                          Navigator.pop(context, "");
                        });
                      },
                      icon: Icon(Icons.keyboard_arrow_down)),
                ],
              ),
            ),
            AnimatedContainer(
              duration: Duration(milliseconds: 500),
              width: double.infinity,
              height: errorText.isEmpty ? 0 : 40,
              decoration: BoxDecoration(
                  color: red0, borderRadius: BorderRadius.circular(0)),
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              //margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: Center(
                  child: Text(
                errorText,
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              )),
            ),
            Expanded(
                child: Container(
              padding: EdgeInsets.all(15),
              child: webPage(),
            ))
          ],
        ),
      ),
    );
  }

  Widget webPage() => Stack(
        children: <Widget>[
          Positioned.fill(
            child: WebView(
              initialUrl: widget.authUrl,
              javascriptMode: JavascriptMode.unrestricted,
              onPageFinished: (String url) {
                print("showWebAuthorization $url");
                if (!loaded) setState(() => loaded = true);
                if (url.startsWith(redirectLink)) {
                  Navigator.pop(context, "completed");
                }
              },
            ),
          ),
          if (!loaded)
            Positioned.fill(
              child: loadingLayout(),
            )
        ],
      );
}
