import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:Loopin/AppEngine.dart';

import 'ChoosePaymentOption.dart';
import 'payEngineV2.dart';

import 'package:Loopin/assets.dart';
import 'package:Loopin/pay/widgets.dart';

class ChooseBankSheet extends StatefulWidget {
  final bool debitBank;

  const ChooseBankSheet({Key key, this.debitBank = false}) : super(key: key);
  @override
  _ChooseBankSheetState createState() => _ChooseBankSheetState();
}

class _ChooseBankSheetState extends State<ChooseBankSheet> {
  FocusNode focusSearch = FocusNode();
  final searchController = TextEditingController();
  bool showCancel = false;

  bool banksBusy = true;
  List<Bank> banksList = [];

  int clampSize(String value) {
    return (searchController.text.length - 1).clamp(0, value.length);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    banksBusy = widget.debitBank ? allDebitBanks.isEmpty : allBanks.isEmpty;
    if (!banksBusy) banksList = widget.debitBank ? allDebitBanks : allBanks;
    if (banksBusy) loadBanks();
    focusSearch.addListener(() {
      if (mounted) setState(() {});
    });
  }

  loadBanks() async {
    try {
      banksList =
          widget.debitBank ? (await getAllDebitBanks()) : (await getAllBanks());
      for (var b in banksList) {
        String name = b.name;
        int p = allBanks.indexWhere((e) => e.name == name);
        if (p == -1) allBanks.add(b);
      }
      banksBusy = false;
      if (mounted) setState(() {});
    } catch (e) {
      print("loadBanks $e");
    }
  }

  reload() async {
    await Future.delayed(Duration(milliseconds: 100));
    String search = searchController.text.trim().toLowerCase();
    print("search $search");
    // if (search.isEmpty) {
    //   banksList = allBanks;
    //   setState(() {});
    // } else {
    //   banksList.clear();
    // }

    banksList =
        allBanks.where((e) => e.name.toLowerCase().contains(search)).toList();

    if (false)
      for (var item in allBanks) {
        String text = item.name.toUpperCase();
        if (search.isNotEmpty) {
          bool exist = text.toLowerCase().trim().contains(search);
          if (!exist) {
            continue;
          }
        }
        banksList.add(item);
      }

    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 150),
      padding: const EdgeInsets.only(top: 0, right: 0, left: 0, bottom: 20),
      decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        ),
      ),
      child: Column(
        children: [
          Container(
            height: 60,
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: pink3.withOpacity(.2),
                border:
                    Border(bottom: BorderSide(color: black.withOpacity(.1)))),
            child: Row(
              children: [
                Expanded(
                  child: Text.rich(
                    TextSpan(children: [
                      TextSpan(text: "Choose your "),
                      TextSpan(
                        text: "Bank",
                        style: TextStyle(
                            fontSize: 16,
                            color: black,
                            fontWeight: FontWeight.bold),
                      ),
                    ]),
                    style: TextStyle(
                        fontSize: 16,
                        color: black.withOpacity(.6),
                        fontWeight: FontWeight.bold),
                  ),
                ),
                IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.keyboard_arrow_down)),
              ],
            ),
          ),
          Container(
            height: 45,
            margin: EdgeInsets.fromLTRB(10, 20, 10, 10),
            decoration: BoxDecoration(
                color: white.withOpacity(.8),
                borderRadius: BorderRadius.circular(10),
                border: Border.all(
                    color: focusSearch.hasFocus
                        ? pink3.withOpacity(.3)
                        : black.withOpacity(.1),
                    width: focusSearch.hasFocus ? 2 : 1)),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              //mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                addSpaceWidth(10),
                Icon(
                  Icons.search,
                  color: black.withOpacity(.5),
                  size: 17,
                ),
                addSpaceWidth(10),
                new Flexible(
                  flex: 1,
                  child: new TextField(
                    textInputAction: TextInputAction.search,
                    textCapitalization: TextCapitalization.sentences,
                    autofocus: false,
                    onSubmitted: (_) {
                      //reload();
                    },
                    decoration: InputDecoration(
                        hintText: "Find your Bank",
                        hintStyle: textStyle(
                          false,
                          18,
                          black.withOpacity(.5),
                        ),
                        border: InputBorder.none,
                        isDense: true),
                    style: textStyle(false, 16, black),
                    controller: searchController,
                    cursorColor: black,
                    cursorWidth: 1,
                    focusNode: focusSearch,
                    keyboardType: TextInputType.text,
                    onChanged: (s) {
                      showCancel = s.trim().isNotEmpty;
                      setState(() {});
                      reload();
                    },
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      focusSearch.unfocus();
                      showCancel = false;
                      searchController.text = "";
                    });
                    reload();
                  },
                  child: showCancel
                      ? Padding(
                          padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                          child: Icon(
                            Icons.close,
                            color: black,
                            size: 20,
                          ),
                        )
                      : new Container(),
                )
              ],
            ),
          ),
          Expanded(
              child: Container(
            padding: EdgeInsets.all(15),
            child: Builder(
              builder: (c) {
                if (banksBusy) return loadingLayout();

                return ListView.builder(
                  itemBuilder: (c, p) {
                    final item = banksList[p];
                    String name = item.name;
                    return InkWell(
                      onTap: () async {
                        print(item);
                        Navigator.pop(context, item);
                      },
                      child: Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            color: transparent,
                            border: Border(
                                bottom:
                                    BorderSide(color: black.withOpacity(.09)))),
                        height: 50,
                        child: Text(
                          item.name,
                          style: textStyle(true, 16, black),
                        ),
                      ),
                    );
                  },
                  physics: BouncingScrollPhysics(),
                  padding: EdgeInsets.only(bottom: 100),
                  itemCount: banksList.length,
                );
              },
            ),
          ))
        ],
      ),
    );
  }
}
