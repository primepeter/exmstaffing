import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/NumericKeypad.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/pay/widgets.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:Loopin/AppEngine.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'payEngineFlwV3.dart';

class AmountSheet extends StatefulWidget {
  @override
  _AmountSheetState createState() => _AmountSheetState();
}

class _AmountSheetState extends State<AmountSheet> {
  final amountController = new MoneyMaskedTextController(
      decimalSeparator: ".", thousandSeparator: ",", precision: 2);

  String errorText = "";
  showError(
    String text,
  ) {
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 4), () {
      errorText = "";
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 300),
      padding: const EdgeInsets.only(top: 0, right: 0, left: 0, bottom: 20),
      decoration: BoxDecoration(
        color: gold,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          addSpace(20),
          Center(
            child: Container(
              width: 40,
              height: 5,
              decoration: BoxDecoration(
                color: white,
                borderRadius: BorderRadius.circular(30),
              ),
            ),
          ),
          addSpace(30),
          Text(
            "Enter Amount",
            style: textStyle(true, 20, white),
          ),
          addSpace(10),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.asset(
                "assets/icons/naira.png",
                height: 20,
                width: 20,
                color: white,
              ),
              addSpaceWidth(4),
              Text(
                text,
                style: textStyle(true, 60, white),
              ),
            ],
          ),
          NumericKeyboard(
              onKeyboardTap: _onKeyboardTap,
              textColor: Colors.red,
              rightButtonFn: () {
                text = text.substring(0, text.length - 1);
                if (text == "0" || text == "0.0") text = "0";
                if (text.isEmpty) text = "0";
                setState(() {});
              },
              mainAxisAlignment: MainAxisAlignment.spaceEvenly),
          addSpace(30),
          TextButton(
              onPressed: () {
                if (text == "0" || text == "0.0") {
                  EdgeAlert.show(context,
                      // title: 'Error',
                      description: 'Enter an amount!',
                      gravity: EdgeAlert.TOP,
                      backgroundColor: red0,
                      icon: Icons.info_outline);
                  return;
                  Alert(
                    context: context,
                    style: AlertStyle(
                      backgroundColor: black.withOpacity(.05),
                      animationType: AnimationType.fromTop,
                      isCloseButton: false,
                      alertAlignment: Alignment.topCenter,
                      isButtonVisible: false,
                      isOverlayTapDismiss: true,
                      overlayColor: black.withOpacity(.5),
                      alertBorder: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0.0),
                        side: BorderSide(
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    //type: AlertType.info,
                    content: Container(
                      color: red,
                      width: 50,
                      height: 50,
                    ),
                    //title: "Please enter an amount",
                    //desc: "Flutter is more awesome with RFlutter Alert.",
                  ).show();
                  return;
                }
              },
              style: TextButton.styleFrom(
                  padding: EdgeInsets.all(15),
                  elevation: 10,
                  backgroundColor: black,
                  primary: white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30))),
              child: Text(
                "Proceed",
                style: textStyle(true, 20, white),
              ))
        ],
      ),
    );
  }

  String text = "0.0";

  _onKeyboardTap(String value) {
    if (text == "0" || text == "0.0") {
      text = value;
    } else {
      text = formatAmount(text + value);
    }

    setState(() {});
  }
}
