import 'package:Loopin/assets.dart';
import 'package:flutter/material.dart';
import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/pay/AmountSheet.dart';

import 'PaymentSheet.dart';
import 'payEngineV2.dart';

List<Bank> allBanks = [];
List<Bank> allDebitBanks = [];

class ChoosePaymentOption extends StatefulWidget {
  const ChoosePaymentOption({Key key}) : super(key: key);

  @override
  _ChoosePaymentOptionState createState() => _ChoosePaymentOptionState();
}

class _ChoosePaymentOptionState extends State<ChoosePaymentOption> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    loadBanks();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  loadBanks() async {
    try {
      allBanks = await getAllBanks();
      allDebitBanks = await getAllDebitBanks();
      if (mounted) setState(() {});
    } catch (e) {
      print("loadBanks $e");
    }
  }

  int currentOption = 0;

  List get giveOptions {
    return [
      {"title": "Custom", "amount": 0.0},
      // {"amount": 100},
      {"amount": 200.0},
      {"amount": 500.0},
      {"amount": 1000.0},
      // {"amount": 5000},
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 180),
      padding: const EdgeInsets.only(top: 0, right: 0, left: 0, bottom: 20),
      decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
      ),
      child: Column(
        children: [
          addSpace(20),
          Center(
            child: Container(
              width: 40,
              height: 5,
              decoration: BoxDecoration(
                color: Colors.grey,
                borderRadius: BorderRadius.circular(30),
              ),
            ),
          ),
          addSpace(10),
          Expanded(
            child: Container(
                padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 0, 60, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "How much voucher do you want?",
                            style: TextStyle(
                                fontSize: 35, fontWeight: FontWeight.bold),
                          ),
                          addSpace(10),
                          Container(
                            width: 110,
                            height: 6,
                            decoration: BoxDecoration(
                                color: gold,
                                borderRadius: BorderRadius.circular(10)),
                          ),
                        ],
                      ),
                    ),
                    addSpace(10),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: List.generate(giveOptions.length, (index) {
                          Map item = giveOptions[index];
                          String title = item["title"];
                          String amount = item["amount"].toString();

                          bool active = currentOption == index;
                          return GestureDetector(
                            onTap: () {
                              currentOption = index;
                              setState(() {});
                            },
                            child: AnimatedContainer(
                              duration: Duration(milliseconds: 600),
                              width: active ? 105 : 80,
                              height: active ? 105 : 80,
                              margin: EdgeInsets.all(5),
                              padding: EdgeInsets.all(5),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  color: active ? gold : black.withOpacity(.04),
                                  border:
                                      Border.all(color: black.withOpacity(.04)),
                                  borderRadius: BorderRadius.circular(10)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  if (index != 0)
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image.asset(
                                          "assets/icons/naira.png",
                                          height: 15,
                                          width: 15,
                                          color: active ? white : black,
                                        ),
                                        addSpaceWidth(2),
                                        Text(
                                          formatAmount(amount),
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: active ? white : black,
                                              fontSize: 15),
                                        )
                                      ],
                                    ),
                                  if (index == 0)
                                    Text(
                                      "Custom \nAmount",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: active ? white : black,
                                          fontSize: 15),
                                    )
                                ],
                              ),
                            ),
                          );
                        }),
                      ),
                    )
                  ],
                )),
          ),
          ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              topRight: Radius.circular(15),
            ),
            child: Column(
              children: List.generate(payItems.length, (index) {
                String title = payItems[index];
                bool expanded = currentPayment == index;

                return GestureDetector(
                  onTap: () {
                    showOptions(index, title);
                  },
                  child: Container(
                    height: 60,
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        color: pink3.withOpacity(.2),
                        border: Border(
                            bottom: BorderSide(color: black.withOpacity(.1)))),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text.rich(
                            TextSpan(children: [
                              TextSpan(text: "Pay with "),
                              TextSpan(
                                text: "$title",
                                style: TextStyle(
                                    fontSize: 16,
                                    color: black,
                                    fontWeight: FontWeight.bold),
                              ),
                            ]),
                            style: TextStyle(
                                fontSize: 16,
                                color: black.withOpacity(.6),
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Icon(expanded
                            ? Icons.keyboard_arrow_down
                            : Icons.keyboard_arrow_up)
                      ],
                    ),
                  ),
                );
              }),
            ),
          )
        ],
      ),
    );
  }

  int currentPayment = -1;

  List get payItems {
    return [
      "Account", "Card", "Bank Transfer",
      //"USSD"
    ];
  }

  showOptions(int index, String title) async {
    double amount = giveOptions[currentOption]["amount"];
    if (currentOption == 0) {
      amount = await getCustomAmount();
      if (amount == 0) return;
    }

    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: false,
      enableDrag: index == 1,
      backgroundColor: black.withOpacity(0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        ),
      ),
      builder: (context) {
        return PaymentSheet(title: title, type: index, amount: amount);
      },
    );
  }

  Future<double> getCustomAmount() async {
    final amount = await showModalBottomSheet<double>(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      enableDrag: true,
      backgroundColor: black.withOpacity(0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        ),
      ),
      builder: (context) {
        return AmountSheet();
      },
    );
    if (null == amount) return 0;
    return amount;
  }
}
