// import 'dart:convert';
// import 'dart:io';
//
// import 'package:flutter/foundation.dart';
// import 'package:http/http.dart' as http;
//
// import 'package:mapps/basemodel.dart';
// import 'package:mapps/pay/PayEncryption.dart';
//
// // https://payment-status-page.firebaseapp.com/
// bool get debugMode => false;
//
// String get baseApiUrl => debugMode
//     ? "https://ravesandboxapi.flutterwave.com/"
//     : "https://api.ravepay.co/";
//
// Map<String, String> get baseApiHeaders => {
//       // 'Content-Type': 'application/json',
//       'Authorization': 'Bearer $secretKey',
//     };
//
// String get secretKey => debugMode
//     ? "FLWSECK-fd4172f2b7fc2935adbf67e8e60d6dbc-X"
//     : "FLWSECK-d9ae6c4b35adc505aba82966c7571365-X";
// String get publicKey => debugMode
//     ? "FLWPUBK-9768585b355bc716fd3343688a0df49b-X"
//     : "FLWPUBK-27a6cca1a5928c5826a50d20c2284a08-X";
//
// String get secretKey1 => debugMode
//     ? "FLWSECK-fd4172f2b7fc2935adbf67e8e60d6dbc-X"
//     : "FLWSECK-aaaf8d3aafd453215193f1649bdebad1-X";
// String get publicKey1 => debugMode
//     ? "FLWPUBK-9768585b355bc716fd3343688a0df49b-X"
//     : "FLWPUBK-6c7b01608d9a353a1f7988733dac316a-X";
//
// final String basePath = "flwv3-pug/getpaidx/api";
// final String _resolveEndpoint = "$basePath/resolve_account";
// final String _feeEndpoint = "$basePath/fee";
// final String _chargeEndpoint = "$basePath/charge";
// final String _validateChargeEndpoint = "$basePath/validatecharge";
// final String _reQueryEndpoint = "$basePath/verify/mpesa";
// final String _bankEndpoint = "$basePath/flwpbf-banks.js?json=1000";
//
// //******* Handle all issues with rave banking API *******
// class Bank {
//   final String name;
//   final String code;
//   final bool internetBanking;
//
//   Bank(Map json)
//       : this.name = json["bankname"],
//         this.code = json["bankcode"],
//         this.internetBanking = json["internetbanking"];
// }
//
// class BankBranch {
//   final int id;
//   final String branchCode;
//   final String branchName;
//   final String swiftCode;
//   final String bic;
//   final String bankId;
//
//   BankBranch(Map json)
//       : this.id = json["id"],
//         this.branchCode = json["branch_code"],
//         this.branchName = json["branch_name"],
//         this.swiftCode = json["swift_code"],
//         this.bic = json["bic"],
//         this.bankId = json["bank_id"];
// }
//
// /// country*
// // string
// // Pass either NG, GH, KE, UG, ZA or TZ to get list of banks in
// // Nigeria, Ghana, Kenya, Uganda, South Africa or Tanzania respectively
// // - https://developer.flutterwave.com/v3.0/reference#get-all-banks
//
// Future<List<Bank>> getAllBanks([String country = "NG"]) async {
//   final url = Uri.parse("$baseApiUrl$_bankEndpoint");
//   final response = await http.get(url, headers: baseApiHeaders);
//   print(response.request.url);
//   print(response.body);
//   if (response.statusCode != HttpStatus.ok) {
//     throw ("Sorry check payEngine getAllBanks code.");
//   }
//   List item = List.from(jsonDecode(response.body));
//   item.sort((a, b) =>
//       a['bankname'].toLowerCase().compareTo(b['bankname'].toLowerCase()));
//   return item.map((e) => Bank(e)).toList();
// }
//
// Future<String> revolveAccount(Charge charge) async {
//   // https://api.ravepay.co/flwv3-pug/getpaidx/api/resolve_account
//   final url = Uri.parse("$baseApiUrl$_resolveEndpoint");
//   final response = await http.post(url, headers: baseApiHeaders, body: {
//     "recipientaccount": charge.accountNumber,
//     "destbankcode": charge.bankCode,
//     "PBFPubKey": publicKey
//   });
//   print("response ${response.request.url}");
//   print("response ${response.body}");
//
//   if (response.statusCode != HttpStatus.ok) {
//     throw ("Sorry check PayEngine revolveAccount code.");
//   }
//   Map item = jsonDecode(response.body);
//   String message = item["message"];
//
//   Map data = (item["data"] ?? {});
//   String accountName = data["data"]["accountname"];
//   if (null == accountName) {
//     throw ("Could not fetch Account, Please Cross-Check Account Number");
//   }
//   return accountName;
// }
//
// // ******* Handle all issues with rave OTP API *******
// //https://developer.flutterwave.com/reference#create-otp
//
// class OTP {
//   // {
//   // "medium": "email",
//   // "reference": "CF-BARTER-20200616015533756952",
//   // "otp": "5378980",
//   // "expiry": "2020-06-16T02:00:33.4426637+00:00"
//   // }
//
//   final String medium;
//   final String reference;
//   final String otp;
//   final String expiry;
//
//   OTP(Map json)
//       : this.medium = json["medium"],
//         this.reference = json["reference"],
//         this.otp = json["otp"],
//         this.expiry = json["expiry"];
// }
//
// // This document shows you how to create an otp
// // - https://developer.flutterwave.com/reference#create-otp
//
// Future<List<OTP>> createOTP(
//     String customerName, String customerEmail, String customerPhone,
//     {int length = 7, int expiry = 10}) async {
//   final url = Uri.parse("$baseApiUrl$_resolveEndpoint");
//   final load = {
//     "length": length,
//     "customer": {
//       "name": customerName,
//       "email": customerEmail,
//       "phone": customerPhone
//     },
//     "sender": "Streams of joy Int'l",
//     "send": true,
//     "medium": ["email", "whatsapp", "sms"],
//     "expiry": expiry
//   };
//   final response = await http.post(url, headers: baseApiHeaders, body: load);
//   if (response.statusCode != HttpStatus.ok) {
//     throw ("Sorry check PayEngine createOTP code.");
//   }
//   Map item = jsonDecode(response.body);
//   String status = item["status"];
//   String message = item["message"];
//   List data = List.from(item["data"] ?? []);
//   return data.map((e) => OTP(e)).toList();
// }
//
// // This document shows you how to validate an otp
// // https://api.flutterwave.com/v3/otps/reference/validate
// Future<String> validateOTP(
//   String reference,
//   String otp,
// ) async {
//   final url = Uri.parse("$baseApiUrl$_resolveEndpoint");
//   // final url = Uri.parse("${baseApiUrlV2}otps/$reference/validate");
//   final load = {
//     "otp": otp,
//   };
//   final response = await http.post(url, headers: baseApiHeaders, body: load);
//   if (response.statusCode != HttpStatus.ok) {
//     throw ("Sorry check PayEngine validateOTP code.");
//   }
//   Map item = jsonDecode(response.body);
//   String status = item["status"];
//   String message = item["message"];
//   List data = List.from(item["data"] ?? []);
//   return message;
// }
//
// //
// // ******* Handle all issues with rave Charge API *******
// //https://developer.flutterwave.com/reference#create-otp
//
// enum ChargeType {
//   card,
//   transfer,
//   ach,
//   bankUK,
//
//   mmGhana,
//   mmRwanda,
//   mmUganda,
//   mmZambia,
//   ussd,
//   mpesa,
//   voucher,
//   mmFrancophone,
//   bankNGN,
// }
//
// class Charge {
//   //** Nigerian Banks
//   // {
//   // "tx_ref":"MC-1585230ew9v5050e8",
//   // "amount":"100",
//   // "account_bank":"044",
//   // "account_number":"0690000032",
//   // "currency":"NGN",
//   // "email":"user@flw.com",
//   // "phone_number":"0902620185",
//   // "fullname":"Yemi Desola"
//   // }
//   final ChargeType chargeType;
//   final String txRef;
//   final int amount;
//   final String bankCode;
//   final String accountNumber;
//   final String currency;
//   final String email;
//   final String phoneNumber;
//   final String fullName;
//   final String pin;
//   final String orderId;
//   final String network;
//   final String redirectUrl;
//   final String cardNumber;
//   final String cardCvv;
//   final String cardExpiryMonth;
//   final String cardExpiryYear;
//   final String authorizationMode;
//   final String authorizationPin;
//   final String clientIp;
//   final String fingerPrint;
//   final String narration;
//   final bool isPermanent;
//
//   Charge({
//     this.chargeType,
//     this.txRef,
//     this.amount,
//     this.bankCode,
//     this.accountNumber,
//     this.currency = "NGN",
//     this.email,
//     this.phoneNumber,
//     this.fullName,
//     this.pin,
//     this.orderId,
//     this.network,
//     this.redirectUrl,
//     this.cardNumber,
//     this.cardCvv,
//     this.cardExpiryMonth,
//     this.cardExpiryYear,
//     this.authorizationMode,
//     this.authorizationPin,
//     this.clientIp,
//     this.fingerPrint,
//     this.narration,
//     this.isPermanent,
//   });
//
//   String get type {
//     if (chargeType == ChargeType.card) return "card";
//     if (chargeType == ChargeType.transfer) return "bank_transfer";
//     if (chargeType == ChargeType.ach) return "ach_payment";
//     if (chargeType == ChargeType.bankUK) return "debit_uk_account";
//     if (chargeType == ChargeType.mmGhana) return "mobile_money_ghana";
//     if (chargeType == ChargeType.mmRwanda) return "mobile_money_rwanda";
//     if (chargeType == ChargeType.mmUganda) return "mobile_money_uganda";
//     if (chargeType == ChargeType.mmZambia) return "mobile_money_zambia";
//     if (chargeType == ChargeType.ussd) return "ussd";
//     if (chargeType == ChargeType.mpesa) return "mpesa";
//     if (chargeType == ChargeType.voucher) return "voucher_payment";
//     if (chargeType == ChargeType.mmFrancophone) return "mobile_money_franco";
//     if (chargeType == ChargeType.bankNGN) return "debit_ng_account";
//     return "debit_ng_account";
//   }
//
//   Map<String, dynamic> get payload {
//     if (null == amount) throw ("Amount is required!");
//     Map<String, dynamic> item = {};
//     item["PBFpublicKey"] = publicKey;
//     item["tx_ref"] = txRef;
//     item["amount"] = amount;
//     item["email"] = email;
//     item["phone_number"] = phoneNumber;
//     item["fullname"] = fullName;
//     item["currency"] = currency;
//     // “amount”:`100`
//     item["client_ip"] = clientIp;
//     item["device_fingerprint"] = fingerPrint;
//
//     if (chargeType == ChargeType.card) {
//       item["card_number"] = cardNumber;
//       item["cvv"] = cardCvv;
//       item["expiry_month"] = cardExpiryMonth;
//       item["expiry_year"] = cardExpiryYear;
//       item["redirect_url"] = redirectUrl;
//       item["authorization"]["mode"] = authorizationMode;
//       item["authorization"]["pin"] = authorizationPin;
//       item["type"] = type;
//     }
//
//     if (chargeType == ChargeType.transfer) {
//       item["currency"] = "NGN";
//       item["narration"] = narration;
//       item["is_permanent"] = isPermanent;
//     }
//
//     if (chargeType == ChargeType.ach) {
//       item["country"] = "ZAR";
//       item["phone_number"] = narration;
//       item["is_permanent"] = isPermanent;
//     }
//
//     if (chargeType == ChargeType.bankUK) {
//       item["type"] = "debit_uk_account";
//       item["currency"] = "GBP";
//     }
//     if (chargeType == ChargeType.bankNGN) {
//       item["account_bank"] = bankCode;
//       item["account_number"] = accountNumber;
//       item["payment_type"] = "account";
//     }
//
//     if (chargeType == ChargeType.mmZambia) {
//       item["order_id"] = orderId;
//       item["network"] = network;
//     }
//
//     if (chargeType == ChargeType.ussd) {
//       item["account_bank"] = bankCode;
//     }
//
//     if (chargeType == ChargeType.mpesa) {
//       item["currency"] = "KES";
//     }
//
//     if (chargeType == ChargeType.voucher) {
//       item["pin"] = pin;
//     }
//
//     if (chargeType == ChargeType.mmFrancophone) {
//       item["country"] = "CM";
//     }
//     print("payload $item");
//     return item;
//   }
// }
//
// class ChargeResponse extends BaseModel {
//   bool get isSuccessful {
//     return status == "success";
//   }
//
//   final String id;
//   final String txRef;
//   final String flwRef;
//   final String fingerPrint;
//   final String amount;
//   final String chargeAmount;
//   final String appFee;
//   final String merchantFee;
//   final String processorResponse;
//   final String authModel;
//   final String currency;
//   final String ip;
//   final String narration;
//   final String status;
//   final String authUrl;
//   final String paymentType;
//   final String fraudStatus;
//   final String createdAt;
//   final String accountId;
//   final String customerId;
//   final String customerEmail;
//   final String accountNumber;
//   final String bankCode;
//   final String accountName;
//   final String mode;
//   final String validateInstructions;
//   final String cardFirst6Digits;
//   final String cardLast4Digits;
//   final String cardIssuer;
//   final String cardCountry;
//   final String cardType;
//   final String cardExpiry;
//
//   //** Nigerian Banks
// // {
//
// // "data":{
// // "id":1190962,
// // "tx_ref":"MC-1585230ew9v5050e8",
// // "flw_ref":"URF_1585311768729_7845335",
// // "device_fingerprint":"62wd23423rq324323qew1",
// // "amount":100,
// // "charged_amount":100,
// // "app_fee":1247.2,
// // "merchant_fee":0,
// // "auth_model":"AUTH",
// // "currency":"NGN",
// // "ip":"154.123.220.1",
// // "narration":"Software Engineering as a service",
// // "status":"pending",
// // "auth_url":"NO-URL",
// // "payment_type":"account",
// // "fraud_status":"ok",
// // "created_at":"2020-03-27T12:22:48.000Z",
// // "account_id":74843,
// // "customer":{
// // "id":349094,
// // "email":"user@flw.com"
// // },
// // "account":{
// // "account_number":"0690000040",
// // "bank_code":"044",
// // "account_name":"Yemi Desola"
// // },
//   //  "card": {
//   //   "first_6digits": "232343",
//   //   "last_4digits": "4567",
//   //   "issuer": "VERVE FIRST CITY MONUMENT BANK PLC",
//   //   "country": "NG",
//   //   "type": "VERVE",
//   //   "expiry": "03/23"
//   //   },
// // "meta":{
// // "authorization":{
// // "mode":"otp",
// // "validate_instructions":"Please validate with the OTP sent to your mobile or email"
// // }
// // }
// // }
// // }
//
//   ChargeResponse(Map json)
//       : this.id = json["status"],
//         this.txRef = json["tx_ref"],
//         this.flwRef = json["flw_ref"],
//         this.fingerPrint = json["device_fingerprint"],
//         this.amount = json["amount"],
//         this.chargeAmount = json["charged_amount"],
//         this.appFee = json["app_fee"],
//         this.merchantFee = json["merchant_fee"],
//         this.processorResponse = json["processor_response"],
//         this.authModel = json["auth_model"],
//         this.currency = json["currency"],
//         this.ip = json["ip"],
//         this.narration = json["narration"],
//         this.status = json["status"],
//         this.authUrl = json["auth_url"],
//         this.paymentType = json["payment_type"],
//         this.fraudStatus = json["fraud_status"],
//         this.createdAt = json["created_at"],
//         this.accountId = json["account_id"],
//         this.customerId = json["customer"]["id"],
//         this.customerEmail = json["customer"]["email"],
//         this.accountNumber = json["account"]["account_number"],
//         this.bankCode = json["account"]["bank_code"],
//         this.accountName = json["account"]["account_name"],
//         this.mode = json["authorization"]["meta"]["mode"],
//         this.validateInstructions =
//             json["authorization"]["meta"]["validate_instructions"],
//         this.cardFirst6Digits = json["card"]["first_6digits"],
//         this.cardLast4Digits = json["card"]["last_4digits"],
//         this.cardIssuer = json["card"]["issuer"],
//         this.cardCountry = json["card"]["country"],
//         this.cardType = json["card"]["type"],
//         this.cardExpiry = json["card"]["expiry"],
//         super(items: json);
// }
//
// // This document shows you how to create an otp
// // - https://developer.flutterwave.com/reference#create-otp
//
// Future<ChargeResponse> chargePayment(Charge charge) async {
//   String type = "card";
//   if (charge.chargeType == ChargeType.bankNGN) type = "account";
//   print("type $type");
//   final payload = PayEncryption(secretKey).createCardRequest({
//     "PBFpublicKey": publicKey,
//     "accountbank": charge.bankCode,
//     "accountnumber": charge.accountNumber,
//     "currency": charge.currency,
//     "payment_type": type,
//     "country": "NG",
//     "amount": charge.amount,
//     "email": charge.email,
//     "bvn": "22178438481",
//     "txRef": charge.txRef,
//     "currency": "NGN",
//     "phonenumber": "08143733836",
//     "firstname": charge.fullName.split(" ")[0],
//     "lastname": charge.fullName.split(" ")[1],
//   });
//   print("payload $payload");
//
//   final url = Uri.parse("$baseApiUrl$_chargeEndpoint");
//
//   // final url = Uri.parse("${baseApiUrl}flwv3-pug/getpaidx/api/charge");
//   final response = await http.post(url, headers: baseApiHeaders, body: payload);
//   print("response ${response.request.url}");
//   print("response ${response.body}");
//
//   Map item = jsonDecode(response.body);
//   String status = item["status"];
//   String message = item["message"];
//
//   if (response.statusCode != HttpStatus.ok) {
//     throw ("PayEngine chargeErr.\n: $message");
//   }
//   Map data = item["data"] ?? {};
//   return ChargeResponse(data);
// }
//
// //
// // ******* Handle all issues with rave Charge Validation API *******
// //https://developer.flutterwave.com/reference#validate-a-charge
//
// enum ValidateType {
//   card,
//   account,
// }
//
// Future<ChargeResponse> validateCharge(
//   String otp,
//   String ref,
//   ValidateType type,
// ) async {
//   final load = {"otp": otp, "flw_ref": ref, "type": describeEnum(type)};
//   final url = Uri.parse("$baseApiUrl$_validateChargeEndpoint");
//   final response = await http.post(url, headers: baseApiHeaders, body: load);
//   if (response.statusCode != HttpStatus.ok) {
//     throw ("Sorry check payEngine createOTP code.");
//   }
//   Map item = jsonDecode(response.body);
//   String status = item["status"];
//   String message = item["message"];
//   Map data = item["data"] ?? {};
//   return ChargeResponse(data);
// }
