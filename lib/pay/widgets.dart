import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/assets.dart';
import 'package:flutter/material.dart';

buttonItem(
  String title,
  onPressed, {
  Color color = gold,
  Color textColor = white,
  double bottomLeft = 25,
  double bottomRight = 25,
  double topRight = 0,
  double topLeft = 0,
  double elevation = 8,
}) {
  return ElevatedButton(
      onPressed: () {
        onPressed();
      },
      onLongPress: () async {},
      style: ElevatedButton.styleFrom(
          primary: color,
          elevation: elevation,
          padding: const EdgeInsets.all(20.0),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(bottomLeft),
            bottomRight: Radius.circular(bottomRight),
            topRight: Radius.circular(topRight),
            topLeft: Radius.circular(topLeft),
          ))
          //shape: CircleBorder(),
          //minimumSize: Size(40, 40)
          ),
      child: Center(
          child: Text(
        title.toUpperCase(),
        style: TextStyle(
            fontSize: 16, color: textColor, fontWeight: FontWeight.bold),
      )));
}

inputItem(
    String title,
    //String text,
    TextEditingController controller,
    icon,
    {onChanged,
    onVisiChanged,
    inputType = TextInputType.text,
    inputAction = TextInputAction.done,
    bool isPass = false,
    bool passwordVisible = false,
    bool isAsset = false,
    bool autofocus = false,
    FocusNode focusNode,
    String hint = "",
    int maxLines = 1,
    bool isPhone = false,
    onCountryTap,
    String countryCode = "+234",
    bool bold = false,
    double size = 16.0,
    bool ignoreColor = false}) {
  //TextEditingController controller = TextEditingController(text: text);
  return ClipRRect(
    borderRadius: BorderRadius.only(
      topLeft: Radius.circular(10),
      bottomLeft: Radius.circular(20),
    ),
    child: Container(
      margin: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
          border: Border(
              left: BorderSide(color: gold.withOpacity(1), width: 4),
              bottom: BorderSide(color: gold.withOpacity(.1)))),
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Builder(
                  builder: (c) {
                    if (icon is IconData)
                      return Container(
                        child: Icon(
                          icon,
                          size: 15,
                          color: white.withOpacity(.6),
                        ),
                      );
                    return Container(
                        child: Image.asset(
                      icon,
                      height: 10,
                      width: 10,
                      fit: BoxFit.cover,
                      color: ignoreColor ? null : white.withOpacity(.6),
                    ));
                  },
                ),
                addSpaceWidth(5),
                Text(
                  title,
                  style: textStyle(false, 11, white.withOpacity(1)),
                ),
              ],
            ),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                color: gold,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10),
                  bottomRight: Radius.circular(0),
                )),
          ),
          //addSpace(10),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              crossAxisAlignment: maxLines != 1
                  ? CrossAxisAlignment.start
                  : CrossAxisAlignment.center,
              children: <Widget>[
                if (isPhone)
                  GestureDetector(
                    onTap: () {
                      onCountryTap();
                    },
                    child: Container(
                      height: 30,
                      width: 60,
                      margin: EdgeInsets.only(right: 10),
                      decoration: BoxDecoration(
                          color: black.withOpacity(.1),
                          borderRadius: BorderRadius.circular(5)),
                      child: Center(
                        child: Text(
                          countryCode,
                          style: textStyle(true, 14, black.withOpacity(.7)),
                        ),
                      ),
                    ),
                  ),
                Flexible(
                  child: new TextField(
                    textInputAction: inputAction,
                    textCapitalization: TextCapitalization.none,
                    autofocus: autofocus,
                    focusNode: focusNode,
                    //maxLength: 20,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: hint,
                        hintStyle:
                            textStyle(false, size, black.withOpacity(.2))),
                    style: textStyle(bold, size, black),
                    cursorColor: black,
                    cursorWidth: 1,
                    maxLines: maxLines,
                    keyboardType: inputType,
                    onChanged: onChanged,
                    obscureText: isPass ? (!passwordVisible) : false,
                    controller: controller,
                  ),
                ),
                if (isPass)
                  GestureDetector(
                      onTap: () {
                        onVisiChanged();
                      },
                      child: Text(
                        passwordVisible ? "HIDE" : "SHOW",
                        style: textStyle(false, 12, black.withOpacity(.5)),
                      )),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}

clickItem(
  String title,
  String value,
  icon,
  onChanged, {
  String hint = "",
}) {
  return ClipRRect(
    borderRadius: BorderRadius.only(
      topLeft: Radius.circular(10),
      bottomLeft: Radius.circular(20),
    ),
    child: Container(
      margin: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
          border: Border(
              left: BorderSide(color: gold.withOpacity(1), width: 4),
              bottom: BorderSide(color: gold.withOpacity(.1)))),
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Builder(
                  builder: (c) {
                    if (icon is IconData)
                      return Container(
                        child: Icon(
                          icon,
                          size: 15,
                          color: white.withOpacity(.6),
                        ),
                      );
                    return Container(
                        child: Image.asset(
                      icon,
                      height: 10,
                      width: 10,
                      fit: BoxFit.cover,
                      color: white.withOpacity(.6),
                    ));
                  },
                ),
                addSpaceWidth(5),
                Text(
                  title,
                  style: textStyle(false, 11, white.withOpacity(1)),
                ),
              ],
            ),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                color: gold,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10),
                  bottomRight: Radius.circular(0),
                )),
          ),
          addSpace(10),
          InkWell(
            onTap: () {
              onChanged();
            },
            child: Container(
              padding: EdgeInsets.all(10),
              height: 60,
              alignment: Alignment.centerLeft,
              child: Row(
                children: [
                  Expanded(
                    child: Text(null == value || value.isEmpty ? (hint) : value,
                        // style: textStyle(true, 14, black.withOpacity(.2)),
                        style: textStyle(
                            false,
                            16,
                            black.withOpacity(
                                null == value || value.isEmpty ? 0.2 : 1))),
                  ),
                  Icon(
                    Icons.arrow_circle_down,
                    color: black.withOpacity(.6),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    ),
  );
}
