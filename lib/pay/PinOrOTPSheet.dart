import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/NumericKeypad.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/pay/ChooseBankSheet.dart';

import 'package:Loopin/assets.dart';
import 'package:Loopin/pay/widgets.dart';

import 'payEngineV2.dart';

class PinOrOTPSheet extends StatefulWidget {
  final String message;
  final bool isOTP;
  const PinOrOTPSheet({Key key, this.message, this.isOTP = false})
      : super(key: key);

  @override
  _PinOrOTPSheetState createState() => _PinOrOTPSheetState();
}

class _PinOrOTPSheetState extends State<PinOrOTPSheet> {
  final otpController = TextEditingController();

  String errorText = "";
  showError(
    String text,
  ) {
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 4), () {
      errorText = "";
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        yesNoDialog(
            context, "Terminate?", "Do you want to terminate this transaction?",
            () {
          Navigator.pop(context, "");
        });
        return false;
      },
      child: LayoutBuilder(
        builder: (c, box) {
          bool isLargeScreen = box.maxWidth > 850;
          return Center(
            child: Container(
              margin: EdgeInsets.only(top: 160),
              padding:
                  const EdgeInsets.only(top: 0, right: 0, left: 0, bottom: 20),
              constraints: BoxConstraints(maxWidth: 500),
              decoration: BoxDecoration(
                color: white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15),
                  topRight: Radius.circular(15),
                ),
              ),
              child: Column(
                children: [
                  Container(
                    height: 60,
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        color: pink3.withOpacity(.2),
                        border: Border(
                            bottom: BorderSide(color: black.withOpacity(.1)))),
                    child: Row(
                      children: [
                        Expanded(
                            child: Text(
                          widget.isOTP ? "Enter OTP" : "Enter Your PIN",
                          style: TextStyle(
                              fontSize: 16,
                              color: black,
                              fontWeight: FontWeight.bold),
                        )),
                        IconButton(
                            onPressed: () {
                              yesNoDialog(context, "Terminate?",
                                  "Do you want to terminate this transaction?",
                                  () {
                                Navigator.pop(context, "");
                              });
                            },
                            icon: Icon(Icons.keyboard_arrow_down)),
                      ],
                    ),
                  ),
                  AnimatedContainer(
                    duration: Duration(milliseconds: 500),
                    width: double.infinity,
                    height: errorText.isEmpty ? 0 : 40,
                    decoration: BoxDecoration(
                        color: red0, borderRadius: BorderRadius.circular(0)),
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    //margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
                    child: Center(
                        child: Text(
                      errorText,
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    )),
                  ),
                  Expanded(
                      child: Container(
                    padding: EdgeInsets.all(15),
                    child: otpPage(),
                  ))
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget otpPage() => Column(
        children: [
          addSpace(20),
          Image.asset(
            "assets/icons/otp.png",
            height: 50,
          ),
          addSpace(20),
          Text(
            widget.isOTP
                ? widget.message + "\nPlease dial *901*4*1# to get your OTP. "
                // "Enter the OTP gotten in the field below."
                : "Please enter your card pin to continue your transaction",
            textAlign: TextAlign.center,
          ),
          addSpace(20),
          inputItem(
              widget.isOTP ? "OTP" : "CARD PIN", otpController, Icons.lock,
              inputType: TextInputType.number),
          addSpace(20),
          buttonItem("ENTER", () {
            FocusScope.of(context).requestFocus(FocusNode());

            String otp = otpController.text;
            if (otp.isEmpty) {
              showError(widget.isOTP ? "Enter OTP" : "Enter Your PIN");
              return;
            }
            if (!widget.isOTP && otp.length != 4) {
              showError("PIN must be exactly 4 digits");
              return;
            }

            Navigator.pop(context, otp);
          })
        ],
      );
}
