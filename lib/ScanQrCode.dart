import 'dart:io';

import 'package:Loopin/AppEngine.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

import 'assets.dart';

class ScanQrCode extends StatefulWidget {
  @override
  _ScanQrCodeState createState() => _ScanQrCodeState();
}

class _ScanQrCodeState extends State<ScanQrCode> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  Barcode result;
  QRViewController controller;
  List scannedData = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  dispose() {
    controller?.dispose();
    super.dispose();
  }

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller.pauseCamera();
    } else if (Platform.isIOS) {
      controller.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 150.0
        : 300.0;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          QRView(
            key: qrKey,
            onQRViewCreated: _onQRViewCreated,
            overlay: QrScannerOverlayShape(
                borderColor: Colors.red,
                borderRadius: 10,
                borderLength: 30,
                borderWidth: 10,
                cutOutSize: 350),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                if (scannedData.isNotEmpty)
                  Container(
                    margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: white,
                        border: Border.all()),
                    child: Column(
                      children: [Text("Detected Data \n\n$scannedData")],
                    ),
                  ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: white,
                              border: Border.all(color: red0, width: 4)),
                          child: Center(
                              child: Icon(
                            Icons.close,
                            color: red0,
                            size: 30,
                          )),
                        )),
                    if (scannedData.isNotEmpty)
                      GestureDetector(
                          onTap: () {
                            Navigator.pop(context, scannedData);
                          },
                          child: Container(
                            margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: white,
                                border: Border.all(color: green, width: 4)),
                            child: Center(
                                child: Icon(
                              Icons.check,
                              color: green,
                              size: 30,
                            )),
                          )),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    if (mounted) setState(() {});
    print("View Created");
    this.controller.scannedDataStream.listen((scanData) {
      result = scanData;
      print("ViewCreated ${scanData.code}");
      if (scanData.format != BarcodeFormat.qrcode) return;

      String currency = "";
      String address = "";
      String amount = "";

      String data = scanData.code;
      List splitList = data.split(":");
      //bitcoin:mqrfe6NuYzfzk9MK7htbw1YBSREMjx2ZPU?amount=0.5
      currency = splitList[0];

      print("splitList $splitList");

      if (splitList.length > 0) {
        List restList = splitList[1].split("?amount=");
        print("restList $restList");
        if (restList.isNotEmpty) {
          if (restList.length == 1) {
            address = restList[0];
          } else {
            address = restList[0];
            amount = restList[1];
          }
          if (restList.length == 2) amount = restList[1];
        } else {
          address = splitList[1];
        }
      } else {
        address = splitList[1];
      }

      //address = restList[0];

      scannedData = [currency, address, amount];
      //print("restList $restList");

      if (mounted) setState(() {});
    });
  }

  String stringValue(string, [List replace]) {
    for (var v in replace) {
      string = string.toString().replaceAll(v, "").replaceAll("]", "");
    }
    print("stringValue $string");
    return string;
  }
}
