import 'package:Loopin/forms/ApplyJob.dart';
import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/pay/PinOrOTPSheet.dart';
import 'package:Loopin/pay/WebAuthSheet.dart';
import 'package:Loopin/pay/payEngineV2.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final emailController = TextEditingController();
  final phoneController = MaskedTextController(mask: "0000-000-0000");
  final passController = TextEditingController();
  final rePassController = TextEditingController();

  final cardNumberController =
      new MaskedTextController(mask: '0000 **** **** 0000');
  final cardExpController = new MaskedTextController(mask: '00/00');
  final cardNameController = TextEditingController();
  final cardCvvController = TextEditingController();
  bool billingRequired = false;

  bool passVisible = false;
  bool termsAccepted = false;

  List<BaseModel> allSongWriters = [];
  bool songWriterBusy = true;

  int currentPage = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appColor,
      body: Stack(
        children: [
          Container(
            height: getScreenHeight(context) * 0.45,
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 20),
            decoration: BoxDecoration(
                color: white,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.elliptical(200, 50),
                  bottomRight: Radius.elliptical(200, 50),
                  // bottomRight: Radius.circular(200),
                )),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: LayoutBuilder(builder: (c, box) {
              final size = box.maxWidth;
              bool isLargeScreen = box.maxWidth > 850;
              final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

              return Center(
                child: Container(
                  margin: EdgeInsets.only(top: 60),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    elevation: 5,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(50, 50, 50, 50),
                      decoration: BoxDecoration(
                        color: white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      width: 500,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Center(
                            child: Image.asset(
                              "assets/icons/logo.png",
                              // scale: 1.6,
                              height: 60,
                              width: 60,
                            ),
                          ),
                          addSpace(20),
                          Center(
                            child: Text(
                              "Exmstaffing Portal",
                              style: TextStyle(
                                  fontSize: 18,
                                  color: black,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          if (false)
                            Row(
                              children: [
                                Image.asset(
                                  "assets/icons/Icon-192.png",
                                  // scale: 1.6,
                                  height: 50,
                                  width: 50,
                                ),
                                addSpaceWidth(10),
                                Text(
                                  "Exmstaffing Portal",
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: black,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          addSpace(20),
                          inputField("Email Address", emailController,
                              onChanged: (s) {
                            setState(() {});
                          }),
                          inputField("Password", passController,
                              isPass: true,
                              passwordVisible: passVisible, onVisiChanged: () {
                            passVisible = !passVisible;
                            setState(() {});
                          }, onChanged: (s) {
                            setState(() {});
                          }),
                          addSpace(20),
                          TextButton(
                            onPressed: () {
                              String email = emailController.text;
                              String pass = passController.text;

                              if (email.isEmpty) {
                                showError(context, "Enter Email address!");
                                return;
                              }
                              if (pass.isEmpty) {
                                showError(context, "Enter Password!");
                                return;
                              }

                              showProgress(true, context, msg: "Logging In");

                              FirebaseFirestore.instance
                                  .collection(USER_BASE)
                                  .where(EMAIL, isEqualTo: email.toLowerCase())
                                  .limit(1)
                                  .get()
                                  .then((value) async {
                                if (null == value || value.docs.isEmpty) {
                                  showProgress(false, context);
                                  showErrorDialog(
                                      context, "No user with record was found.",
                                      delay: 900);
                                  return;
                                }

                                final bm = BaseModel(doc: value.docs.first);
                                // if (bm.getInt(PRIVILEGE_INDEX) > 1) {
                                //   showProgress(false, context);
                                //   showErrorDialog(context,
                                //       "Sorry you don't have access to this page.Contact Admin",
                                //       delay: 900);
                                //   return;
                                // }

                                final pref =
                                    await SharedPreferences.getInstance();
                                userModel = BaseModel(doc: value.docs.first);
                                pref.setString("user", userModel.getObjectId());

                                Navigator.pushReplacementNamed(
                                    context, "/home");

                                // pushAndResult(context, HomePage(),
                                //     clear: true, fade: true);
                              }).catchError((e) {
                                showProgress(false, context);
                                showErrorDialog(context, e.toString(),
                                    delay: 900);
                              });
                              return;
                            },
                            child: Center(
                              child: Text(
                                "Login",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                            ),
                            style: TextButton.styleFrom(
                                primary: white,
                                backgroundColor: appColor,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                minimumSize: Size(300, 70)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            }),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              padding: EdgeInsets.all(10),
              child: Text(
                "Designed by TripleTime Technologies",
                style: TextStyle(
                    color: white, fontWeight: FontWeight.bold, fontSize: 14),
              ),
            ),
          )
        ],
      ),
    );
  }
}

showError(context, String err) {
  EdgeAlert.show(context,
      description: err,
      gravity: EdgeAlert.TOP,
      backgroundColor: red0,
      icon: Icons.info_outline);
}
