part of lucro;

class LucroCore {
  static LucroCore _instance;
  static LucroCore get instance {
    if (null == _instance) _instance = LucroCore();
    return _instance;
  }

  // LucroCore() {
  //   // getSupportedCountries();
  // }

  static List<Country> _supportedCountries = [];
  List<Country> get supportedCountries =>
      _supportedCountries.isEmpty ? allCountriesX : _supportedCountries;

  List<Country> allCountriesX = [
    Country(
        name: "United Kingdom",
        currency: "Pound sterling",
        currencyShort: "GBP",
        flag: "https://www.worldometers.info/img/flags/uk-flag.gif",
        dialCode: "+44",
        countryShort: "UK"),
    Country(
        name: "Nigeria",
        currency: "Naira",
        currencyShort: "NGN",
        flag: "https://www.worldometers.info/img/flags/ni-flag.gif",
        dialCode: "+234",
        countryShort: "NGN"),
    Country(
        name: "United States Of America",
        currency: "US Dollars",
        currencyShort: "USD",
        flag: "https://www.worldometers.info/img/flags/small/tn_us-flag.gif",
        dialCode: "+1",
        countryShort: "USA"),
    Country(
        name: "Canada",
        currency: "Canadian Dollars",
        currencyShort: "CAD",
        flag: "https://www.worldometers.info/img/flags/ca-flag.gif",
        dialCode: "+1",
        countryShort: "CA")
  ];

  static List<Offer> _myOffers = [];
  List<Offer> get myOffers => _myOffers;

  static List<Offer> _marketOffers = [];
  List<Offer> get marketOffers => _marketOffers;

  static List<Bid> _allBids = [];
  List<Bid> get allBids => _allBids;

  static List<Notifications> _allNotification = [];
  List<Notifications> get allNotification => _allNotification;

  String path = LucroApi.baseUrl + "/core";
  String cacheKey = "coreData";

  Future<List<Country>> getSupportedCountries() async {
    final url = Uri.parse(path + "/countries");
    if (null == LucroApi.headers) throw "LucroApi.headers not set";
    final response = await http.get(
      url,
      headers: LucroApi.headers,
    );

    print(response.request.url);
    print(response.request.headers);
    print(response.body);

    var data = jsonDecode(response.body);

    if (response.statusCode == 401) {
      LucroAuth.instance.clearUser();
      return [];
    }

    if (response.statusCode != 200) {
      var data = jsonDecode(response.body);
      var message = data["message"];
      String status = data["status"];

      throw message;
    }
    print(data);
    _supportedCountries = List.from(data).map((e) => Country.json(e)).toList();
    return _supportedCountries;
  }

  Future<Rate> checkRate(String fromCurrency, String toCurrency) async {
    final url = Uri.parse(path + "/check_rate");

    if (null == LucroApi.headers) throw "LucroApi.headers not set";

    final response = await http.post(
      url,
      headers: LucroApi.headers,
      body: jsonEncode(
          {"from_currency": fromCurrency, "to_currency": toCurrency}),
    );
    Map data = jsonDecode(response.body);
    var message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    return Rate.json(message);
  }

  Future<List<Offer>> getAllOffers([int limit = 100]) async {
    final url = Uri.parse(path + "/offers?limit=$limit");
    if (null == LucroApi.headers) throw "LucroApi.headers not set";
    final response = await http.get(url, headers: LucroApi.headers);

    Map data = jsonDecode(response.body);
    var message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }

    _marketOffers = List.from(message).map((e) => Offer.json(e)).toList();
    return _marketOffers;
  }

  Future<String> createOffer(Offer offer) async {
    final url = Uri.parse(path + "/create_offer");
    if (null == LucroApi.headers) throw "LucroApi.headers not set";

    final payload = {
      "from_amount": offer.fromAmount,
      "to_amount": offer.toAmount,
      "from_currency": offer.fromCurrency,
      "to_currency": offer.toCurrency,
      "rate": offer.rate,
      "recipient_account_name": offer.recipientAccountName,
      "recipient_account_number": offer.recipientAccountNumber,
      "recipient_bank": offer.recipientBank,
      "recipient_email": offer.recipientEmail,
      "recipient_phone": offer.recipientPhone,
      "recipient_country": offer.recipientCountry
    };

    print(payload);

    final response = await http.post(url,
        headers: LucroApi.headers, body: jsonEncode(payload));

    print(response.request.url);
    print(response.request.headers);

    Map data = jsonDecode(response.body);
    String message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    return message;
  }

  Future<List<Offer>> getPendingOffers([int limit = 100]) async {
    final url = Uri.parse(path + "/pending?limit=$limit");
    if (null == LucroApi.headers) throw "LucroApi.headers not set";
    final response = await http.get(url, headers: LucroApi.headers);

    Map data = jsonDecode(response.body);
    var message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    return List.from(message).map((e) => Offer.json(e)).toList();
  }

  Future<List<Offer>> getMyOffers([int limit = 100]) async {
    final url = Uri.parse(path + "/offer?limit=$limit");
    if (null == LucroApi.headers) throw "LucroApi.headers not set";
    final response = await http.get(url, headers: LucroApi.headers);

    print(response.request.url);
    print(response.request.headers);
    print(response.body);

    Map data = jsonDecode(response.body);
    var message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    _myOffers = List.from(message).map((e) => Offer.json(e)).toList();
    return _myOffers;
  }

  Future<List<Offer>> getOffersById(String id) async {
    final url = Uri.parse(path + "/offers/$id");
    if (null == LucroApi.headers) throw "LucroApi.headers not set";
    final response = await http.get(url, headers: LucroApi.headers);

    Map data = jsonDecode(response.body);
    var message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    return List.from(message).map((e) => Offer.json(e)).toList();
  }

  Future<String> editOffer(String id, Offer offer) async {
    final url = Uri.parse(path + "/offers/$id");
    if (null == LucroApi.headers) throw "LucroApi.headers not set";
    final response = await http.patch(url,
        headers: LucroApi.headers, body: jsonEncode(offer.toJson()));

    print(response.request.url);

    Map data = jsonDecode(response.body);
    var message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    return message;
    // return List.from(message).map((e) => Offer.json(e)).toList();
  }

  Future<String> deleteOffer(String id) async {
    final url = Uri.parse(path + "/offers/$id");
    if (null == LucroApi.headers) throw "LucroApi.headers not set";
    final response = await http.delete(
      url,
      headers: LucroApi.headers,
    );

    Map data = jsonDecode(response.body);
    var message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    _marketOffers.removeWhere((e) => e.id == id);
    _myOffers.removeWhere((e) => e.id == id);
    return message;
    // return List.from(message).map((e) => Offer.json(e)).toList();
  }

  Future<String> bidOnOffer(Bid bid) async {
    final url = Uri.parse(path + "/bid_offer");
    if (null == LucroApi.headers) throw "LucroApi.headers not set";
    final response = await http.post(
      url,
      body: jsonEncode(bid.toJson()),
      headers: LucroApi.headers,
    );

    print(bid.toJson());
    print(response.request.url);
    print(response.request.headers);
    print(response.body);

    Map data = jsonDecode(response.body);
    var message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    return message;
    // return List.from(message).map((e) => Offer.json(e)).toList();
  }

  Future<List<Bid>> getBidOnOffer([int limit = 100]) async {
    final url = Uri.parse(path + "/bids?limit=$limit");
    if (null == LucroApi.headers) throw "LucroApi.headers not set";
    final response = await http.get(url, headers: LucroApi.headers);

    Map data = jsonDecode(response.body);
    var message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    final _myOffers = List.from(message).map((e) => Bid.json(e)).toList();
    return _myOffers;
  }

  Future<List<Bid>> getAllBidsOnOffer(String offerId) async {
    final url = Uri.parse(path + "/get_bids/$offerId");
    if (null == LucroApi.headers) throw "LucroApi.headers not set";
    final response = await http.get(url, headers: LucroApi.headers);

    // print(response.request.url);
    // print(response.request.headers);
    // print(response.body);

    Map data = jsonDecode(response.body);
    var message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    final _myOffers = List.from(message).map((e) => Bid.json(e)).toList();
    return _myOffers;
  }

  Future<List<Bid>> getAllUserBids() async {
    final url = Uri.parse(path + "/user_bids");
    if (null == LucroApi.headers) throw "LucroApi.headers not set";
    final response = await http.get(url, headers: LucroApi.headers);

    Map data = jsonDecode(response.body);
    var message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    final _myOffers = List.from(message).map((e) => Bid.json(e)).toList();
    return _myOffers;
  }

  Future<String> approveBid(String offerId, String bidId) async {
    final url = Uri.parse(path + "/accept_bid");
    if (null == LucroApi.headers) throw "LucroApi.headers not set";
    final response = await http.post(url,
        body: jsonEncode({"offer_id": offerId, "bid_id": bidId}),
        headers: LucroApi.headers);

    Map data = jsonDecode(response.body);
    var message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    return message;
  }

  Future<String> rejectBid(String bidId) async {
    final url = Uri.parse(path + "/reject_bid");
    if (null == LucroApi.headers) throw "LucroApi.headers not set";
    final response = await http.post(url,
        body: jsonEncode({"bid_id": bidId}), headers: LucroApi.headers);

    Map data = jsonDecode(response.body);
    var message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    return message;
  }

  Future<List<Notifications>> getNotifications() async {
    final user = LucroAuth.instance.currentUser;
    final url = Uri.parse(path + "/notifications?uuid=${user.userId}");
    if (null == LucroApi.headers) throw "LucroApi.headers not set";
    final response = await http.get(url, headers: LucroApi.headers);

    print(response.request.url);
    print(response.body);

    Map data = jsonDecode(response.body);
    var message = data["message"];
    var items = message["data"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    _allNotification =
        List.from(items).map((e) => Notifications.json(e)).toList();
    return _allNotification;
  }
}
