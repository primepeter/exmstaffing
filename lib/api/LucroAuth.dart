part of lucro;

class LucroAuth {
  static LucroAuth _instance;
  static LucroAuth get instance {
    if (null == _instance) {
      _instance = LucroAuth();
    }
    return _instance;
  }

  static User _user;
  User get currentUser => _user;

  String path = LucroApi.baseUrl + "/accounts";
  String cacheKey = "currentUser";

  Future<String> getOtp(String email) async {
    final url = Uri.parse(path + "/reset_request");
    final response = await http.post(url, body: jsonEncode({"email": email}));

    Map data = jsonDecode(response.body);
    String message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    return message;
  }

  Future<String> resetPassword(
      {@required String email,
      @required String otp,
      @required String password}) async {
    final url = Uri.parse(path + "/reset_password");
    final response = await http.post(url,
        body:
            jsonEncode({"email": email, "otp": otp, "new_password": password}));

    Map data = jsonDecode(response.body);
    String message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    return message;
  }

  Future<String> registerUser(User user) async {
    final url = Uri.parse(path + "/register");
    final response = await http.post(url,
        body: jsonEncode({
          "email": user.email,
          "phone": user.phone,
          "first_name": user.firstName,
          "last_name": user.lastName,
          "password": user.password
        }));

    Map data = jsonDecode(response.body);
    String message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }

    // loginUser(user.email, user.password);

    return message;
  }

  Future<String> resendOtp(String email) async {
    final url = Uri.parse(path + "/resend_otp");
    final response = await http.post(url, body: jsonEncode({"email": email}));

    Map data = jsonDecode(response.body);
    String message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    return message;
  }

  Future<String> verifyOtp(String otp, String email) async {
    final url = Uri.parse(path + "/verify_otp");
    final response =
        await http.post(url, body: jsonEncode({"otp": otp, "email": email}));

    Map data = jsonDecode(response.body);
    String message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    return message;
  }

  Future<User> loginUser(String email, String password) async {
    final prefs = await SharedPreferences.getInstance();
    final url = Uri.parse(path + "/login");
    final response = await http.post(url,
        body: jsonEncode({"email": email, "password": password}));
    print(response.request.url);
    print(response.body);

    Map data = jsonDecode(response.body);
    var message = data["message"];
    String status = data["status"];
    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    await prefs.setString(cacheKey, jsonEncode(message));
    _user = User.json(message);
    LucroApi.headers = {
      "Authorization": "Token ${currentUser.token}",
      "Access-Control-Request-Headers": currentUser.userId,
      "Content-Type": "application/json"
    };
    await LucroApi.reload();
    return _user;
  }

  Future<String> logoutUser() async {
    final prefs = await SharedPreferences.getInstance();
    final url = Uri.parse(path + "/logout");

    if (null == LucroApi.headers) throw "LucroApi.headers not set";

    final response = await http.post(url, headers: LucroApi.headers);
    Map data = jsonDecode(response.body);
    String message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    await prefs.remove(cacheKey);
    LucroApi.headers = null;
    _user = null;
    return message;
  }

  Future<String> updatePassword(
    String oldPassword,
    String newPassword,
  ) async {
    final url = Uri.parse(path + "/update_password");
    final response = await http.post(url,
        body: jsonEncode({
          "current_password": oldPassword,
          "new_password": newPassword,
        }));

    Map data = jsonDecode(response.body);
    String message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    return message;
  }

  Future<String> updateProfile(
    User user,
  ) async {
    final url = Uri.parse(path + "/me/${user.id}");
    final response = await http.patch(url, body: jsonEncode(user.toJson));

    Map data = jsonDecode(response.body);
    String message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    return message;
  }

  Future<User> getProfile(String id, [bool mine = false]) async {
    final prefs = await SharedPreferences.getInstance();
    final url = Uri.parse(path + "/me/$id");
    final response = await http.get(
      url,
    );

    Map data = jsonDecode(response.body);
    var message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    if (mine) {
      _user = User.remap(message);
      await prefs.setString(cacheKey, jsonEncode(_user.toJson));
    }
    return User.json(message);
  }

  Future<void> clearUser() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove(cacheKey);
    LucroApi.headers = null;
    _user = null;
  }

  Future<User> approvalStatus(String email, String password) async {
    final url =
        Uri.parse(path + "/get_approval_status?email=sylvia.onwukwe@gmail.com");

    if (null == LucroApi.headers) throw "LucroApi.headers not set";

    final response = await http.post(url, headers: LucroApi.headers);
    Map data = jsonDecode(response.body);
    var message = data["message"];
    String status = data["status"];

    if (response.statusCode != 200 || status != "Success") {
      throw message;
    }
    _user = User.remap(message);
    return _user;
  }

  Future<void> loadAuth() async {
    final prefs = await SharedPreferences.getInstance();
    String userAsString = prefs.getString(cacheKey);
    if (null == userAsString) return;
    _user = User.json(jsonDecode(userAsString));

    LucroApi.headers = {
      "Authorization": "Token ${currentUser.token}",
      "Access-Control-Request-Headers": currentUser.userId,
      "Content-Type": "application/json"
    };
  }
}
