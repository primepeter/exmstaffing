library lucro;

import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

part 'model/User.dart';
part 'model/Rate.dart';
part 'model/Offer.dart';
part 'model/Bid.dart';
part 'model/Notifications.dart';
part 'model/Country.dart';
part 'LucroAuth.dart';
part 'LucroCore.dart';
part 'LucroApi.dart';
