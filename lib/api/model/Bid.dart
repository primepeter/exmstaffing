part of 'package:Loopin/api/Lucro.dart';

class Bid {
  String id;
  double proposedRate;
  String status;
  String recipientAccountName;
  String recipientAccountNumber;
  String recipientBank;
  String recipientEmail;
  String recipientPhone;
  String recipientCountry;
  String offerId;
  String userId;

  Bid({
    this.id,
    @required this.offerId,
    @required this.proposedRate,
    this.status,
    @required this.recipientAccountName,
    @required this.recipientAccountNumber,
    @required this.recipientBank,
    @required this.recipientEmail,
    @required this.recipientPhone,
    @required this.recipientCountry,
    this.userId,
  });

  Bid.json(Map json) {
    this.id = json["id"].toString();
    this.proposedRate = json["proposed_rate"] ?? 0.0;
    this.status = json["status"] ?? "";
    this.recipientAccountName = json["recipient_account_name"] ?? "";
    this.recipientAccountNumber = json["recipient_account_number"] ?? "";
    this.recipientBank = json["recipient_bank"] ?? "";
    this.recipientEmail = json["recipient_email"] ?? "";
    this.recipientPhone = json["recipient_phone"] ?? "";
    this.recipientCountry = json["recipient_country"] ?? "";
    this.userId = json["user_id"].toString() ?? "";
    this.offerId = json["offer_id"].toString() ?? "";
  }

  void remap(Map json) {
    this.id = json["id"] ?? this.id;
    this.proposedRate = json["proposed_rate"] ?? this.proposedRate;
    this.status = json["status"] ?? this.status;
    this.recipientAccountName =
        json["recipient_account_name"] ?? this.recipientAccountName;
    this.recipientAccountNumber =
        json["recipient_account_number"] ?? this.recipientAccountNumber;
    this.recipientBank = json["recipient_bank"] ?? this.recipientBank;
    this.recipientEmail = json["recipient_email"] ?? this.recipientEmail;
    this.recipientPhone = json["recipient_phone"] ?? this.recipientPhone;
    this.recipientCountry = json["recipient_country"] ?? this.recipientCountry;
    this.userId = json["user_id"].toString() ?? this.userId;
    this.offerId = json["offer_id"].toString() ?? this.offerId;
  }

  Map toJson() {
    return {
      "id": this.id,
      "proposed_rate": this.proposedRate,
      "status": this.status,
      "recipient_account_name": this.recipientAccountName,
      "recipient_account_number": this.recipientAccountNumber,
      "recipient_bank": this.recipientBank,
      "recipient_email": this.recipientEmail,
      "recipient_phone": this.recipientPhone,
      "recipient_country": this.recipientCountry,
      "user_id": this.userId,
      "offer_id": this.offerId,
    };
  }
}
