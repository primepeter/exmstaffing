part of 'package:Loopin/api/Lucro.dart';

class Country {
  String id;
  String name;
  String currency;
  String currencyShort;
  String countryShort;
  String flag;
  String dialCode;

  Country({
    this.id,
    @required this.name,
    @required this.currency,
    @required this.currencyShort,
    @required this.countryShort,
    @required this.flag,
    @required this.dialCode,
  });

  Country.json(Map json) {
    this.id = json["id"].toString();
    this.name = json["name"];
    this.currency = json["currency"];
    this.currencyShort = json["currency_short_code"];
    this.countryShort = json["country_short_code"];
    this.flag = json["flag"];
    this.dialCode = json["country_dial_code"];
  }

  void remap(Map json) {
    this.id = json["id"] ?? this.id;
    this.name = json["name"] ?? this.name;
    this.currency = json["currency"] ?? this.currency;
    this.currencyShort = json["currency_short_code"] ?? this.currencyShort;
    this.countryShort = json["country_short_code"] ?? this.countryShort;
    this.flag = json["flag"] ?? this.flag;
    this.dialCode = json["country_dial_code"] ?? this.dialCode;
  }

  Map toJson() {
    return {
      "id": this.id,
      "name": this.name,
      "currency": this.currency,
      "currency_short_code": this.currencyShort,
      "country_short_code": this.countryShort,
      "flag": this.flag,
      "country_dial_code": this.dialCode
    };
  }
}
