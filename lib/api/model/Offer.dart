part of 'package:Loopin/api/Lucro.dart';

class Offer {
  String id;
  double fromAmount;
  double toAmount;
  String fromCurrency;
  String toCurrency;
  double rate;
  String status;
  String recipientAccountName;
  String recipientAccountNumber;
  String recipientBank;
  String recipientEmail;
  String recipientPhone;
  String recipientCountry;
  String timestamp;
  String userId;
  String uuid;
  String userUid;
  List bidsOnOffer;

  Offer({
    this.id,
    @required this.fromAmount,
    @required this.toAmount,
    @required this.fromCurrency,
    @required this.toCurrency,
    @required this.rate,
    this.status,
    this.recipientAccountName,
    this.recipientAccountNumber,
    this.recipientBank,
    this.recipientEmail,
    this.recipientPhone,
    this.recipientCountry,
    this.timestamp,
    this.userId,
    this.uuid,
    this.userUid,
    this.bidsOnOffer,
  });

  Offer.json(Map json) {
    this.id = json["id"].toString();
    this.fromAmount = json["from_amount"] ?? 0;
    this.toAmount = json["to_amount"] ?? 0;
    this.fromCurrency = json["from_currency"] ?? "";
    this.toCurrency = json["to_currency"] ?? "";
    this.rate = json["rate"] ?? 0;
    this.status = json["status"] ?? "";
    this.recipientAccountName = json["recipient_account_name"] ?? "";
    this.recipientAccountNumber = json["recipient_account_number"] ?? "";
    this.recipientBank = json["recipient_bank"] ?? "";
    this.recipientEmail = json["recipient_email"] ?? "";
    this.recipientPhone = json["recipient_phone"] ?? "";
    this.recipientCountry = json["recipient_country"] ?? "";
    this.timestamp = json["timestamp"].toString() ?? "";
    this.userId = json["user_id"].toString() ?? "";
    this.uuid = json["uuid"].toString() ?? "";
    this.userUid = json["user_uid"].toString() ?? "";
    this.bidsOnOffer = List.from(json["bidsOnOffer"] ?? []);
  }

  void remap(Map json) {
    this.id = json["id"] ?? this.id;
    this.fromAmount = num.parse(json["from_amount"]) ?? this.fromAmount;
    this.toAmount = num.parse(json["to_amount"]) ?? this.toAmount;
    this.fromCurrency = json["from_currency"] ?? this.fromCurrency;
    this.toCurrency = json["to_currency"] ?? this.toCurrency;
    this.rate = num.parse(json["rate"]) ?? this.rate;
    this.status = json["status"] ?? this.status;
    this.recipientAccountName =
        json["recipient_account_name"] ?? this.recipientAccountName;
    this.recipientAccountNumber =
        json["recipient_account_number"] ?? this.recipientAccountNumber;
    this.recipientBank = json["recipient_bank"] ?? this.recipientBank;
    this.recipientEmail = json["recipient_email"] ?? this.recipientEmail;
    this.recipientPhone = json["recipient_phone"] ?? this.recipientPhone;
    this.recipientCountry = json["recipient_country"] ?? this.recipientCountry;
    this.timestamp = json["timestamp"].toString() ?? this.timestamp;
    this.userId = json["user_id"].toString() ?? this.userId;
    this.uuid = json["uuid"].toString() ?? this.userId;
    this.userUid = json["user_uid"].toString() ?? this.userUid;
    this.bidsOnOffer = json["bidsOnOffer"] ?? this.bidsOnOffer;
  }

  Map toJson() {
    return {
      "id": this.id,
      "from_amount": this.fromAmount,
      "to_amount": this.toAmount,
      "from_currency": this.fromCurrency,
      "to_currency": this.toCurrency,
      "rate": this.rate,
      "status": this.status,
      "recipient_account_name": this.recipientAccountName,
      "recipient_account_number": this.recipientAccountNumber,
      "recipient_bank": this.recipientBank,
      "recipient_email": this.recipientEmail,
      "recipient_phone": this.recipientPhone,
      "recipient_country": this.recipientCountry,
      "timestamp": this.timestamp,
      "user_id": this.userId,
      "uuid": this.uuid,
      "user_uid": this.userUid,
      "bidsOnOffer": this.bidsOnOffer,
    };
  }
}
