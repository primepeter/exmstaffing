part of 'package:Loopin/api/Lucro.dart';

class Rate {
  String from;
  String to;
  double amount;
  double rate;

  Rate(
      {@required this.from,
      @required this.to,
      @required this.amount,
      @required this.rate});

  Rate.json(Map json) {
    this.from = json["from"] ?? "";
    this.to = json["to"] ?? "";
    this.amount = num.parse(json["amount"].toString()).toDouble() ?? 0.0;
    this.rate = num.parse(json["rate"].toString()) ?? 0.0;
  }

  Map get toJson {
    return {
      "from": this.from,
      "to": this.to,
      "amount": this.amount,
      "rate": this.rate
    };
  }
}
