part of 'package:Loopin/api/Lucro.dart';

class User {
  String email;
  String phone;
  String firstName;
  String lastName;
  String password;
  String token;
  String userId;
  String id;
  bool isApproved;
  bool isDeactivated;

  String company;
  String address;
  String city;
  String country;
  String postalCode;
  String bankAccountNumber;
  String bankAccountName;
  String internationalPassportNumber;
  String driverLicenseNumber;
  String bankName;
  String socialSecurityNumber;
  String mothersMaidenName;
  String middleName;
  String dateOfBirth;
  String gender;
  String jobPosition;

  User(
      {@required this.email,
      @required this.phone,
      @required this.firstName,
      @required this.lastName,
      @required this.password,
      this.token,
      this.userId,
      this.id,
      this.isApproved,
      this.isDeactivated,
      this.company,
      this.address,
      this.city,
      this.country,
      this.postalCode,
      this.bankAccountNumber,
      this.bankAccountName,
      this.internationalPassportNumber,
      this.driverLicenseNumber,
      this.bankName,
      this.socialSecurityNumber,
      this.mothersMaidenName,
      this.middleName,
      this.dateOfBirth,
      this.gender,
      this.jobPosition});

  User.json(Map json) {
    this.email = json["email"] ?? "";
    this.phone = json["phone"] ?? "";
    this.firstName = json["first_name"] ?? "";
    this.lastName = json["last_name"] ?? "";
    this.password = json["password"] ?? "";
    this.token = json["token"] ?? "";
    this.userId = json["uuid"] ?? "";
    this.id = json["id"].toString() ?? "";
    this.isApproved = json["is_approved"] ?? false;
    this.isDeactivated = json["is_deactivated"] ?? false;

    this.company = json["company"] ?? "";
    this.address = json["address"] ?? "";
    this.city = json["city"] ?? "";
    this.country = json["country"] ?? "";
    this.postalCode = json["postal_code"] ?? "";
    this.bankAccountNumber = json["bank_account_number"] ?? "";
    this.internationalPassportNumber =
        json["international_passport_number"] ?? "";
    this.driverLicenseNumber = json["driver_license_number"] ?? "";
    this.bankName = json["bank_name"] ?? "";
    this.socialSecurityNumber = json["social_security_number"] ?? "";
    this.mothersMaidenName = json["mothers_maiden_name"] ?? "";
    this.dateOfBirth = json["date_of_birth"] ?? "";
    this.gender = json["gender"] ?? "";
    this.jobPosition = json["job_position"] ?? "";
  }

  User.remap(Map json) {
    this.email = json["email"] ?? this.email;
    this.phone = json["phone"] ?? this.phone;
    this.firstName = json["first_name"] ?? this.firstName;
    this.middleName = json["middle_name"] ?? this.middleName;
    this.lastName = json["last_name"] ?? this.lastName;
    this.password = json["password"] ?? this.password;
    this.token = json["token"] ?? this.token;
    this.userId = json["uuid"] ?? this.userId;
    this.id = json["id"] ?? this.id;
    this.isApproved = json["is_approved"] ?? this.isApproved;
    this.isDeactivated = json["is_deactivated"] ?? this.isDeactivated;
    this.company = json["company"] ?? this.company;
    this.address = json["address"] ?? this.address;
    this.city = json["city"] ?? this.city;
    this.country = json["country"] ?? this.country;
    this.postalCode = json["postal_code"] ?? this.postalCode;
    this.bankAccountNumber =
        json["bank_account_number"] ?? this.bankAccountNumber;
    this.internationalPassportNumber = json["international_passport_number"] ??
        this.internationalPassportNumber;
    this.driverLicenseNumber =
        json["driver_license_number"] ?? this.driverLicenseNumber;
    this.bankName = json["bank_name"] ?? this.bankName;
    this.socialSecurityNumber =
        json["social_security_number"] ?? this.socialSecurityNumber;
    this.mothersMaidenName =
        json["mothers_maiden_name"] ?? this.mothersMaidenName;
    this.dateOfBirth = json["date_of_birth"] ?? this.dateOfBirth;
    this.gender = json["gender"] ?? this.gender;
    this.jobPosition = json["job_position"] ?? this.jobPosition;
  }

  Map get toJson {
    return {
      "email": this.email,
      "phone": this.phone,
      "first_name": this.firstName,
      "last_name": this.lastName,
      "password": this.password,
      "token": this.token,
      "uuid": this.userId,
      "id": this.id,
      "is_approved": this.isApproved,
      "is_deactivated": this.isDeactivated,
      "company": null,
      "address": null,
      "city": null,
      "country": null,
      "postal_code": null,
      "bank_account_number": null,
      "bank_account_name": null,
      "international_passport_number": null,
      "driver_license_number": null,
      "bank_name": null,
      "social_security_number": null,
      "mothers_maiden_name": null,
      "middle_name": null,
      "date_of_birth": null,
      "gender": null,
      "job_position": null,
    };
  }
}
