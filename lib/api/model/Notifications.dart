part of 'package:Loopin/api/Lucro.dart';

class Notifications {
  String id;
  String message;
  String status;
  String timestamp;
  String receiverId;
  String type;

  double bidAmount;
  double offerAmount;
  String offerId;
  String bidId;

  Notifications({
    @required this.id,
    @required this.message,
    @required this.status,
    @required this.timestamp,
    @required this.receiverId,
  });

  Notifications.json(Map json) {
    this.id = json["id"].toString() ?? "";
    this.message = json["message"] ?? "";
    this.status = json["status"] ?? "";
    this.timestamp = json["timestamp"] ?? "";
    this.receiverId = json["receiver_id"].toString() ?? "";
    this.bidId = json["bid_id"].toString() ?? "";
    this.offerId = json["offer_id"].toString() ?? "";
    this.type = json["type"] ?? "";
    this.offerAmount = json["offer_amount"] ?? 0.0;
    this.bidAmount = json["bid_amount"] ?? 0.0;
  }

  Map get toJson {
    return {
      "id": this.id,
      "message": this.message,
      "status": this.status,
      "timestamp": this.timestamp,
      "type": this.type,
      "offer_amount": this.offerAmount,
      "bid_amount": this.bidAmount,
      "receiver_id": this.receiverId,
      "offer_id": this.offerId,
      "bid_id": this.bidId
    };
  }
}
