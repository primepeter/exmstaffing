part of lucro;

class LucroApi {
  static Map<String, String> headers;
  static String baseUrl;

  static initialize(String url) async {
    baseUrl = url;
    await LucroAuth.instance.loadAuth();
    if (null != LucroAuth._user) reload();
  }

  static reload() async {
    try {
      await LucroCore.instance.getMyOffers();
      await LucroCore.instance.getSupportedCountries();
      await LucroCore.instance.getAllOffers();
      await LucroCore.instance.getNotifications();
      String userId = LucroAuth.instance.currentUser.id;
      await LucroAuth.instance.getProfile(userId, true);
    } catch (e) {
      print("reload error $e");
    }
  }
}
