import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/assets.dart';

class messageDialog extends StatefulWidget {
  var icon;
  Color iconColor;
  String title;
  String message;
  String yesText;
  String noText;
  bool cancellable;
  double iconPadding;

  messageDialog(
      this.icon, this.iconColor, this.title, this.message, this.yesText,
      {this.noText,
      bool this.cancellable = false,
      double this.iconPadding = 0});
  @override
  _messageDialogState createState() => _messageDialogState();
}

class _messageDialogState extends State<messageDialog> {
  var icon;
  Color iconColor;
  String title;
  String message;
  String yesText;
  String noText;
  bool cancellable;
  double iconPadding;
  bool showBack = true;
  bool hideUI = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    icon = widget.icon;
    iconColor = widget.iconColor;
    title = widget.title;
    message = widget.message;
    yesText = widget.yesText;
    noText = widget.noText;
    cancellable = widget.cancellable;
    iconPadding = widget.iconPadding;

    Future.delayed(Duration(milliseconds: 100), () {
      hideUI = false;
      setState(() {});
    });
    Future.delayed(Duration(milliseconds: 300), () {
      showBack = true;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: EdgeInsets.fromLTRB(40, 0, 40, 0),
        constraints: BoxConstraints(maxHeight: 350, maxWidth: 500),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.center,
              child: new Card(
                clipBehavior: Clip.antiAlias,
                color: white,
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15)),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
//              crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 50, 20, 15),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            title,
                            style: textStyle(true, 20, black),
                            textAlign: TextAlign.center,
                          ),
                          if (message.isNotEmpty) addSpace(10),
                          if (message.isNotEmpty)
                            Flexible(
                              child: SingleChildScrollView(
                                child: Text(
                                  "$message",
                                  style: textStyle(
                                      false, 16, black.withOpacity(.5)),
                                  textAlign: TextAlign.center,
                                  maxLines: 6,
//                                    maxLines: 1,
                                ),
                              ),
                            ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                      child: Row(
                        //mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              height: 50,
                              child: TextButton(
                                  style: TextButton.styleFrom(
                                    tapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    backgroundColor: appColor,
                                  ),
                                  onPressed: () {
                                    closePage(() async {
//                                    await Future.delayed(Duration(milliseconds: 1000));
                                      Navigator.pop(context, true);
                                    });
                                  },
                                  child: Text(
                                    yesText,
                                    maxLines: 1,
                                    style: textStyle(true, 18, white),
                                  )),
                            ),
                          ),
                          if (noText != null) addSpaceWidth(10),
                          if (noText != null)
                            Expanded(
                              child: Container(
                                height: 50,
                                child: TextButton(
                                    style: TextButton.styleFrom(
                                        tapTargetSize:
                                            MaterialTapTargetSize.shrinkWrap,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        backgroundColor: white,
                                        primary: red),
                                    onPressed: () {
                                      closePage(() async {
//                                    await Future.delayed(Duration(milliseconds: 1000));
                                        Navigator.pop(context, false);
                                      });
                                    },
                                    child: Text(
                                      noText,
                                      maxLines: 1,
                                      style: textStyle(true, 18, red),
                                    )),
                              ),
                            ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                  width: 60,
                  height: 60,
                  margin: EdgeInsets.only(top: 30),
                  padding:
                      EdgeInsets.all(iconPadding == null ? 0 : iconPadding),
                  decoration: BoxDecoration(
                    color: white,
                    boxShadow: [
                      BoxShadow(
                          color: black.withOpacity(.09),
                          spreadRadius: 5,
                          blurRadius: 5)
                    ],
                    border: Border.all(color: iconColor, width: 8),
                    shape: BoxShape.circle,
                  ),
                  child: (icon is String)
                      ? (Image.asset(
                          icon,
                          color: iconColor,
                          width: 35,
                          height: 35,
                        ))
                      : Icon(
                          icon,
                          color: iconColor,
                          size: 35,
                        )),
            ),
          ],
        ),
      ),
    );

    return WillPopScope(
      onWillPop: () async {
        if (cancellable) Navigator.pop(context);
        // closePage(() {
        //   Navigator.pop(context);
        // });
        return false;
      },
      child: Stack(fit: StackFit.expand, children: <Widget>[
        GestureDetector(
          onTap: () {
            if (cancellable)
              closePage(() {
                Navigator.pop(context);
              });
          },
          child: AnimatedOpacity(
            opacity: showBack ? 1 : 0,
            duration: Duration(milliseconds: 300),
            child: ClipRect(
                child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
                    child: Container(
                      color: black.withOpacity(.7),
                    ))),
          ),
        ),
        page()
      ]),
    );
  }

  page() {
    return OrientationBuilder(
      builder: (c, o) {
        return AnimatedOpacity(
          opacity: hideUI ? 0 : 1,
          duration: Duration(milliseconds: 400),
          child: Center(
            child: Container(
              margin: EdgeInsets.fromLTRB(40, 0, 40, 0),
              constraints: BoxConstraints(maxHeight: 350, maxWidth: 500),
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: new Card(
                      clipBehavior: Clip.antiAlias,
                      color: white,
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
//              crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.fromLTRB(20, 50, 20, 15),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  title,
                                  style: textStyle(true, 20, black),
                                  textAlign: TextAlign.center,
                                ),
                                if (message.isNotEmpty) addSpace(10),
                                if (message.isNotEmpty)
                                  Flexible(
                                    child: SingleChildScrollView(
                                      child: Text(
                                        "$message",
                                        style: textStyle(
                                            false, 16, black.withOpacity(.5)),
                                        textAlign: TextAlign.center,
                                        maxLines: 6,
//                                    maxLines: 1,
                                      ),
                                    ),
                                  ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: Row(
                              //mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    height: 50,
                                    child: TextButton(
                                        style: TextButton.styleFrom(
                                          tapTargetSize:
                                              MaterialTapTargetSize.shrinkWrap,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          backgroundColor: appColor,
                                        ),
                                        onPressed: () {
                                          closePage(() async {
//                                    await Future.delayed(Duration(milliseconds: 1000));
                                            Navigator.pop(context, true);
                                          });
                                        },
                                        child: Text(
                                          yesText,
                                          maxLines: 1,
                                          style: textStyle(true, 18, white),
                                        )),
                                  ),
                                ),
                                if (noText != null) addSpaceWidth(10),
                                if (noText != null)
                                  Expanded(
                                    child: Container(
                                      height: 50,
                                      child: TextButton(
                                          style: TextButton.styleFrom(
                                              tapTargetSize:
                                                  MaterialTapTargetSize
                                                      .shrinkWrap,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              backgroundColor: white,
                                              primary: red),
                                          onPressed: () {
                                            closePage(() async {
//                                    await Future.delayed(Duration(milliseconds: 1000));
                                              Navigator.pop(context, false);
                                            });
                                          },
                                          child: Text(
                                            noText,
                                            maxLines: 1,
                                            style: textStyle(true, 18, red),
                                          )),
                                    ),
                                  ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                        width: 60,
                        height: 60,
                        margin: EdgeInsets.only(top: 30),
                        padding: EdgeInsets.all(
                            iconPadding == null ? 0 : iconPadding),
                        decoration: BoxDecoration(
                          color: white,
                          boxShadow: [
                            BoxShadow(
                                color: black.withOpacity(.09),
                                spreadRadius: 5,
                                blurRadius: 5)
                          ],
                          border: Border.all(color: iconColor, width: 8),
                          shape: BoxShape.circle,
                        ),
                        child: (icon is String)
                            ? (Image.asset(
                                icon,
                                color: iconColor,
                                width: 35,
                                height: 35,
                              ))
                            : Icon(
                                icon,
                                color: iconColor,
                                size: 35,
                              )),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  closePage(onClosed) {
    showBack = false;
    setState(() {});
    Future.delayed(Duration(milliseconds: 100), () {
      Future.delayed(Duration(milliseconds: 100), () {
        hideUI = true;
        setState(() {});
      });
      onClosed();
    });
  }
}
