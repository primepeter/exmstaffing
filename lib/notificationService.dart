import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:http/http.dart';
import 'package:meta/meta.dart';

class NotificationService {
  static final Client client = Client();

  static const String serverKey =
      "AAAAaxlK76U:APA91bEeu-8qK8JlI6v9V2fcJcBnOUiat8U9-ls3xIoHXSuV07IGinv4vNqzVBbTvC9e-aBThiRXr6WsD0RlR4e8Tyy_g3AJ2bq_b-dtqD2pfXfQMukNcudY61lRfGOjdpXF04Ilcjhq";

  static sendPush({
    String topic,
    String token,
    int liveTimeInSeconds = (Duration.secondsPerDay * 7),
    String title,
    String body,
    String image,
    Map data,
    String tag,
  }) async {
    String fcmToken = topic != null ? '/topics/$topic' : token;
    data = data ?? Map();
    data['click_action'] = 'FLUTTER_NOTIFICATION_CLICK';
    data['id'] = '1';
    data['status'] = 'done';
    client.post(
      Uri.parse('https://fcm.googleapis.com/fcm/send'),
      body: json.encode({
        'notification': {
          'body': body,
          'title': title,
          'image': image,
          'icon': "ic_notify",
          'color': "#ffffff",
          'tag': tag
        },
        'data': data,
        'to': fcmToken,
        'time_to_live': liveTimeInSeconds
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'key=$serverKey',
      },
    );
  }
}
