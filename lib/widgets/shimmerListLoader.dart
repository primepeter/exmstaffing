import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/assets.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

shimmerListLoader({int count = 10, bool isNotification = false}) {
  return ListView.builder(
      itemCount: count,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (c, p) {
        if (isNotification)
          return Container(
            decoration: BoxDecoration(
                color: white,
                boxShadow: [
                  BoxShadow(
                      color: black.withOpacity(.05),
                      blurRadius: 5,
                      spreadRadius: 2)
                ],
                borderRadius: BorderRadius.circular(8)),
            padding: EdgeInsets.all(20),
            margin: EdgeInsets.fromLTRB(5, 20, 5, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(30),
                      child: Shimmer.fromColors(
                        baseColor: black.withOpacity(.15),
                        highlightColor: black.withOpacity(.1),
                        child: Container(
                          height: 30,
                          width: 30,
                          color: white,
                        ),
                      ),
                    ),
                  ],
                ),
                addSpaceWidth(10),
                Expanded(
                  child: Container(
                    alignment: Alignment.centerLeft,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Shimmer.fromColors(
                          baseColor: black.withOpacity(.15),
                          highlightColor: black.withOpacity(.1),
                          child: Container(
                            height: 8,
                            width: 120,
                            color: white,
                          ),
                        ),
                        addSpace(5),
                        Shimmer.fromColors(
                          baseColor: black.withOpacity(.15),
                          highlightColor: black.withOpacity(.1),
                          child: Container(
                            height: 5,
                            width: 80,
                            color: white,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        return Container(
          decoration: BoxDecoration(
              color: white,
              boxShadow: [
                BoxShadow(
                    color: black.withOpacity(.05),
                    blurRadius: 5,
                    spreadRadius: 2)
              ],
              borderRadius: BorderRadius.circular(8)),
          padding: EdgeInsets.all(20),
          margin: EdgeInsets.fromLTRB(5, 20, 5, 0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Container(
                      alignment: Alignment.centerLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Shimmer.fromColors(
                            baseColor: black.withOpacity(.15),
                            highlightColor: black.withOpacity(.1),
                            child: Container(
                              height: 8,
                              width: 120,
                              color: white,
                            ),
                          ),
                          addSpace(5),
                          Shimmer.fromColors(
                            baseColor: black.withOpacity(.15),
                            highlightColor: black.withOpacity(.1),
                            child: Container(
                              height: 5,
                              width: 80,
                              color: white,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Column(
                    children: [
                      Shimmer.fromColors(
                        baseColor: black.withOpacity(.15),
                        highlightColor: black.withOpacity(.1),
                        child: Image.asset(
                          "assets/icons/arrow_right.png",
                          height: 20,
                          color: white,
                        ),
                      ),
                    ],
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.centerRight,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Shimmer.fromColors(
                            baseColor: black.withOpacity(.15),
                            highlightColor: black.withOpacity(.1),
                            child: Container(
                              height: 8,
                              width: 120,
                              color: white,
                            ),
                          ),
                          addSpace(5),
                          Shimmer.fromColors(
                            baseColor: black.withOpacity(.15),
                            highlightColor: black.withOpacity(.1),
                            child: Container(
                              height: 5,
                              width: 80,
                              color: white,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      });
}
