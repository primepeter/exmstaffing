import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/assets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

Widget inputField(String hint, TextEditingController controller,
    {bool isPass = false,
    bool passwordVisible = false,
    bool readOnly = false,
    bool isNum = false,
    FocusNode focusNode,
    onVisiChanged,
    // TextInputType inputType,
    // inputFormatters,
    onChanged,
    onRemoved,
    int maxLines}) {
  return Container(
    // height: 70,
    constraints: BoxConstraints(minHeight: null == maxLines ? 70 : 150),
    child: Stack(
      children: [
        Container(
          //height: 60,
          constraints: BoxConstraints(minHeight: null == maxLines ? 60 : 130),
          margin: EdgeInsets.only(bottom: 15),
          padding: EdgeInsets.only(
              left: 10,
              right: null != onRemoved ? 0 : 10,
              top: null == maxLines ? 0 : 10),
          // height: 65,
          // margin: EdgeInsets.only(bottom: 10, top: 0),
          // padding: EdgeInsets.only(left: 10, right: 0),

          decoration: BoxDecoration(
              color: Color(0XFFFAFAFA),
              border: Border.all(color: Color(0XFF1F1F1F1)),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            crossAxisAlignment: null == maxLines
                ? CrossAxisAlignment.center
                : CrossAxisAlignment.start,
            children: [
              Expanded(
                child: TextField(
                  controller: controller,
                  // keyboardType: inputType,
                  keyboardType:
                      isNum ? TextInputType.number : TextInputType.text,
                  inputFormatters: [
                    if (isNum) FilteringTextInputFormatter.digitsOnly,
                  ],
                  readOnly: readOnly,

                  maxLines: maxLines ?? 1,
                  obscureText: isPass ? (!passwordVisible) : false,
                  decoration:
                      InputDecoration(border: InputBorder.none, hintText: hint),
                  onChanged: (s) {
                    if (null != onChanged) onChanged(s);
                  },
                ),
              ),
              if (isPass)
                GestureDetector(
                    onTap: () {
                      onVisiChanged();
                    },
                    child: Text(
                      passwordVisible ? "HIDE" : "SHOW",
                      style: TextStyle(
                          fontSize: 12, color: Colors.black.withOpacity(.5)),
                    )),
              if (null != onRemoved)
                InkWell(
                  onTap: onRemoved,
                  child: Container(
                    height: 65,
                    width: 120,
                    padding: EdgeInsets.only(left: 18, right: 18),
                    decoration: BoxDecoration(
                        color: Color(0XFF0B123E),
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(10),
                          bottomRight: Radius.circular(10),
                        )),
                    alignment: Alignment.center,
                    child: Row(
                      children: [
                        Icon(
                          Icons.clear,
                          size: 16,
                          color: white,
                        ),
                        addSpaceWidth(4),
                        Text(
                          "Remove",
                          style: TextStyle(
                              color: white,
                              fontSize: 14,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ),
                )
            ],
          ),
        ),
        if ((null != controller && controller.text.isNotEmpty))
          Align(
              alignment: Alignment.topLeft,
              child: Container(
                child: Text(
                  hint,
                  style: TextStyle(color: white, fontSize: 10),
                ),
                padding: EdgeInsets.fromLTRB(5, 2, 5, 2),
                decoration: BoxDecoration(
                    color: appColor,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                    )),
              ))
      ],
    ),
  );
}

Widget clickField(String hint, String value, onTap) {
  return GestureDetector(
    onTap: onTap,
    child: Container(
      // height: 70,
      constraints: BoxConstraints(minHeight: 70),
      child: Stack(
        children: [
          Container(
            //height: 60,
            constraints: BoxConstraints(minHeight: 60),
            margin: EdgeInsets.only(top: 0, bottom: 15),
            padding: EdgeInsets.only(left: 10, right: 10, top: 0),

            decoration: BoxDecoration(
                color: Color(0XFFFAFAFA),
                border: Border.all(color: Color(0XFF1F1F1F1)),
                borderRadius: BorderRadius.circular(10)),
            alignment: Alignment.centerLeft,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      null == value || value.isEmpty ? hint : value,
                      style: TextStyle(
                          color: black.withOpacity(
                              null == value || value.isEmpty ? 0.7 : 1),
                          fontSize: 16),
                    ),
                  ),
                ),
                if (null != onTap)
                  Icon(
                    Icons.navigate_next,
                    color: black
                        .withOpacity(null == value || value.isEmpty ? 0.7 : 1),
                  )
              ],
            ),
          ),
          if ((null != value && value.isNotEmpty))
            Align(
                alignment: Alignment.topLeft,
                child: Container(
                  child: Text(
                    hint,
                    style: TextStyle(color: white, fontSize: 10),
                  ),
                  padding: EdgeInsets.fromLTRB(5, 2, 5, 2),
                  decoration: BoxDecoration(
                      color: appColor,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10),
                      )),
                ))
        ],
      ),
    ),
  );
}

yesNoField(String title, int selectedIndex, onSelected) {
  return Container(
    margin: EdgeInsets.only(top: 0, bottom: 15),
    padding: EdgeInsets.only(left: 10, right: 10, top: 0),
    child: Row(
      children: [
        Text(
          title,
          style: TextStyle(
              fontSize: 14,
              color: black.withOpacity(.5),
              fontWeight: FontWeight.bold),
        ),
        addSpaceWidth(10),
        Wrap(
          children: List.generate(2, (p) {
            String title = p == 0 ? "Yes" : "No";
            bool active = p == selectedIndex;
            return GestureDetector(
              onTap: () {
                onSelected(p);
              },
              child: Container(
                height: 40,
                width: 50,
                margin: EdgeInsets.all(2),
                child: Text(
                  title,
                  style: TextStyle(
                      color: active ? white : black,
                      fontSize: 13,
                      fontWeight: active ? FontWeight.bold : FontWeight.normal),
                ),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: active ? (black) : white,
                    boxShadow: [
                      BoxShadow(
                          color: black.withOpacity(.05),
                          spreadRadius: 2,
                          blurRadius: 5)
                    ],
                    borderRadius: BorderRadius.circular(8)),
              ),
            );
          }),
        ),
      ],
    ),
  );
}

groupFields(String title, List items, selectedItem, onSelected,
    {double width = 300}) {
  return Container(
    margin: EdgeInsets.only(top: 0, bottom: 15),
    padding: EdgeInsets.only(left: 10, right: 10, top: 0),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(
              fontSize: 14,
              color: black.withOpacity(.5),
              fontWeight: FontWeight.bold),
        ),
        addSpace(10),
        Wrap(
          children: List.generate(items.length, (p) {
            String title = items[p];
            // bool active = p == selectedIndex;
            bool active = false;

            if (selectedItem is List) {
              selectedItem.contains(title);
            } else if (selectedItem is String) {
              active = title == selectedItem;
            } else {
              active = p == selectedItem;
            }
            return GestureDetector(
              onTap: () {
                if (selectedItem is List) {
                  if (active) {
                    selectedItem.remove(title);
                  } else {
                    selectedItem.add(title);
                  }
                  onSelected(selectedItem);
                } else if (selectedItem is String) {
                  onSelected(title);
                } else {
                  onSelected(p);
                }
              },
              child: Container(
                height: 40,
                width: width,
                margin: EdgeInsets.all(2),
                padding: EdgeInsets.all(2),
                child: Text(
                  title,
                  style: TextStyle(
                      color: active ? white : black,
                      fontSize: 13,
                      fontWeight: active ? FontWeight.bold : FontWeight.normal),
                ),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: active ? (black) : white,
                    boxShadow: [
                      BoxShadow(
                          color: black.withOpacity(.05),
                          spreadRadius: 2,
                          blurRadius: 5)
                    ],
                    borderRadius: BorderRadius.circular(8)),
              ),
            );
          }),
        ),
      ],
    ),
  );
}
