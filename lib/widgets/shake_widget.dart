library shake_widget;

import 'package:flutter/material.dart';
import 'package:shake_widget/controller.dart';

class ShakeWidget extends StatefulWidget {
  final Widget child;
  Axis axisDirection;
  bool vibrate;
  Duration duration;
  ShakeController shakeController;

  ShakeWidget(
      {@required this.child,
      this.axisDirection = Axis.horizontal,
      this.duration = const Duration(milliseconds: 500),
      @required this.shakeController,
      this.vibrate = false})
      : assert(shakeController != null);

  @override
  State<StatefulWidget> createState() => _ShakeWidgetState();
}

class _ShakeWidgetState extends State<ShakeWidget>
    with SingleTickerProviderStateMixin {
  final TextEditingController textController = TextEditingController();

  @override
  void initState() {
    widget.shakeController.controller =
        AnimationController(duration: widget.duration, vsync: this);
    super.initState();
  }

  EdgeInsets getAnimatedPadding(Animation<double> offsetAnimation) {
    if (widget.axisDirection == Axis.horizontal)
      return EdgeInsets.only(
          left: offsetAnimation.value + 24.0,
          right: 24.0 - offsetAnimation.value);
    else
      return EdgeInsets.only(
          top: offsetAnimation.value + 24.0,
          bottom: 24.0 - offsetAnimation.value);
  }

  @override
  Widget build(BuildContext context) {
    final Animation<double> offsetAnimation = Tween(begin: 0.0, end: 24.0)
        .chain(CurveTween(curve: Curves.elasticIn))
        .animate(widget.shakeController.controller)
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              widget.shakeController.controller.reverse();
            }
          });

    return AnimatedBuilder(
        animation: offsetAnimation,
        builder: (buildContext, child) {
          return Container(
            padding: getAnimatedPadding(offsetAnimation),
            child: widget.child,
          );
        });
  }
}
