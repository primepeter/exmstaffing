import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:flutter/material.dart';

import 'controller.dart';

offerField(String hint, Country country, onTap,
    {TextEditingController controller,
    ShakeController shakeController,
    bool slide = false,
    String value,
    onChange,
    FocusNode focusNode}) {
  Color boxColor = Color(0XFFFAFAFA);
  if (null != focusNode && focusNode.hasFocus) {
    boxColor = Color(0XFF0B123E).withOpacity(.09);
  }
  return Container(
    height: 70,
    child: Stack(
      children: [
        Container(
          height: 65,
          margin: EdgeInsets.only(bottom: 10, top: 0),
          padding: EdgeInsets.only(left: 10, right: 0),
          decoration: BoxDecoration(
              color: boxColor,
              border: Border.all(color: Color(0XFF1F1F1F1)),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: [
              if (null != controller)
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(
                      top: 5,
                    ),
                    child: TextField(
                      controller: controller,
                      keyboardType: TextInputType.number,
                      focusNode: focusNode,
                      onChanged: (s) {
                        if (null != onChange) onChange(s);
                      },
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: hint,
                        hintStyle: TextStyle(
                            fontSize: 18,
                            color: black.withOpacity(.4),
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                )
              else
                Expanded(
                    child: Container(
                  padding: EdgeInsets.only(
                    top: 5,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        null == value || value.isEmpty ? hint : value,
                        style: TextStyle(
                            fontSize: 18,
                            color: black.withOpacity(
                                null == value || value.isEmpty ? 0.4 : 1),
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                )),
              GestureDetector(
                onTap: onTap,
                child: Container(
                  height: 65,
                  width: 120,
                  padding: EdgeInsets.only(left: 18, right: 18),
                  decoration: BoxDecoration(
                      color: Color(0XFF0B123E),
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10),
                        bottomRight: Radius.circular(10),
                      )),
                  alignment: Alignment.center,
                  child: Row(
                    children: [
                      if (null != country) ...[
                        CachedNetworkImage(
                            imageUrl: country.flag, height: 18, width: 25),
                        addSpaceWidth(5),
                        Text(
                          country.currencyShort,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: white,
                              fontSize: 18),
                        )
                      ] else ...[
                        Icon(
                          Icons.flag,
                          size: 20,
                          color: white,
                        ),
                        addSpaceWidth(4),
                        Text(
                          "Crypto",
                          style: TextStyle(
                              color: white,
                              fontSize: 14,
                              fontWeight: FontWeight.bold),
                        )
                      ]
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        if ((null != value && value.isNotEmpty) ||
            (null != controller && controller.text.isNotEmpty))
          Align(
              alignment: Alignment.topLeft,
              child: Container(
                child: Text(
                  hint,
                  style: TextStyle(color: white, fontSize: 10),
                ),
                padding: EdgeInsets.fromLTRB(5, 2, 5, 2),
                decoration: BoxDecoration(
                    color: appColor,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                    )),
              ))
      ],
    ),
  );
}
