import 'package:flutter/animation.dart';

class ShakeController {
  AnimationController controller;

  shake() {
    controller.forward(from: 0.0);
  }
}
