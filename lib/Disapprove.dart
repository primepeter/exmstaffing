
import 'dart:io';

import 'package:Loopin/basemodel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/assets.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';


class Disapprove extends StatefulWidget {

  @override
  _DisapproveState createState() => _DisapproveState();
}

class _DisapproveState extends State<Disapprove> {

  TextEditingController messageController = TextEditingController();
  int clickBack = 0;
  List samples = [];
  int selectedItem = -1;
  FocusNode focusMessage = FocusNode();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    focusMessage.addListener(() {setState(() {

    });});
    String disapproveText = appSettingsModel.getString(DISAPPROVE_SAMPLES);

    if(disapproveText.isNotEmpty) {
      samples = convertStringToList("+", disapproveText);
    }
    samples.add("Others Specify");
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext c) {
    return WillPopScope(
        onWillPop: () {
          int now = DateTime.now().millisecondsSinceEpoch;
          if ((now - clickBack) > 5000) {
            clickBack = now;
            showError("Click back again to exit");
            return;
          }
          Navigator.pop(context);
          return;
        },
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: white,
            body: page()));
  }


 page() {
   return Column(
     mainAxisSize: MainAxisSize.min,
     crossAxisAlignment: CrossAxisAlignment.start,
     children: <Widget>[
       addSpace(50),
       new Container(
         width: double.infinity,
         child: new Row(
           crossAxisAlignment: CrossAxisAlignment.center,
           mainAxisSize: MainAxisSize.max,
           children: <Widget>[
             InkWell(
                 onTap: () {
                   Navigator.of(context).pop();
                 },
                 child: Container(
                   width: 50,
                   height: 50,
                   child: Center(
                       child: Icon(
                         Icons.keyboard_backspace,
                         color: black,
                         size: 25,
                       )),
                 )),
             Flexible(
               fit: FlexFit.tight,
               flex: 1,
               child:
               Text(
                 "Reason for Disapproval",
                 style: textStyle(true, 20, black),
               ),
             ),
            addSpaceWidth(15),
           ],
         ),
       ),
       //addLine(1, black.withOpacity(.2), 0, 5, 0, 0),
       AnimatedContainer(duration: Duration(milliseconds: 500),
         width: double.infinity,
         height: errorText.isEmpty?0:40,
         color: red0,
         padding: EdgeInsets.fromLTRB(10,0,10,0),
         child:Center(child: Text(errorText,style: textStyle(true, 16, white),)),
       ),
       Expanded(
         flex: 1,
         child: SingleChildScrollView(
             padding: EdgeInsets.all(20),
             child: Column(
                 mainAxisSize: MainAxisSize.min,
//                 crossAxisAlignment: CrossAxisAlignment.start,
                 children: <Widget>[
                 Column(
                   children: List.generate(samples.length, (index){
                     String message = samples[index];
                     return GestureDetector(
                       onTap: (){
                         selectedItem=index;
                         if(index!=samples.length-1){
                           messageController.text = message;
                         }
                         setState(() {
                         });
                       },
                       child: Container(
                         decoration: BoxDecoration(
                           color: default_white,
                           borderRadius: BorderRadius.all(Radius.circular(5))
                         ),
                         padding: EdgeInsets.all(15),
                         child: Row(
                           children: [
                             checkBox(selectedItem==index),
                             addSpaceWidth(10),
                             Flexible(
                               child: Text(
                                 samples[index],
                                 style: textStyle(true, 16, black),
                               ),
                             ),
                           ],
                         ),
                       ),
                     );
                   },),
                   mainAxisSize: MainAxisSize.min,
                 ),
                   // addSpace(10),
                 inputTextField(textController: messageController, focusNode: focusMessage, prefixIcon: null, title: "Compose Message", hint: "",
                 maxLine: 5),
                   addSpace(15),
                   Container(
                     height: 50,
//               margin: EdgeInsets.fromLTRB(20, 0, 20, 20),
//                     width: double.infinity,
                     child: RaisedButton(
                         materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
//                                padding: EdgeInsets.all(0),
                         shape: RoundedRectangleBorder(
                             borderRadius: BorderRadius.circular(10),
//                             side: BorderSide(color: dark_green0,width: 2)
                         ),
                         color: red0,elevation: 1,
                         onPressed: () {
                           post();
                         },
                         child: Text(
                           "Disapprove",
                           style: textStyle(true, 20, white),
                         )),
                   ),
                   addSpace(50),
                 ])
         ),
       ),
     ],
   );
  }


  void post()async {

    String message = messageController.text.trim();
    if(message.isEmpty){
      showError("Compose Message");
      return;
    }

    yesNoDialog(context, "Disapprove?", "are you sure", (){
      Navigator.pop(context,message);
    });
  }


  String errorText ="";
  showError(String text){
    errorText=text;
    setState(() {


    });
    Future.delayed(Duration(seconds: 1),(){
      errorText="";
      setState(() {});
    });
  }
}
