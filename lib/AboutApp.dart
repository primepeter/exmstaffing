import 'dart:io';

import 'package:Loopin/AppEngine.dart';

import 'package:Loopin/main.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'assets.dart';

class AboutApp extends StatefulWidget {
  @override
  _ChoosecountryListtate createState() => _ChoosecountryListtate();
}

class _ChoosecountryListtate extends State<AboutApp> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: page(),
      backgroundColor: red0,
    );
  }

  page() {
    String androidVersion = appSettingsModel.getString(ANDROID_VERSION_TEXT);
    String iosVersion = appSettingsModel.getString(APPLE_VERSION_TEXT);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        addSpace(30),
        Row(
          children: <Widget>[
            InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  width: 50,
                  height: 50,
                  child: Center(
                      child: Icon(
                    Icons.cancel,
                    color: white,
                    size: 30,
                  )),
                )),
//             Flexible(flex:1,fit:FlexFit.tight,child: Text("Select Country",style: textStyle(true, 25, black),)),
//             addSpaceWidth(20),
          ],
        ),
//         addSpace(10),

        Expanded(
            child: Center(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.asset(
                  ic_plain,
                  width: 60,
                  height: 60,
                ),
                Text("Loopin Material", style: textStyle(true, 25, white)),
//                             RichText(text: TextSpan(children:
//                             [ TextSpan(text: "Loopin Material",style: textStyle(true, 18, white.withOpacity(.5),),
//                                 recognizer: new TapGestureRecognizer()
//                                   ..onTap = () {
//                                     openLink(appSettingsModel.getString(WEB_LINK));
//                                   },),
//
//                             ]),textAlign: TextAlign.center,),
                if (Platform.isAndroid)
                  Text("Android Version: ${androidVersion}",
                      style: textStyle(false, 12, white.withOpacity(.5))),
                if (Platform.isIOS)
                  Text("IOS Version: ${iosVersion}",
                      style: textStyle(false, 12, white.withOpacity(.5))),
                addSpace(10),
              ],
            ),
          ),
        ))
      ],
    );
  }
}
