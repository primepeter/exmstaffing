var map = {
  "_fieldsProto": {
    "time": {"integerValue": "1614077883497", "valueType": "integerValue"},
    "phoneNumber": {
      "stringValue": "+2348143733836",
      "valueType": "stringValue"
    },
    "walletIds": {
      "arrayValue": {
        "values": [
          {
            "stringValue": "603995beabd3a625a56f3c52",
            "valueType": "stringValue"
          },
          {
            "stringValue": "603995c0662c1b25de00233a",
            "valueType": "stringValue"
          },
          {
            "stringValue": "603995c2d118b6262c9e9433",
            "valueType": "stringValue"
          },
          {
            "stringValue": "603995c4286593a60e7e5e9b",
            "valueType": "stringValue"
          }
        ]
      },
      "valueType": "arrayValue"
    },
    "visibility": {"integerValue": "0", "valueType": "integerValue"},
    "token": {
      "stringValue":
          "eKMX2WL4QXW7beoqypKPpK:APA91bEscrdl-LgSSqj47Q_E6sd8JX8RMGPFkCd0NVc9QRc7Y32kj7L_vrModzhV_0FFmURkpW-LWqrLgG7T9Jis2MfyoWCnKENlzHmsdtn_rRGURLUTIUP8SXZ-1psohCQIVvAexiUq",
      "valueType": "stringValue"
    },
    "timeUpdated": {
      "integerValue": "1614530755811",
      "valueType": "integerValue"
    },
    "databaseName": {"stringValue": "userBase", "valueType": "stringValue"},
    "topics": {
      "arrayValue": {
        "values": [
          {"stringValue": "all", "valueType": "stringValue"},
          {
            "stringValue": "jvunqxHhDFW0S4BdPK38s1KeiiC2",
            "valueType": "stringValue"
          }
        ]
      },
      "valueType": "arrayValue"
    },
    "deviceId": {"stringValue": "540524d163e395cd", "valueType": "stringValue"},
    "createdAt": {
      "timestampValue": {"seconds": "1614077884", "nanos": 532000000},
      "valueType": "timestampValue"
    },
    "objectId": {
      "stringValue": "jvunqxHhDFW0S4BdPK38s1KeiiC2",
      "valueType": "stringValue"
    },
    "wallets": {
      "arrayValue": {
        "values": [
          {
            "mapValue": {
              "fields": {
                "itemId": {"stringValue": "BTC", "valueType": "stringValue"},
                "xpub": {
                  "stringValue":
                      "tpubDE6QT1R9spyHNeFUB1CFK7TDs83j3fHXvAr9q8WVNjmK4LK3KU3xWGqi3sHDhPcwBPxfHfTp4zQUsXcH3xFsmCrzRuqsMigLzoKhQhQczGr",
                  "valueType": "stringValue"
                },
                "active": {"booleanValue": true, "valueType": "booleanValue"},
                "externalId": {
                  "stringValue": "LoopIn_Exchange.BTC.Bitcoin",
                  "valueType": "stringValue"
                },
                "balance": {
                  "doubleValue": 0.03242644,
                  "valueType": "doubleValue"
                },
                "accountId": {
                  "stringValue": "603995beabd3a625a56f3c52",
                  "valueType": "stringValue"
                },
                "mnemonic": {
                  "stringValue":
                      "short cable emerge sister air market diary impact oval custom employ assume biology begin cancel buzz music celery hello often limit ridge wait faculty",
                  "valueType": "stringValue"
                },
                "rates": {
                  "doubleValue": 16643013.054195,
                  "valueType": "doubleValue"
                },
                "addressList": {
                  "arrayValue": {
                    "values": [
                      {
                        "mapValue": {
                          "fields": {
                            "time": {
                              "integerValue": "1614386614238",
                              "valueType": "integerValue"
                            },
                            "name": {
                              "stringValue": "LoopIn Default",
                              "valueType": "stringValue"
                            },
                            "address": {
                              "stringValue":
                                  "mqrfe6NuYzfzk9MK7htbw1YBSREMjx2ZPU",
                              "valueType": "stringValue"
                            }
                          }
                        },
                        "valueType": "mapValue"
                      }
                    ]
                  },
                  "valueType": "arrayValue"
                },
                "icon": {
                  "stringValue":
                      "https://firebasestorage.googleapis.com/v0/b/cheetah-2021.appspot.com/o/487861d0-75c8-11eb-b680-1129ed1a7a06?alt=media&token=be73b77e-ff8d-4a77-bcf8-e6e5d4680f3c",
                  "valueType": "stringValue"
                },
                "itemName": {
                  "stringValue": "Bitcoin",
                  "valueType": "stringValue"
                }
              }
            },
            "valueType": "mapValue"
          },
          {
            "mapValue": {
              "fields": {
                "balance": {"doubleValue": 0.01, "valueType": "doubleValue"},
                "addressList": {
                  "arrayValue": {
                    "values": [
                      {
                        "mapValue": {
                          "fields": {
                            "time": {
                              "integerValue": "1614386616322",
                              "valueType": "integerValue"
                            },
                            "name": {
                              "stringValue": "LoopIn Default",
                              "valueType": "stringValue"
                            },
                            "address": {
                              "stringValue":
                                  "bchtest:qqmxuslufsax339n38hn6fdmn4gc4zhjwvsmmhphgh",
                              "valueType": "stringValue"
                            }
                          }
                        },
                        "valueType": "mapValue"
                      }
                    ]
                  },
                  "valueType": "arrayValue"
                },
                "icon": {
                  "stringValue":
                      "https://firebasestorage.googleapis.com/v0/b/cheetah-2021.appspot.com/o/0b3fb030-75d1-11eb-b877-e5d16125825a?alt=media&token=531032ab-beb6-4294-a951-4dd7be23ea68",
                  "valueType": "stringValue"
                },
                "externalId": {
                  "stringValue": "LoopIn_Exchange.BCH.Bitcoin Cash",
                  "valueType": "stringValue"
                },
                "itemId": {"stringValue": "BCH", "valueType": "stringValue"},
                "rates": {
                  "doubleValue": 168518.73046085,
                  "valueType": "doubleValue"
                },
                "xpub": {
                  "stringValue":
                      "tpubDELzxdvQQQtLBDs6hw7qxYaAWeJBJiZPnmnxC1moj8xRhyHRVRza5T9d4k4PEAMUroepN8sQicbEc5CqdzNx7YuDzMirt3thBKrmWib5as7",
                  "valueType": "stringValue"
                },
                "active": {"booleanValue": true, "valueType": "booleanValue"},
                "accountId": {
                  "stringValue": "603995c0662c1b25de00233a",
                  "valueType": "stringValue"
                },
                "mnemonic": {
                  "stringValue":
                      "they close inherit wise regret stick reopen time donkey check height diamond alert room occur unique near celery company settle adapt lumber banner direct",
                  "valueType": "stringValue"
                },
                "itemName": {
                  "stringValue": "Bitcoin Cash",
                  "valueType": "stringValue"
                }
              }
            },
            "valueType": "mapValue"
          },
          {
            "mapValue": {
              "fields": {
                "itemName": {
                  "stringValue": "Ethereum",
                  "valueType": "stringValue"
                },
                "accountId": {
                  "stringValue": "603995c2d118b6262c9e9433",
                  "valueType": "stringValue"
                },
                "itemId": {"stringValue": "ETH", "valueType": "stringValue"},
                "externalId": {
                  "stringValue": "LoopIn_Exchange.ETH.Ethereum",
                  "valueType": "stringValue"
                },
                "mnemonic": {
                  "stringValue":
                      "escape beyond sudden drill deputy eternal merge gain surprise payment appear group lava rug ready sibling tenant smooth final few tiger gold waste section",
                  "valueType": "stringValue"
                },
                "xpub": {
                  "stringValue":
                      "xpub6FBBS4dBLeUeCLx4KPmXQ3DrkLMAayze1q3mCKKeCFyRUrryFvYNH8bzudMJa7FsjYF9zbXu2418CWTCsq8UupMFmfLHbNjDSmvRNJ2FM4D",
                  "valueType": "stringValue"
                },
                "rates": {
                  "doubleValue": 519132.0908554,
                  "valueType": "doubleValue"
                },
                "addressList": {
                  "arrayValue": {
                    "values": [
                      {
                        "mapValue": {
                          "fields": {
                            "time": {
                              "integerValue": "1614386618491",
                              "valueType": "integerValue"
                            },
                            "address": {
                              "stringValue":
                                  "0x61d29a944543668b73876e8961c61fe31e854454",
                              "valueType": "stringValue"
                            },
                            "name": {
                              "stringValue": "LoopIn Default",
                              "valueType": "stringValue"
                            }
                          }
                        },
                        "valueType": "mapValue"
                      }
                    ]
                  },
                  "valueType": "arrayValue"
                },
                "icon": {
                  "stringValue":
                      "https://firebasestorage.googleapis.com/v0/b/cheetah-2021.appspot.com/o/6c650c50-75c9-11eb-b076-6798acb75aa9?alt=media&token=5b81c5c4-3d5c-4ff5-ba52-6a6c8e6a1df5",
                  "valueType": "stringValue"
                },
                "active": {"booleanValue": true, "valueType": "booleanValue"},
                "balance": {"doubleValue": 3.6115, "valueType": "doubleValue"}
              }
            },
            "valueType": "mapValue"
          },
          {
            "mapValue": {
              "fields": {
                "itemName": {
                  "stringValue": "Litecoin",
                  "valueType": "stringValue"
                },
                "icon": {
                  "stringValue":
                      "https://firebasestorage.googleapis.com/v0/b/cheetah-2021.appspot.com/o/e4ec2260-75d0-11eb-9702-37c06cc4a0e5?alt=media&token=5b0421ec-3a4f-4bb9-b93b-5b336dd99788",
                  "valueType": "stringValue"
                },
                "externalId": {
                  "stringValue": "LoopIn_Exchange.LTC.Litecoin",
                  "valueType": "stringValue"
                },
                "xpub": {
                  "stringValue":
                      "ttub4ggC4Uy1S3NfaWZwfiLH18UKuMkEu4J1F2WFk2MbzyH8PTBcC9jabxKa9CpHQ2MEwApPrGXzy5PfSg6vm5EgFFJCa4176scjnEHWuz258mL",
                  "valueType": "stringValue"
                },
                "itemId": {"stringValue": "LTC", "valueType": "stringValue"},
                "active": {"booleanValue": true, "valueType": "booleanValue"},
                "addressList": {
                  "arrayValue": {
                    "values": [
                      {
                        "mapValue": {
                          "fields": {
                            "time": {
                              "integerValue": "1614386620598",
                              "valueType": "integerValue"
                            },
                            "name": {
                              "stringValue": "LoopIn Default",
                              "valueType": "stringValue"
                            },
                            "address": {
                              "stringValue":
                                  "mmB17mQc1jPYKt4U9BVdN2KBY6RiegzKWA",
                              "valueType": "stringValue"
                            }
                          }
                        },
                        "valueType": "mapValue"
                      }
                    ]
                  },
                  "valueType": "arrayValue"
                },
                "balance": {
                  "doubleValue": 5.70905271,
                  "valueType": "doubleValue"
                },
                "rates": {
                  "doubleValue": 60767.21658315,
                  "valueType": "doubleValue"
                },
                "mnemonic": {
                  "stringValue":
                      "frozen talent canal charge infant carpet armed topple crop pioneer obvious nest stereo setup stereo hint cinnamon farm luggage easy resource pave bar second",
                  "valueType": "stringValue"
                },
                "accountId": {
                  "stringValue": "603995c4286593a60e7e5e9b",
                  "valueType": "stringValue"
                }
              }
            },
            "valueType": "mapValue"
          }
        ]
      },
      "valueType": "arrayValue"
    },
    "updatedAt": {
      "timestampValue": {"seconds": "1614530756", "nanos": 933000000},
      "valueType": "timestampValue"
    }
  },
  "_ref": {
    "_firestore": {
      "_settings": {
        "projectId": "cheetah-2021",
        "firebaseVersion": "9.5.0",
        "libName": "gccl",
        "libVersion": "4.9.4 fire/9.5.0"
      },
      "_settingsFrozen": true,
      "_serializer": {"allowUndefined": false},
      "_projectId": "cheetah-2021",
      "registeredListenersCount": 0,
      "bulkWritersCount": 0,
      "_backoffSettings": {
        "initialDelayMs": 100,
        "maxDelayMs": 60000,
        "backoffFactor": 1.3
      },
      "_clientPool": {
        "concurrentOperationLimit": 100,
        "maxIdleClients": 1,
        "activeClients": {},
        "failedClients": {},
        "terminated": false,
        "terminateDeferred": {"promise": {}}
      }
    },
    "_path": {
      "segments": ["userBase", "jvunqxHhDFW0S4BdPK38s1KeiiC2"],
      "projectId": "cheetah-2021",
      "databaseId": "(default)"
    },
    "_converter": {}
  },
  "_serializer": {"allowUndefined": false},
  "_readTime": {"_seconds": 1614535351, "_nanoseconds": 872675000},
  "_createTime": {"_seconds": 1614077884, "_nanoseconds": 579990000},
  "_updateTime": {"_seconds": 1614530756, "_nanoseconds": 950900000}
};
