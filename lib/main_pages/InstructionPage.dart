import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';

class InstructionPage extends StatefulWidget {
  const InstructionPage({Key key}) : super(key: key);

  @override
  _InstructionPageState createState() => _InstructionPageState();
}

class _InstructionPageState extends State<InstructionPage> {
  final scrollController = ScrollController();
  final controller = TextEditingController();

  List<BaseModel> mainItemList = [];
  List<BaseModel> itemList = [];
  bool isBusy = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // loadItems();
  }

  loadItems() async {
    FirebaseFirestore.instance
        .collection(INSTRUCTION_BASE)
        .where(PARTIES, arrayContainsAny: ["All", userModel.getObjectId()])
        .orderBy(TIME)
        .snapshots()
        .listen((value) {
          for (var doc in value.docs) {
            final bm = BaseModel(doc: doc);
            int p =
                itemList.indexWhere((e) => e.getObjectId() == bm.getObjectId());
            if (p == -1) {
              itemList.add(bm);
            } else {
              itemList[p] = bm;
            }
          }
          mainItemList = itemList;
          isBusy = false;
          setState(() {});
        });
  }

  @override
  Widget build(BuildContext context) {
    // 123.peter@exmstaffing.com

    return LayoutBuilder(builder: (c, box) {
      bool isLargeScreen = box.maxWidth > 850;
      final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

      return Container(
        decoration: BoxDecoration(
            // color: isLargeScreen ? white : null,

            borderRadius: BorderRadius.circular(10)),
        padding: EdgeInsets.only(
          left: isLargeScreen ? 50 : 20,
          right: isLargeScreen ? 50 : 20,
          top: isLargeScreen ? 20 : 0,
          bottom: isLargeScreen ? 50 : 20,
        ),
        child: Material(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          elevation: 5,
          color: white,
          child: Container(
            decoration: BoxDecoration(
                color: white, borderRadius: BorderRadius.circular(15)

                // borderRadius: BorderRadius.only(
                //   topRight: Radius.circular(15),
                //   bottomRight: Radius.circular(15),
                // )
                ),
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.only(left: 10),
            child: Builder(
              builder: (c) {
                if (instructionsBusy)
                  return Center(
                    child: SizedBox(
                      height: 16,
                      width: 16,
                      child: CircularProgressIndicator(
                        strokeWidth: 4,
                        valueColor: AlwaysStoppedAnimation(black),
                      ),
                    ),
                  );

                if (instructionsList.isEmpty)
                  return Center(
                    child: Container(
                      padding: EdgeInsets.all(0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Container(
                            height: 150,
                            alignment: Alignment.center,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.message,
                                  color: black.withOpacity(.5),
                                  size: 50,
                                ),
                                addSpace(10),
                                Text(
                                  "You have not received any instructions from admin yet",
                                  style: TextStyle(
                                      fontSize: 14, color: black.withOpacity(.5)
                                      // fontWeight: FontWeight.bold,
                                      ),
                                )
                              ],
                            )),
                      ),
                    ),
                  );

                return Scrollbar(
                  controller: scrollController,
                  isAlwaysShown: true,
                  child: ListView.builder(
                      controller: scrollController,
                      padding: const EdgeInsets.fromLTRB(8, 8, 15, 8),
                      itemCount: instructionsList.length,
                      itemBuilder: (c, p) {
                        final bm = instructionsList[p];
                        String sentTo = bm.getString("sentTo");
                        String msg = bm.getString(MESSAGE);
                        String time = getChatDate(bm.getTime());

                        return Container(
                          decoration: BoxDecoration(
                              color: black.withOpacity(.03),
                              borderRadius: BorderRadius.circular(10),
                              border:
                                  Border.all(color: black.withOpacity(.03))),
                          padding: EdgeInsets.all(4),
                          margin: EdgeInsets.all(4),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                msg,
                                style: TextStyle(
                                  fontSize: 16,
                                ),
                              ),
                              addSpace(2),
                              Text(
                                sentTo,
                                style: TextStyle(
                                  fontSize: 16,
                                ),
                              ),
                              addSpace(2),
                              Text(
                                time,
                                style: TextStyle(
                                    fontSize: 12, color: black.withOpacity(.4)),
                              ),
                            ],
                          ),
                        );
                      }),
                );
              },
            ),
          ),
        ),
      );
    });
  }
}
