import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EmailApi.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/forms/AddSkillsForm.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  final scrollController = ScrollController();

  List get pageItems => [
        // {"icon": Icons.dashboard, "title": "DASHBOARD"},
        {
          "icon": Icons.person,
          "title": "USERS",
          "count": appSettingsModel.getList(USER_BASE_COUNT).length
        },
        {
          "icon": Icons.group_work,
          "title": "WORK ORDERS",
          "count": appSettingsModel.getList(WORK_BASE_COUNT).length
        },
        {
          "icon": Icons.people_alt,
          "title": "EMPLOYEES",
          "count": appSettingsModel.getList(EMPLOYEE_BASE_COUNT).length
        },
        // {"icon": Icons.home_work, "title": "JOBS"},
        // {"icon": Icons.message, "title": "INSTRUCTIONS", "count": 0},
        {
          "icon": Icons.handyman,
          "title": "JOB SKILLS",
          "count": appSettingsModel.getList(SKILLS).length
        },
      ];

  List get jobsItems => [
        {
          "icon": Icons.home_work,
          "title": "JOBS PENDING",
          "count": appSettingsModel.getList(JOB_PENDING_LIST).length
        },
        {
          "icon": Icons.home_work,
          "title": "JOB ASSIGNED",
          "count": appSettingsModel.getList(JOB_ASSIGNED_LIST).length
        },
        {
          "icon": Icons.home_work,
          "title": "JOBS COMPLETED",
          "count": appSettingsModel.getList(JOB_COMPLETED_LIST).length
        },
      ];

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (c, box) {
      bool isLargeScreen = box.maxWidth > 850;
      final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

      return Scrollbar(
        controller: scrollController,
        isAlwaysShown: true,
        child: SingleChildScrollView(
          child: Container(
            decoration: BoxDecoration(
                // color: isLargeScreen ? white : null,
                borderRadius: BorderRadius.circular(10)),
            // padding: EdgeInsets.only(
            //   left: isLargeScreen ? 50 : 20,
            //   right: isLargeScreen ? 50 : 20,
            //   top: isLargeScreen ? 20 : 0,
            //   bottom: isLargeScreen ? 50 : 50,
            // ),
            padding: EdgeInsets.only(
              left: 20,
              right: 20,
              top: isLargeScreen ? 5 : 0,
              bottom: 20,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Hello ${userModel.getString(FIRST_NAME)}",
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: black,
                            fontSize: 25,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "Welcome to your account, from your dashboard "
                        "you will be able to manage all of your"
                        " services in a few simple steps",
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: black,
                            fontSize: 14,
                            fontWeight: FontWeight.normal),
                      ),
                    ],
                  ),
                ),
                addSpace(20),
                Container(
                  decoration: BoxDecoration(
                      color: white,
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: black.withOpacity(.05))),
                  padding: EdgeInsets.all(15),
                  alignment: Alignment.centerLeft,
                  child: Wrap(
                    children: List.generate(pageItems.length, (p) {
                      String title = pageItems[p]["title"];
                      int count = pageItems[p]["count"] ?? 0;
                      int page = p + 1;

                      return Container(
                        height: 150,
                        width: 150,
                        margin: EdgeInsets.all(3),
                        child: TextButton(
                            onPressed: () {
                              if (p == 3) {
                                pushAndResult(context, AddSkillsForm(),
                                    fade: true, result: (_) {
                                  setState(() {});
                                });
                                return;
                              }

                              if (p == 0 && userModel.isCoordinator) {
                                showErrorDialog(context,
                                    "Operation can only be done by an admin");
                                return;
                              }

                              pageChangeController.add(p + 1);
                            },
                            style: TextButton.styleFrom(
                                minimumSize: Size(150, 40),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                backgroundColor: appColor),
                            child: Container(
                              padding: EdgeInsets.only(left: 5, right: 5),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  // if (count > 0)
                                  Text(
                                    count == 0 ? "" : count.toString(),
                                    style: TextStyle(
                                        color: white,
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    title,
                                    style:
                                        TextStyle(color: white, fontSize: 12),
                                  ),
                                ],
                              ),
                            )),
                      );
                    }),
                  ),
                ),
                addSpace(20),

                // addLine(20, black.withOpacity(.03), 0, 20, 0, 20),
                Text(
                  "Jobs",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                addSpace(20),
                Container(
                  decoration: BoxDecoration(
                      color: white,
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: black.withOpacity(.05))),
                  padding: EdgeInsets.all(15),
                  alignment: Alignment.centerLeft,
                  child: Wrap(
                    children: List.generate(jobsItems.length, (p) {
                      String title = jobsItems[p]["title"];
                      int count = jobsItems[p]["count"] ?? 0;
                      int page = p + 1;

                      return Container(
                        height: 150,
                        width: 150,
                        margin: EdgeInsets.all(3),
                        child: TextButton(
                            onPressed: () {},
                            style: TextButton.styleFrom(
                                minimumSize: Size(150, 40),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                backgroundColor: appColor),
                            child: Container(
                              padding: EdgeInsets.only(left: 5, right: 5),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  // if (count > 0)
                                  Text(
                                    // count == 0 ? "" :
                                    count.toString(),
                                    style: TextStyle(
                                        color: white,
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    title,
                                    style:
                                        TextStyle(color: white, fontSize: 12),
                                  ),
                                ],
                              ),
                            )),
                      );
                    }),
                  ),
                ),
                addSpace(20),
              ],
            ),
          ),
        ),
      );
    });
  }

  Widget FlexRowColumn(List<Widget> children, {bool isLargeScreen = false}) {
    if (!isLargeScreen)
      return Row(
        children: List.generate(
            children.length, (index) => Expanded(child: children[index])),
      );

    return Column(
      children: children,
    );
  }
}
