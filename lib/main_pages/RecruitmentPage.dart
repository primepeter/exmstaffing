import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/forms/AddEmployeeForm.dart';
import 'package:Loopin/forms/AddJobForm.dart';
import 'package:Loopin/forms/AddUserForm.dart';
import 'package:Loopin/forms/AddWorkForm.dart';
import 'package:Loopin/forms/FilterForm.dart';
import 'package:Loopin/forms/KeysForm.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/link.dart';

class RecruitmentPage extends StatefulWidget {
  const RecruitmentPage({Key key}) : super(key: key);

  @override
  _RecruitmentPageState createState() => _RecruitmentPageState();
}

class _RecruitmentPageState extends State<RecruitmentPage> {
  final scrollController = ScrollController();

  List<BaseModel> itemList = [];
  List<BaseModel> mainItemList = [];
  bool isBusy = true;
  bool isFiltering = false;

  final searchController = TextEditingController();
  bool showCancel = false;
  bool searching = false;
  FocusNode focusNode = FocusNode();

  List selectedSkills = [];
  List selectedShifts = [];
  String selectedState = "";
  String selectedCity = "";
  int type = -1;
  List types = ["All", "Old", "New"];

  int pageCounter = appSettingsModel.getInt("pageCounter");
  int currentPage = 0;
  int defPageCounter = appSettingsModel.getInt("pageCounter");

  int get nextPage {
    return (currentPage * pageCounter) + pageCounter;
  }

  int get previousPage {
    return (currentPage * pageCounter);
  }

  int get pageCount {
    return appSettingsModel.getList(JOB_BASE_COUNT).length;
  }

  List<BaseModel> get pageList {
    List<BaseModel> items = [];
    for (var item in itemList) {
      int p = itemList.indexWhere((e) => e.getObjectId() == item.getObjectId());
      if (p == -1) continue;
      if (p >= previousPage && p < nextPage) items.add(item);
    }
    return items;
  }

  bool get canGoNext {
    return nextPage < pageCount;
  }

  bool get canGoPrevious {
    return currentPage != 0;
  }

  loadNextDocument(bool previous) {
    if (previous) {
      if (!canGoPrevious) return;
      currentPage--;
    } else {
      if (!canGoNext) return;
      currentPage++;
    }
    print("pageList ${pageList.isEmpty}");
    if (pageList.isEmpty) {
      searching = true;
      loadItems();
    }

    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadItems();
  }

  loadItems([bool isNew = false]) async {
    FirebaseFirestore.instance
        .collection(JOB_BASE)
        .limit(pageCounter)
        .orderBy(TIME, descending: !isNew)
        .startAt([
          isNew
              ? (itemList.isEmpty ? 0 : itemList.first.getTime())
              : (itemList.isEmpty
                  ? DateTime.now().millisecondsSinceEpoch
                  : itemList.last.getTime())
        ])
        .get()
        .then((value) {
          for (var doc in value.docs) {
            final bm = BaseModel(doc: doc);
            int p =
                itemList.indexWhere((e) => e.getObjectId() == bm.getObjectId());
            if (p == -1) {
              itemList.add(bm);
            } else {
              itemList[p] = bm;
            }
          }
          isBusy = false;
          searching = false;
          setState(() {});
        });
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (c, box) {
      bool isLargeScreen = box.maxWidth > 850;
      final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

      return Container(
        margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  decoration: BoxDecoration(
                      border: Border(
                          left:
                              BorderSide(color: Colors.deepOrange, width: 4))),
                  padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
                  child: Text(
                    "Job Listings",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
                Spacer(),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                        margin: EdgeInsets.only(left: 5, right: 5),
                        height: 40,
                        alignment: Alignment.center,
                        child: Text(
                          "Rows per page:",
                          style: TextStyle(
                              color: black.withOpacity(.5),
                              fontSize: 12,
                              fontWeight: FontWeight.bold),
                        )),
                    PopupMenuButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      elevation: 5,
                      child: Container(
                        height: 40,
                        alignment: Alignment.center,
                        padding: EdgeInsets.only(left: 5, right: 5),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              "$pageCounter",
                              style: TextStyle(
                                  color: black.withOpacity(.6),
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold),
                            ),
                            addSpaceWidth(
                              4,
                            ),
                            Icon(
                              Icons.keyboard_arrow_down,
                              size: 20,
                            ),
                          ],
                        ),
                      ),
                      color: white,
                      itemBuilder: (context) => List.generate(3, (p) {
                        int val = defPageCounter * (p + 1);
                        return PopupMenuItem<int>(
                          value: p,
                          onTap: () {},
                          child: Text(
                            "$val",
                            style: TextStyle(fontSize: 12, color: black),
                          ),
                        );
                      }),
                      onSelected: (_) {
                        pageCounter = (_ + 1) * defPageCounter;
                        if (pageList.length < pageCounter - 1) {
                          searching = true;
                          loadItems();
                        }
                        setState(() {});
                      },
                    ),
                    addSpaceWidth(10),
                    TextButton(
                        onPressed: () {
                          loadNextDocument(true);
                        },
                        style: TextButton.styleFrom(
                          minimumSize: Size(40, 40),
                          backgroundColor: white,
                          primary: black.withOpacity(canGoPrevious ? 1 : 0.4),
                          elevation: 4,
                          // shape: CircleBorder()
                        ),
                        child: Icon(Icons.navigate_before)),
                    addSpaceWidth(5),
                    TextButton(
                        onPressed: () {
                          loadNextDocument(false);
                        },
                        style: TextButton.styleFrom(
                          minimumSize: Size(40, 40),
                          backgroundColor: white,
                          primary: black.withOpacity(canGoNext ? 1 : 0.4),
                          elevation: 4,
                          // shape: CircleBorder()
                        ),
                        child: Icon(Icons.navigate_next)),
                    Container(
                        // width: 80,
                        height: 40,
                        margin: EdgeInsets.only(left: 15, right: 5),
                        alignment: Alignment.center,
                        child: Text(
                          "$previousPage - $nextPage out of $pageCount",
                          // "${currentPage * pageCounter} - $nextPage out of ${mainItemList.length}",
                          style: TextStyle(
                              color: black.withOpacity(.6),
                              fontSize: 12,
                              fontWeight: FontWeight.bold),
                        )),
                  ],
                ),
                addSpaceWidth(10),
                Link(
                    uri: Uri.parse("/postJob"),
                    // target: LinkTarget.blank,
                    builder: (c, onClick) {
                      return TextButton(
                        // onPressed: onClick,
                        onPressed: () {
                          pushAndResult(context, AddJobForm(), dialog: true,
                              result: (_) {
                            if (null == _) return;
                            itemList.insert(0, _);
                            setState(() {});
                          });
                        },
                        child: Text(
                          "Post Job".toUpperCase(),
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                            primary: white,
                            backgroundColor: black,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            minimumSize: Size(100, 50)),
                      );
                    }),
              ],
            ),
            addSpace(20),
            Expanded(
              child: Builder(
                builder: (c) {
                  if (isBusy || searching)
                    return Center(
                        child: SizedBox(
                            height: 20,
                            width: 20,
                            child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(black),
                            )));

                  if (pageList.isEmpty)
                    return Center(
                      child: Text("Sorry no open job listings yet.",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: black.withOpacity(.5),
                            fontSize: 14,
                            // fontWeight: FontWeight.bold
                          )),
                    );

                  return Scrollbar(
                    controller: scrollController,
                    isAlwaysShown: true,
                    child: SingleChildScrollView(
                      controller: scrollController,
                      padding: const EdgeInsets.fromLTRB(8, 8, 15, 8),
                      child: Column(
                        children: List.generate(pageList.length, (p) {
                          final bm = pageList[p];
                          String jobTitle = bm.getString(TITLE);
                          String skill = bm.getString(SKILLS);
                          String jobDescription =
                              bm.getString("jobDescription");
                          String jobRequirement =
                              bm.getString("jobRequirement");
                          // double jobSalary = bm.getDouble("jobSalary");
                          String jobSalary = bm.getString("jobSalary");

                          return Container(
                            padding: EdgeInsets.all(10),
                            margin: EdgeInsets.only(bottom: 15, top: 0),
                            decoration: BoxDecoration(
                                color: white,
                                border:
                                    Border.all(color: black.withOpacity(.01)),
                                borderRadius: BorderRadius.circular(10)),
                            alignment: Alignment.centerLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    // Icon(Icons.stars_rounded),
                                    // addSpaceWidth(3),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            jobTitle,
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                color: black,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w900),
                                          ),
                                          Text(
                                            skill,
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                color: black.withOpacity(.7),
                                                fontSize: 14,
                                                fontWeight: FontWeight.w700),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Link(
                                        // target: LinkTarget.blank,
                                        uri: Uri.parse(
                                            "/postJob??${bm.getObjectId()}"),
                                        builder: (c, onClick) {
                                          return TextButton(
                                            // onPressed: onClick,
                                            onPressed: () {
                                              pushAndResult(
                                                  context,
                                                  AddJobForm(
                                                    bm: bm,
                                                  ),
                                                  dialog: true, result: (_) {
                                                if (null == _) return;
                                                itemList[p] = _;
                                                setState(() {});
                                              });
                                            },
                                            child: Text(
                                              "Edit".toUpperCase(),
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            style: TextButton.styleFrom(
                                                padding: EdgeInsets.zero,
                                                primary: white,
                                                backgroundColor: green_dark,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10)),
                                                minimumSize: Size(100, 40)),
                                          );
                                        }),

                                    addSpaceWidth(10),
                                    TextButton(
                                      onPressed: () {
                                        yesNoDialog(context, "Delete Job?",
                                            "Are you sure you want to delete this job?",
                                            () {
                                          itemList.removeAt(p);
                                          bm.deleteItem();
                                          setState(() {});
                                        });
                                      },
                                      child: Icon(
                                        Icons.clear,
                                        size: 16,
                                      ),
                                      style: TextButton.styleFrom(
                                          padding: EdgeInsets.zero,
                                          primary: white,
                                          backgroundColor: red,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          minimumSize: Size(40, 40)),
                                    ),
                                  ],
                                ),
                                addSpace(5),
                                Text(
                                  "Job Description",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      color: black,
                                      fontSize: 14,
                                      decoration: TextDecoration.underline,
                                      fontWeight: FontWeight.bold),
                                ),
                                addSpace(4),
                                ReadMoreText(
                                  jobDescription,
                                  textColor: black.withOpacity(.5),
                                  fontSize: 14,
                                  minLength: 200,
                                ),
                                addSpace(5),
                                Text(
                                  "Job Requirements",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      color: black,
                                      fontSize: 14,
                                      decoration: TextDecoration.underline,
                                      fontWeight: FontWeight.bold),
                                ),
                                addSpace(4),
                                ReadMoreText(
                                  jobRequirement,
                                  textColor: black.withOpacity(.5),
                                  fontSize: 16,
                                  minLength: 200,
                                ),
                                addSpace(8),
                                Row(
                                  children: [
                                    Icon(
                                      Icons.location_on,
                                      size: 14,
                                    ),
                                    addSpaceWidth(3),
                                    Text(
                                      "${bm.getString(CITY)} in ${bm.getString(STATE)} ",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w900),
                                    ),
                                  ],
                                ),
                                addSpace(8),
                                Row(
                                  children: [
                                    Icon(
                                      Icons.event,
                                      size: 14,
                                      color: Colors.deepOrange,
                                    ),
                                    addSpaceWidth(3),
                                    Text(
                                      "Deadline:  " +
                                          getSimpleDate(bm.getInt("jobDate"),
                                              pattern: "EEEE MMMM dd, yyyy"),
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          color: Colors.deepOrange,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w900),
                                    ),
                                  ],
                                ),
                                addSpace(8),
                                Row(
                                  children: [
                                    Icon(
                                      Icons.account_balance_wallet_outlined,
                                      size: 14,
                                      color: black,
                                    ),
                                    addSpaceWidth(3),
                                    Text(
                                      "Expected Pay Per Hour:  " +
                                          "\$$jobSalary",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          color: black,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w900),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          );
                        }),
                      ),
                    ),
                  );
                },
              ),
            )
          ],
        ),
      );
    });
  }
}
