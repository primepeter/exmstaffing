import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/forms/AddEmployeeForm.dart';
import 'package:Loopin/forms/AddUserForm.dart';
import 'package:Loopin/forms/AddWorkForm.dart';
import 'package:Loopin/forms/FilterForm.dart';
import 'package:Loopin/forms/KeysForm.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class EmployeePage extends StatefulWidget {
  const EmployeePage({Key key}) : super(key: key);

  @override
  _EmployeePageState createState() => _EmployeePageState();
}

class _EmployeePageState extends State<EmployeePage> {
  final scrollController = ScrollController();

  List<BaseModel> itemList = [];
  List<BaseModel> mainItemList = [];
  bool isBusy = true;
  bool isFiltering = false;

  final searchController = TextEditingController();
  bool showCancel = false;
  bool searching = true;
  FocusNode focusNode = FocusNode();

  List selectedSkills = [];
  List selectedShifts = [];
  String selectedState = "";
  String selectedCity = "";
  int type = -1;
  List types = ["All", "Old", "New"];

  int pageCounter = appSettingsModel.getInt("pageCounter");
  int currentPage = 0;
  int defPageCounter = appSettingsModel.getInt("pageCounter");

  int get nextPage {
    return (currentPage * pageCounter) + pageCounter;
  }

  int get previousPage {
    return (currentPage * pageCounter);
  }

  int get pageCount {
    return appSettingsModel.getList(EMPLOYEE_BASE_COUNT).length;
  }

  List<BaseModel> get pageList {
    List<BaseModel> items = [];
    for (var item in itemList) {
      int p = itemList.indexWhere((e) => e.getObjectId() == item.getObjectId());
      if (p == -1) continue;
      if (p >= previousPage && p < nextPage) items.add(item);
    }
    return items;
  }

  bool get canGoNext {
    return nextPage < pageCount;
  }

  bool get canGoPrevious {
    return currentPage != 0;
  }

  loadNextDocument(bool previous) {
    if (previous) {
      if (!canGoPrevious) return;
      currentPage--;
    } else {
      if (!canGoNext) return;
      currentPage++;
    }
    print("pageList ${pageList.isEmpty}");
    if (pageList.isEmpty) {
      searching = true;
      loadItems();
    }

    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadItems();
    handleSearching();
  }

  handleSearching() async {
    searchController.addListener(() async {
      List<DocumentSnapshot> docList = [];
      String text = searchController.text.trim().toLowerCase();
      setState(() {});
      if (text.isEmpty) {
        itemList = mainItemList;
        showCancel = false;
        searching = false;
        if (mounted) setState(() {});
        return;
      }
      print("is searching");
      itemList.clear();
      showCancel = true;
      searching = true;
      if (mounted) setState(() {});
      final shots = await FirebaseFirestore.instance
          .collection(WORK_BASE)
          .where(SEARCH, arrayContains: text.trim())
          .limit(30)
          .get();
      for (var doc in shots.docs) {
        final bm = BaseModel(doc: doc);
        int p = itemList.indexWhere((e) => e.getObjectId() == bm.getObjectId());
        if (p != -1) {
          itemList[p] = bm;
        } else {
          itemList.add(bm);
        }
      }
      searching = false;
      if (mounted) setState(() {});
    });
  }

  loadItems([bool isNew = false]) async {
    QuerySnapshot shots;

    List tags = [];
    if (selectedSkills.isNotEmpty) tags.addAll(selectedSkills);
    if (selectedShifts.isNotEmpty) tags.addAll(selectedShifts);
    if (selectedState.isNotEmpty) tags.add(selectedState);
    if (selectedCity.isNotEmpty) tags.add(selectedCity);

    if (isFiltering) {
      print("tags $tags");
      shots = await FirebaseFirestore.instance
          .collection(EMPLOYEE_BASE)
          .where("tags", arrayContainsAny: tags)
          .get();
    } else {
      if (type == -1) {
        shots = await FirebaseFirestore.instance
            .collection(EMPLOYEE_BASE)
            .limit(pageCounter)
            .orderBy(TIME, descending: !isNew)
            .startAt([
          isNew
              ? (itemList.isEmpty ? 0 : itemList.first.getTime())
              : (itemList.isEmpty
                  ? DateTime.now().millisecondsSinceEpoch
                  : itemList.last.getTime())
        ]).get();
      } else {
        shots = await FirebaseFirestore.instance
            .collection(EMPLOYEE_BASE)
            .where(TYPE, isEqualTo: type)
            .limit(pageCounter)
            .orderBy(TIME, descending: !isNew)
            .startAfter([
          isNew
              ? (itemList.isEmpty ? 0 : itemList.first.getTime())
              : (itemList.isEmpty
                  ? DateTime.now().millisecondsSinceEpoch
                  : itemList.last.getTime())
        ]).get();
      }
    }

    for (var doc in shots.docs) {
      final bm = BaseModel(doc: doc);
      String state = bm.getString(STATE);
      String city = bm.getString(CITY);
      List skills = bm.getList(SKILLS);
      List shifts = bm.getList(SHIFTS);

      if (isFiltering) {
        bool canAdd = true;
        if (selectedState.isNotEmpty && !tags.contains(state)) if (selectedState
                .isNotEmpty &&
            !tags.contains(state)) {
          // continue;
          canAdd = false;
        }

        if (selectedCity.isNotEmpty && !tags.contains(city)) if (selectedCity
                .isNotEmpty &&
            !tags.contains(city)) {
          // continue;
          canAdd = false;
        }

        print("canAdd $canAdd");

        if (selectedSkills.isNotEmpty &&
            skills.every((item) => !tags.contains(item))) {
          canAdd = false;
        }

        if (selectedShifts.isNotEmpty &&
            shifts.every((item) => !tags.contains(item))) {
          canAdd = false;
        }

        if (!canAdd) continue;
      }

      int p = itemList.indexWhere((e) => e.getObjectId() == bm.getObjectId());
      if (p == -1) {
        itemList.add(bm);
      } else {
        itemList[p] = bm;
      }
    }
    if (!isFiltering) mainItemList = itemList;
    // if (isFiltering)
    searching = false;
    isBusy = false;
    setState(() {});
  }

  List keys = [
    {"title": "Action", "key": "action", "visible": true},
    {"title": "FirstName", "key": FIRST_NAME, "visible": true},
    {"title": "LastName", "key": LAST_NAME, "visible": true},
    {"title": "Email", "key": EMAIL, "visible": false},
    {"title": "Tel No", "key": TELEPHONE, "visible": true},
    {"title": "Address", "key": ADDRESS, "visible": false},
    {"title": "State", "key": STATE, "visible": true},
    {"title": "City", "key": CITY, "visible": true},
    {"title": "Status", "key": STATUS, "visible": true},
    {"title": "Days Available", "key": DAYS, "visible": true, "isList": true},
    {
      "title": "Months Available",
      "key": MONTHS,
      "visible": false,
      "isList": true
    },
    {"title": "SkillSet", "key": SKILLS, "visible": true, "isList": true},
  ];

  List get visibleKeys => keys.where((e) => e["visible"] ?? false).toList();

  List<DataColumn2> headerColumns() {
    return List.generate(visibleKeys.length, (p) {
      String title = visibleKeys[p]["title"];
      bool action = title == "Action";
      return DataColumn2(
        label: Text(
          title,
          style: TextStyle(
              fontSize: 10,
              color: black.withOpacity(.5),
              fontWeight: FontWeight.bold),
        ),
        size: action ? ColumnSize.S : ColumnSize.L,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (c, box) {
      bool isLargeScreen = box.maxWidth > 850;
      final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

      return Column(
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Row(
                    children: List.generate(types.length, (p) {
                      String title = types[p] + " Employees";
                      int theType = p - 1;
                      bool active = type == theType;
                      return InkWell(
                        onTap: () {
                          type = theType;
                          if (!active) {
                            searching = true;
                            itemList.clear();
                            isBusy = true;
                            currentPage = 0;
                            pageCounter = 5;
                            loadItems();
                          }
                          setState(() {});
                        },
                        child: Container(
                          width: 120,
                          height: 40,
                          alignment: Alignment.center,
                          margin: EdgeInsets.fromLTRB(2, 0, 2, 0),
                          padding: EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              color: active ? black : white,
                              border: Border.all(color: black.withOpacity(.1)),
                              borderRadius: BorderRadius.circular(8)),
                          child: Text(
                            title,
                            style: TextStyle(
                                color: active ? white : black,
                                fontSize: 12,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      );
                    }),
                  ),
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                        margin: EdgeInsets.only(left: 5, right: 5),
                        height: 40,
                        alignment: Alignment.center,
                        child: Text(
                          "Rows per page:",
                          style: TextStyle(
                              color: black.withOpacity(.5),
                              fontSize: 12,
                              fontWeight: FontWeight.bold),
                        )),
                    PopupMenuButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      elevation: 5,
                      child: Container(
                        height: 40,
                        alignment: Alignment.center,
                        padding: EdgeInsets.only(left: 5, right: 5),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              "$pageCounter",
                              style: TextStyle(
                                  color: black.withOpacity(.6),
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold),
                            ),
                            addSpaceWidth(
                              4,
                            ),
                            Icon(
                              Icons.keyboard_arrow_down,
                              size: 20,
                            ),
                          ],
                        ),
                      ),
                      color: white,
                      itemBuilder: (context) => List.generate(3, (p) {
                        int val = defPageCounter * (p + 1);
                        return PopupMenuItem<int>(
                          value: p,
                          onTap: () {},
                          child: Text(
                            "$val",
                            style: TextStyle(fontSize: 12, color: black),
                          ),
                        );
                      }),
                      onSelected: (_) {
                        pageCounter = (_ + 1) * defPageCounter;
                        if (pageList.length < pageCounter - 1) {
                          searching = true;
                          loadItems();
                        }
                        setState(() {});
                      },
                    ),
                    addSpaceWidth(10),
                    TextButton(
                        onPressed: () {
                          loadNextDocument(true);
                        },
                        style: TextButton.styleFrom(
                          minimumSize: Size(40, 40),
                          backgroundColor: white,
                          primary: black.withOpacity(canGoPrevious ? 1 : 0.4),
                          elevation: 4,
                          // shape: CircleBorder()
                        ),
                        child: Icon(Icons.navigate_before)),
                    addSpaceWidth(5),
                    TextButton(
                        onPressed: () {
                          loadNextDocument(false);
                        },
                        style: TextButton.styleFrom(
                          minimumSize: Size(40, 40),
                          backgroundColor: white,
                          primary: black.withOpacity(canGoNext ? 1 : 0.4),
                          elevation: 4,
                          // shape: CircleBorder()
                        ),
                        child: Icon(Icons.navigate_next)),
                    Container(
                        // width: 80,
                        height: 40,
                        margin: EdgeInsets.only(left: 15, right: 5),
                        alignment: Alignment.center,
                        child: Text(
                          "$previousPage - $nextPage out of $pageCount",
                          // "${currentPage * pageCounter} - $nextPage out of ${mainItemList.length}",
                          style: TextStyle(
                              color: black.withOpacity(.6),
                              fontSize: 12,
                              fontWeight: FontWeight.bold),
                        )),
                  ],
                ),
              ],
            ),
          ),
          Expanded(
            child: Scrollbar(
              controller: scrollController,
              isAlwaysShown: true,
              child: SingleChildScrollView(
                controller: scrollController,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(8, 8, 15, 8),
                  child: Material(
                    color: white,
                    borderRadius: BorderRadius.circular(10),
                    elevation: 5,
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Column(
                        // restorationId: "empPageList",
                        // padding: const EdgeInsets.all(16),
                        children: [
                          Row(
                            children: [
                              Expanded(
                                  child: Container(
                                height: 45,
                                decoration: BoxDecoration(
                                    color: black.withOpacity(.09),
                                    border: Border.all(
                                        color: black.withOpacity(.04)),
                                    borderRadius: BorderRadius.circular(8)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    addSpaceWidth(10),
                                    Icon(
                                      Icons.search,
                                      color: black.withOpacity(.5),
                                      size: 18,
                                    ),
                                    addSpaceWidth(5),
                                    Expanded(
                                      child: TextField(
                                        controller: searchController,
                                        focusNode: focusNode,
                                        style: TextStyle(
                                            color: black.withOpacity(.5),
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold),
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          isCollapsed: true,
                                          //  contentPadding: EdgeInsets.zero,

                                          hintText:
                                              "Search by Employee name, address, phone number or State",
                                          hintStyle: TextStyle(
                                              color: black.withOpacity(.5),
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                    if (showCancel)
                                      GestureDetector(
                                        onTap: () {
                                          searchController.clear();
                                          itemList = mainItemList;
                                          showCancel = false;
                                          searching = true;

                                          loadItems();
                                          setState(() {});
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              left: 10, right: 10),
                                          child: Icon(
                                            Icons.close,
                                            color: black.withOpacity(.5),
                                            size: 18,
                                          ),
                                        ),
                                      ),
                                  ],
                                ),
                              )),
                              addSpaceWidth(10),
                              TextButton(
                                onPressed: () {
                                  pushAndResult(context, AddEmployeeForm(),
                                      dialog: true, result: (BaseModel _) {
                                    if (null == _) return;
                                    int p = itemList.indexWhere((e) =>
                                        e.getObjectId() == _.getObjectId());
                                    if (p == -1) {
                                      itemList.insert(0, _);
                                    }
                                    int p1 = mainItemList.indexWhere((e) =>
                                        e.getObjectId() == _.getObjectId());
                                    if (p1 == -1) {
                                      mainItemList.insert(0, _);
                                    }
                                    setState(() {});
                                  });
                                },
                                child: Text(
                                  "Add Employee",
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                ),
                                style: TextButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                    primary: white,
                                    backgroundColor: green_dark,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    minimumSize: Size(120, 50)),
                              ),
                              addSpaceWidth(10),
                              TextButton(
                                onPressed: () {
                                  pushAndResult(context, KeysForm(keys),
                                      dialog: true, result: (_) {
                                    if (null == _) return;
                                    keys = _;
                                    setState(() {});
                                  });
                                },
                                child: Icon(Icons.more_vert),
                                style: TextButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                    primary: black,
                                    backgroundColor: white,
                                    shape: CircleBorder(),
                                    minimumSize: Size(50, 50)),
                              ),
                              addSpaceWidth(10),
                              TextButton(
                                onPressed: () {
                                  pushAndResult(
                                      context,
                                      FilterForm(
                                        filtering: isFiltering,
                                        selectedShifts: selectedShifts,
                                        selectedSkills: selectedSkills,
                                        selectedState: selectedState,
                                        selectedCity: selectedCity,
                                      ),
                                      dialog: true, result: (_) {
                                    if (null == _) return;
                                    // keys = _;

                                    isFiltering = _[0];
                                    selectedShifts = _[1];
                                    selectedSkills = _[2];
                                    selectedState = _[3];
                                    selectedCity = _[4];

                                    print("filter stuff $_");
                                    print("isFiltering $isFiltering");

                                    if (isFiltering) {
                                      searching = true;
                                      itemList.clear();
                                      isBusy = true;
                                      setState(() {});
                                      loadItems();
                                    } else {
                                      itemList = List.from(mainItemList);
                                      searching = false;
                                      isBusy = false;
                                      loadItems();
                                      setState(() {});
                                    }
                                  });
                                },
                                child: Icon(Icons.filter_list),
                                style: TextButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                    primary: isFiltering ? white : black,
                                    backgroundColor:
                                        isFiltering ? black : white,
                                    shape: CircleBorder(),
                                    minimumSize: Size(50, 50)),
                              )
                            ],
                          ),
                          ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: AnimatedContainer(
                              duration: Duration(milliseconds: 400),
                              child: LinearProgressIndicator(
                                valueColor: AlwaysStoppedAnimation(orange03),
                              ),
                              height: searching ? 2 : 0,
                              margin: EdgeInsets.only(
                                top: searching ? 5 : 0,
                                bottom: searching ? 5 : 0,
                              ),
                            ),
                          ),
                          DataTable2(
                              columnSpacing: 12,
                              horizontalMargin: 12,
                              //minWidth: visibleKeys.length > 5 ? 1200 : 800,
                              showCheckboxColumn: true,
                              columns: headerColumns(),
                              rows:
                                  List<DataRow>.generate(pageList.length, (p) {
                                final bm = pageList[p];
                                return DataRow(
                                    cells: List.generate(visibleKeys.length,
                                        (index) {
                                  String key = visibleKeys[index]["key"];
                                  bool isList =
                                      visibleKeys[index]["isList"] ?? false;
                                  String value = (bm.getString(key));

                                  if (key == STATUS) {
                                    int s = bm.getInt(key);
                                    value = s == APPROVED
                                        ? "Approved"
                                        : s == SENT
                                            ? "Email Sent"
                                            : "Pending";
                                  }

                                  if (isList) {
                                    value = bm.getList(key).toString();
                                    value = value.replaceAll("[", "");
                                    value = value.replaceAll("]", "");
                                  }

                                  return DataCell(Builder(
                                    builder: (c) {
                                      if (key == "action")
                                        return PopupMenuButton(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          elevation: 5,
                                          icon: Icon(Icons
                                              .more_vert), //don't specify icon if you want 3 dot menu
                                          color: Colors.blue,
                                          itemBuilder: (context) => [
                                            PopupMenuItem<int>(
                                              value: 0,
                                              child: Text(
                                                "Edit",
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                            ),
                                            PopupMenuItem<int>(
                                              value: 1,
                                              child: Text(
                                                "Delete",
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                            ),
                                          ],
                                          onSelected: (_) {
                                            if (_ == 0) {
                                              pushAndResult(
                                                  context,
                                                  AddEmployeeForm(
                                                    bm: bm,
                                                  ),
                                                  dialog: true,
                                                  result: (BaseModel _) {
                                                if (null == _) return;

                                                int p = itemList.indexWhere(
                                                    (e) =>
                                                        e.getObjectId() ==
                                                        _.getObjectId());
                                                if (p != -1) {
                                                  itemList[p] = _;
                                                }

                                                int p1 = mainItemList
                                                    .indexWhere((e) =>
                                                        e.getObjectId() ==
                                                        _.getObjectId());
                                                if (p1 != -1) {
                                                  mainItemList[p1] = _;
                                                }

                                                setState(() {});
                                              });
                                              return;
                                            }

                                            if (_ == 1) {
                                              yesNoDialog(
                                                  context,
                                                  "Delete Item?",
                                                  "Are you sure you want to delete this item?",
                                                  () {
                                                appSettingsModel
                                                  ..putInList(
                                                      EMPLOYEE_BASE_COUNT,
                                                      bm.getObjectId(),
                                                      add: false)
                                                  ..updateItems();

                                                bm.deleteItem();
                                                itemList.removeAt(p);
                                                setState(() {});
                                              });
                                              return;
                                            }
                                          },
                                        );

                                      return Text(
                                        value.isEmpty ? "------" : value,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            fontSize: 12,
                                            // color: black.withOpacity(.5),
                                            fontWeight: FontWeight.bold),
                                      );
                                    },
                                  ), onTap: () {
                                    if (key == "action") return;
                                    pushAndResult(
                                        context,
                                        AddEmployeeForm(
                                          bm: bm,
                                          readOnly: true,
                                        ),
                                        dialog: true, result: (_) {
                                      itemList[p] = _;
                                      setState(() {});
                                    });
                                  });
                                }));
                              })),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      );
    });
  }
}
