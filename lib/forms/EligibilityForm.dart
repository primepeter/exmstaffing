import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/main.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Employer {
  final titleController = TextEditingController();
  final issuingController = TextEditingController();
  final docNoController = TextEditingController();
  int expDate = 0;

  Employer({Map items}) {
    if (null != items) {
      titleController.text = items["title"];
      issuingController.text = items["issuingAuth"];
      docNoController.text = items["docNo"];
      expDate = items["expDate"];
    }
  }

  bool get isValidated {
    bool validated = true;
    if (titleController.text.isEmpty) return false;
    if (issuingController.text.isEmpty) return false;
    if (docNoController.text.isEmpty) return false;
    // if (expDate == 0) return false;
    return validated;
  }

  Map get items {
    return {
      "title": titleController.text,
      "issuingAuth": issuingController.text,
      "docNo": docNoController.text,
      "expDate": expDate,
    };
  }
}

class EligibilityForm extends StatefulWidget {
  const EligibilityForm({Key key}) : super(key: key);

  @override
  _EligibilityFormState createState() => _EligibilityFormState();
}

class _EligibilityFormState extends State<EligibilityForm> {
  List<Employer> employerInfo = [];

  final firstNameController = TextEditingController();
  final middleNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final emailController = TextEditingController();
  final phoneController = TextEditingController();
  final addressController = TextEditingController();
  final zipController = TextEditingController();

  final nameController = TextEditingController();
  final ssNController = TextEditingController();
  final aptNumController = TextEditingController();

  String selectedState;
  String selectedCity;
  String selectedSkill = "";

  bool termsAccepted = false;

  int dateOfBirth = 0;

  List citizenTypes = [
    "A citizen of the US",
    "A non-citizen national of the US",
    "A lawful permanent resident",
    "An alien authorized to work",
  ];
  int selectedCitizen = -1;
  final regNumController = TextEditingController();
  int expirationDate = 0;

  List translatorTypes = [
    "I did not use a preparer or translator.",
    "A preparer(s) and/or translator(s) assisted the employee in completing Section 1",
  ];
  int selectedTranslator = -1;
  final transFirstNameController = TextEditingController();
  final transLastNameController = TextEditingController();
  final transAddressController = TextEditingController();
  final transZipController = TextEditingController();

  String transState;
  String transCity;

  List<StreamSubscription> subs = [];

  String path = "eligibilityForm";
  BaseModel mainModel;
  BaseModel formModel;
  String get formId => routeDataHolder["/eligibilityForm"];
  String get name => mainModel.getFullName();
  List<BaseModel> formsToFill = [];

  @override
  initState() {
    super.initState();
    loadSettings();
    loadItems();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadSettings() async {
    var settingsSub = FirebaseFirestore.instance
        .collection(APP_SETTINGS_BASE)
        .doc(APP_SETTINGS)
        .snapshots()
        .listen((shot) {
      if (shot != null) {
        appSettingsModel = BaseModel(doc: shot);
        setState(() {});
      }
    });
    subs.add(settingsSub);
  }

  loadItems() async {
    if (null == formId || formId.isEmpty) {
      showErrorDialog(
          context, "Ops the application link is invalid or has expired!",
          cancellable: false);
      return;
    }

    var sub = FirebaseFirestore.instance
        .collection(EMPLOYEE_BASE)
        .where(OBJECT_ID, isEqualTo: formId)
        .limit(1)
        .snapshots()
        .listen((value) {
      if (value.size == 0) {
        showErrorDialog(
            context, "Ops the application link is invalid or has expired!",
            cancellable: false);
        return;
      }
      mainModel = BaseModel(doc: value.docs[0]);
      employerInfo = mainModel
          .getListModel(EMPLOYERS_INFO)
          .map((e) => Employer(items: e.items))
          .toList();

      lastNameController.text = mainModel.getString(LAST_NAME);
      firstNameController.text = mainModel.getString(FIRST_NAME);
      middleNameController.text = mainModel.getString(MIDDLE_NAME);
      emailController.text = mainModel.getString(EMAIL);
      phoneController.text = mainModel.getString(TELEPHONE);
      addressController.text = mainModel.getString(ADDRESS);
      aptNumController.text = mainModel.getString(APT_NUMBER);
      dateOfBirth = mainModel.getInt(DATE_OF_BIRTH);
      ssNController.text = mainModel.getString(SOCIAL_SECURITY_NUMBER);
      selectedState = mainModel.getString(STATE);
      selectedCity = mainModel.getString(CITY);
      zipController.text = mainModel.getString(ZIPCODE);

      selectedCitizen = mainModel.getInt(CITIZEN_INDEX, true);
      selectedTranslator = mainModel.getInt(TRANS_INDEX, true);
      transAddressController.text = mainModel.getString(TRANS_ADDRESS);
      transState = mainModel.getString(TRANS_STATE);
      transCity = mainModel.getString(TRANS_CITY);
      transZipController.text = mainModel.getString(TRANS_ZIP);

      formsToFill = mainModel.getListModel(FORMS_TO_FILL);
      int p = formsToFill.indexWhere((e) => e.getString("path") == path);
      if (p != -1) {
        formModel = formsToFill[p];

        // employerInfo = formModel
        //     .getListModel(EMPLOYERS_INFO)
        //     .map((e) => Employer(items: e.items))
        //     .toList();
        //
        // lastNameController.text = formModel.getString(LAST_NAME);
        // firstNameController.text = formModel.getString(FIRST_NAME);
        // middleNameController.text = formModel.getString(MIDDLE_NAME);
        // emailController.text = formModel.getString(EMAIL);
        // phoneController.text = formModel.getString(TELEPHONE);
        // addressController.text = formModel.getString(ADDRESS);
        // aptNumController.text = formModel.getString(APT_NUMBER);
        // dateOfBirth = formModel.getInt(DATE_OF_BIRTH);
        // ssNController.text = formModel.getString(SOCIAL_SECURITY_NUMBER);
        // selectedState = formModel.getString(STATE);
        // selectedCity = formModel.getString(CITY);
        // zipController.text = formModel.getString(ZIPCODE);
        //
        // selectedCitizen = formModel.getInt(CITIZEN_INDEX, true);
        // selectedTranslator = formModel.getInt(TRANS_INDEX, true);
        // transAddressController.text = formModel.getString(TRANS_ADDRESS);
        // transState = formModel.getString(TRANS_STATE);
        // transCity = formModel.getString(TRANS_CITY);
        // transZipController.text = formModel.getString(TRANS_ZIP);
      }
      setState(() {});
    });
    subs.add(sub);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: LayoutBuilder(builder: (c, box) {
              final size = box.maxWidth;
              print("size $size");
              bool isLargeScreen = box.maxWidth > 850;
              final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

              return Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                // padding: EdgeInsets.only(
                //     top: 130, bottom: 50, left: size * 0.2, right: size * 0.2),
                padding: EdgeInsets.only(
                  left: isLargeScreen ? size * 0.15 : 20,
                  right: isLargeScreen ? size * 0.15 : 20,
                  top: 120,
                  bottom: 20,
                ),
                decoration: BoxDecoration(
                    color: isLargeScreen ? scaffoldColor : white,
                    borderRadius: BorderRadius.circular(10)),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: isLargeScreen ? white : null,
                          borderRadius: BorderRadius.circular(10)),
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              style: TextButton.styleFrom(
                                minimumSize: Size(40, 40),
                                backgroundColor: white,
                                primary: black,
                                elevation: 4,
                                // shape: CircleBorder()
                              ),
                              child: Icon(Icons.navigate_before)),
                          addSpace(10),
                          Center(
                            child: Text(
                              "Employment Eligibility Verification "
                              "Department of Homeland Security",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(8),
                            margin: EdgeInsets.only(top: 20, bottom: 20),
                            color: black.withOpacity(.03),
                            alignment: Alignment.centerLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Section 1. Employee Information and Attestation",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                                addSpace(2),
                                Text(
                                  "(Employees must complete and sign Section 1 "
                                  "of Form I-9 no later than the first day "
                                  "of employment, but not before accepting "
                                  "a job offer.)",
                                  style: TextStyle(
                                    fontSize: 12,
                                    // fontWeight: FontWeight.bold
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: inputField("Last Name (Family Name)",
                                    lastNameController, onChanged: (s) {
                                  setState(() {});
                                }),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child: inputField("First Name (Given Name)",
                                    firstNameController, onChanged: (s) {
                                  setState(() {});
                                }),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child: inputField(
                                    "Middle Name", middleNameController,
                                    onChanged: (s) {
                                  setState(() {});
                                }),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: inputField(
                                    "Employee's Email", emailController,
                                    onChanged: (s) {
                                  setState(() {});
                                }),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child: inputField(
                                    "Employee's Telephone", phoneController,
                                    isNum: true, onChanged: (s) {
                                  setState(() {});
                                }),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: inputField(
                                    "Address (Street Number & Name)",
                                    addressController, onChanged: (s) {
                                  setState(() {});
                                }),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child:
                                    inputField("Apt. Number", aptNumController,
                                        onChanged: (s) {
                                  setState(() {});
                                }),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: clickField(
                                    "Date of Birth",
                                    dateOfBirth == 0
                                        ? ""
                                        : getSimpleDate(dateOfBirth,
                                            pattern: "EEE MMMM dd, yyyy"),
                                    () async {
                                  final DateTime picked = await showDatePicker(
                                      builder: (ctx, child) {
                                        return Theme(
                                            data: Theme.of(context).copyWith(
                                              colorScheme: ColorScheme.light(
                                                primary: appColor,
                                                onPrimary:
                                                    black, // header text color
                                                // onSurface: Colors.green, // body text color
                                              ),
                                              buttonTheme: ButtonThemeData(
                                                  textTheme:
                                                      ButtonTextTheme.primary),
                                            ),
                                            child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                child: child));
                                      },
                                      context: context,
                                      initialDate: dateOfBirth == 0
                                          ? DateTime(DateTime.now().year - 18)
                                          : DateTime.fromMillisecondsSinceEpoch(
                                              dateOfBirth),
                                      firstDate: DateTime(1960),
                                      lastDate:
                                          DateTime(DateTime.now().year - 18));

                                  if (picked != null &&
                                      picked.millisecondsSinceEpoch !=
                                          dateOfBirth)
                                    dateOfBirth = picked.millisecondsSinceEpoch;
                                  setState(() {});
                                }),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                  child: inputField(
                                      "U.S Social Security Number",
                                      ssNController, onChanged: (s) {
                                setState(() {});
                              })),
                            ],
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: clickField("State", selectedState, () {
                                  showListDialog(
                                      context, usaStates.keys.toList(), (_) {
                                    selectedState = usaStates.keys.toList()[_];
                                    selectedCity = null;
                                    setState(() {});
                                  }, returnIndex: true);
                                }),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                  child: clickField("City", selectedCity, () {
                                if (null == selectedState) {
                                  showError(context, "Select a state first");
                                  return;
                                }
                                showListDialog(
                                    context, usaStates[selectedState].toList(),
                                    (_) {
                                  selectedCity =
                                      usaStates[selectedState].toList()[_];
                                  setState(() {});
                                }, returnIndex: true);
                              })),
                              addSpaceWidth(10),
                              Expanded(
                                  child: inputField("ZipCode", zipController,
                                      onChanged: (s) {
                                setState(() {});
                              })),
                            ],
                          ),
                          addSpace(10),
                          Text(
                            "I am aware that federal law provides for imprisonment "
                            "and/or fines for false statements or use of false "
                            "documents in connection with the completion of this form. "
                            "I attest, under penalty of perjury, that I am "
                            "(check one of the following boxes):",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                          addSpace(10),
                          Wrap(
                            children: List.generate(citizenTypes.length, (p) {
                              String title = citizenTypes[p];
                              bool active = p == selectedCitizen;
                              return GestureDetector(
                                onTap: () {
                                  selectedCitizen = p;
                                  setState(() {});
                                },
                                child: Container(
                                  height: 40,
                                  width: 250,
                                  margin: EdgeInsets.all(2),
                                  child: Text(
                                    title,
                                    style: TextStyle(
                                        color: active ? white : black,
                                        fontWeight: active
                                            ? FontWeight.bold
                                            : FontWeight.normal),
                                  ),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: active ? (black) : white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: black.withOpacity(.05),
                                            spreadRadius: 2,
                                            blurRadius: 5)
                                      ],
                                      borderRadius: BorderRadius.circular(8)),
                                ),
                              );
                            }),
                          ),
                          if (selectedCitizen > 1)
                            Row(
                              children: [
                                Expanded(
                                    child: inputField(
                                        "(Alien Registration Number/USCIS Number):",
                                        regNumController, onChanged: (s) {
                                  setState(() {});
                                })),
                                addSpaceWidth(10),
                                Expanded(
                                  child: clickField(
                                      "(Expiration date, if applicable):",
                                      expirationDate == 0
                                          ? ""
                                          : getSimpleDate(expirationDate,
                                              pattern: "EEE MMMM dd, yyyy"),
                                      () async {
                                    final DateTime picked =
                                        await showDatePicker(
                                            builder: (ctx, child) {
                                              return Theme(
                                                  data: Theme.of(context)
                                                      .copyWith(
                                                    colorScheme:
                                                        ColorScheme.light(
                                                      primary: appColor,
                                                      onPrimary:
                                                          black, // header text color
                                                      // onSurface: Colors.green, // body text color
                                                    ),
                                                    buttonTheme:
                                                        ButtonThemeData(
                                                            textTheme:
                                                                ButtonTextTheme
                                                                    .primary),
                                                  ),
                                                  child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      child: child));
                                            },
                                            context: context,
                                            initialDate: expirationDate == 0
                                                ? DateTime.now()
                                                : DateTime
                                                    .fromMillisecondsSinceEpoch(
                                                        expirationDate),
                                            firstDate: DateTime.now(),
                                            lastDate: DateTime(
                                                DateTime.now().year + 8));

                                    if (picked != null &&
                                        picked.millisecondsSinceEpoch !=
                                            expirationDate)
                                      expirationDate =
                                          picked.millisecondsSinceEpoch;
                                    setState(() {});
                                  }),
                                ),
                              ],
                            ),
                          addSpace(10),
                          Text(
                            "Preparer and/or Translator Certification (check one):",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                          addSpace(10),
                          Row(
                            children:
                                List.generate(translatorTypes.length, (p) {
                              String title = translatorTypes[p];
                              bool active = p == selectedTranslator;
                              return Expanded(
                                child: GestureDetector(
                                  onTap: () {
                                    selectedTranslator = p;
                                    setState(() {});
                                  },
                                  child: Container(
                                    height: 60,
                                    //width: 250,
                                    padding: EdgeInsets.all(4),
                                    margin: EdgeInsets.all(2),
                                    child: Text(
                                      title,
                                      style: TextStyle(
                                          color: active ? white : black,
                                          fontWeight: active
                                              ? FontWeight.bold
                                              : FontWeight.normal),
                                    ),
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: active ? (black) : white,
                                        boxShadow: [
                                          BoxShadow(
                                              color: black.withOpacity(.05),
                                              spreadRadius: 2,
                                              blurRadius: 5)
                                        ],
                                        borderRadius: BorderRadius.circular(8)),
                                  ),
                                ),
                              );
                            }),
                          ),
                          if (selectedTranslator == 1) ...[
                            Row(
                              children: [
                                Expanded(
                                    child: inputField("Last Name (Family Name)",
                                        transLastNameController,
                                        onChanged: (s) {
                                  setState(() {});
                                })),
                                addSpaceWidth(10),
                                Expanded(
                                    child: inputField("First Name (Given Name)",
                                        transLastNameController,
                                        onChanged: (s) {
                                  setState(() {});
                                })),
                              ],
                            ),
                            addSpace(10),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                    child: inputField(
                                        "Address", transAddressController,
                                        onChanged: (s) {
                                  setState(() {});
                                })),
                                addSpaceWidth(10),
                                Expanded(
                                  child: clickField("State", transState, () {
                                    showListDialog(
                                        context, usaStates.keys.toList(), (_) {
                                      transState = usaStates.keys.toList()[_];
                                      transCity = null;
                                      setState(() {});
                                    }, returnIndex: true);
                                  }),
                                ),
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                    child: clickField("City", transCity, () {
                                  if (null == transState) {
                                    showError(context, "Select a state first");
                                    return;
                                  }
                                  showListDialog(
                                      context, usaStates[transState].toList(),
                                      (_) {
                                    transCity =
                                        usaStates[transState].toList()[_];
                                    setState(() {});
                                  }, returnIndex: true);
                                })),
                                addSpaceWidth(10),
                                Expanded(
                                    child: inputField(
                                        "ZipCode", transZipController,
                                        onChanged: (s) {
                                  setState(() {});
                                })),
                              ],
                            ),
                          ],
                          Container(
                            padding: EdgeInsets.all(8),
                            margin: EdgeInsets.only(top: 20, bottom: 20),
                            color: black.withOpacity(.03),
                            alignment: Alignment.centerLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Section 2. Employer or Authorized Representative Review and Verification",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                                addSpace(2),
                                Text(
                                  "(Employers or their authorized representative "
                                  "must complete and sign Section 2 within 3 "
                                  "business days of the employee's first day "
                                  "of employment. You must physically examine "
                                  "one document from List A OR a combination of "
                                  "one document from List B and one document from "
                                  "List C as listed on the Lists of Acceptable Documents.)",
                                  style: TextStyle(
                                    fontSize: 12,
                                    // fontWeight: FontWeight.bold
                                  ),
                                ),
                              ],
                            ),
                          ),
                          addLine(20, black.withOpacity(.03), 0, 10, 0, 10),
                          Row(
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Employer Information (Employer Only!)",
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      "Add Identity and Employment Authorization",
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: black.withOpacity(.5),
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                              if (employerInfo.length < 3)
                                TextButton(
                                  onPressed: () {
                                    employerInfo.add(Employer());
                                    setState(() {});
                                  },
                                  child: Text(
                                    "Add Document",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  style: TextButton.styleFrom(
                                      padding: EdgeInsets.zero,
                                      primary: white,
                                      backgroundColor: black,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      minimumSize: Size(160, 50)),
                                )
                            ],
                          ),
                          addSpace(20),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            child: Builder(builder: (ctx) {
                              if (employerInfo.isEmpty)
                                return Container(
                                  padding: EdgeInsets.all(0),
                                  child: DottedBorder(
                                    color: black.withOpacity(.5),
                                    strokeWidth: 1,
                                    borderType: BorderType.RRect,
                                    radius: Radius.circular(10),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Container(
                                          height: 150,
                                          alignment: Alignment.center,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Icon(
                                                Icons.person,
                                                color: black.withOpacity(.5),
                                                size: 30,
                                              ),
                                              addSpace(10),
                                              Text(
                                                "You have not added any employer information yet",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: black.withOpacity(.5)
                                                    // fontWeight: FontWeight.bold,
                                                    ),
                                              )
                                            ],
                                          )),
                                    ),
                                  ),
                                );

                              return Column(
                                children:
                                    List.generate(employerInfo.length, (index) {
                                  final fm = employerInfo[index];

                                  return Container(
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                children: [
                                                  Expanded(
                                                      child: inputField(
                                                          "Document Title",
                                                          fm.titleController,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                      child: inputField(
                                                          "Issuing Authority",
                                                          fm.issuingController,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                      child: inputField(
                                                          "Document Number",
                                                          fm.docNoController,
                                                          isNum: true,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                    child: clickField(
                                                        "Expiration Date (If Any)",
                                                        fm.expDate == 0
                                                            ? ""
                                                            : getSimpleDate(
                                                                fm.expDate,
                                                                pattern:
                                                                    "EEE MMMM dd, yyyy"),
                                                        () async {
                                                      final DateTime picked =
                                                          await showDatePicker(
                                                              builder:
                                                                  (ctx, child) {
                                                                return Theme(
                                                                    data: Theme.of(
                                                                            context)
                                                                        .copyWith(
                                                                      colorScheme:
                                                                          ColorScheme
                                                                              .light(
                                                                        primary:
                                                                            appColor,
                                                                        onPrimary:
                                                                            black, // header text color
                                                                        // onSurface: Colors.green, // body text color
                                                                      ),
                                                                      buttonTheme:
                                                                          ButtonThemeData(
                                                                              textTheme: ButtonTextTheme.primary),
                                                                    ),
                                                                    child: ClipRRect(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                10),
                                                                        child:
                                                                            child));
                                                              },
                                                              context: context,
                                                              initialDate: fm
                                                                          .expDate ==
                                                                      0
                                                                  ? DateTime
                                                                      .now()
                                                                  : DateTime
                                                                      .fromMillisecondsSinceEpoch(fm
                                                                          .expDate),
                                                              firstDate:
                                                                  DateTime
                                                                      .now(),
                                                              lastDate: DateTime(
                                                                  DateTime.now()
                                                                          .year +
                                                                      8));

                                                      if (picked != null &&
                                                          picked.millisecondsSinceEpoch !=
                                                              fm.expDate)
                                                        fm.expDate = picked
                                                            .millisecondsSinceEpoch;
                                                      setState(() {});
                                                    }),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        addSpaceWidth(10),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 15),
                                          child: TextButton(
                                            onPressed: () {
                                              employerInfo.removeAt(index);
                                              setState(() {});
                                            },
                                            child: Text(
                                              "Remove",
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            style: TextButton.styleFrom(
                                                padding: EdgeInsets.zero,
                                                primary: white,
                                                backgroundColor: red,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8)),
                                                minimumSize: Size(100, 50)),
                                          ),
                                        )
                                      ],
                                    ),
                                  );
                                }),
                              );
                            }),
                          ),
                          addSpace(50),
                        ],
                      ),
                    ),
                    addSpace(20),
                    InkWell(
                      onTap: () {
                        termsAccepted = !termsAccepted;
                        setState(() {});
                      },
                      child: Container(
                        child: Row(
                          children: [
                            checkBox(termsAccepted),
                            addSpaceWidth(20),
                            Expanded(
                                child: Text(
                              footerTermsText,
                              style: TextStyle(fontSize: 12),
                            ))
                          ],
                        ),
                      ),
                    ),
                    addSpace(20),
                    Container(
                      height: 60,
                      child: TextButton(
                        onPressed: !termsAccepted
                            ? null
                            : () {
                                String lName = lastNameController.text;
                                String fName = firstNameController.text;
                                String mName = middleNameController.text;
                                String email = emailController.text;
                                String tel = phoneController.text;
                                String address = addressController.text;
                                String aptNumber = aptNumController.text;
                                String ssn = ssNController.text;
                                String zip = zipController.text;

                                String transLName =
                                    transLastNameController.text;
                                String transFName =
                                    transFirstNameController.text;
                                String transAddress =
                                    transAddressController.text;
                                String transZip = transZipController.text;

                                int status = STATUS_COMPLETED;

                                if (
                                    // employerInfo.isEmpty &&
                                    lName.isEmpty &&
                                        fName.isEmpty &&
                                        mName.isEmpty &&
                                        email.isEmpty &&
                                        tel.isEmpty &&
                                        address.isEmpty &&
                                        // aptNumber.isEmpty &&
                                        dateOfBirth == 0 &&
                                        ssn.isEmpty &&
                                        (null == selectedState ||
                                            selectedState.isEmpty) &&
                                        (null == selectedCity ||
                                            selectedCity.isEmpty) &&
                                        zip.isEmpty &&
                                        selectedCitizen == -1 &&
                                        selectedTranslator == -1 &&
                                        (selectedTranslator == 1 &&
                                            transLName.isEmpty) &&
                                        (selectedTranslator == 1 &&
                                            transFName.isEmpty) &&
                                        (selectedTranslator == 1 &&
                                            transAddress.isEmpty) &&
                                        (selectedTranslator == 1 &&
                                            transState.isEmpty) &&
                                        (selectedTranslator == 1 &&
                                            transCity.isEmpty) &&
                                        (selectedTranslator == 1 &&
                                            transZip.isEmpty)) {
                                  status = STATUS_UNDONE;
                                }

                                if (
                                    // employerInfo.isEmpty ||
                                    lName.isEmpty ||
                                        fName.isEmpty ||
                                        mName.isEmpty ||
                                        email.isEmpty ||
                                        tel.isEmpty ||
                                        address.isEmpty ||
                                        // aptNumber.isEmpty ||
                                        dateOfBirth == 0 ||
                                        ssn.isEmpty ||
                                        (null == selectedState ||
                                            selectedState.isEmpty) ||
                                        (null == selectedCity ||
                                            selectedCity.isEmpty) ||
                                        zip.isEmpty && selectedCitizen == -1 ||
                                        selectedTranslator == -1 ||
                                        (selectedTranslator == 1 &&
                                            transLName.isEmpty) ||
                                        (selectedTranslator == 1 &&
                                            transFName.isEmpty) ||
                                        (selectedTranslator == 1 &&
                                            transAddress.isEmpty) ||
                                        (selectedTranslator == 1 &&
                                            transState.isEmpty) ||
                                        (selectedTranslator == 1 &&
                                            transCity.isEmpty) ||
                                        (selectedTranslator == 1 &&
                                            transZip.isEmpty)) {
                                  status = STATUS_IN_PROGRESS;
                                }

                                mainModel.put(EMPLOYERS_INFO,
                                    employerInfo.map((e) => e.items).toList());

                                mainModel.put(LAST_NAME, lName);
                                mainModel.put(FIRST_NAME, fName);
                                mainModel.put(MIDDLE_NAME, mName);
                                mainModel.put(EMAIL, email);
                                mainModel.put(TELEPHONE, tel);
                                mainModel.put(ADDRESS, address);
                                mainModel.put(APT_NUMBER, aptNumber);
                                mainModel.put(DATE_OF_BIRTH, dateOfBirth);
                                mainModel.put(SOCIAL_SECURITY_NUMBER, ssn);
                                mainModel.put(STATE, selectedState);
                                mainModel.put(CITY, selectedCity);
                                mainModel.put(ZIPCODE, zip);

                                mainModel.put(CITIZEN_INDEX, selectedCitizen);
                                mainModel.put(TRANS_INDEX, selectedTranslator);
                                mainModel.put(TRANS_ADDRESS, transAddress);
                                mainModel.put(TRANS_STATE, transState);
                                mainModel.put(TRANS_CITY, transCity);
                                mainModel.put(TRANS_ZIP, transZip);

                                // formModel.put(EMPLOYERS_INFO,
                                //     employerInfo.map((e) => e.items).toList());
                                //
                                // formModel.put(LAST_NAME, lName);
                                // formModel.put(FIRST_NAME, fName);
                                // formModel.put(MIDDLE_NAME, mName);
                                // formModel.put(EMAIL, email);
                                // formModel.put(TELEPHONE, tel);
                                // formModel.put(ADDRESS, address);
                                // formModel.put(APT_NUMBER, aptNumber);
                                // formModel.put(DATE_OF_BIRTH, dateOfBirth);
                                // formModel.put(SOCIAL_SECURITY_NUMBER, ssn);
                                // formModel.put(STATE, selectedState);
                                // formModel.put(CITY, selectedCity);
                                // formModel.put(ZIPCODE, zip);
                                //
                                // formModel.put(CITIZEN_INDEX, selectedCitizen);
                                // formModel.put(TRANS_INDEX, selectedTranslator);
                                // formModel.put(TRANS_ADDRESS, transAddress);
                                // formModel.put(TRANS_STATE, transState);
                                // formModel.put(TRANS_CITY, transCity);
                                // formModel.put(TRANS_ZIP, transZip);

                                formModel.put(STATUS, status);

                                int p = formsToFill.indexWhere(
                                    (e) => e.getString("path") == path);
                                if (p != -1) {
                                  formsToFill[p] = formModel;
                                }

                                mainModel.put(FORMS_TO_FILL,
                                    formsToFill.map((e) => e.items).toList());
                                mainModel.updateItems();

                                showMessage(
                                  context,
                                  Icons.check,
                                  green,
                                  "Form Submitted!",
                                  "You have successful completed your Employment"
                                      " Eligibility Verification Department "
                                      "of Homeland Security form",
                                  // cancellable: false,
                                  delayInMilli: 700,
                                  clickYesText: "Go Back",
                                  cancellable: false,
                                  onClicked: (_) {
                                    // Navigator.pop(context, true);
                                    Navigator.pushReplacementNamed(
                                        context, "/applicationForm??$formId");
                                  },
                                );
                              },
                        child: Center(
                          child: Text(
                            "Save Information".toUpperCase(),
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            minimumSize: Size(300, 50)),
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
          appBarHeader(context)
        ],
      ),
    );
  }
}
