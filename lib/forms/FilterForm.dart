import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class FilterForm extends StatefulWidget {
  const FilterForm(
      {this.filtering = false,
      this.selectedShifts = const [],
      this.selectedSkills = const [],
      this.selectedState = "",
      this.selectedCity = ""});

  final bool filtering;
  final List selectedSkills, selectedShifts;
  final String selectedState;
  final String selectedCity;

  @override
  _FilterFormState createState() => _FilterFormState();
}

class _FilterFormState extends State<FilterForm> {
  List skills = appSettingsModel.getList("skills");
  List selectedSkills = [];
  List selectedShifts = [];
  bool isFiltering = false;

  List shiftPeriod = [
    "First Shift",
    "Second Shift",
    "Third/Overnight Shift",
  ];
  String selectedState = "";
  String selectedCity = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isFiltering = widget.filtering;
    selectedShifts = widget.selectedShifts;
    selectedSkills = widget.selectedSkills;
    selectedState = widget.selectedState;
    selectedCity = widget.selectedCity;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: black.withOpacity(.8),
      child: LayoutBuilder(builder: (c, box) {
        bool isLargeScreen = box.maxWidth > 850;
        // final size = getScreenWidth(c);
        final size = box.maxWidth;
        final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

        return SingleChildScrollView(
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: Container(
                decoration: BoxDecoration(
                    color: white, borderRadius: BorderRadius.circular(15)),
                padding: EdgeInsets.all(30),
                margin: EdgeInsets.only(
                    top: 20,
                    bottom: 20,
                    left: ((0.2) * size),
                    right: ((0.2) * size)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            "Filter Settings",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            final items = [false, [], [], "", ""];

                            Navigator.pop(context, items);
                          },
                          child: Text(
                            "Reset",
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.bold),
                          ),
                          style: TextButton.styleFrom(
                              padding: EdgeInsets.zero,
                              primary: white,
                              backgroundColor: red,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              minimumSize: Size(100, 50)),
                        ),
                        addSpaceWidth(10),
                        TextButton(
                          onPressed: () {
                            final items = [
                              isFiltering,
                              selectedShifts,
                              selectedSkills,
                              selectedState,
                              selectedCity,
                            ];
                            Navigator.pop(context, items);
                          },
                          child: Center(
                            child: Text(
                              "Apply",
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold),
                            ),
                          ),
                          style: TextButton.styleFrom(
                              primary: white,
                              backgroundColor: green_dark,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              minimumSize: Size(100, 50)),
                        ),
                      ],
                    ),
                    addSpace(20),
                    clickField("Choose State", selectedState, () {
                      showListDialog(context, usaStates.keys.toList(), (_) {
                        selectedState = usaStates.keys.toList()[_];
                        selectedCity = "";
                        isFiltering = true;
                        setState(() {});
                      }, returnIndex: true);
                    }),
                    clickField("Choose City", selectedCity, () {
                      if (null == selectedState || selectedState.isEmpty) {
                        showError(context, "Select a state first");
                        return;
                      }
                      showListDialog(context, usaStates[selectedState].toList(),
                          (_) {
                        selectedCity = usaStates[selectedState].toList()[_];
                        setState(() {});
                      }, returnIndex: true);
                    }),
                    addSpace(10),
                    Text(
                      "Skill Set",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    addSpace(5),
                    Wrap(
                      children: List.generate(skills.length, (p) {
                        String title = skills[p];
                        bool active = selectedSkills.contains(title);
                        return GestureDetector(
                          onTap: () {
                            if (active) {
                              selectedSkills.remove(title);
                            } else {
                              isFiltering = true;
                              selectedSkills.add(title);
                            }
                            setState(() {});
                          },
                          child: Container(
                            height: 40,
                            width: 160,
                            margin: EdgeInsets.all(2),
                            child: Text(
                              title,
                              style: TextStyle(
                                  fontSize: 12,
                                  color: active ? white : black,
                                  fontWeight: active
                                      ? FontWeight.bold
                                      : FontWeight.normal),
                            ),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: active ? (black) : white,
                                boxShadow: [
                                  BoxShadow(
                                      color: black.withOpacity(.05),
                                      spreadRadius: 2,
                                      blurRadius: 5)
                                ],
                                borderRadius: BorderRadius.circular(8)),
                          ),
                        );
                      }),
                    ),
                    addSpace(20),
                    Text(
                      "Work Shift",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    addSpace(5),
                    Wrap(
                      children: List.generate(shiftPeriod.length, (p) {
                        String title = shiftPeriod[p];
                        bool active = selectedShifts.contains(title);
                        return GestureDetector(
                          onTap: () {
                            if (active) {
                              selectedShifts.remove(title);
                            } else {
                              isFiltering = true;
                              selectedShifts.add(title);
                            }
                            setState(() {});
                          },
                          child: Container(
                            height: 40,
                            width: 160,
                            margin: EdgeInsets.all(2),
                            child: Text(
                              title,
                              style: TextStyle(
                                  color: active ? white : black,
                                  fontWeight: active
                                      ? FontWeight.bold
                                      : FontWeight.normal),
                            ),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: active ? black : white,
                                boxShadow: [
                                  BoxShadow(
                                      color: black.withOpacity(.05),
                                      spreadRadius: 2,
                                      blurRadius: 5)
                                ],
                                borderRadius: BorderRadius.circular(8)),
                          ),
                        );
                      }),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      }),
    );
  }
}
