import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/main.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:platform_device_id/platform_device_id.dart';
import 'package:url_launcher/link.dart';

class LinkForms extends StatefulWidget {
  const LinkForms({Key key}) : super(key: key);

  @override
  _LinkFormsState createState() => _LinkFormsState();
}

class _LinkFormsState extends State<LinkForms> with WidgetsBindingObserver {
  List<BaseModel> itemList = [];
  bool isBusy = true;
  bool noData = true;

  BaseModel model;
  String get formId => routeDataHolder["/applicationForm"];
  String get name => model.getString(FIRST_NAME);
  List<BaseModel> formsToFill = [];
  List<StreamSubscription> subs = [];

  int formExpiry = 0;
  int formSent = 0;
  bool formsSecured = false;
  bool formsUnLocked = false;
  bool formsExpired = false;
  String formsPassword = "";

  final passwordController = TextEditingController();
  final passwordController2 = TextEditingController();
  bool passVisible = false;
  bool passVisible2 = false;

  String errorMsg = "";

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    //loadItems();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
  }

  loadItems() async {
    print("formId $formId");
    print("formId $routeDataHolder");
    if (null == formId || formId.isEmpty) {
      errorMsg = "Ops the application link is invalid or has expired!";
      isBusy = false;
      setState(() {});
      return;
    }

    var sub = FirebaseFirestore.instance
        .collection(EMPLOYEE_BASE)
        .where(OBJECT_ID, isEqualTo: formId)
        .limit(1)
        .snapshots()
        .listen((value) async {
      // String deviceId = await PlatformDeviceId.getDeviceId;

      if (value.size == 0) {
        errorMsg = "Ops the application link is invalid or has expired!";
        isBusy = false;
        setState(() {});
        return;
      }

      model = BaseModel(doc: value.docs[0]);
      String devId = model.getString(DEVICE_ID);
      formsSecured = model.getBoolean(FORMS_SECURED);
      // formsUnLocked = model.getBoolean(FORMS_UNLOCKED);
      formsPassword = model.getString(FORMS_PASSWORD);
      formExpiry = model.getInt(FORMS_EXPIRY);
      formSent = model.getInt(FORMS_SENT);
      int time = model.getTime();
      int against = (DateTime.now().millisecondsSinceEpoch -
          (Duration.millisecondsPerDay * 3));
      final dt = DateTime.fromMillisecondsSinceEpoch(formSent);
      int inDays = DateTime.now().difference(dt).inDays;

      int timeUpdated = model.getInt(TIME_UPDATED);
      int against2 = (DateTime.now().millisecondsSinceEpoch -
          (Duration.millisecondsPerMinute * 30));
      final dt2 = DateTime.fromMillisecondsSinceEpoch(timeUpdated);
      int inMinutes = DateTime.now().difference(dt2).inMinutes;
      //formsUnLocked = inMinutes < 30;

      formsExpired = inDays > 3;
      print("days $inDays"); //1634926846228
      print("time $time"); //1634926846228
      // print("against $against"); //1635019344960

      //1635620610583
      formsToFill = model.getListModel(FORMS_TO_FILL);
      isBusy = false;
      if (formsToFill.isEmpty) formsExpired = true;
      setState(() {});
    });
    subs.add(sub);
  }

  String getStatusMsg(int status) {
    if (status == STATUS_UNDONE) return "Your yet to start filling this form.";
    if (status == STATUS_IN_PROGRESS) return "Your yet to complete this form.";
    return "Your have completed this form.";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          LayoutBuilder(builder: (c, box) {
            final size = box.maxWidth;
            bool isLargeScreen = box.maxWidth > 850;
            final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

            return Container(
              margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
              // padding: EdgeInsets.only(
              //     top: 130, bottom: 50, left: size * 0.2, right: size * 0.2),
              padding: EdgeInsets.only(
                left: isLargeScreen ? size * 0.25 : 20,
                right: isLargeScreen ? size * 0.25 : 20,
                top: 120,
                bottom: 20,
              ),
              decoration: BoxDecoration(
                  color: isLargeScreen ? scaffoldColor : white,
                  borderRadius: BorderRadius.circular(10)),
              alignment: Alignment.center,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        border: Border(
                            left: BorderSide(
                                color: Colors.deepOrange, width: 4))),
                    padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
                    child: Text(
                      "Public Links",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  addSpace(20),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: List.generate(3, (p) {
                        final bm = formsToFill[p];
                        String title = "Payroll Link";
                        if (p == 1) title = "Sample Timesheet";
                        if (p == 2) title = "Timesheet";

                        String path =
                            "https://exmstaffing.web.app/${p == 0 ? "payroll" : p == 1 ? "sample_timesheet" : "timesheet"}.pdf";

                        return Container(
                          padding: EdgeInsets.all(10),
                          margin: EdgeInsets.only(bottom: 15, top: 0),
                          decoration: BoxDecoration(
                              color: white,
                              border: Border.all(color: black.withOpacity(.01)),
                              borderRadius: BorderRadius.circular(10)),
                          alignment: Alignment.centerLeft,
                          child: Row(
                            children: [
                              Text(
                                title,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    color: black,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w900),
                              ),
                              Spacer(),
                              Link(
                                uri: Uri.parse(path),
                                builder: (c, onClick) {
                                  return TextButton(
                                    onPressed: onClick,
                                    child: Text(
                                      "Open",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    style: TextButton.styleFrom(
                                        padding: EdgeInsets.zero,
                                        primary: white,
                                        backgroundColor: green_dark,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        minimumSize: Size(100, 40)),
                                  );
                                },
                              )
                            ],
                          ),
                        );
                      }),
                    ),
                  )
                ],
              ),
            );
          }),
          appBarHeader(context)
        ],
      ),
    );
  }
}
