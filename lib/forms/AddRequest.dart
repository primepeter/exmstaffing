import 'package:Loopin/universal_ui/universal_ui.dart';
import 'package:tuple/tuple.dart';
import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EmailApi.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_quill/flutter_quill.dart' hide Text;
import 'package:shimmer/shimmer.dart';

import 'ShowEmployees.dart';

enum QuillType { editor, viewer }

class AddRequest extends StatefulWidget {
  const AddRequest(
      {Key key, this.bm, this.readOnly = false, this.assign = false})
      : super(key: key);
  final BaseModel bm;
  final bool readOnly;
  final bool assign;

  @override
  _AddRequestState createState() => _AddRequestState();
}

class _AddRequestState extends State<AddRequest> {
  final scrollController = ScrollController();

  var quillControllerEditor = QuillController.basic();
  var quillControllerViewer = QuillController.basic();
  final _focusNode = FocusNode();

  final titleController = TextEditingController();
  final zipController = TextEditingController();
  final descriptionController = TextEditingController();
  final requirementController = TextEditingController();
  final salaryController = MoneyMaskedTextController(leftSymbol: "\$");
  String selectedState;
  String selectedCity;
  String selectedSkill = "";

  int employmentIndex = -1;
  int vaccinationIndex = -1;
  int negativeIndex = -1;
  bool passVisible = false;
  bool termsAccepted = false;

  List days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];
  List daysAvailable = [];

  List shiftPeriod = [
    "First Shift",
    "Second Shift",
    "Third/Overnight Shift",
  ];

  List shiftAvailable = [];
  int startDate = 0;

  // List<StreamSubscription> subs = [];

  String path = "postJob";
  BaseModel mainModel;
  BaseModel formModel;
  // String get formId => routeDataHolder["/postJob"];

  String objectId = getRandomId();
  BaseModel model = BaseModel();

  @override
  initState() {
    super.initState();
    if (widget.bm != null) {
      model = widget.bm;
      objectId = model.getObjectId();

      titleController.text = model.getString(TITLE);
      selectedSkill = model.getString(SKILLS);
      startDate = model.getInt("jobDate");
      descriptionController.text = model.getString("jobDescription");
      requirementController.text = model.getString("jobRequirement");
      salaryController.updateValue(model.getDouble("jobSalary"));
      selectedState = model.getString(STATE);
      selectedCity = model.getString(CITY);
      loadLessonDocument(QuillType.editor, model.getList("document"));
      setState(() {});
    }
  }

  void loadLessonDocument(QuillType type, List<dynamic> document) {
    try {
      final doc = Document.fromJson(document);
      final controller = QuillController(
          document: doc, selection: const TextSelection.collapsed(offset: 0));
      if (type == QuillType.editor) quillControllerEditor = controller;
      if (type == QuillType.viewer) quillControllerViewer = controller;
      setState(() {});
    } catch (error) {
      final doc = Document()..insert(0, 'Enter Request Description');
      final controller = QuillController(
          document: doc, selection: const TextSelection.collapsed(offset: 0));
      if (type == QuillType.editor) quillControllerEditor = controller;
      if (type == QuillType.viewer) quillControllerViewer = controller;
      setState(() {});
    }
  }

  pasteText() async {
    final data = await Clipboard.getData(Clipboard.kTextPlain);
    String text = data.text;
    if (text.isEmpty) return;
    final index = quillControllerEditor.selection.baseOffset;
    quillControllerEditor.document.insert(index, text);
    quillControllerEditor.updateSelection(
        TextSelection.collapsed(offset: index + 2), ChangeSource.LOCAL);
    setState(() {});
    // quillControllerEditor
  }

  @override
  void dispose() {
    // TODO: implement dispose
    // for (var s in subs) s?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: black.withOpacity(.8),
      child: RawKeyboardListener(
        focusNode: FocusNode(),
        onKey: (event) {
          if (event.data.isControlPressed && event.character == 'b') {
            if (quillControllerEditor
                .getSelectionStyle()
                .attributes
                .keys
                .contains('bold')) {
              quillControllerEditor
                  .formatSelection(Attribute.clone(Attribute.bold, null));
            } else {
              quillControllerEditor.formatSelection(Attribute.bold);
            }
          }
        },
        child: LayoutBuilder(builder: (c, box) {
          bool isLargeScreen = box.maxWidth > 850;
          // final size = getScreenWidth(c);
          final size = box.maxWidth;
          final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

          return RawScrollbar(
            controller: scrollController,
            isAlwaysShown: true,
            thumbColor: Colors.white,
            radius: Radius.circular(20),
            thickness: 20,
            interactive: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Container(
                    decoration: BoxDecoration(
                        color: white, borderRadius: BorderRadius.circular(15)),
                    padding: EdgeInsets.all(30),
                    margin: EdgeInsets.only(
                        top: 20,
                        bottom: 20,
                        left: ((0.12) * size),
                        right: ((0.12) * size)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                "Create Request",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ),
                            TextButton(
                              onPressed: () {
                                String title = titleController.text;
                                String jobDesc = descriptionController.text;
                                String jobReq = requirementController.text;
                                double salary = salaryController.numberValue;
                                final document = quillControllerEditor.document
                                    .toDelta()
                                    .toJson();

                                if (startDate == 0) {
                                  showError(
                                      context, "Choose a date to start work");
                                  return;
                                }

                                if (document.isEmpty) {
                                  showError(context, "Enter Job Description!");
                                  return;
                                }

                                showProgress(true, context,
                                    msg: "Please wait...");
                                model.put(OBJECT_ID, objectId);
                                model.put("jobDate", startDate);
                                model.put("jobDescription", jobDesc);
                                model.put("jobRequirement", jobReq);
                                model.put("document", document);
                                model.put(STATUS, STATUS_PENDING);
                                // model.put(FIRST_NAME, STATUS_PENDING);
                                // model.put(LAST_NAME, STATUS_PENDING);
                                model.saveItem(REQUEST_BASE, true,
                                    document: objectId, onComplete: (e) {
                                  showProgress(
                                    false,
                                    context,
                                  );
                                  if (null != e) {
                                    showErrorDialog(context, e);
                                    return;
                                  }

                                  appSettingsModel
                                    ..putInList(REQUEST_BASE_COUNT, objectId)
                                    ..updateItems();

                                  String baseUrl =
                                      appSettingsModel.getString("baseUrl");
                                  String url = "${baseUrl}setter";

                                  FirebaseFirestore.instance
                                      .collection(USER_BASE)
                                      .where(PRIVILEGE_INDEX, isEqualTo: 0)
                                      .get()
                                      .then((value) async {
                                    final docs = value.docs
                                        .map((e) => BaseModel(doc: e))
                                        .toList();
                                    final items = docs
                                        .map((e) => {
                                              "email": e.getEmail(),
                                              "name": e.getString(FIRST_NAME)
                                            })
                                        .toList();

                                    await EmailApi.sendEmailAll(
                                        type: EmailApi.TYPE_REQUEST,
                                        items: items,
                                        url: url,
                                        message:
                                            "Your Client ${userModel.getString(FIRST_NAME)} created a new request!",
                                        date:
                                            "Date of Request is ${getSimpleDate(startDate, pattern: "EEE MMMM dd, yyyy")}",
                                        request: quillControllerEditor.document
                                            .toPlainText());
                                  });

                                  showMessage(
                                    context,
                                    Icons.check,
                                    green,
                                    "Request Submitted!",
                                    "You have successful posted your job request",
                                    // cancellable: false,
                                    delayInMilli: 700,
                                    clickYesText: "Go Back",
                                    cancellable: false,
                                    onClicked: (_) {
                                      Navigator.pop(context, model);
                                    },
                                  );
                                });
                              },
                              child: Center(
                                child: Text(
                                  null != widget.bm
                                      ? "Update Request"
                                      : "Add Request",
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              style: TextButton.styleFrom(
                                  primary: white,
                                  backgroundColor: black,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  minimumSize: Size(100, 50)),
                            ),
                            addSpaceWidth(10),
                            Container(
                              // margin: EdgeInsets.only(bottom: 15),
                              child: TextButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Icon(
                                  Icons.clear,
                                  size: 15,
                                ),
                                style: TextButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                    primary: white,
                                    backgroundColor: red,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    minimumSize: Size(40, 40)),
                              ),
                            )
                          ],
                        ),
                        addSpace(20),
                        clickField(
                            "Date of Request",
                            startDate == 0
                                ? ""
                                : getSimpleDate(startDate,
                                    pattern: "EEE MMMM dd, yyyy"), () async {
                          final DateTime picked = await showDatePicker(
                              builder: (ctx, child) {
                                return Theme(
                                    data: Theme.of(context).copyWith(
                                      colorScheme: ColorScheme.light(
                                        primary: appColor,
                                        onPrimary:
                                            Colors.black, // header text color
                                        // onSurface: Colors.green, // body text color
                                      ),
                                      buttonTheme: ButtonThemeData(
                                          textTheme: ButtonTextTheme.primary),
                                    ),
                                    child: ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: child));
                              },
                              context: context,
                              initialDate: startDate == 0
                                  ? DateTime.now()
                                  : DateTime.fromMillisecondsSinceEpoch(
                                      startDate),
                              firstDate: DateTime.now(),
                              lastDate: DateTime(DateTime.now().year + 2));

                          if (picked != null &&
                              picked.millisecondsSinceEpoch != startDate)
                            startDate = picked.millisecondsSinceEpoch;
                          setState(() {});
                        }),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: TextButton(
                            onPressed: () {
                              pasteText();
                            },
                            child: Text(
                              "Click here to paste",
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold),
                            ),
                            style: TextButton.styleFrom(
                                primary: white,
                                backgroundColor: black,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                minimumSize: Size(150, 50)),
                          ),
                        ),
                        if (null != quillControllerEditor)
                          Container(
                            height: 400,
                            margin: EdgeInsets.only(top: 10),
                            // width: screenWidth,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border:
                                    Border.all(color: black.withOpacity(.5))),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: QuillEditor(
                                  controller: quillControllerEditor,
                                  scrollController: ScrollController(),
                                  scrollable: true,
                                  focusNode: _focusNode,
                                  autoFocus: false,
                                  readOnly: false,
                                  placeholder: 'Enter Request Description',
                                  expands: false,
                                  padding: EdgeInsets.all(5),
                                  customStyles: DefaultStyles(
                                    h1: DefaultTextBlockStyle(
                                        const TextStyle(
                                          fontSize: 32,
                                          color: Colors.black,
                                          height: 1.15,
                                          fontWeight: FontWeight.w300,
                                        ),
                                        Tuple2(16, 0),
                                        Tuple2(0, 0),
                                        null),
                                    sizeSmall: const TextStyle(fontSize: 9),
                                  ),
                                  embedBuilder: defaultEmbedBuilderWeb),
                            ),
                          ),
                        addLine(20, black.withOpacity(.03), 0, 20, 0, 20),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}
