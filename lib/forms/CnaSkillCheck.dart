import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/main.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CnaSkillCheck extends StatefulWidget {
  const CnaSkillCheck({Key key}) : super(key: key);

  @override
  _CnaSkillCheckState createState() => _CnaSkillCheckState();
}

class _CnaSkillCheckState extends State<CnaSkillCheck> {
  bool termsAccepted = false;
  List<StreamSubscription> subs = [];
  BaseModel model = BaseModel();

  String path = "cnaSkillCheck";
  BaseModel mainModel;
  BaseModel formModel;
  String get formId => routeDataHolder["/cnaSkillCheck"];
  String get name => mainModel.getFullName();
  List<BaseModel> formsToFill = [];

  List keys = [
    "0 - No experience, Not Comfortable, Not Competent",
    "1 - Minimal experience, need review and supervision, have performed at least once",
    "2 - Comfortable performing with resource available",
    "3 - Competent to perform independently and safely",
    "4 - Expert, able to act as a resource to others"
  ];

  @override
  initState() {
    super.initState();
    loadItems();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadItems() async {
    if (null == formId || formId.isEmpty) {
      showErrorDialog(
          context, "Ops the application link is invalid or has expired!",
          cancellable: false);
      return;
    }

    var sub = FirebaseFirestore.instance
        .collection(EMPLOYEE_BASE)
        .where(OBJECT_ID, isEqualTo: formId)
        .limit(1)
        .snapshots()
        .listen((value) {
      if (value.size == 0) {
        showErrorDialog(
            context, "Ops the application link is invalid or has expired!",
            cancellable: false);
        return;
      }
      mainModel = BaseModel(doc: value.docs[0]);
      model = mainModel.getModel(SKILL_CHECK);
      formsToFill = mainModel.getListModel(FORMS_TO_FILL);
      int p = formsToFill.indexWhere((e) => e.getString("path") == path);
      if (p != -1) {
        formModel = formsToFill[p];
        // model = formModel.getModel(SKILL_CHECK);
      }
      setState(() {});
    });
    subs.add(sub);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: LayoutBuilder(builder: (c, box) {
              final size = box.maxWidth;
              print("size $size");
              bool isLargeScreen = box.maxWidth > 850;
              final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

              return Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                // padding: EdgeInsets.only(
                //     top: 130, bottom: 50, left: size * 0.2, right: size * 0.2),
                padding: EdgeInsets.only(
                  left: isLargeScreen ? size * 0.15 : 20,
                  right: isLargeScreen ? size * 0.15 : 20,
                  top: 120,
                  bottom: 20,
                ),
                decoration: BoxDecoration(
                    color: isLargeScreen ? scaffoldColor : white,
                    borderRadius: BorderRadius.circular(10)),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: isLargeScreen ? white : null,
                          borderRadius: BorderRadius.circular(10)),
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              style: TextButton.styleFrom(
                                minimumSize: Size(40, 40),
                                backgroundColor: white,
                                primary: black,
                                elevation: 4,
                                // shape: CircleBorder()
                              ),
                              child: Icon(Icons.navigate_before)),
                          addSpace(10),
                          Text(
                            "Long Term Care CNA Skills Checklist",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "Please check the appropriate boxes to describe your experience level with each skill listed below.",
                            style: TextStyle(
                                fontSize: 13, fontWeight: FontWeight.w400),
                          ),
                          Container(
                            padding: EdgeInsets.all(8),
                            margin: EdgeInsets.only(top: 10, bottom: 20),
                            color: black.withOpacity(.03),
                            alignment: Alignment.centerLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: List.generate(
                                  keys.length,
                                  (p) => Text(
                                        keys[p],
                                        // style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                                      )),
                            ),
                          ),
                          // addSpace(20),
                          itemBuilder(
                            "Vital Signs (Take & Record)",
                            values: [
                              "Temperature (Auxiliary)",
                              "Temperature (Oral)",
                              "Temperature (Rectal)",
                              "Pulse (Radial)",
                              "Pulse (Apical)",
                              "Pulse (Brachial)",
                              "Respirations",
                              "Blood Pressure",
                              "Height and Weight"
                            ],
                          ),
                          itemBuilder(
                            "Communications",
                            values: [
                              "Verbal and non-verbal with cognitively impaired patients",
                              "Maintaining patient confidentiality",
                            ],
                          ),
                          itemBuilder("Environment", values: [
                            "Linen Change (Occupied)",
                            "Linen Change (Unoccupied)",
                            "Light housekeeping",
                            "Meal/ Snack Preparation"
                          ]),
                          itemBuilder("Ambulating", values: [
                            "Cane",
                            "Crutches",
                            "Walker",
                            "Standby assistant"
                          ]),
                          itemBuilder("Transfer Techniques", values: [
                            "Gait Belt",
                            "Weight bearing",
                            "Hoyer/Easystand",
                            "Two Person Transfer",
                            "Pivot",
                            "Wheelchair"
                          ]),
                          itemBuilder("Positioning/Turning", values: [
                            "Supine",
                            "Side Lying",
                            "Use of draw sheet",
                            "Range of motion exercise",
                            "In Chair"
                          ]),
                          itemBuilder("Personal Care", values: [
                            "Bath (Bed)",
                            "Bath (Tub)",
                            "Bath (Shower)",
                            "Skin Care (Back rub)",
                            "Skin Care (Apply Lotion)",
                            "Skin Care (Decubitus Care)",
                            "Shampoo",
                            "Nail Care",
                            "Oral Hygiene",
                            "Denture Care",
                            "Shaving (Safety)",
                            "Shaving (Electric razor)",
                            "Dressing: Assist/Complete",
                            "Perennial Care (Male)",
                            "Perennial Care (Female)"
                          ]),
                          itemBuilder("Nutrition/Hydration", values: [
                            "Encourage fluids per patient orders",
                            "Types of diet",
                            "Assist in feeding",
                            "Feeding techniques",
                            "Measure and record intake and output"
                          ]),
                          itemBuilder("Infection Control", values: [
                            "Hand washing",
                            "Universal Precautions"
                          ]),
                          itemBuilder("Elimination", values: [
                            "Bed Pan/urinal and Fracture Pan",
                            "Bedside Commode",
                            "Measure and record output",
                            "Foley Catheter Care",
                            "External Catheter Care/Placement",
                            "Enemas (Tap H2O)",
                            "Enemas (Fleets)",
                            "Enemas (Soap Suds)",
                          ]),
                          itemBuilder("Specimen Collections",
                              values: ["Urine", "Stool", "Sputum"]),
                          itemBuilder("Safety Devices", values: [
                            "Vest Restraint",
                            "Soft ankle",
                            "Wrist Restraints"
                          ]),
                          itemBuilder("Oxygen Therapy", values: [
                            "Flow Rate (Check for accuracy)",
                            "Cannulla/mask placement"
                          ]),
                          itemBuilder("Observations/Reporting/Documentation",
                              values: [
                                "Change in body functions",
                                "Change in behaviour",
                                "Change in routines",
                                "Change in mentation"
                              ]),
                          addSpace(50),
                        ],
                      ),
                    ),
                    addSpace(20),
                    InkWell(
                      onTap: () {
                        termsAccepted = !termsAccepted;
                        setState(() {});
                      },
                      child: Container(
                        child: Row(
                          children: [
                            checkBox(termsAccepted),
                            addSpaceWidth(20),
                            Expanded(
                                child: Text(
                              "By my signature below, I have checked over the"
                              " following document to ensure we are proving"
                              " EXCEPTIONAL STAFFING to all clients.",
                              style: TextStyle(fontSize: 12),
                            ))
                          ],
                        ),
                      ),
                    ),
                    addSpace(20),
                    Container(
                      height: 60,
                      child: TextButton(
                        onPressed: !termsAccepted
                            ? null
                            : () {
                                mainModel.put(SKILL_CHECK, model.items);
                                // formModel.put(SKILL_CHECK, model.items);
                                formModel.put(STATUS, STATUS_COMPLETED);
                                int p = formsToFill.indexWhere(
                                    (e) => e.getString("path") == path);
                                if (p != -1) {
                                  formsToFill[p] = formModel;
                                }

                                mainModel.put(FORMS_TO_FILL,
                                    formsToFill.map((e) => e.items).toList());
                                mainModel.updateItems();

                                showMessage(
                                  context,
                                  Icons.check,
                                  green,
                                  "Form Submitted!",
                                  "You have successful completed your Training Program Agreement form",
                                  // cancellable: false,
                                  delayInMilli: 700,
                                  clickYesText: "Go Back",
                                  cancellable: false,
                                  onClicked: (_) {
                                    // Navigator.pop(context, true);
                                    Navigator.pushReplacementNamed(
                                        context, "/applicationForm??$formId");
                                  },
                                );
                              },
                        child: Center(
                          child: Text(
                            "Save Information".toUpperCase(),
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            minimumSize: Size(300, 50)),
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
          appBarHeader(context)
        ],
      ),
    );
  }

  itemBuilder(String key, {List values = const []}) {
    final items = model.getModel(key);

    return Container(
        margin: EdgeInsets.only(bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.all(8),
              margin: EdgeInsets.only(top: 20, bottom: 20),
              color: black.withOpacity(.03),
              alignment: Alignment.centerLeft,
              child: Text(
                key,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
            Column(
              children: List.generate(values.length, (index) {
                String value = values[index];

                return Container(
                  margin: EdgeInsets.only(left: 10, bottom: 10),
                  padding: EdgeInsets.all(5),
                  child: Row(
                    children: [
                      Text(values[index],
                          style: TextStyle(
                            fontSize: 14,
                            // fontWeight: FontWeight.bold
                          )),
                      addSpaceWidth(20),
                      Wrap(
                        children: List.generate(5, (p) {
                          String title = p.toString();
                          bool active = items.getInt(value, true) == p;
                          return GestureDetector(
                            onTap: () {
                              items.put(value, p);
                              model.put(key, items.items);
                              setState(() {});
                            },
                            child: Container(
                              height: 40,
                              width: 50,
                              margin: EdgeInsets.all(2),
                              child: Text(
                                title,
                                style: TextStyle(
                                    color: active ? white : black,
                                    fontWeight: active
                                        ? FontWeight.bold
                                        : FontWeight.normal),
                              ),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  color: active ? black : white,
                                  boxShadow: [
                                    BoxShadow(
                                        color: black.withOpacity(.05),
                                        spreadRadius: 2,
                                        blurRadius: 5)
                                  ],
                                  borderRadius: BorderRadius.circular(8)),
                            ),
                          );
                        }),
                      ),
                    ],
                  ),
                );
              }),
            )
          ],
        ));
  }
}
