import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/main.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:platform_device_id/platform_device_id.dart';

import 'AddWorkForm.dart';
import 'package:location/location.dart';
import 'package:geocode/geocode.dart';

class ClockInPage extends StatefulWidget {
  const ClockInPage({Key key}) : super(key: key);

  @override
  _ClockInPageState createState() => _ClockInPageState();
}

class _ClockInPageState extends State<ClockInPage> with WidgetsBindingObserver {
  List<BaseModel> itemList = [];
  bool isBusy = true;
  bool hasLocation = false;

  BaseModel workOrderModel;
  List<AssignInfo> assignedInfo = [];
  int workOrderIndex = -1;

  BaseModel model;
  String get formId => routeDataHolder["/clockIn"];
  String get name => model.getString(FIRST_NAME);
  List<BaseModel> formsToFill = [];
  List<StreamSubscription> subs = [];

  int formExpiry = 0;
  int formSent = 0;
  bool formsSecured = false;
  bool formsUnLocked = false;
  bool formsExpired = false;
  String formsPassword = "";

  final passwordController = TextEditingController();
  final passwordController2 = TextEditingController();
  bool passVisible = false;
  bool passVisible2 = false;

  final employeeIdController = TextEditingController();
  int currentShift = -1;

  List shiftPeriod = [
    "First Shift",
    "Second Shift",
    "Third/Overnight Shift",
  ];
  List<Shifts> shiftsInfo = [];

  String errorMsg = "";
  String assignedId = "";

  final Location location = Location();
  LocationData locationData;
  GeoCode geoCode = GeoCode(apiKey: "925869374545003581687x12675");
  Address address;

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    loadItems();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.detached) {
      model
        ..put(FORMS_UNLOCKED, false)
        ..updateItems();
    }
  }

  loadItems() async {
    print("formId $formId");
    print("formId $routeDataHolder");

    final pem = await location.requestPermission();

    print(pem);
    if (pem == PermissionStatus.granted) {
      hasLocation = true;
      locationData = await location.getLocation();
      loadAddress();
      // address = await geoCode.reverseGeocoding(
      //   latitude: locationData.latitude,
      //   longitude: locationData.longitude,
      // );
      // setState(() {});
    }

    if (null == formId || formId.isEmpty) {
      errorMsg = "Ops this employee data link is invalid or has expired!";
      isBusy = false;
      setState(() {});
    }

    var sub = FirebaseFirestore.instance
        .collection(EMPLOYEE_BASE)
        .where(OBJECT_ID, isEqualTo: formId)
        .limit(1)
        .snapshots()
        .listen((value) async {
      if (value.size == 0) {
        errorMsg = "Ops this WorkOrder link is invalid or has expired!";
        isBusy = false;
        setState(() {});
        return;
      }

      model = BaseModel(doc: value.docs[0]);
      assignedId = model.getString(ASSIGNED_ID);
      if (assignedId.isEmpty) {
        errorMsg = "Ops this WorkOrder link is invalid or has expired!";
        isBusy = false;
        setState(() {});
        return;
      }
      formsSecured = model.getBoolean(FORMS_SECURED);
      formsPassword = model.getString(FORMS_PASSWORD);
      formsToFill = model.getListModel(FORMS_TO_FILL);
      // isBusy = false;
      loadWorkOrder();
      if (formsToFill.isEmpty) formsExpired = true;
      setState(() {});
    });
    subs.add(sub);
  }

  loadAddress() async {
    await Future.delayed(Duration(seconds: 2));
    address = await geoCode.reverseGeocoding(
      latitude: locationData.latitude,
      longitude: locationData.longitude,
    );
    setState(() {});
  }

  loadWorkOrder() async {
    print("assignedId $assignedId");
    var sub = FirebaseFirestore.instance
        .collection(WORK_BASE)
        .where(OBJECT_ID, isEqualTo: assignedId)
        .limit(1)
        .snapshots()
        .listen((value) async {
      if (value.size == 0) {
        errorMsg = "Ops this employee data link is invalid or has expired!";
        setState(() {});
        return;
      }

      workOrderModel = BaseModel(doc: value.docs[0]);

      shiftsInfo = workOrderModel
          .getListModel(SHIFTS_INFO)
          .map((e) => Shifts(items: e.items))
          .toList();

      assignedInfo = workOrderModel
          .getListModel(ASSIGNED_INFO)
          .map((e) => AssignInfo(items: e.items))
          .toList();

      if (assignedInfo.isEmpty) {
        errorMsg = "Ops this work hasn't been assigned yet!";
        isBusy = false;
        setState(() {});
        return;
      }

      bool hasAssigned = false;
      for (var d in assignedInfo) {
        final p =
            d.employees.indexWhere((e) => e[OBJECT_ID] == model.getObjectId());
        if (p == -1) continue;
        hasAssigned = true;
      }

      if (!hasAssigned) {
        errorMsg = "Ops this work wasn't assigned to you!";
        isBusy = false;
        setState(() {});

        return;
      }

      isBusy = false;
      setState(() {});
    });
    subs.add(sub);
  }

  bool get hasClockIn {
    bool value = false;
    for (int p = 0; p < assignedInfo.length; p++) {
      final d = assignedInfo[p];
      if (p != currentShift) continue;

      final pe =
          d.employees.indexWhere((e) => e[OBJECT_ID] == model.getObjectId());
      if (pe == -1) continue;
      Map employeeItem = d.employees[pe];
      Map clockInfo = employeeItem[CLOCKING_INFO];
      if (clockInfo == null) break;

      Map item = clockInfo[CLOCKED_IN];
      if (null != item) {
        value = true;
        break;
      }
    }
    return value;
  }

  bool get hasClockOut {
    bool value = false;
    for (int p = 0; p < assignedInfo.length; p++) {
      final d = assignedInfo[p];
      if (p != currentShift) continue;

      final pe =
          d.employees.indexWhere((e) => e[OBJECT_ID] == model.getObjectId());
      if (pe == -1) continue;
      Map employeeItem = d.employees[pe];
      Map clockInfo = employeeItem[CLOCKING_INFO];
      if (clockInfo == null) break;

      Map item = clockInfo[CLOCKED_OUT];
      if (null != item) {
        value = true;
        break;
      }
    }
    return value;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          LayoutBuilder(builder: (c, box) {
            final size = box.maxWidth;
            bool isLargeScreen = box.maxWidth > 850;
            final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

            return Container(
              margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
              // padding: EdgeInsets.only(
              //     top: 130, bottom: 50, left: size * 0.2, right: size * 0.2),
              padding: EdgeInsets.only(
                left: isLargeScreen ? size * 0.25 : 20,
                right: isLargeScreen ? size * 0.25 : 20,
                top: 120,
                bottom: 20,
              ),
              decoration: BoxDecoration(
                  color: isLargeScreen ? scaffoldColor : white,
                  borderRadius: BorderRadius.circular(10)),
              alignment: Alignment.center,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        border: Border(
                            left: BorderSide(
                                color: Colors.deepOrange, width: 4))),
                    padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
                    child: Text(
                      "ClockIn WorkOrder",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  addSpace(20),
                  Expanded(
                    child: Builder(
                      builder: (c) {
                        if (isBusy)
                          return Center(
                              child: SizedBox(
                                  height: 20,
                                  width: 20,
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation(black),
                                  )));

                        if (!hasLocation)
                          return Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.wrong_location,
                                  size: 50,
                                ),
                                addSpace(10),
                                Text(
                                    "Your location is required before you can proceed",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: black.withOpacity(.5),
                                      fontSize: 14,
                                      // fontWeight: FontWeight.bold
                                    )),
                                addSpace(10),
                                TextButton(
                                  onPressed: () async {
                                    isBusy = true;
                                    setState(() {});
                                    final pem =
                                        await location.requestPermission();
                                    if (pem == PermissionStatus.granted) {
                                      hasLocation = true;
                                      locationData =
                                          await location.getLocation();
                                      setState(() {});
                                    }
                                  },
                                  child: Center(
                                    child: Text(
                                      "Enable Location",
                                      style: TextStyle(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal),
                                    ),
                                  ),
                                  style: TextButton.styleFrom(
                                      primary: white,
                                      backgroundColor: black,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      minimumSize: Size(120, 60)),
                                )
                              ],
                            ),
                          );
                        if (errorMsg.isNotEmpty)
                          return Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.access_time,
                                  size: 50,
                                ),
                                addSpace(10),
                                Text(errorMsg,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: black.withOpacity(.5),
                                      fontSize: 14,
                                      // fontWeight: FontWeight.bold
                                    )),
                              ],
                            ),
                          );

                        if (!formsUnLocked)
                          return Center(
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  border: Border.all(
                                      color: black.withOpacity(.05))),
                              padding: EdgeInsets.all(10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.lock_outline,
                                    size: 50,
                                  ),
                                  addSpace(10),
                                  Text(
                                      !formsSecured
                                          ? "Your required to create a One-Time password to secure your account."
                                          : "Your required to enter your One-Time password to have access this page.",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        color: black.withOpacity(.5),
                                        fontSize: 14,
                                        // fontWeight: FontWeight.bold
                                      )),
                                  addSpace(10),
                                  inputField(
                                      "Enter Password", passwordController,
                                      isPass: true,
                                      passwordVisible: passVisible,
                                      onChanged: (s) {
                                    setState(() {});
                                  }, onVisiChanged: () {
                                    passVisible = !passVisible;
                                    setState(() {});
                                  }),
                                  if (!formsSecured)
                                    inputField(
                                        "Confirm Password", passwordController2,
                                        isPass: true,
                                        passwordVisible: passVisible2,
                                        onChanged: (s) {
                                      setState(() {});
                                    }, onVisiChanged: () {
                                      passVisible2 = !passVisible2;
                                      setState(() {});
                                    }),
                                  Container(
                                    // height: 60,
                                    alignment: Alignment.center,
                                    margin:
                                        EdgeInsets.only(left: 0, bottom: 15),
                                    child: TextButton(
                                      onPressed: () async {
                                        String pass = passwordController.text;
                                        String pass2 = passwordController2.text;
                                        // String deviceId =
                                        //     await PlatformDeviceId.getDeviceId;

                                        if (pass.isEmpty) {
                                          showError(
                                              context, "Enter Your Password!");
                                          return;
                                        }

                                        if (!formsSecured && pass2.isEmpty) {
                                          showError(context,
                                              "Confirm Your Password!");
                                          return;
                                        }

                                        if (!formsSecured && pass != pass2) {
                                          showError(context,
                                              "Passwords don't match!");
                                          return;
                                        }

                                        if (formsSecured &&
                                            !formsUnLocked &&
                                            pass != formsPassword) {
                                          showError(context,
                                              "Invalid Form Password!");
                                          return;
                                        }

                                        if (!formsSecured) {
                                          model.put(FORMS_SECURED, true);
                                          // model.put(DEVICE_ID, deviceId);
                                          model.put(FORMS_PASSWORD, pass);
                                        }
                                        formsUnLocked = !formsUnLocked;
                                        model.put(
                                            FORMS_UNLOCKED, formsUnLocked);
                                        model.updateItems();

                                        setState(() {});
                                      },
                                      child: Center(
                                        child: Text(
                                          (!formsSecured
                                              ? "CREATE PASSWORD"
                                              : "VERIFY PASSWORD"),
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.normal),
                                        ),
                                      ),
                                      style: TextButton.styleFrom(
                                          primary: white,
                                          backgroundColor: black,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          minimumSize: Size(120, 60)),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );

                        return Center(
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                border:
                                    Border.all(color: black.withOpacity(.05))),
                            padding: EdgeInsets.all(10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.timer,
                                  size: 50,
                                ),
                                addSpace(10),
                                Text(
                                    "Clock into your workOrder to start or end your shift.",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: black.withOpacity(.5),
                                      fontSize: 14,
                                      // fontWeight: FontWeight.bold
                                    )),
                                addSpace(30),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: groupFields("Choose shifts",
                                      shiftPeriod, currentShift, (p) {
                                    final checkIndex = shiftsInfo
                                        .indexWhere((e) => e.shiftIndex == p);

                                    if (checkIndex == -1) {
                                      showError(context,
                                          "This shift cannot be selected, because it's not inclusive in the number of shifts for this order!");
                                      return;
                                    }

                                    currentShift = p;
                                    setState(() {});
                                  }, width: 140),
                                ),
                                // if (currentShift != -1)
                                if (currentShift != -1 && !hasClockOut)
                                  Container(
                                    // height: 60,
                                    alignment: Alignment.center,
                                    margin:
                                        EdgeInsets.only(left: 0, bottom: 15),
                                    child: TextButton(
                                      onPressed: () async {
                                        final data = assignedInfo[currentShift];
                                        final p = data.employees.indexWhere(
                                            (e) =>
                                                e[OBJECT_ID] ==
                                                model.getObjectId());

                                        if (p == -1) return;
                                        Map empItem = data.employees[p];
                                        Map clockInfo =
                                            empItem[CLOCKING_INFO] ?? {};

                                        final timeMs = DateTime.now()
                                            .millisecondsSinceEpoch;

                                        String key = hasClockIn
                                            ? CLOCKED_OUT
                                            : CLOCKED_IN;

                                        // empItem[CLOCKING_INFO]

                                        clockInfo[key] = {
                                          TIME: timeMs,
                                          LATITUDE: locationData.latitude,
                                          LONGITUDE: locationData.longitude,
                                          PLACE_NAME: address.streetAddress,
                                          COUNTRY: address.countryName,
                                          CITY: address.city
                                        };
                                        empItem[CLOCKING_INFO] = clockInfo;

                                        print("empItem $empItem");
                                        print("clockInfo $clockInfo");

                                        data.employees[p] = empItem;
                                        assignedInfo[currentShift] = data;

                                        // print(assignedInfo.map((e) => e.items));

                                        workOrderModel
                                          ..put(
                                              ASSIGNED_INFO,
                                              assignedInfo
                                                  .map((e) => e.items)
                                                  .toList())
                                          ..updateItems();

                                        setState(() {});
                                      },
                                      child: Center(
                                        child: Text(
                                          (hasClockIn
                                              ? "CLOCK OUT"
                                              : "CLOCK IN"),
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.normal),
                                        ),
                                      ),
                                      style: TextButton.styleFrom(
                                          primary: white,
                                          backgroundColor: black,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          minimumSize: Size(120, 60)),
                                    ),
                                  )
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  )
                ],
              ),
            );
          }),
          appBarHeader(context)
        ],
      ),
    );
  }
}
