import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/main.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ClinicalSkillCheck extends StatefulWidget {
  const ClinicalSkillCheck({Key key}) : super(key: key);

  @override
  _ClinicalSkillCheckState createState() => _ClinicalSkillCheckState();
}

class _ClinicalSkillCheckState extends State<ClinicalSkillCheck> {
  bool termsAccepted = false;
  List<StreamSubscription> subs = [];
  BaseModel model = BaseModel();

  String path = "clinicalSkillCheck";
  BaseModel mainModel;
  BaseModel formModel;
  String get formId => routeDataHolder["/clinicalSkillCheck"];
  String get name => mainModel.getString(NAME);
  List<BaseModel> formsToFill = [];

  @override
  initState() {
    super.initState();
    loadItems();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadItems() async {
    if (null == formId || formId.isEmpty) {
      showErrorDialog(
          context, "Ops the application link is invalid or has expired!",
          cancellable: false);
      return;
    }

    var sub = FirebaseFirestore.instance
        .collection(EMPLOYEE_BASE)
        .where(OBJECT_ID, isEqualTo: formId)
        .limit(1)
        .snapshots()
        .listen((value) {
      if (value.size == 0) {
        showErrorDialog(
            context, "Ops the application link is invalid or has expired!",
            cancellable: false);
        return;
      }
      mainModel = BaseModel(doc: value.docs[0]);
      model = mainModel.getModel(SKILL_CHECK);
      formsToFill = mainModel.getListModel(FORMS_TO_FILL);
      int p = formsToFill.indexWhere((e) => e.getString("path") == path);
      if (p != -1) {
        formModel = formsToFill[p];
        // model = formModel.getModel(SKILL_CHECK);
      }
      setState(() {});
    });
    subs.add(sub);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: LayoutBuilder(builder: (c, box) {
              final size = box.maxWidth;
              print("size $size");
              bool isLargeScreen = box.maxWidth > 850;
              final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

              return Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                // padding: EdgeInsets.only(
                //     top: 130, bottom: 50, left: size * 0.2, right: size * 0.2),
                padding: EdgeInsets.only(
                  left: isLargeScreen ? size * 0.15 : 20,
                  right: isLargeScreen ? size * 0.15 : 20,
                  top: 120,
                  bottom: 20,
                ),
                decoration: BoxDecoration(
                    color: isLargeScreen ? scaffoldColor : white,
                    borderRadius: BorderRadius.circular(10)),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: isLargeScreen ? white : null,
                          borderRadius: BorderRadius.circular(10)),
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              style: TextButton.styleFrom(
                                minimumSize: Size(40, 40),
                                backgroundColor: white,
                                primary: black,
                                elevation: 4,
                                // shape: CircleBorder()
                              ),
                              child: Icon(Icons.navigate_before)),
                          addSpace(10),
                          Text(
                            "Clinical Skills Checklist",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          addSpace(20),
                          itemBuilder(
                            "BASIC SKILLS:",
                            values: [
                              "Blood Pressure",
                              "Apical Pulse Rate & Radial Pulse rate",
                              "Temperature (ORAL, AXILLARY,RECTAL)",
                              "Hand washing",
                              "Use of non-sterile gloves",
                              "Pain as the 5th vital sign",
                              "Skin Care",
                              "Mouth Care",
                              "Bathing Complete",
                              "Bathing Partial",
                              "Assist with feeding",
                              "BedMaking Occupied",
                              "BedMaking UnOccupied",
                              "Standing Scale Weight",
                              "BedScale Weight",
                              "Transfer WheelChair",
                              "Transfer Chair",
                              "Transfer Stretcher",
                              "Ambulation Of Client With Tubes & Equipments",
                              "Glucometer",
                              "Isolation Procedures",
                              "Postmortem Care",
                              "REPOSITIONING USING LIFT (DRAW) SHEET",
                              "Repositioning Using lift (Draw) Sheet",
                              "Repositioning Frail Client Supine",
                              "Repositioning Frail Client SideLying",
                              "Assisting Movement Up in Bed"
                            ],
                          ),
                          itemBuilder(
                            "NEUROLOGICAL:",
                            values: [
                              "Glaucoma Scale",
                              "Motor/Sensory Function Assessment",
                            ],
                          ),
                          itemBuilder("CARDIOPULMONARY:", values: [
                            "NORMAL HEART SOUNDS (S1,S2)",
                            "PERIPHERAL PULSES",
                            "EDEMA ASSESSMENT",
                            "APPLICATION OF ANTIEMBOLISM HOSE",
                            "MANAGEMENT OF SEQUENTIAL COMPRESSION BOOTS",
                            "RESPIRATORY ASSESSMENT",
                            "NORMAL BREATH SOUNDS",
                            "ADVENTITIOUS BREATH SOUNDS: WHEEZING",
                            "ADVNETITIOUS BREATH SOUNDS: CRACKLES (RALES)",
                            "ADVNETITIOUS BREATH SOUNDS: RHONCHI",
                            "PULSE OXIMETRY",
                            "NASAL CANNULA",
                            "VENTI (VENTURI) MASKS",
                            "PARTIAL REBREATHER MASK",
                            "NONBREATHER MASKS",
                            "ORAL SUCTIONING (YANKAUER)",
                            "NASOPHARYNGEAL SUCTIOING",
                            "TRACHEAL (ENDOTRACHEAL) SUCTIONING",
                            "CLOSED (INLINE) SUCTIOING SYSTEM",
                            "TRACHEOSTOMY CARE",
                            "CHest TUbe CARE",
                            "SPUTUM COLLECTION",
                            "INCENTIVE SPIROMETER",
                            "12 LEAD EKG"
                          ]),
                          itemBuilder("GASTROINTESTINAL:", values: [
                            "AUSCULTATION OF BOWEL SOUNDS",
                            "ABDOMINAL PALPATION",
                            "MEASUREMENT OF PO/ENTERAL INTAKE",
                            "CALORIE COUNTS",
                            "ENTERAL FEEDING: NGT",
                            "ENTERAL FEEDING; PEG",
                            "ENTERAL FEEDING: J TUBES",
                            "ENTERAL TUBE SITE CARE",
                            "INSERTION AND PLACEMENT OF NGT",
                            "MANAGEMENT OF NG SUCTION",
                            "TAP WATER/SSE ENEMA",
                            "FLEET ENEMA",
                            "OSTOMY CARE",
                            "FECAL BAG",
                            "STOOL SPECIMENT COLLECTION",
                            "ASSIST WITH BEDPAN"
                          ]),
                          itemBuilder("GENITOURINARY:", values: [
                            "ASSIST WITH URINAL",
                            "URINE ASSESSMENT: APPEARANCE",
                            "BLADDER ASSESSMENT",
                            "MEASUREMENT OF URINE OUTPUT",
                            "URINARY CATHETER INSERTION",
                            "URINARY CATHETER REMOVAL",
                            "BLADDER IRRIGATION",
                            "CONDOMN CATHETER (TEXAS CATHETER)",
                            "CATHETER LEG BAG",
                            "SUPRPUBIC CATHETER CARE",
                            "NEPHROSTOMY TUBE MANAGEMENT",
                            "NEPHROSTOMY CARE",
                            "PERITONEAL DIALYSIS",
                            "HEMODIALYSIS ACCESS(PERMACATH, FISTULA, GRAFT)",
                            "INCONTINENCE CARE"
                          ]),
                          itemBuilder("MUSCULOSKELETAL:", values: [
                            "INSPECTION AND PALPATION OF MUSCLES AND JOINTS",
                            "ASSESSMENT OF ROM: ACTIVE AND PASSIVE",
                            "NEUROVASCULAR ASSESSMENT OF EXTREMITY",
                            "SKIN TRACTION (BUCKS)",
                            "SKELETAL TRACTION /EXTERNAL FIXATION DEVICE MANAGEMENT"
                          ]),
                          itemBuilder("SKIN", values: [
                            "GENRAL:COLOR TEMPERTURE",
                            "TEXTURE, TURGOR",
                            "PRESSURE ULCER RISK ASSESSMENT",
                            "PRESSURE ULCER CARE",
                            "DRY STERILE DRESSING",
                            "WOUND IRRIGATION",
                            "WOUND PACKING",
                            "HYDROCOLLOID DRESSINGS",
                            "HYDROGEL DRESSINGS",
                            "DRAIN SITE CARE (JP, PENROSE, HEMOVAC)",
                            "POSTOP WOUND ASSESSMENT AND CARE",
                            "WARM AND COOL COMPRESSES",
                            "K-PAD",
                            "ICE PACKS",
                            "SPLINTING"
                          ]),
                          itemBuilder("PSYCHOSOCIAL:", values: [
                            "THERAPEUTIC COMMUNICATION",
                            "DEVELOPMENTAL ASSESSMENT ACROSS",
                            "THE LIFESPAN",
                            "CULTURAL/SPIRITUAL ASSESSMENT",
                            "TEACHING AND LEARNING ASSESSMENT",
                            "DEVELOP TEACHING PLAN",
                            "IMPLEMENT TEACHING PLAN"
                          ]),
                          itemBuilder("MEDICATION", values: [
                            "INTERPETATION OF MEDICATION ORDER",
                            "PO MEDS",
                            "MEDS VIA FEEDING TUBE",
                            "EYE DROPS",
                            "EYE OINTMENTS",
                            "NOSE DROPS/SPRAYS",
                            "METERED DOSE INHALERS",
                            "TOPICALS",
                            "RECTAL/VAGINAL SUPPOSITORIES",
                            "INTRADERMAL (PPD)",
                            "SUBCUTANEOUS INJECTIONS AND SITES",
                            "INTRAMUSCULAR INJECTIONS AND SITES",
                            "RECONSTITUTING MED",
                            "MIXING TWO MEDS IN A SYRINGE",
                            "MEDICATION SAFETY – HIGH RISK",
                            "PCA",
                            "RATIONALE FOR MED ORDER",
                            "NURSING IMPLICATIONS FOR MEDICATIONS"
                          ]),
                          itemBuilder("INTRAVENOUS", values: [
                            "ASSESSMENT OF PERIPHERAL SITE",
                            "SETTING UP NEW IV, PRIMING TUBING",
                            " ATTACHING TO PATIENT, VIA GRAVITY AND PUMP",
                            "SECONDARY IVPB VIA LOCK AND PUMP",
                            "CONVERSION OF IV TO LOCK",
                            "FLUSHING INTERMITTENT INFUSION LOCK",
                            "INFUSION PUMP (ML/HR)",
                            "GRAVITY FLOW (GTTS/MIN)",
                            "D/C IV PERIPHERAL LINE",
                            "CENTRAL LINE SITE ASSESSMENT",
                            "CENTRAL LINE DRESSING CHANGE (MUST PERFORM WITH PRECEPTOR)",
                            "STERILE GLOVES",
                            "STERILE FIELD AND DRESSING",
                            "PICC LINE ASSESSMENT AND MANAGEMENT",
                            "HYPERALIMENTATION"
                          ]),
                          itemBuilder("PROFESSIONAL ROLE:", values: [
                            "COMMUNICATION WITH NURSING STAFF",
                            "COMMUNICATION WITH PHYSICIANS",
                            "COMMUNICATION WITH OTHER DISCIPLINES",
                            "DELEGATION SKILLS",
                            "CLINICAL DECISION MAKING",
                            "ADMITTING A CLIENT",
                            "DISCHARGE PLANNING",
                            "TRANSFERRING A CLIENT",
                            "DISCHARGING A CLIENT",
                            "DOCUMENTATION",
                            "GIVING AND GETTING REPORT",
                            "CRITICAL THINKING"
                          ]),
                          itemBuilder("SELF ASSESSMENT", values: [
                            "I COMMUNICATE ASSERTIVELY",
                            "I HAVE CONFIDENCE IN MY NURSING SKILLS",
                            "I ASSUME PRIMARY RESPONSIBILITY FOR "
                                "IDENTIFYING MY LEARNING NEED CONSIDERING "
                                "BOTH MY STRENGTHS AND WEAKNESSES"
                          ]),
                          addSpace(50),
                        ],
                      ),
                    ),
                    addSpace(20),
                    InkWell(
                      onTap: () {
                        termsAccepted = !termsAccepted;
                        setState(() {});
                      },
                      child: Container(
                        child: Row(
                          children: [
                            checkBox(termsAccepted),
                            addSpaceWidth(20),
                            Expanded(
                                child: Text(
                              "By my signature below, I have checked over the"
                              " following document to ensure we are proving"
                              " EXCEPTIONAL STAFFING to all clients.",
                              style: TextStyle(fontSize: 12),
                            ))
                          ],
                        ),
                      ),
                    ),
                    addSpace(20),
                    Container(
                      height: 60,
                      child: TextButton(
                        onPressed: !termsAccepted
                            ? null
                            : () {
                                mainModel.put(SKILL_CHECK, model.items);
                                // formModel.put(SKILL_CHECK, model.items);
                                formModel.put(STATUS, STATUS_COMPLETED);
                                int p = formsToFill.indexWhere(
                                    (e) => e.getString("path") == path);
                                if (p != -1) {
                                  formsToFill[p] = formModel;
                                }

                                mainModel.put(FORMS_TO_FILL,
                                    formsToFill.map((e) => e.items).toList());
                                mainModel.updateItems();

                                showMessage(
                                  context,
                                  Icons.check,
                                  green,
                                  "Form Submitted!",
                                  "You have successful completed your Training Program Agreement form",
                                  // cancellable: false,
                                  delayInMilli: 700,
                                  clickYesText: "Go Back",
                                  cancellable: false,
                                  onClicked: (_) {
                                    // Navigator.pop(context, true);
                                    Navigator.pushReplacementNamed(
                                        context, "/applicationForm??$formId");
                                  },
                                );
                              },
                        child: Center(
                          child: Text(
                            "Save Information".toUpperCase(),
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            minimumSize: Size(300, 50)),
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
          appBarHeader(context)
        ],
      ),
    );
  }

  itemBuilder(String key, {List values = const []}) {
    final items = model.getModel(key);

    return Container(
        margin: EdgeInsets.only(bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.all(8),
              margin: EdgeInsets.only(top: 20, bottom: 20),
              color: black.withOpacity(.03),
              alignment: Alignment.centerLeft,
              child: Text(
                key,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
            Column(
              children: List.generate(values.length, (index) {
                String value = values[index];

                return Container(
                  margin: EdgeInsets.only(left: 10, bottom: 10),
                  padding: EdgeInsets.all(5),
                  child: Row(
                    children: [
                      Text(values[index],
                          style: TextStyle(
                            fontSize: 14,
                            // fontWeight: FontWeight.bold
                          )),
                      addSpaceWidth(20),
                      Wrap(
                        children: List.generate(4, (p) {
                          String title = p.toString();
                          bool active = items.getInt(value, true) == p;
                          return GestureDetector(
                            onTap: () {
                              items.put(value, p);
                              model.put(key, items.items);
                              setState(() {});
                            },
                            child: Container(
                              height: 40,
                              width: 50,
                              margin: EdgeInsets.all(2),
                              child: Text(
                                title,
                                style: TextStyle(
                                    color: active ? white : black,
                                    fontWeight: active
                                        ? FontWeight.bold
                                        : FontWeight.normal),
                              ),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  color: active ? black : white,
                                  boxShadow: [
                                    BoxShadow(
                                        color: black.withOpacity(.05),
                                        spreadRadius: 2,
                                        blurRadius: 5)
                                  ],
                                  borderRadius: BorderRadius.circular(8)),
                            ),
                          );
                        }),
                      ),
                    ],
                  ),
                );
              }),
            )
          ],
        ));
  }
}
