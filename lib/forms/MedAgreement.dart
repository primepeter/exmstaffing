import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/main.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:styled_text/styled_text.dart';

class MedAgreement extends StatefulWidget {
  const MedAgreement({Key key}) : super(key: key);

  @override
  _MedAgreementState createState() => _MedAgreementState();
}

class _MedAgreementState extends State<MedAgreement> {
  // String applicantName = "Steven Danny";
  int startDate = 0;

  bool termsAccepted = false;

  List<StreamSubscription> subs = [];
  String path = "medAgreement";
  BaseModel mainModel;
  BaseModel formModel;
  String get formId => routeDataHolder["/medAgreement"];
  String get name => mainModel.getFullName();
  String get offerAmount => mainModel.getString(OFFER_AMOUNT);
  List<BaseModel> formsToFill = [];

  @override
  initState() {
    super.initState();
    loadItems();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadItems() async {
    if (null == formId || formId.isEmpty) {
      showErrorDialog(
          context, "Ops the application link is invalid or has expired!",
          cancellable: false);
      return;
    }

    var sub = FirebaseFirestore.instance
        .collection(EMPLOYEE_BASE)
        .where(OBJECT_ID, isEqualTo: formId)
        .limit(1)
        .snapshots()
        .listen((value) {
      if (value.size == 0) {
        showErrorDialog(
            context, "Ops the application link is invalid or has expired!",
            cancellable: false);
        return;
      }
      mainModel = BaseModel(doc: value.docs[0]);
      startDate = mainModel.getInt(AGREEMENT_DATE);
      if (startDate == 0) startDate = DateTime.now().millisecondsSinceEpoch;

      formsToFill = mainModel.getListModel(FORMS_TO_FILL);
      int p = formsToFill.indexWhere((e) => e.getString("path") == path);
      if (p != -1) {
        formModel = formsToFill[p];
        // startDate = formModel.getInt(AGREEMENT_DATE);
        // if (startDate == 0) startDate = DateTime.now().millisecondsSinceEpoch;
      }
      setState(() {});
    });
    subs.add(sub);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: LayoutBuilder(builder: (c, box) {
              final size = box.maxWidth;
              print("size $size");
              bool isLargeScreen = box.maxWidth > 850;
              final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

              return Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                padding: EdgeInsets.only(
                  // left: isLargeScreen ? size * 0.25 : 20,
                  // right: isLargeScreen ? size * 0.25 : 20,

                  left: isLargeScreen ? size * 0.15 : 20,
                  right: isLargeScreen ? size * 0.15 : 20,
                  top: 120,
                  bottom: 20,
                ),
                decoration: BoxDecoration(
                    color: isLargeScreen ? scaffoldColor : white,
                    borderRadius: BorderRadius.circular(10)),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    styledTextContainer(),
                    addSpace(20),
                    InkWell(
                      onTap: () {
                        termsAccepted = !termsAccepted;
                        setState(() {});
                      },
                      child: Container(
                        child: Row(
                          children: [
                            checkBox(termsAccepted),
                            addSpaceWidth(20),
                            Expanded(
                                child: Text(
                              "By clicking on  SAVE INFORMATION below, you understand and accept"
                              " the terms of this offer presented above. "
                              "We look forward to you joining our team and making "
                              "a positive contribution to the company.",
                              style: TextStyle(fontSize: 12),
                            ))
                          ],
                        ),
                      ),
                    ),
                    addSpace(20),
                    Container(
                      height: 60,
                      child: TextButton(
                        onPressed: !termsAccepted
                            ? null
                            : () {
                                mainModel.put(AGREEMENT_DATE,
                                    DateTime.now().millisecondsSinceEpoch);

                                formModel.put(STATUS, STATUS_COMPLETED);
                                int p = formsToFill.indexWhere(
                                    (e) => e.getString("path") == path);
                                if (p != -1) {
                                  // formModel.put(AGREEMENT_DATE,
                                  //     DateTime.now().millisecondsSinceEpoch);
                                  formsToFill[p] = formModel;
                                }

                                mainModel.put(FORMS_TO_FILL,
                                    formsToFill.map((e) => e.items).toList());

                                mainModel.updateItems();

                                showMessage(
                                  context,
                                  Icons.check,
                                  green,
                                  "Form Submitted!",
                                  "You have successful completed your Caregiver Agreement form",
                                  // cancellable: false,
                                  delayInMilli: 700,
                                  clickYesText: "Go Back",
                                  cancellable: false,
                                  onClicked: (_) {
                                    // Navigator.pop(context, true);
                                    Navigator.pushReplacementNamed(
                                        context, "/applicationForm??$formId");
                                    // Navigator.pushNamed(context, "/$path??$formId");
                                  },
                                );
                              },
                        child: Center(
                          child: Text(
                            "Save Information".toUpperCase(),
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            minimumSize: Size(300, 50)),
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
          appBarHeader(context)
        ],
      ),
    );
  }

  styledTextContainer() {
    return Container(
      padding: EdgeInsets.all(20),
      decoration:
          BoxDecoration(color: white, borderRadius: BorderRadius.circular(10)),
      child: StyledText(
        text: """
Dear $name,

Congratulations! Exceptional Medical Staffing Inc. is pleased to offer you a contract PRN MedTech position, contingent upon the acceptable results of a background check and drug screen. As a reminder, this offer of employment can be extended or shortened based on business needs. As a contracted employee, contract length can be adjusted at any time without notice.

<u>Engagement</u>: By signing below you are agreeing to the following: you agree to perform tasks associated with the position of a MedTech. 

<u>Work Hours</u>: Your work hours is based on work availability; and the number of hours you are willing to accept.

<u>Pay Rate</u>: Your pay rate will be $offerAmount	per hour. You will be paid for the number of hours worked in a
40-hour workweek. Any time worked over 40 hours must be pre-approved by your manager and Human Resources to avoid a delay in payment. Note: As a PRN contract worker, we do not pay for over-time.

<u>Benefits</u>: This is a non-benefited position. Any time off your assignment would be considered non-paid. 

<u>Employee Status</u>: You are a W2 PRN contract worker and must complete all statutory withholding forms.

<u>Payroll Administrator</u>: Exceptional Medical Staffing Inc. has contracted Aten Solutions Inc. as Employer of Records for Payroll purposes. Aten Solution Inc. has hired Time and Payroll to process your payroll. Please provide a voided check or direct deposit bank Information document to avoid errors in payroll remittances.

<u>Pay Period and Payment</u>: Payroll will be every other Friday. We pay two weeks in arrears. A payroll calendar will be sent to you upon the start of your assignment. You must submit your weekly timesheet the Monday after the end of your week which ends each Sunday. Failure to submit your weekly timesheet the Monday following your work week by 2.00pm EST, will result in you not getting paid timely, and you will have to wait until the next payroll cycle following the receipt of your late timesheet.

<u>Confidentiality of Client Information</u>: You may have access to information, which is proprietary of Client and confidential in nature. Accordingly, you agree that (a) you will not publish any such information, nor disseminate the same, and will use such information only for the purpose of providing the services to Clients, and for no other purpose, and (b) all such information is and will remain the property of Client, and upon completion of the Services, shall be returned to Client.

<u>Confidentiality of Information</u>: You agree not to disclose to any third party, including Client, without the prior written permission of Exceptional Medical Staffing Inc., any information relating to Exceptional Medical Staffing’s customers, target customers, clients, employees, and consultants.

<u>Intellectual Property Rights</u>: All materials and work product developed by you for Exceptional Medical Staffing Inc. shall be the exclusive property of Exceptional Medical Staffing Inc. You agree that all intellectual property privileges for the materials developed shall be the sole and exclusive property of Exceptional Medical Staffing Inc, free from any claim or retention of those rights.

<u>Non-Compete Clause</u>: Exceptional Medical Staffing Inc. has invested considerable time, effort, and money in locating qualified personnel, contractors, and clients. In recognition of that investment, you agree to work for Exceptional Medical Staffing Inc. for a minimum of 24 months before accepting any position with any client of Exceptional Medical Staffing Inc. You also agree not to directly or indirectly solicit, provide, or advise others of the opportunity to provide, any services to any client (or client affiliate or client customer) of  Exceptional Medical Staffing Inc. (a) that you have been introduced to by Exceptional Medical Staffing Inc. or (b) whom you have received information from Exceptional Medical Staffing Inc. or (c) have been associated with due to your association with Exceptional Medical Staffing Inc. for a period of 1 year after the termination of this agreement.
Any Client of Exceptional Medical Staffing Inc. who wishes to hire any staff of Exceptional Medical Staffing Inc. before the expiration of this agreement shall pay to Exceptional Medical Staffing Inc a Buy-Out Fee the sum equivalent to the two months full-time salary of the said employee.

By your signature, you understand and accept the terms of this agreement presented above. Thank you for being a valuable team member of Exceptional Medical Staffing Inc.
          """,
        tags: {
          'b': StyledTextTag(style: TextStyle(fontWeight: FontWeight.bold)),
          'u': StyledTextTag(
              style: TextStyle(decoration: TextDecoration.underline)),
        },
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
