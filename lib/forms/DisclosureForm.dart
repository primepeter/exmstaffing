import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/main.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'EmploymentApplication.dart';

class Addresses {
  int fromDate = 0;
  int toDate = 0;
  String state;
  String city;

  final addressController = TextEditingController();
  final zipController = TextEditingController();

  Addresses({Map items}) {
    if (null != items) {
      fromDate = items["fromDate"];
      toDate = items["toDate"];
      state = items["state"];
      city = items["city"];
      zipController.text = items["zip"];
      addressController.text = items["address"];
    }
  }

  bool get isValidated {
    bool validated = true;
    if (fromDate == 0) return false;
    if (toDate == 0) return false;
    if (null == state || state.isEmpty) return false;
    if (null == city || city.isEmpty) return false;
    if (zipController.text.isEmpty) return false;
    if (addressController.text.isEmpty) return false;
    return validated;
  }

  Map get items {
    return {
      "fromDate": fromDate,
      "toDate": toDate,
      "state": state,
      "city": city,
      "zip": zipController.text,
      "address": addressController.text,
    };
  }
}

class Licences {
  int expiry = 0;
  String state;
  String city;

  final typeController = TextEditingController();
  final numberController = TextEditingController();
  final professionController = TextEditingController();

  Licences({Map items}) {
    if (null != items) {
      expiry = items["expiry"];
      state = items["state"];
      city = items["city"];
      typeController.text = items["type"];
      numberController.text = items["number"];
      professionController.text = items["profession"];
    }
  }

  bool get isValidated {
    bool validated = true;
    if (0 == expiry) return false;
    if (null == state || state.isEmpty) return false;
    if (null == city || city.isEmpty) return false;
    if (typeController.text.isEmpty) return false;
    if (numberController.text.isEmpty) return false;
    if (professionController.text.isEmpty) return false;
    return validated;
  }

  Map get items {
    return {
      "expiry": expiry,
      "state": state,
      "city": city,
      "type": typeController.text,
      "profession": numberController.text,
      "number": professionController.text,
    };
  }
}

class DisclosureForm extends StatefulWidget {
  const DisclosureForm({Key key}) : super(key: key);

  @override
  _DisclosureFormState createState() => _DisclosureFormState();
}

class _DisclosureFormState extends State<DisclosureForm> {
  List educationLevels = [
    "Grammar School",
    "High School",
    "College School",
    "Trade,Business or Correspondence School",
  ];

  final lastNameController = TextEditingController();
  final firstNameController = TextEditingController();
  final middleNameController = TextEditingController();
  final maidenNameController = TextEditingController();
  final ssnController = TextEditingController();
  final emailController = TextEditingController();
  final phoneController = TextEditingController();

  // final contactNameController = TextEditingController();
  // final contactPhoneController = TextEditingController();
  // final contactEmailController = TextEditingController();
  // final contactRelationshipController = TextEditingController();
  // final addressController = TextEditingController();
  // final driversLicenceController = TextEditingController();
  // final contactZipController = TextEditingController();
  String contactState;
  String contactCity;
  int dateOfBirth = 0;

  List<Addresses> addresses = [];
  List<Licences> licences = [];
  List<Education> educationInfo = [];

  bool termsAccepted = false;
  bool termsAccepted2 = false;

  List<StreamSubscription> subs = [];

  String path = "consentForm";
  BaseModel mainModel;
  BaseModel formModel;
  String get formId => routeDataHolder["/consentForm"];
  String get name => mainModel.getString(FIRST_NAME);
  List<BaseModel> formsToFill = [];

  @override
  initState() {
    super.initState();
    loadSettings();
    loadItems();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadSettings() async {
    var settingsSub = FirebaseFirestore.instance
        .collection(APP_SETTINGS_BASE)
        .doc(APP_SETTINGS)
        .snapshots()
        .listen((shot) {
      if (shot != null) {
        appSettingsModel = BaseModel(doc: shot);
        List banned = appSettingsModel.getList(BANNED);
      }
    });
    subs.add(settingsSub);
  }

  loadItems() async {
    if (null == formId || formId.isEmpty) {
      showErrorDialog(
          context, "Ops the application link is invalid or has expired!",
          cancellable: false);
      return;
    }

    var sub = FirebaseFirestore.instance
        .collection(EMPLOYEE_BASE)
        .where(OBJECT_ID, isEqualTo: formId)
        .limit(1)
        .snapshots()
        .listen((value) {
      if (value.size == 0) {
        showErrorDialog(
            context, "Ops the application link is invalid or has expired!",
            cancellable: false);
        return;
      }
      mainModel = BaseModel(doc: value.docs[0]);
      addresses = mainModel
          .getListModel(ADDRESS_INFO)
          .map((e) => Addresses(items: e.items))
          .toList();

      licences = mainModel
          .getListModel(LICENCE_INFO)
          .map((e) => Licences(items: e.items))
          .toList();

      educationInfo = mainModel
          .getListModel(EDUCATION_INFO)
          .map((e) => Education(items: e.items))
          .toList();

      lastNameController.text = mainModel.getString(LAST_NAME);
      firstNameController.text = mainModel.getString(FIRST_NAME);
      middleNameController.text = mainModel.getString(MIDDLE_NAME);
      // relationshipController.text = formModel.getString(RELATIONSHIP);
      ssnController.text = mainModel.getString(SOCIAL_SECURITY_NUMBER);
      dateOfBirth = mainModel.getInt(DATE_OF_BIRTH);

      ssnController.text = mainModel.getString(SOCIAL_SECURITY_NUMBER);
      emailController.text = mainModel.getString(EMAIL);
      phoneController.text = mainModel.getString(TELEPHONE);
      contactState = mainModel.getString(CONTACT_STATE);
      contactCity = mainModel.getString(CONTACT_CITY);

      formsToFill = mainModel.getListModel(FORMS_TO_FILL);
      int p = formsToFill.indexWhere((e) => e.getString("path") == path);
      if (p != -1) {
        formModel = formsToFill[p];
      }
      setState(() {});
    });
    subs.add(sub);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: LayoutBuilder(builder: (c, box) {
              final size = box.maxWidth;
              bool isLargeScreen = box.maxWidth > 850;
              final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

              return Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                padding: EdgeInsets.only(
                  // left: isLargeScreen ? size * 0.25 : 20,
                  // right: isLargeScreen ? size * 0.25 : 20,

                  left: isLargeScreen ? size * 0.15 : 20,
                  right: isLargeScreen ? size * 0.15 : 20,
                  top: 120,
                  bottom: 20,
                ),
                decoration: BoxDecoration(
                    color: isLargeScreen ? scaffoldColor : white,
                    borderRadius: BorderRadius.circular(10)),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: isLargeScreen ? white : null,
                          borderRadius: BorderRadius.circular(10)),
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              style: TextButton.styleFrom(
                                minimumSize: Size(40, 40),
                                backgroundColor: white,
                                primary: black,
                                elevation: 4,
                                // shape: CircleBorder()
                              ),
                              child: Icon(Icons.navigate_before)),
                          addSpace(10),
                          Text(
                            "Accutrace Consent",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          // addSpace(20),
                          // Text(
                          //   "Personal Information",
                          //   style: TextStyle(
                          //       fontSize: 20, fontWeight: FontWeight.bold),
                          // ),
                          addSpace(20),
                          Row(
                            children: [
                              Expanded(
                                child: inputField(
                                  "Last Name",
                                  lastNameController,
                                  onChanged: (s) {
                                    setState(() {});
                                  },
                                ),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child: inputField(
                                  "First Name",
                                  firstNameController,
                                  onChanged: (s) {
                                    setState(() {});
                                  },
                                ),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child: inputField(
                                  "Middle Name",
                                  middleNameController,
                                  onChanged: (s) {
                                    setState(() {});
                                  },
                                ),
                              ),
                            ],
                          ),

                          Row(
                            children: [
                              Expanded(
                                child: clickField(
                                    "Date of Birth?",
                                    dateOfBirth == 0
                                        ? ""
                                        : getSimpleDate(dateOfBirth,
                                            pattern: "EEE MMMM dd, yyyy"),
                                    () async {
                                  final DateTime picked = await showDatePicker(
                                      builder: (ctx, child) {
                                        return Theme(
                                            data: Theme.of(context).copyWith(
                                              colorScheme: ColorScheme.light(
                                                primary: appColor,
                                                onPrimary:
                                                    black, // header text color
                                                // onSurface: Colors.green, // body text color
                                              ),
                                              buttonTheme: ButtonThemeData(
                                                  textTheme:
                                                      ButtonTextTheme.primary),
                                            ),
                                            child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                child: child));
                                      },
                                      context: context,
                                      initialDate: dateOfBirth == 0
                                          ? DateTime(DateTime.now().year - 18)
                                          : DateTime.fromMillisecondsSinceEpoch(
                                              dateOfBirth),
                                      firstDate: DateTime(1960),
                                      lastDate:
                                          DateTime(DateTime.now().year - 18));

                                  if (picked != null &&
                                      picked.millisecondsSinceEpoch !=
                                          dateOfBirth)
                                    dateOfBirth = picked.millisecondsSinceEpoch;
                                  setState(() {});
                                }),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child: inputField(
                                  "Social Security Number",
                                  ssnController,
                                  onChanged: (s) {
                                    setState(() {});
                                  },
                                ),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child: inputField(
                                  "Email",
                                  emailController,
                                  onChanged: (s) {
                                    setState(() {});
                                  },
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: clickField(
                                    "Date of Birth?",
                                    dateOfBirth == 0
                                        ? ""
                                        : getSimpleDate(dateOfBirth,
                                            pattern: "EEE MMMM dd, yyyy"),
                                    () async {
                                  final DateTime picked = await showDatePicker(
                                      builder: (ctx, child) {
                                        return Theme(
                                            data: Theme.of(context).copyWith(
                                              colorScheme: ColorScheme.light(
                                                primary: appColor,
                                                onPrimary:
                                                    black, // header text color
                                                // onSurface: Colors.green, // body text color
                                              ),
                                              buttonTheme: ButtonThemeData(
                                                  textTheme:
                                                      ButtonTextTheme.primary),
                                            ),
                                            child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                child: child));
                                      },
                                      context: context,
                                      initialDate: dateOfBirth == 0
                                          ? DateTime(DateTime.now().year - 18)
                                          : DateTime.fromMillisecondsSinceEpoch(
                                              dateOfBirth),
                                      firstDate: DateTime(1960),
                                      lastDate:
                                          DateTime(DateTime.now().year - 18));

                                  if (picked != null &&
                                      picked.millisecondsSinceEpoch !=
                                          dateOfBirth)
                                    dateOfBirth = picked.millisecondsSinceEpoch;
                                  setState(() {});
                                }),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child: inputField(
                                  "Phone",
                                  phoneController,
                                  isNum: true,
                                  onChanged: (s) {
                                    setState(() {});
                                  },
                                ),
                              ),
                              addSpaceWidth(10),
                            ],
                          ),

                          addLine(20, black.withOpacity(.03), 0, 20, 0, 20),
                          Row(
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Addresses",
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      "Please list all address within the last 7 years beginning with your current address.",
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: black.withOpacity(.5),
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                              if (addresses.length < 4)
                                TextButton(
                                  onPressed: () {
                                    addresses.add(Addresses());
                                    setState(() {});
                                  },
                                  child: Text(
                                    "Add Address",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  style: TextButton.styleFrom(
                                      padding: EdgeInsets.zero,
                                      primary: white,
                                      backgroundColor: black,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      minimumSize: Size(160, 50)),
                                )
                            ],
                          ),
                          addSpace(20),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            child: Builder(builder: (ctx) {
                              if (addresses.isEmpty)
                                return Container(
                                  padding: EdgeInsets.all(0),
                                  child: DottedBorder(
                                    color: black.withOpacity(.5),
                                    strokeWidth: 1,
                                    borderType: BorderType.RRect,
                                    radius: Radius.circular(10),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Container(
                                          height: 150,
                                          alignment: Alignment.center,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Icon(
                                                Icons.home,
                                                color: black.withOpacity(.5),
                                                size: 30,
                                              ),
                                              addSpace(10),
                                              Text(
                                                "You have not added addresses yet",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: black.withOpacity(.5)
                                                    // fontWeight: FontWeight.bold,
                                                    ),
                                              )
                                            ],
                                          )),
                                    ),
                                  ),
                                );

                              return Column(
                                children:
                                    List.generate(addresses.length, (index) {
                                  final fm = addresses[index];

                                  return Container(
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: clickField(
                                                        "From",
                                                        fm.fromDate == 0
                                                            ? ""
                                                            : getSimpleDate(
                                                                fm.fromDate,
                                                                pattern:
                                                                    "EEE MMMM dd, yyyy"),
                                                        () async {
                                                      final picked =
                                                          await showDatePicker(
                                                              builder:
                                                                  (ctx, child) {
                                                                return Theme(
                                                                    data: Theme.of(
                                                                            context)
                                                                        .copyWith(
                                                                      colorScheme:
                                                                          ColorScheme
                                                                              .light(
                                                                        primary:
                                                                            appColor,
                                                                        onPrimary:
                                                                            black, // header text color
                                                                        // onSurface: Colors.green, // body text color
                                                                      ),
                                                                      buttonTheme:
                                                                          ButtonThemeData(
                                                                              textTheme: ButtonTextTheme.primary),
                                                                    ),
                                                                    child: ClipRRect(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                10),
                                                                        child:
                                                                            child));
                                                              },
                                                              context: context,
                                                              initialDate: fm
                                                                          .fromDate ==
                                                                      0
                                                                  ? DateTime
                                                                      .now()
                                                                  : DateTime
                                                                      .fromMillisecondsSinceEpoch(fm
                                                                          .fromDate),
                                                              firstDate:
                                                                  DateTime
                                                                      .now(),
                                                              lastDate: DateTime(
                                                                  DateTime.now()
                                                                          .year +
                                                                      2));
                                                      if (picked != null)
                                                        fm.fromDate = picked
                                                            .millisecondsSinceEpoch;
                                                      setState(() {});
                                                    }),
                                                  ),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                    child: clickField(
                                                        "To",
                                                        fm.toDate == 0
                                                            ? ""
                                                            : getSimpleDate(
                                                                fm.toDate,
                                                                pattern:
                                                                    "EEE MMMM dd, yyyy"),
                                                        () async {
                                                      final picked =
                                                          await showDatePicker(
                                                              builder: (ctx,
                                                                  child) {
                                                                return Theme(
                                                                    data: Theme.of(
                                                                            context)
                                                                        .copyWith(
                                                                      colorScheme:
                                                                          ColorScheme
                                                                              .light(
                                                                        primary:
                                                                            appColor,
                                                                        onPrimary:
                                                                            black, // header text color
                                                                        // onSurface: Colors.green, // body text color
                                                                      ),
                                                                      buttonTheme:
                                                                          ButtonThemeData(
                                                                              textTheme: ButtonTextTheme.primary),
                                                                    ),
                                                                    child: ClipRRect(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                10),
                                                                        child:
                                                                            child));
                                                              },
                                                              context: context,
                                                              initialDate: fm
                                                                          .toDate ==
                                                                      0
                                                                  ? DateTime
                                                                      .now()
                                                                  : DateTime
                                                                      .fromMillisecondsSinceEpoch(fm
                                                                          .toDate),
                                                              firstDate:
                                                                  DateTime
                                                                      .now(),
                                                              lastDate: DateTime(
                                                                  DateTime.now()
                                                                          .year +
                                                                      2));
                                                      if (picked != null)
                                                        fm.toDate = picked
                                                            .millisecondsSinceEpoch;
                                                      setState(() {});
                                                    }),
                                                  )
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                      child: inputField(
                                                          "Street Address",
                                                          fm.addressController,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                      child: inputField(
                                                          "Zip Code",
                                                          fm.zipController,
                                                          isNum: true,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                ],
                                              ),
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Expanded(
                                                    child: clickField(
                                                        "State", fm.state, () {
                                                      showListDialog(
                                                          context,
                                                          usaStates.keys
                                                              .toList(), (_) {
                                                        fm.state = usaStates
                                                            .keys
                                                            .toList()[_];
                                                        setState(() {});
                                                      }, returnIndex: true);
                                                    }),
                                                  ),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                      child: clickField(
                                                          "City", fm.city, () {
                                                    if (null == fm.state) {
                                                      showError(context,
                                                          "Select a state first");
                                                      return;
                                                    }
                                                    showListDialog(
                                                        context,
                                                        usaStates[fm.state]
                                                            .toList(), (_) {
                                                      fm.city =
                                                          usaStates[fm.state]
                                                              .toList()[_];
                                                      setState(() {});
                                                    }, returnIndex: true);
                                                  })),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        addSpaceWidth(10),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 15),
                                          child: TextButton(
                                            onPressed: () {
                                              addresses.removeAt(index);
                                              setState(() {});
                                            },
                                            child: Text(
                                              "Remove",
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            style: TextButton.styleFrom(
                                                padding: EdgeInsets.zero,
                                                primary: white,
                                                backgroundColor: red,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8)),
                                                minimumSize: Size(100, 50)),
                                          ),
                                        )
                                      ],
                                    ),
                                  );
                                }),
                              );
                            }),
                          ),
                          addLine(20, black.withOpacity(.03), 0, 20, 0, 20),

                          Row(
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Licences",
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      "Please list all your professional licences.",
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: black.withOpacity(.5),
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                              if (licences.length < 4)
                                TextButton(
                                  onPressed: () {
                                    licences.add(Licences());
                                    setState(() {});
                                  },
                                  child: Text(
                                    "Add Licence",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  style: TextButton.styleFrom(
                                      padding: EdgeInsets.zero,
                                      primary: white,
                                      backgroundColor: black,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      minimumSize: Size(160, 50)),
                                )
                            ],
                          ),
                          addSpace(20),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            child: Builder(builder: (ctx) {
                              if (licences.isEmpty)
                                return Container(
                                  padding: EdgeInsets.all(0),
                                  child: DottedBorder(
                                    color: black.withOpacity(.5),
                                    strokeWidth: 1,
                                    borderType: BorderType.RRect,
                                    radius: Radius.circular(10),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Container(
                                          height: 150,
                                          alignment: Alignment.center,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Icon(
                                                Icons.file_present,
                                                color: black.withOpacity(.5),
                                                size: 30,
                                              ),
                                              addSpace(10),
                                              Text(
                                                "You have not added licences yet",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: black.withOpacity(.5)
                                                    // fontWeight: FontWeight.bold,
                                                    ),
                                              )
                                            ],
                                          )),
                                    ),
                                  ),
                                );

                              return Column(
                                children:
                                    List.generate(licences.length, (index) {
                                  final fm = licences[index];

                                  return Container(
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: clickField(
                                                        "Expiry date",
                                                        fm.expiry == 0
                                                            ? ""
                                                            : getSimpleDate(
                                                                fm.expiry,
                                                                pattern:
                                                                    "EEE MMMM dd, yyyy"),
                                                        () async {
                                                      final picked =
                                                          await showDatePicker(
                                                              builder: (ctx,
                                                                  child) {
                                                                return Theme(
                                                                    data: Theme.of(
                                                                            context)
                                                                        .copyWith(
                                                                      colorScheme:
                                                                          ColorScheme
                                                                              .light(
                                                                        primary:
                                                                            appColor,
                                                                        onPrimary:
                                                                            black, // header text color
                                                                        // onSurface: Colors.green, // body text color
                                                                      ),
                                                                      buttonTheme:
                                                                          ButtonThemeData(
                                                                              textTheme: ButtonTextTheme.primary),
                                                                    ),
                                                                    child: ClipRRect(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                10),
                                                                        child:
                                                                            child));
                                                              },
                                                              context: context,
                                                              initialDate: fm
                                                                          .expiry ==
                                                                      0
                                                                  ? DateTime
                                                                      .now()
                                                                  : DateTime
                                                                      .fromMillisecondsSinceEpoch(fm
                                                                          .expiry),
                                                              firstDate:
                                                                  DateTime
                                                                      .now(),
                                                              lastDate: DateTime(
                                                                  DateTime.now()
                                                                          .year +
                                                                      2));
                                                      if (picked != null)
                                                        fm.expiry = picked
                                                            .millisecondsSinceEpoch;
                                                      setState(() {});
                                                    }),
                                                  ),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                      child: inputField(
                                                          "Licence Type",
                                                          fm.typeController,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                      child: inputField(
                                                          "Licence Number",
                                                          fm.numberController,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                      child: inputField(
                                                          "Profession",
                                                          fm.professionController,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                ],
                                              ),
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Expanded(
                                                    child: clickField(
                                                        "State", fm.state, () {
                                                      showListDialog(
                                                          context,
                                                          usaStates.keys
                                                              .toList(), (_) {
                                                        fm.state = usaStates
                                                            .keys
                                                            .toList()[_];
                                                        setState(() {});
                                                      }, returnIndex: true);
                                                    }),
                                                  ),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                      child: clickField(
                                                          "City", fm.city, () {
                                                    if (null == fm.state) {
                                                      showError(context,
                                                          "Select a state first");
                                                      return;
                                                    }
                                                    showListDialog(
                                                        context,
                                                        usaStates[fm.state]
                                                            .toList(), (_) {
                                                      fm.city =
                                                          usaStates[fm.state]
                                                              .toList()[_];
                                                      setState(() {});
                                                    }, returnIndex: true);
                                                  })),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        addSpaceWidth(10),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 15),
                                          child: TextButton(
                                            onPressed: () {
                                              licences.removeAt(index);
                                              setState(() {});
                                            },
                                            child: Text(
                                              "Remove",
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            style: TextButton.styleFrom(
                                                padding: EdgeInsets.zero,
                                                primary: white,
                                                backgroundColor: red,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8)),
                                                minimumSize: Size(100, 50)),
                                          ),
                                        )
                                      ],
                                    ),
                                  );
                                }),
                              );
                            }),
                          ),

                          addLine(20, black.withOpacity(.03), 0, 20, 0, 20),
                          Row(
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Education",
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      "List your educational qualifications.",
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: black.withOpacity(.5),
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                              if (educationInfo.length < 4)
                                TextButton(
                                  onPressed: () {
                                    if (educationInfo.isNotEmpty &&
                                        !educationInfo[educationInfo.length - 1]
                                            .isValidated) {
                                      showError(context,
                                          "Some fields on the current education item are empty!");
                                      return;
                                    }

                                    educationInfo.add(Education());
                                    setState(() {});
                                  },
                                  child: Text(
                                    "Add Education",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  style: TextButton.styleFrom(
                                      padding: EdgeInsets.zero,
                                      primary: white,
                                      backgroundColor: black,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      minimumSize: Size(160, 50)),
                                )
                            ],
                          ),
                          addSpace(20),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            child: Builder(builder: (ctx) {
                              if (educationInfo.isEmpty)
                                return Container(
                                  padding: EdgeInsets.all(0),
                                  child: DottedBorder(
                                    color: black.withOpacity(.5),
                                    strokeWidth: 1,
                                    borderType: BorderType.RRect,
                                    radius: Radius.circular(10),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Container(
                                          height: 150,
                                          alignment: Alignment.center,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Icon(
                                                Icons.school,
                                                color: black.withOpacity(.5),
                                                size: 30,
                                              ),
                                              addSpace(10),
                                              Text(
                                                "You have not added any educational qualifications yet.",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: black.withOpacity(.5)
                                                    // fontWeight: FontWeight.bold,
                                                    ),
                                              )
                                            ],
                                          )),
                                    ),
                                  ),
                                );

                              return Column(
                                children: List.generate(educationInfo.length,
                                    (index) {
                                  final ed = educationInfo[index];

                                  return Container(
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              addSpace(20),
                                              groupFields(
                                                  "Education Level",
                                                  educationLevels,
                                                  ed.levelIndex, (p) {
                                                ed.levelIndex = p;
                                                setState(() {});
                                              }),
                                              addSpace(10),
                                              Row(
                                                children: [
                                                  Expanded(
                                                      child: inputField(
                                                          "School Name",
                                                          ed.nameController,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                      child: inputField(
                                                          "School Location",
                                                          ed.locationController,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: inputField(
                                                        "Subject Studied",
                                                        ed.subjectController,
                                                        onChanged: (s) {
                                                      setState(() {});
                                                    }),
                                                  ),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                    child: yesNoField(
                                                        "Graduate?",
                                                        ed.graduateIndex, (p) {
                                                      ed.graduateIndex = p;
                                                      setState(() {});
                                                    }),
                                                  ),
                                                ],
                                              ),
                                              addSpace(20),
                                              groupFields(
                                                  "Years Completed?",
                                                  [
                                                    "1 Years",
                                                    "2 years",
                                                    "3 years",
                                                    "4 years",
                                                    "5 years"
                                                  ],
                                                  ed.yearsCompletedIndex, (p) {
                                                ed.yearsCompletedIndex = p;
                                                setState(() {});
                                              }),
                                              addSpace(20),
                                            ],
                                          ),
                                        ),
                                        addSpaceWidth(10),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 15),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              TextButton(
                                                onPressed: () {
                                                  educationInfo.removeAt(index);
                                                  setState(() {});
                                                },
                                                child: Text(
                                                  "Remove",
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                style: TextButton.styleFrom(
                                                    padding: EdgeInsets.zero,
                                                    primary: white,
                                                    backgroundColor: red,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        8)),
                                                    minimumSize: Size(140, 50)),
                                              ),
                                              addSpace(10),
                                              TextButton(
                                                onPressed: () {
                                                  if (educationInfo
                                                          .isNotEmpty &&
                                                      !educationInfo[
                                                              educationInfo
                                                                      .length -
                                                                  1]
                                                          .isValidated) {
                                                    showError(context,
                                                        "Some fields on the current education item are empty!");
                                                    return;
                                                  }

                                                  educationInfo
                                                      .add(Education());
                                                  setState(() {});
                                                },
                                                child: Text(
                                                  "Add Another",
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                style: TextButton.styleFrom(
                                                    padding: EdgeInsets.zero,
                                                    primary: white,
                                                    backgroundColor: black,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        8)),
                                                    minimumSize: Size(140, 50)),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  );
                                }),
                              );
                            }),
                          ),
                          addLine(20, black.withOpacity(.03), 0, 20, 0, 20),

                          addSpace(50),
                        ],
                      ),
                    ),
                    addSpace(20),
                    InkWell(
                      onTap: () {
                        termsAccepted2 = !termsAccepted2;
                        setState(() {});
                      },
                      child: Container(
                        child: Row(
                          children: [
                            checkBox(termsAccepted2),
                            addSpaceWidth(20),
                            Expanded(
                                child: Text(
                              "We may obtain information about you from a consumer reporting agency for employment "
                              "purposes. Thus, you may be the subject of a “consumer report” and/or an “investigative consumer report” which may include "
                              "information about your character, general reputation, personal characteristics, and/or mode of living, and which can involve personal "
                              "interviews with sources. These reports may be obtained at any time after receipt of your authorization and, if you are hired, throughout "
                              "your employment. You have the right, upon written request made within a reasonable time after receipt of this notice, to request "
                              "disclosure of the nature and scope of any investigative consumer report. Please be advised that the nature and scope of the most "
                              "common form of investigative consumer report obtained with regard to applicants for employment is an investigation into your criminal "
                              "history, education and/or employment history conducted by AccuTrace, Inc. P.O. Box 624, Bryn Mawr, PA 19010 or by contacting us at "
                              "1-888-54 -TRACE or another outside organization. The scope of this notice and authorization is all-encompassing, however, allowing "
                              "Employer to obtain from any outside organization all manner of consumer reports and investigative consumer reports now and, if you "
                              "are hired, throughout the course of your employment is limited to the extent permitted by law. As a result, you should carefully consider "
                              "whether to exercise your right to request disclosure of the nature and scope of any investigative consumer report.",
                              style: TextStyle(fontSize: 12),
                            ))
                          ],
                        ),
                      ),
                    ),
                    addSpace(20),
                    InkWell(
                      onTap: () {
                        termsAccepted = !termsAccepted;
                        setState(() {});
                      },
                      child: Container(
                        child: Row(
                          children: [
                            checkBox(termsAccepted),
                            addSpaceWidth(20),
                            Expanded(
                                child: Text(
                              "I acknowledge receipt of the NOTICE REGARDING BACKGROUND INVESTIGATION and A SUMMARY OF YOUR RIGHTS UNDER, "
                              "THE FAIR CREDIT REPORTING ACT and certify that I have read and understand both of those documents. I hereby authorize the "
                              "obtaining of “consumer reports” and/or “investigative consumer reports” at any time after receipt of this authorization and, if I am hired "
                              "throughout my employment. To this end, I hereby authorize, without reservation, any law enforcement agency, administrator, state or "
                              "federal agency, institution, school or university (public or private), information service bureau, employer, or insurance company to furnish "
                              "any and all background information requested by AccuTrace, Inc. or another outside organization acting on behalf of Employer, and/or ",
                              style: TextStyle(fontSize: 12),
                            ))
                          ],
                        ),
                      ),
                    ),
                    addSpace(20),
                    Container(
                      height: 60,
                      child: TextButton(
                        onPressed: !(termsAccepted && termsAccepted2)
                            ? null
                            : () {
                                String lName = lastNameController.text;
                                String fName = firstNameController.text;
                                String mName = middleNameController.text;
                                String ssn = ssnController.text;
                                String email = emailController.text;
                                String tel = phoneController.text;

                                int status = STATUS_COMPLETED;

                                if (addresses.isEmpty &&
                                    licences.isEmpty &&
                                    educationInfo.isEmpty &&
                                    lName.isEmpty &&
                                    fName.isEmpty &&
                                    mName.isEmpty &&
                                    // rel.isEmpty &&
                                    ssn.isEmpty &&
                                    email.isEmpty &&
                                    dateOfBirth == 0 &&
                                    tel.isEmpty &&
                                    contactState.isEmpty &&
                                    contactCity.isEmpty) {
                                  status = STATUS_UNDONE;
                                }

                                if (addresses.isEmpty ||
                                    licences.isEmpty ||
                                    educationInfo.isEmpty ||
                                    lName.isEmpty ||
                                    fName.isEmpty ||
                                    mName.isEmpty ||
                                    // rel.isEmpty ||
                                    ssn.isEmpty ||
                                    email.isEmpty ||
                                    dateOfBirth == 0 ||
                                    tel.isEmpty ||
                                    contactState.isEmpty ||
                                    contactCity.isEmpty) {
                                  status = STATUS_IN_PROGRESS;
                                }

                                mainModel.put(ADDRESS_INFO,
                                    addresses.map((e) => e.items).toList());

                                mainModel.put(LICENCE_INFO,
                                    licences.map((e) => e.items).toList());

                                mainModel.put(EDUCATION_INFO,
                                    educationInfo.map((e) => e.items).toList());

                                mainModel.put(LAST_NAME, lName);
                                mainModel.put(FIRST_NAME, fName);
                                mainModel.put(MIDDLE_NAME, mName);

                                mainModel.put(SOCIAL_SECURITY_NUMBER, ssn);
                                mainModel.put(EMAIL, email);
                                mainModel.put(DATE_OF_BIRTH, dateOfBirth);

                                mainModel.put(EMAIL, email);
                                mainModel.put(TELEPHONE, tel);

                                mainModel.put(CONTACT_STATE, contactState);
                                mainModel.put(CONTACT_CITY, contactCity);

                                formModel.put(STATUS, status);

                                int p = formsToFill.indexWhere(
                                    (e) => e.getString("path") == path);
                                if (p != -1) {
                                  formsToFill[p] = formModel;
                                }

                                mainModel.put(FORMS_TO_FILL,
                                    formsToFill.map((e) => e.items).toList());
                                mainModel.updateItems();

                                showMessage(
                                  context,
                                  Icons.check,
                                  green,
                                  "Form Submitted!",
                                  "You have successful completed your hire information application form",
                                  // cancellable: false,
                                  delayInMilli: 700,
                                  clickYesText: "Go Back",
                                  cancellable: false,
                                  onClicked: (_) {
                                    // Navigator.pop(context, true);
                                    Navigator.pushReplacementNamed(
                                        context, "/applicationForm??$formId");
                                  },
                                );
                              },
                        child: Center(
                          child: Text(
                            "Save Information".toUpperCase(),
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            minimumSize: Size(300, 50)),
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
          appBarHeader(context)
        ],
      ),
    );
  }

  educationItem(
    String title,
    TextEditingController nameController,
    TextEditingController subjectController,
    int graduateIndex,
    int yearsCompletedIndex,
    onGradChanged,
    onYearsChanged,
  ) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        addSpace(20),
        Text(
          title,
          style: TextStyle(
              fontSize: 16,
              color: black.withOpacity(.5),
              fontWeight: FontWeight.bold),
        ),
        addSpace(10),
        Row(
          children: [
            Expanded(
                child: inputField("Name & School Location", nameController,
                    onChanged: (s) {
              setState(() {});
            })),
            addSpaceWidth(10),
            Expanded(
              child: inputField("Subject Studied", subjectController,
                  onChanged: (s) {
                setState(() {});
              }),
            ),
          ],
        ),
        yesNoField("Did you graduate?", graduateIndex, (p) {
          onGradChanged(p);
        }),
        addSpace(20),
        Text(
          "Years Completed?",
          style: TextStyle(
              fontSize: 14,
              color: black.withOpacity(.5),
              fontWeight: FontWeight.bold),
        ),
        addSpace(10),
        Wrap(
          children: List.generate(4, (p) {
            String title = "${p + 1} Years";
            bool active = p == yearsCompletedIndex;
            return GestureDetector(
              onTap: () {
                onYearsChanged(p);
              },
              child: Container(
                height: 40,
                width: 160,
                margin: EdgeInsets.all(2),
                child: Text(
                  title,
                  style: TextStyle(
                      color: active ? white : black,
                      fontWeight: active ? FontWeight.bold : FontWeight.normal),
                ),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: active ? (black) : white,
                    boxShadow: [
                      BoxShadow(
                          color: black.withOpacity(.05),
                          spreadRadius: 2,
                          blurRadius: 5)
                    ],
                    borderRadius: BorderRadius.circular(8)),
              ),
            );
          }),
        ),
        addSpace(20),
      ],
    );
  }
}
