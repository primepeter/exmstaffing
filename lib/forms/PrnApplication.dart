import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PrnApplication extends StatefulWidget {
  const PrnApplication({Key key}) : super(key: key);

  @override
  _PrnApplicationState createState() => _PrnApplicationState();
}

class _PrnApplicationState extends State<PrnApplication> {
  String applicantName = "Steven Danny";
  int startDate = DateTime.now().add(Duration(days: 1)).millisecondsSinceEpoch;
  double offerAmount = 800;

  bool termsAccepted = false;

  @override
  initState() {
    super.initState();
    //loadSettings();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    //   for (var s in subs) s?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: LayoutBuilder(builder: (c, box) {
              final size = box.maxWidth;
              print("size $size");
              bool isLargeScreen = box.maxWidth > 850;
              final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

              return Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                padding: EdgeInsets.only(
                  // left: isLargeScreen ? size * 0.25 : 20,
                  // right: isLargeScreen ? size * 0.25 : 20,

                  left: isLargeScreen ? size * 0.15 : 20,
                  right: isLargeScreen ? size * 0.15 : 20,
                  top: 120,
                  bottom: 20,
                ),
                decoration: BoxDecoration(
                    color: isLargeScreen ? scaffoldColor : white,
                    borderRadius: BorderRadius.circular(10)),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: isLargeScreen ? white : null,
                          borderRadius: BorderRadius.circular(10)),
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              style: TextButton.styleFrom(
                                minimumSize: Size(40, 40),
                                backgroundColor: white,
                                primary: black,
                                elevation: 4,
                                // shape: CircleBorder()
                              ),
                              child: Icon(Icons.navigate_before)),
                          // addSpaceWidth(5),
                          Text(
                            "Dear $applicantName",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          addSpace(20),
                          Text.rich(
                            TextSpan(children: [
                              TextSpan(
                                text:
                                    "Congratulations! A10 Exceptional Medical Staffing (A10) subsidiary of Aten Solutions is pleased to offer you a",
                              ),
                              TextSpan(
                                text: "\nRegistered Nurse (PRN)",
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text:
                                    "position with our healthcare client, contingent upon the results of a background"
                                    "check and drug screen. As a reminder, this offer of employment can be extended or shortened based on"
                                    "business needs. As a contracted employee, contract length can be adjusted at any time without notice.",
                              ),
                              TextSpan(
                                text: "\n\nEngagement:",
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text:
                                    " By signing below you are agreeing to the following: you agree to perform tasks associated with"
                                    "the position of a ",
                              ),
                              TextSpan(
                                text: "Registered Nurse (PRN). ",
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text:
                                    " Requirements and responsibilities include but are not limited to:",
                              ),
                              WidgetSpan(
                                  child: Container(
                                margin: EdgeInsets.only(
                                    left: 30, top: 20, bottom: 20),
                                child: Text(
                                    "Coordinate self-driven wellness programming for those individuals who are more independent,"
                                    "and less reliant on assistance Create services plans for individual residents in assisted living "
                                    "and other long-term care facilities Test and re-evaluate the effectiveness and "
                                    "appropriateness of those plans Coordinate healthcare services (specifically those services that "
                                    "come from sources outside of the assisted living facility or long-term care facility— like onboarding"
                                    " and supporting hospice staff) \nBuild and maintain relationships with other home health organizations "
                                    "Maintain consistent contact with local physicians to stay up-to-date on each individuals' medical needs "
                                    "and service expectations\n Coordinate discharges for individuals whose needs are no longer been met."
                                    " And any other duties required by the facility."),
                              )),
                              TextSpan(
                                text: "\n\nWork Hours:",
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text:
                                    " Your anticipated start date will be on ",
                              ),
                              TextSpan(
                                text: getSimpleDate(startDate),
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text: " pending the successful completion of a "
                                    "negative Covid-19 test, background "
                                    "check and drug screen. Your workdays "
                                    "will be Monday to Friday, 8.00am to 5.00pm."
                                    " You are expected to work 35-40 hours/week.",
                              ),
                              TextSpan(
                                text: "\n\nPay:",
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text: " Your pay rate will be ",
                              ),
                              TextSpan(
                                text: "\$$offerAmount",
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                  text: " You will be paid for the number"
                                      " of hours worked in a 40-hour workweek. "
                                      "Any time worked over 40 hours must be pre-approved"
                                      " by your manager and HumanResources "
                                      "to avoid a delay in payment."),
                              TextSpan(
                                text: "\n\nBenefits:",
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text:
                                    " This is a non-benefited position. Any time"
                                    " off of your assignment would be considered non-paid."
                                    "Employees who have a full-time status "
                                    "(FT is defined as working 30 or more hours a week) ",
                              ),
                              TextSpan(
                                text: "\n\nEmployee Status:",
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text:
                                    " You are a W2 employee and must complete all statutory withholding forms.",
                              ),
                              TextSpan(
                                text: "\n\nPayroll Administration:",
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text:
                                    " A10 has contracted with Time and Payroll "
                                    "to process your payroll. Please provide"
                                    " avoided check for direct deposit.",
                              ),
                              TextSpan(
                                text: "\n\nPay Period and Payment:",
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text:
                                    " Payroll will be every other Friday. We pay"
                                    " two weeks in arrears. A payroll calendar"
                                    " will be sent to you upon the start of your assignment."
                                    " If your weekly timesheet is not submitted "
                                    "and approved by your manager by the following"
                                    " Monday at 12:00p EDT, you will NOT get paid"
                                    " until the next payroll cycle following receipt of approvals.",
                              ),
                              TextSpan(
                                text:
                                    "\n\nConfidentiality of Client Information:",
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text:
                                    "You may have access to information, which is"
                                    " proprietary of Client and confidential in nature. "
                                    "Accordingly, you agree that (a) you will not publish "
                                    "any such information, nor disseminate the same, "
                                    "and will use such information only for the purpose"
                                    " of providing the services to Clients,  and for no other purpose,"
                                    " and (b) all such information is and will "
                                    "remain the property of Client, and upon completion "
                                    "of the Services, shall be returned to Client.",
                              ),
                              TextSpan(
                                text: "\n\nConfidentiality of A10 Information:",
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text:
                                    " You agree not to disclose to any third party, including Client,"
                                    " without the prior written permission of A10 and "
                                    "Exceptional Medical Staffing, any information relating to"
                                    " A10’s and Exceptional Medical Staffing’s customers, "
                                    "target customers clients, employees, and consultants.",
                              ),
                              TextSpan(
                                text: "\n\nIntellectual Property Rights:",
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text:
                                    " All materials and work product developed by you"
                                    " for A10 and Exceptional Medical Staffing shall "
                                    "be the exclusive property of A10 and Exceptional Medical Staffing."
                                    " You agree that all intellectual property privileges "
                                    "for the materials developed shall be the sole and "
                                    "exclusive property of Client or A10 and Exceptional "
                                    "Medical Staffing, free from any claim or retention of those rights.",
                              ),
                              TextSpan(
                                text: "\n\nNon-Compete Clause:",
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text:
                                    " A10 and Exceptional Medical Staffing has invested considerable time,"
                                    " effort, and money in locating qualified personnel, contractors,"
                                    " and clients. In recognition of that investment, "
                                    "you agree not to directly or indirectly solicit, provide,"
                                    " or advise others of the opportunity to provide, "
                                    "any services to any client (or client affiliate or client customer)"
                                    " of A10 and Exceptional Medical Staffing (a) "
                                    "that you have been introduced to by A10 and Exceptional "
                                    "Medical Staffing or (b) whom you have received information "
                                    "from A10 and Exceptional Medical Staffing or (c) have been associated "
                                    "with due to your association with A10 and Exceptional Medical Staffing "
                                    "for a period of 1 year after the termination of this agreement. ",
                              ),
                            ]),
                            style: TextStyle(
                              fontSize: 14,
                            ),
                          ),
                          addSpace(20),
                          addSpace(50),
                        ],
                      ),
                    ),
                    addSpace(20),
                    InkWell(
                      onTap: () {
                        termsAccepted = !termsAccepted;
                        setState(() {});
                      },
                      child: Container(
                        child: Row(
                          children: [
                            checkBox(termsAccepted),
                            addSpaceWidth(20),
                            Expanded(
                                child: Text(
                              "By clicking on  SAVE INFORMATION below, you understand and accept"
                              " the terms of this offer presented above. "
                              "We look forward to you joining our team and making "
                              "a positive contribution to the company.",
                              style: TextStyle(fontSize: 12),
                            ))
                          ],
                        ),
                      ),
                    ),
                    addSpace(20),
                    Container(
                      height: 60,
                      child: TextButton(
                        onPressed: !termsAccepted
                            ? null
                            : () {
                                return;
                                showProgress(true, context,
                                    msg: "Please wait...");
                                String id = getRandomId();
                                BaseModel model = BaseModel();
                                model.put(OBJECT_ID, id);
                                // model.put(EMAIL, email);
                                // model.put(FIRST_NAME, fName);
                                // model.put(MIDDLE_NAME, mName);
                                // model.put(LAST_NAME, lName);
                                // model.put(ADDRESS, address);
                                // model.put(STATE, selectedState);
                                // model.put(CITY, selectedCity);
                                // model.put(ZIPCODE, zip);
                                // model.put(MY_SKILLS, selectedSkill);
                                // model.put(EMPLOYMENT_INDEX, employmentIndex);
                                // model.put(NEGATIVE_INDEX, negativeIndex);
                                // model.put(VACCINATION_INDEX, vaccinationIndex);
                                // model.put(SHIFTS, shiftAvailable);
                                model.saveItem(EMPLOYEE_BASE, true,
                                    document: id, onComplete: (e) {
                                  showProgress(
                                    false,
                                    context,
                                  );
                                  if (null != e) {
                                    showErrorDialog(context, e);
                                    return;
                                  }

                                  showMessage(
                                    context,
                                    Icons.check,
                                    green,
                                    "Application Submitted!",
                                    "You have successful submitted your job application",
                                    // cancellable: false,
                                    delayInMilli: 700,
                                    clickYesText: "Go Back",
                                    cancellable: false,
                                    onClicked: (_) {
                                      Navigator.pop(context, true);
                                    },
                                  );
                                });
                              },
                        child: Center(
                          child: Text(
                            "Save Information".toUpperCase(),
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            minimumSize: Size(300, 50)),
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
          appBarHeader(context)
        ],
      ),
    );
  }

  educationItem(
    String title,
    TextEditingController nameController,
    TextEditingController subjectController,
    int graduateIndex,
    int yearsCompletedIndex,
    onGradChanged,
    onYearsChanged,
  ) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        addSpace(20),
        Text(
          title,
          style: TextStyle(
              fontSize: 16,
              color: black.withOpacity(.5),
              fontWeight: FontWeight.bold),
        ),
        addSpace(10),
        Row(
          children: [
            Expanded(
                child: inputField("Name & School Location", nameController,
                    onChanged: (s) {
              setState(() {});
            })),
            addSpaceWidth(10),
            Expanded(
              child: inputField("Subject Studied", subjectController,
                  onChanged: (s) {
                setState(() {});
              }),
            ),
          ],
        ),
        yesNoField("Did you graduate?", graduateIndex, (p) {
          onGradChanged(p);
        }),
        addSpace(20),
        Text(
          "Years Completed?",
          style: TextStyle(
              fontSize: 14,
              color: black.withOpacity(.5),
              fontWeight: FontWeight.bold),
        ),
        addSpace(10),
        Wrap(
          children: List.generate(4, (p) {
            String title = "${p + 1} Years";
            bool active = p == yearsCompletedIndex;
            return GestureDetector(
              onTap: () {
                onYearsChanged(p);
              },
              child: Container(
                height: 40,
                width: 160,
                margin: EdgeInsets.all(2),
                child: Text(
                  title,
                  style: TextStyle(
                      color: active ? white : black,
                      fontWeight: active ? FontWeight.bold : FontWeight.normal),
                ),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: active ? (black) : white,
                    boxShadow: [
                      BoxShadow(
                          color: black.withOpacity(.05),
                          spreadRadius: 2,
                          blurRadius: 5)
                    ],
                    borderRadius: BorderRadius.circular(8)),
              ),
            );
          }),
        ),
        addSpace(20),
      ],
    );
  }
}
