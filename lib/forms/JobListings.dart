import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class JobListings extends StatefulWidget {
  const JobListings({Key key}) : super(key: key);

  @override
  _JobListingsState createState() => _JobListingsState();
}

class _JobListingsState extends State<JobListings> {
  List<BaseModel> itemList = [];
  bool isBusy = true;

  @override
  initState() {
    super.initState();
    loadItems();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  loadItems() async {
    FirebaseFirestore.instance
        .collection(JOB_BASE)
        // .where(USER_ID, isEqualTo: userModel.getObjectId())
        .get()
        .then((value) {
      for (var doc in value.docs) {
        final bm = BaseModel(doc: doc);
        int p = itemList.indexWhere((e) => e.getObjectId() == bm.getObjectId());
        if (p == -1) {
          itemList.add(bm);
        } else {
          itemList[p] = bm;
        }
      }
      isBusy = false;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          LayoutBuilder(builder: (c, box) {
            final size = box.maxWidth;
            print("size $size");
            bool isLargeScreen = box.maxWidth > 850;
            final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

            return Container(
              margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
              // padding: EdgeInsets.only(
              //     top: 130, bottom: 50, left: size * 0.2, right: size * 0.2),
              padding: EdgeInsets.only(
                left: isLargeScreen ? size * 0.25 : 20,
                right: isLargeScreen ? size * 0.25 : 20,
                top: 120,
                bottom: 20,
              ),
              decoration: BoxDecoration(
                  color: isLargeScreen ? scaffoldColor : white,
                  borderRadius: BorderRadius.circular(10)),
              alignment: Alignment.center,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        border: Border(
                            left: BorderSide(
                                color: Colors.deepOrange, width: 4))),
                    padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
                    child: Text(
                      "New Jobs",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  addSpace(20),
                  Expanded(
                    child: Builder(
                      builder: (c) {
                        if (isBusy)
                          return Center(
                              child: SizedBox(
                                  height: 20,
                                  width: 20,
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation(black),
                                  )));

                        if (itemList.isEmpty)
                          return Center(
                            child: Text("Sorry no open job listings yet.",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  color: black.withOpacity(.5),
                                  fontSize: 14,
                                  // fontWeight: FontWeight.bold
                                )),
                          );

                        return SingleChildScrollView(
                          child: Column(
                            children: List.generate(itemList.length, (p) {
                              final bm = itemList[p];
                              String jobTitle = bm.getString(TITLE);
                              String skill = bm.getString(SKILLS);
                              String jobDescription =
                                  bm.getString("jobDescription");
                              String jobRequirement =
                                  bm.getString("jobRequirement");
                              // double jobSalary = bm.getDouble("jobSalary");
                              String jobSalary = bm.getString("jobSalary");

                              return Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(bottom: 15, top: 0),
                                decoration: BoxDecoration(
                                    color: white,
                                    border: Border.all(
                                        color: black.withOpacity(.01)),
                                    borderRadius: BorderRadius.circular(10)),
                                alignment: Alignment.centerLeft,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        // Icon(Icons.stars_rounded),
                                        // addSpaceWidth(3),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                jobTitle,
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                    color: black,
                                                    fontSize: 20,
                                                    fontWeight:
                                                        FontWeight.w900),
                                              ),
                                              Text(
                                                skill,
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                    color:
                                                        black.withOpacity(.7),
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w700),
                                              ),
                                            ],
                                          ),
                                        ),

                                        TextButton(
                                          onPressed: () {
                                            // Navigator
                                            //     .pushReplacementNamed(
                                            //     context,
                                            //     "/$path??$formId");
                                            String formId = bm.getObjectId();
                                            Navigator.pushReplacementNamed(
                                                context, "/applyJob??$formId");
                                          },
                                          child: Text(
                                            "Apply".toUpperCase(),
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                          style: TextButton.styleFrom(
                                              padding: EdgeInsets.zero,
                                              primary: white,
                                              backgroundColor: green_dark,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              minimumSize: Size(100, 40)),
                                        )
                                      ],
                                    ),
                                    addSpace(5),
                                    Text(
                                      "Job Description",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          color: black,
                                          fontSize: 14,
                                          decoration: TextDecoration.underline,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    addSpace(4),
                                    ReadMoreText(
                                      jobDescription,
                                      textColor: black.withOpacity(.5),
                                      fontSize: 16,
                                      minLength: 200,
                                    ),
                                    addSpace(5),
                                    Text(
                                      "Job Requirements",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          color: black,
                                          fontSize: 14,
                                          decoration: TextDecoration.underline,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    addSpace(4),
                                    ReadMoreText(
                                      jobRequirement,
                                      textColor: black.withOpacity(.5),
                                      fontSize: 16,
                                      minLength: 200,
                                    ),
                                    addSpace(8),
                                    if (false)
                                      Row(
                                        children: [
                                          Icon(
                                            Icons.location_on,
                                            size: 14,
                                          ),
                                          addSpaceWidth(3),
                                          Text(
                                            "${bm.getString(CITY)} in ${bm.getString(STATE)} ",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w900),
                                          ),
                                        ],
                                      ),
                                    addSpace(8),
                                    Row(
                                      children: [
                                        Icon(
                                          Icons.event,
                                          size: 14,
                                          color: Colors.deepOrange,
                                        ),
                                        addSpaceWidth(3),
                                        Text(
                                          "Deadline:  " +
                                              getSimpleDate(
                                                  bm.getInt("jobDate"),
                                                  pattern:
                                                      "EEEE MMMM dd, yyyy"),
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                              color: Colors.deepOrange,
                                              fontSize: 16,
                                              fontWeight: FontWeight.w900),
                                        ),
                                      ],
                                    ),
                                    addSpace(8),
                                    Row(
                                      children: [
                                        Icon(
                                          Icons.account_balance_wallet_outlined,
                                          size: 14,
                                          color: black,
                                        ),
                                        addSpaceWidth(3),
                                        Text(
                                          "Expected Pay Per Hour:  " +
                                              "\$$jobSalary",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                              color: black,
                                              fontSize: 16,
                                              fontWeight: FontWeight.w900),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              );
                            }),
                          ),
                        );
                      },
                    ),
                  )
                ],
              ),
            );
          }),
          appBarHeader(context)
        ],
      ),
    );
  }
}
