import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/main.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class BBpForm extends StatefulWidget {
  const BBpForm({Key key}) : super(key: key);

  @override
  _BBpFormState createState() => _BBpFormState();
}

class _BBpFormState extends State<BBpForm> {
  int vaccinationIndex = -1;
  int proceedIndex = -1;
  int dateOfVaccination = 0;
  final locationController = TextEditingController();
  bool termsAccepted = false;

  List<StreamSubscription> subs = [];

  String path = "bbpForm";
  BaseModel mainModel;
  BaseModel formModel;
  String get formId => routeDataHolder["/bbpForm"];
  String get name => mainModel.getFullName();
  List<BaseModel> formsToFill = [];

  @override
  initState() {
    super.initState();
    loadItems();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadItems() async {
    if (null == formId || formId.isEmpty) {
      showErrorDialog(
          context, "Ops the application link is invalid or has expired!",
          cancellable: false);
      return;
    }

    var sub = FirebaseFirestore.instance
        .collection(EMPLOYEE_BASE)
        .where(OBJECT_ID, isEqualTo: formId)
        .limit(1)
        .snapshots()
        .listen((value) {
      if (value.size == 0) {
        showErrorDialog(
            context, "Ops the application link is invalid or has expired!",
            cancellable: false);
        return;
      }
      mainModel = BaseModel(doc: value.docs[0]);
      vaccinationIndex = mainModel.getInt(VACCINATION_INDEX, true);
      proceedIndex = mainModel.getInt(PROCEED_INDEX, true);
      dateOfVaccination = mainModel.getInt(DATE_OF_VACCINATION);
      locationController.text = mainModel.getString(LOCATION);

      formsToFill = mainModel.getListModel(FORMS_TO_FILL);
      int p = formsToFill.indexWhere((e) => e.getString("path") == path);
      if (p != -1) {
        formModel = formsToFill[p];
        // vaccinationIndex = formModel.getInt(VACCINATION_INDEX, true);
        // proceedIndex = formModel.getInt(PROCEED_INDEX, true);
        // dateOfVaccination = formModel.getInt(DATE_OF_VACCINATION);
        // locationController.text = formModel.getString(LOCATION);
      }
      setState(() {});
    });
    subs.add(sub);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: LayoutBuilder(builder: (c, box) {
              final size = box.maxWidth;
              bool isLargeScreen = box.maxWidth > 850;
              final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

              return Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                padding: EdgeInsets.only(
                  // left: isLargeScreen ? size * 0.25 : 20,
                  // right: isLargeScreen ? size * 0.25 : 20,

                  left: isLargeScreen ? size * 0.15 : 20,
                  right: isLargeScreen ? size * 0.15 : 20,
                  top: 120,
                  bottom: 20,
                ),
                decoration: BoxDecoration(
                    color: isLargeScreen ? scaffoldColor : white,
                    borderRadius: BorderRadius.circular(10)),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: isLargeScreen ? white : null,
                          borderRadius: BorderRadius.circular(10)),
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              style: TextButton.styleFrom(
                                minimumSize: Size(40, 40),
                                backgroundColor: white,
                                primary: black,
                                elevation: 4,
                                // shape: CircleBorder()
                              ),
                              child: Icon(Icons.navigate_before)),
                          addSpace(10),
                          Text(
                            "Blood-borne Pathogens Guideline",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          addSpace(20),
                          Text.rich(
                            TextSpan(children: [
                              TextSpan(
                                text: "PURPOSE:\n",
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text:
                                    "The purpose of the blood borne pathogen surveillance"
                                    " procedure is to protect colleagues who have a "
                                    "potential occupational exposure to human blood borne pathogens (BBP).",
                              ),
                              TextSpan(
                                text: "\n\nSCOPE:\n",
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text:
                                    "Colleagues in the BBP exposure group are identified "
                                    "by Environmental Health and Safety (EHS)."
                                    " Colleagues will be placed in Classifications by EHS as per the OSHA standard."
                                    " Below are the OSHA definitions for classification which are used by EHS:"
                                    "\nClass I: potential exposure is a routine part of the job "
                                    "\nClass II: potential exposure exists, but is not part of routine job"
                                    " duties (maintenance,  First Aid team) "
                                    "\nClass III: no risk of exposure as part of the job duties.",
                              ),
                              TextSpan(
                                text: "\n\nPROCEDURE:\n",
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text:
                                    "1. New hire colleagues or those colleagues "
                                    "who are transferring into areas with potential"
                                    " BBP exposure should present documentation of "
                                    "evidence of Hepatitis B vaccination "
                                    "\n2. Colleagues in the BBP surveillance group receive a "
                                    "declination from (Attachment A) and can decline the Hepatitis B"
                                    " vaccine series. "
                                    "\n3. Colleagues who indicate that they have received the series"
                                    " at another location, must make all attempts to provide "
                                    "Exemptional Medical Staffing with evidence of vaccination.",
                              ),
                            ]),
                            style: TextStyle(
                              fontSize: 14,
                            ),
                          ),
                          addLine(20, black.withOpacity(.03), 0, 20, 0, 20),
                          Text(
                            "Exceptional Medical Staffing Inc Hepatitis B Vaccine Status and/or Request for Vaccination form",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          addSpace(20),
                          Text(
                            "I understand that due to my job and potential "
                            "of occupational exposure to blood or other "
                            "potentially infectious materials, "
                            "I may be at risk of acquiring Hepatitis B"
                            " virus (HBV) infection. Please check the appropriate statement:",
                            style: TextStyle(
                              fontSize: 12,
                              // fontWeight: FontWeight.bold
                            ),
                          ),
                          addSpace(10),
                          yesNoField(
                              "I received the vaccination from an outside source.",
                              vaccinationIndex, (p) {
                            vaccinationIndex = p;
                            setState(() {});
                          }),
                          if (vaccinationIndex == 0)
                            Row(
                              children: [
                                Expanded(
                                  child: clickField(
                                      "Date Of Vaccination",
                                      dateOfVaccination == 0
                                          ? ""
                                          : getSimpleDate(dateOfVaccination,
                                              pattern: "EEE MMMM dd, yyyy"),
                                      () async {
                                    final DateTime picked =
                                        await showDatePicker(
                                            builder: (ctx, child) {
                                              return Theme(
                                                  data: Theme.of(context)
                                                      .copyWith(
                                                    colorScheme:
                                                        ColorScheme.light(
                                                      primary: appColor,
                                                      onPrimary:
                                                          black, // header text color
                                                      // onSurface: Colors.green, // body text color
                                                    ),
                                                    buttonTheme:
                                                        ButtonThemeData(
                                                            textTheme:
                                                                ButtonTextTheme
                                                                    .primary),
                                                  ),
                                                  child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      child: child));
                                            },
                                            context: context,
                                            initialDate: dateOfVaccination == 0
                                                ? DateTime.now()
                                                : DateTime
                                                    .fromMillisecondsSinceEpoch(
                                                        dateOfVaccination),
                                            firstDate: DateTime.now(),
                                            lastDate: DateTime(
                                                DateTime.now().year + 2));

                                    if (picked != null &&
                                        picked.millisecondsSinceEpoch !=
                                            dateOfVaccination)
                                      dateOfVaccination =
                                          picked.millisecondsSinceEpoch;
                                    setState(() {});
                                  }),
                                ),
                                addSpaceWidth(10),
                                Expanded(
                                    child: inputField("Location of Vaccination",
                                        locationController, onChanged: (s) {
                                  setState(() {});
                                })),
                              ],
                            ),
                          Text(
                            "I wish to move forward with employment and decline the Hepatitis "
                            " B vaccination at this time. I have carefully read"
                            " and fully understand the risk and agree to indemnify,"
                            " defend, and hold harmless Exceptional Medical Staffing Inc."
                            " ,its directors, officers, employees, and agents,  "
                            "from and against any and all claims, demands, suits, "
                            "losses, damages, costs, and expenses (including without "
                            "limitation reasonable attorney’s fees) which may arise "
                            "or result from me having occupational exposure to "
                            "blood or other potentially infectious materials.",
                            style: TextStyle(
                              fontSize: 12,
                              // fontWeight: FontWeight.bold
                            ),
                          ),
                          addSpace(10),
                          yesNoField(
                              "Do you still want to proceed?", proceedIndex,
                              (p) {
                            proceedIndex = p;
                            setState(() {});
                          }),
                          addSpace(50),
                        ],
                      ),
                    ),
                    addSpace(20),
                    InkWell(
                      onTap: () {
                        termsAccepted = !termsAccepted;
                        setState(() {});
                      },
                      child: Container(
                        child: Row(
                          children: [
                            checkBox(termsAccepted),
                            addSpaceWidth(20),
                            Expanded(
                                child: Text(
                              "By clicking on  SAVE INFORMATION below, you understand and accept"
                              " the terms of this offer presented above. "
                              "We look forward to you joining our team and making "
                              "a positive contribution to the company.",
                              style: TextStyle(fontSize: 12),
                            ))
                          ],
                        ),
                      ),
                    ),
                    addSpace(20),
                    Container(
                      height: 60,
                      child: TextButton(
                        onPressed: !termsAccepted
                            ? null
                            : () {
                                String location = locationController.text;
                                int status = STATUS_COMPLETED;

                                if (vaccinationIndex == -1 &&
                                    proceedIndex == -1 &&
                                    dateOfVaccination == 0 &&
                                    location.isEmpty) {
                                  status = STATUS_UNDONE;
                                }

                                if (vaccinationIndex == -1 ||
                                    proceedIndex == -1 ||
                                    dateOfVaccination == 0 ||
                                    location.isEmpty) {
                                  status = STATUS_IN_PROGRESS;
                                }

                                mainModel.put(
                                    VACCINATION_INDEX, vaccinationIndex);
                                mainModel.put(PROCEED_INDEX, proceedIndex);
                                mainModel.put(
                                    DATE_OF_VACCINATION, dateOfVaccination);
                                mainModel.put(LOCATION, location);

                                // formModel.put(
                                //     VACCINATION_INDEX, vaccinationIndex);
                                // formModel.put(PROCEED_INDEX, proceedIndex);
                                // formModel.put(
                                //     DATE_OF_VACCINATION, dateOfVaccination);
                                // formModel.put(LOCATION, location);
                                formModel.put(STATUS, status);

                                int p = formsToFill.indexWhere(
                                    (e) => e.getString("path") == path);
                                if (p != -1) {
                                  formsToFill[p] = formModel;
                                }

                                mainModel.put(FORMS_TO_FILL,
                                    formsToFill.map((e) => e.items).toList());
                                mainModel.updateItems();

                                showMessage(
                                  context,
                                  Icons.check,
                                  green,
                                  "Form Submitted!",
                                  "You have successful completed your blood-borne pathogens guidelines form",
                                  // cancellable: false,
                                  delayInMilli: 700,
                                  clickYesText: "Go Back",
                                  cancellable: false,
                                  onClicked: (_) {
                                    // Navigator.pop(context, true);
                                    Navigator.pushReplacementNamed(
                                        context, "/applicationForm??$formId");
                                  },
                                );
                              },
                        child: Center(
                          child: Text(
                            "Save Information".toUpperCase(),
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            minimumSize: Size(300, 50)),
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
          appBarHeader(context)
        ],
      ),
    );
  }

  educationItem(
    String title,
    TextEditingController nameController,
    TextEditingController subjectController,
    int graduateIndex,
    int yearsCompletedIndex,
    onGradChanged,
    onYearsChanged,
  ) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        addSpace(20),
        Text(
          title,
          style: TextStyle(
              fontSize: 16,
              color: black.withOpacity(.5),
              fontWeight: FontWeight.bold),
        ),
        addSpace(10),
        Row(
          children: [
            Expanded(
                child: inputField("Name & School Location", nameController,
                    onChanged: (s) {
              setState(() {});
            })),
            addSpaceWidth(10),
            Expanded(
              child: inputField("Subject Studied", subjectController,
                  onChanged: (s) {
                setState(() {});
              }),
            ),
          ],
        ),
        yesNoField("Did you graduate?", graduateIndex, (p) {
          onGradChanged(p);
        }),
        addSpace(20),
        Text(
          "Years Completed?",
          style: TextStyle(
              fontSize: 14,
              color: black.withOpacity(.5),
              fontWeight: FontWeight.bold),
        ),
        addSpace(10),
        Wrap(
          children: List.generate(4, (p) {
            String title = "${p + 1} Years";
            bool active = p == yearsCompletedIndex;
            return GestureDetector(
              onTap: () {
                onYearsChanged(p);
              },
              child: Container(
                height: 40,
                width: 160,
                margin: EdgeInsets.all(2),
                child: Text(
                  title,
                  style: TextStyle(
                      color: active ? white : black,
                      fontWeight: active ? FontWeight.bold : FontWeight.normal),
                ),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: active ? (black) : white,
                    boxShadow: [
                      BoxShadow(
                          color: black.withOpacity(.05),
                          spreadRadius: 2,
                          blurRadius: 5)
                    ],
                    borderRadius: BorderRadius.circular(8)),
              ),
            );
          }),
        ),
        addSpace(20),
      ],
    );
  }
}

showError(context, String err) {
  EdgeAlert.show(context,
      description: err,
      gravity: EdgeAlert.TOP,
      backgroundColor: red0,
      icon: Icons.info_outline);
}
