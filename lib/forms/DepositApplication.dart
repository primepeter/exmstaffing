import 'dart:async';
import 'dart:typed_data';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/main.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'DocUploads.dart';

class Bank {
  final nameController = TextEditingController();
  final routingNoController = TextEditingController();
  int accountType = -1;
  final percentageController = TextEditingController();
  final accountNoController = TextEditingController();

  String bankSlip = "";
  String bankSlipUrl = "";
  String fileExt = "";
  bool uploading = false;
  Uint8List bankByte = Uint8List(0);

  Bank({Map items}) {
    if (null != items) {
      nameController.text = items["accountName"];
      routingNoController.text = items["routingNumber"];
      percentageController.text = items["percentage"];
      accountNoController.text = items["accountNumber"];
      accountType = items["accountType"];
      bankSlipUrl = items["bankSlipUrl"];
      fileExt = items["fileExt"];
    }
  }

  bool get isValidated {
    bool validated = true;
    if (nameController.text.isEmpty) return false;
    if (routingNoController.text.isEmpty) return false;
    if (percentageController.text.isEmpty) return false;
    if (accountNoController.text.isEmpty) return false;
    if (accountType == -1) return false;
    if (bankSlipUrl.isEmpty) return false;
    return validated;
  }

  Map get items {
    return {
      "accountName": nameController.text,
      "routingNumber": routingNoController.text,
      "percentage": percentageController.text,
      "accountNumber": accountNoController.text,
      "accountType": accountType,
      "bankSlipUrl": bankSlipUrl,
      "fileExt": fileExt,
    };
  }
}

class DepositApplication extends StatefulWidget {
  const DepositApplication({Key key}) : super(key: key);

  @override
  _DepositApplicationState createState() => _DepositApplicationState();
}

class _DepositApplicationState extends State<DepositApplication> {
  List<Bank> bankingInfo = [];
  List accountType = ["Checking", "Savings"];

  final nameController = TextEditingController();
  final ssNController = TextEditingController();

  bool termsAccepted = false;

  List<StreamSubscription> subs = [];

  String path = "depositApp";
  BaseModel mainModel;
  BaseModel formModel;
  String get formId => routeDataHolder["/depositApp"];
  String get name => mainModel.getFullName();
  List<BaseModel> formsToFill = [];

  @override
  initState() {
    super.initState();
    loadSettings();
    loadItems();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadSettings() async {
    var settingsSub = FirebaseFirestore.instance
        .collection(APP_SETTINGS_BASE)
        .doc(APP_SETTINGS)
        .snapshots()
        .listen((shot) {
      if (shot != null) {
        appSettingsModel = BaseModel(doc: shot);
        List banned = appSettingsModel.getList(BANNED);
      }
    });
    subs.add(settingsSub);
  }

  loadItems() async {
    if (null == formId || formId.isEmpty) {
      showErrorDialog(
          context, "Ops the application link is invalid or has expired!",
          cancellable: false);
      return;
    }

    var sub = FirebaseFirestore.instance
        .collection(EMPLOYEE_BASE)
        .where(OBJECT_ID, isEqualTo: formId)
        .limit(1)
        .snapshots()
        .listen((value) {
      if (value.size == 0) {
        showErrorDialog(
            context, "Ops the application link is invalid or has expired!",
            cancellable: false);
        return;
      }
      mainModel = BaseModel(doc: value.docs[0]);
      bankingInfo = mainModel
          .getListModel(BANKING_INFO)
          .map((e) => Bank(items: e.items))
          .toList();
      nameController.text = mainModel.getString(NAME);
      ssNController.text = mainModel.getString(SOCIAL_SECURITY_NUMBER);

      formsToFill = mainModel.getListModel(FORMS_TO_FILL);
      int p = formsToFill.indexWhere((e) => e.getString("path") == path);
      if (p != -1) {
        formModel = formsToFill[p];

        // bankingInfo = formModel
        //     .getListModel(BANKING_INFO)
        //     .map((e) => Bank(items: e.items))
        //     .toList();
        // nameController.text = formModel.getString(NAME);
        // ssNController.text = formModel.getString(SOCIAL_SECURITY_NUMBER);
      }
      setState(() {});
    });
    subs.add(sub);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: LayoutBuilder(builder: (c, box) {
              final size = box.maxWidth;
              print("size $size");
              bool isLargeScreen = box.maxWidth > 850;
              final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

              return Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                // padding: EdgeInsets.only(
                //     top: 130, bottom: 50, left: size * 0.2, right: size * 0.2),
                padding: EdgeInsets.only(
                  left: isLargeScreen ? size * 0.15 : 20,
                  right: isLargeScreen ? size * 0.15 : 20,
                  top: 120,
                  bottom: 20,
                ),
                decoration: BoxDecoration(
                    color: isLargeScreen ? scaffoldColor : white,
                    borderRadius: BorderRadius.circular(10)),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: isLargeScreen ? white : null,
                          borderRadius: BorderRadius.circular(10)),
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Direct Deposit Employee Authorization Form",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          addSpace(20),
                          Row(
                            children: [
                              Expanded(
                                child: clickField("Employee Name", name, () {}),
                              ),
                              // Expanded(
                              //   child:
                              //       inputField("Employee Name", nameController,
                              //           onChanged: (s) {
                              //     setState(() {});
                              //   }),
                              // ),
                              addSpaceWidth(10),
                              Expanded(
                                child: inputField(
                                    "Social Security Number", ssNController,
                                    onChanged: (s) {
                                  setState(() {});
                                }),
                              )
                            ],
                          ),
                          addSpace(10),
                          Text(
                            "I hereby authorize Time & Payroll and the financial institution(s)"
                            " listed below to initiate credit entries or adjusting entries "
                            "(either credit or debit, which are necessary for corrections)"
                            " to the indicated account(s) below. "
                            "\n\nI understand that I will not write checks or otherwise debit "
                            "my account before first making certain that sufficient funds are available, "
                            "and that neither my employer nor Time & Payroll shall be liable"
                            " for such overdrafts due to delay of funds posting.",
                            style: TextStyle(fontSize: 12),
                          ),
                          addLine(20, black.withOpacity(.03), 0, 20, 0, 20),
                          Row(
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Banking Information",
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      "Add bank account payout information",
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: black.withOpacity(.5),
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                              if (bankingInfo.length < 3)
                                TextButton(
                                  onPressed: () {
                                    if (bankingInfo.isNotEmpty &&
                                        !bankingInfo[bankingInfo.length - 1]
                                            .isValidated) {
                                      showError(context,
                                          "Some fields on the current bank item are empty!");
                                      return;
                                    }

                                    bankingInfo.add(Bank());
                                    setState(() {});
                                  },
                                  child: Text(
                                    bankingInfo.isNotEmpty
                                        ? "Add Another"
                                        : "Add Bank",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  style: TextButton.styleFrom(
                                      padding: EdgeInsets.zero,
                                      primary: white,
                                      backgroundColor: black,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      minimumSize: Size(160, 50)),
                                )
                            ],
                          ),
                          addSpace(20),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            child: Builder(builder: (ctx) {
                              if (bankingInfo.isEmpty)
                                return Container(
                                  padding: EdgeInsets.all(0),
                                  child: DottedBorder(
                                    color: black.withOpacity(.5),
                                    strokeWidth: 1,
                                    borderType: BorderType.RRect,
                                    radius: Radius.circular(10),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Container(
                                          height: 150,
                                          alignment: Alignment.center,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Icon(
                                                Icons.account_balance,
                                                color: black.withOpacity(.5),
                                                size: 30,
                                              ),
                                              addSpace(10),
                                              Text(
                                                "You have not added any bank information yet",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: black.withOpacity(.5)
                                                    // fontWeight: FontWeight.bold,
                                                    ),
                                              )
                                            ],
                                          )),
                                    ),
                                  ),
                                );

                              return Column(
                                children:
                                    List.generate(bankingInfo.length, (index) {
                                  final fm = bankingInfo[index];
                                  final byteData = fm.bankByte;
                                  final fileUrl = fm.bankSlipUrl;
                                  final fileExt = fm.fileExt;
                                  bool uploading = fm.uploading;
                                  bool uploaded = fileUrl.isNotEmpty;
                                  String string = "Document Uploaded";
                                  if (byteData.isEmpty && fileUrl.isEmpty)
                                    string = "No Document added";
                                  if (byteData.isNotEmpty && fileUrl.isEmpty)
                                    string = "Document added";

                                  return Container(
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                children: [
                                                  Expanded(
                                                      child: inputField(
                                                          "Bank Name",
                                                          fm.nameController,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                      child: inputField(
                                                          "Routing Number",
                                                          fm
                                                              .routingNoController,
                                                          isNum: true,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                      child: inputField(
                                                          "Percentage of Pay",
                                                          fm
                                                              .percentageController,
                                                          isNum: true,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                      child: inputField(
                                                          "Account Number",
                                                          fm
                                                              .accountNoController,
                                                          isNum: true,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                ],
                                              ),
                                              Text(
                                                "Account Type",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              addSpace(10),
                                              Wrap(
                                                children: List.generate(2, (p) {
                                                  String title = p == 0
                                                      ? "Checking"
                                                      : "Savings";
                                                  bool active =
                                                      fm.accountType == p;
                                                  return GestureDetector(
                                                    onTap: () {
                                                      fm.accountType = p;
                                                      setState(() {});
                                                    },
                                                    child: Container(
                                                      height: 40,
                                                      width: 150,
                                                      margin: EdgeInsets.all(2),
                                                      child: Text(
                                                        title,
                                                        style: TextStyle(
                                                            color: active
                                                                ? white
                                                                : black,
                                                            fontWeight: active
                                                                ? FontWeight
                                                                    .bold
                                                                : FontWeight
                                                                    .normal),
                                                      ),
                                                      alignment:
                                                          Alignment.center,
                                                      decoration: BoxDecoration(
                                                          color: active
                                                              ? (green)
                                                              : white,
                                                          boxShadow: [
                                                            BoxShadow(
                                                                color: black
                                                                    .withOpacity(
                                                                        .05),
                                                                spreadRadius: 2,
                                                                blurRadius: 5)
                                                          ],
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(8)),
                                                    ),
                                                  );
                                                }),
                                              ),
                                              addSpace(10),
                                              GestureDetector(
                                                onTap: () async {
                                                  final result =
                                                      await pickByte();
                                                  if (result == null) return {};
                                                  fm.bankByte = result[0];
                                                  fm.fileExt = result[1];
                                                  fm.uploading = false;
                                                  fm.bankSlipUrl = "";
                                                  setState(() {});
                                                },
                                                child: Container(
                                                  padding: EdgeInsets.all(0),
                                                  child: DottedBorder(
                                                    color:
                                                        black.withOpacity(.5),
                                                    strokeWidth: 1,
                                                    borderType:
                                                        BorderType.RRect,
                                                    radius: Radius.circular(10),
                                                    child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      child: Container(
                                                          height: 100,
                                                          // width: 60,
                                                          color: colorString(
                                                              string),
                                                          alignment:
                                                              Alignment.center,
                                                          child: Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Icon(
                                                                Icons
                                                                    .file_present,
                                                                color: string ==
                                                                        "No Document added"
                                                                    ? black
                                                                        .withOpacity(
                                                                            .5)
                                                                    : white,
                                                                size: 20,
                                                              ),
                                                              addSpace(10),
                                                              Text(
                                                                string,
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        10,
                                                                    color: string ==
                                                                            "No Document added"
                                                                        ? black
                                                                            .withOpacity(.5)
                                                                        : white

                                                                    // fontWeight: FontWeight.bold,
                                                                    ),
                                                              )
                                                            ],
                                                          )),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        // addSpaceWidth(10),
                                        Container(
                                          margin: EdgeInsets.only(
                                              left: 10, bottom: 0),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              if (byteData.isNotEmpty)
                                                TextButton(
                                                  onPressed: () async {
                                                    if (uploading) return;
                                                    if (uploaded) return;
                                                    setState(() {
                                                      fm.uploading = true;
                                                    });

                                                    try {
                                                      final url =
                                                          await uploadByte(
                                                              byteData,
                                                              fileExt);
                                                      fm.uploading = false;
                                                      fm.bankSlipUrl = url;
                                                      setState(() {});
                                                    } catch (e) {
                                                      setState(() {
                                                        fm.uploading = false;
                                                      });
                                                    }
                                                  },
                                                  child: Row(
                                                    children: [
                                                      Text(
                                                        uploaded
                                                            ? "Uploaded"
                                                            : "Upload",
                                                        style: TextStyle(
                                                            fontSize: 12,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                      if (uploading)
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 5),
                                                          alignment:
                                                              Alignment.center,
                                                          child: SizedBox(
                                                            height: 15,
                                                            width: 15,
                                                            child:
                                                                CircularProgressIndicator(
                                                              strokeWidth: 2,
                                                            ),
                                                          ),
                                                        ),
                                                    ],
                                                  ),
                                                  style: TextButton.styleFrom(
                                                      padding: EdgeInsets.zero,
                                                      primary: white,
                                                      backgroundColor: black
                                                          .withOpacity(uploading
                                                              ? 0.6
                                                              : 1),
                                                      shape:
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          8)),
                                                      minimumSize:
                                                          Size(100, 50)),
                                                ),
                                              if (byteData.isNotEmpty)
                                                addSpace(10),
                                              TextButton(
                                                onPressed: () {
                                                  bankingInfo.removeAt(index);
                                                  setState(() {});
                                                },
                                                child: Text(
                                                  "Remove",
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                style: TextButton.styleFrom(
                                                    padding: EdgeInsets.zero,
                                                    primary: white,
                                                    backgroundColor: red,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        8)),
                                                    minimumSize: Size(100, 50)),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  );
                                }),
                              );
                            }),
                          ),
                          addSpace(50),
                        ],
                      ),
                    ),
                    addSpace(20),
                    InkWell(
                      onTap: () {
                        termsAccepted = !termsAccepted;
                        setState(() {});
                      },
                      child: Container(
                        child: Row(
                          children: [
                            checkBox(termsAccepted),
                            addSpaceWidth(20),
                            Expanded(
                                child: Text(
                              "By my signature below, I authorize Exceptional Medical Staffing Inc. (“EXM”) to contact "
                              "any current and prior employers, educational institutions, licensing boards and personal "
                              "references referenced in my resume or other materials provided by me to EXM. I authorize "
                              "my current and prior employers, educational institutions, licensing board and personal "
                              "references to supply any information concerning my background, character or qualifications "
                              "for employment with EXM. I release any current or prior employer, educational institution, "
                              "licensing board and individual who provides such information and I release EXM and any "
                              "related subsidiaries, partnerships or affiliated companies and their owners, employees and "
                              "agents from any and all liability and responsibility for damages or claims of any kind for "
                              "revealing or receiving information concerning my background, character or qualifications for "
                              "employment with EXM.",
                              style: TextStyle(fontSize: 12),
                            ))
                          ],
                        ),
                      ),
                    ),
                    addSpace(20),
                    Container(
                      height: 60,
                      child: TextButton(
                        onPressed: !termsAccepted
                            ? null
                            : () {
                                // String name = nameController.text;
                                String ssn = ssNController.text;

                                int status = STATUS_COMPLETED;

                                if (bankingInfo.isEmpty &&
                                    // name.isEmpty &&
                                    ssn.isEmpty) {
                                  status = STATUS_UNDONE;
                                }

                                if (bankingInfo.isEmpty ||
                                    // name.isEmpty ||
                                    ssn.isEmpty) {
                                  status = STATUS_IN_PROGRESS;
                                }

                                // mainModel.put(NAME, name);
                                mainModel.put(SOCIAL_SECURITY_NUMBER, ssn);
                                mainModel.put(BANKING_INFO,
                                    bankingInfo.map((e) => e.items).toList());

                                // formModel.put(NAME, name);
                                // formModel.put(SOCIAL_SECURITY_NUMBER, ssn);
                                // formModel.put(BANKING_INFO,
                                //     bankingInfo.map((e) => e.items).toList());

                                formModel.put(STATUS, status);

                                int p = formsToFill.indexWhere(
                                    (e) => e.getString("path") == path);
                                if (p != -1) {
                                  formsToFill[p] = formModel;
                                }

                                mainModel.put(FORMS_TO_FILL,
                                    formsToFill.map((e) => e.items).toList());
                                mainModel.updateItems();

                                showMessage(
                                  context,
                                  Icons.check,
                                  green,
                                  "Form Submitted!",
                                  "You have successful completed your deposit application form",
                                  // cancellable: false,
                                  delayInMilli: 700,
                                  clickYesText: "Go Back",
                                  cancellable: false,
                                  onClicked: (_) {
                                    // Navigator.pop(context, true);
                                    Navigator.pushReplacementNamed(
                                        context, "/applicationForm??$formId");
                                  },
                                );
                              },
                        child: Center(
                          child: Text(
                            "Save Information".toUpperCase(),
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            minimumSize: Size(300, 50)),
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
          appBarHeader(context)
        ],
      ),
    );
  }

  // Color colorString(String string) {
  //   if (string == "Document added") return orange0;
  //   if (string == "No Document added") return transparent;
  //   return green_dark;
  // }
}
