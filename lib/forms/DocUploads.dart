import 'dart:async';
import 'dart:typed_data';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/main.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/link.dart';

class DocUploads extends StatefulWidget {
  const DocUploads({Key key}) : super(key: key);

  @override
  _DocUploadsState createState() => _DocUploadsState();
}

class _DocUploadsState extends State<DocUploads> {
  List uploadItems = [];

  Uint8List driversLicenceByte;
  Uint8List ssnByte;
  Uint8List blsByte;
  Uint8List tbTestByte;
  Uint8List stateLicenceByte;
  Uint8List covidByte;
  Uint8List residentByte;

  String driversLicenceExt;
  String ssnExt;
  String blsExt;
  String tbTestExt;
  String stateLicenceExt;
  String covidExt;
  String residentExt;

  int driversLicenceExpiry = 0;
  int ssnCardExpiry = 0;
  int blsExpiry = 0;
  int tbExpiry = 0;
  int stateLicenceExpiry = 0;
  int covidExpiry = 0;
  int residentExpiry = 0;

  String driversLicenceUrl = "";
  String ssnCardUrl = "";
  String blsUrl = "";
  String tbUrl = "";
  String stateLicenceUrl = "";
  String covidUrl = "";
  String residentUrl = "";

  bool termsAccepted = false;

  List<StreamSubscription> subs = [];

  String path = "docUploads";
  BaseModel mainModel;
  BaseModel formModel;

  String get formId => routeDataHolder["/docUploads"];

  String get name => mainModel.getString(FIRST_NAME);
  List<BaseModel> formsToFill = [];

  @override
  initState() {
    super.initState();
    loadSettings();
    loadItems();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadSettings() async {
    var settingsSub = FirebaseFirestore.instance
        .collection(APP_SETTINGS_BASE)
        .doc(APP_SETTINGS)
        .snapshots()
        .listen((shot) {
      if (shot != null) {
        appSettingsModel = BaseModel(doc: shot);
        setState(() {});
      }
    });
    subs.add(settingsSub);
  }

  loadItems() async {
    if (null == formId || formId.isEmpty) {
      showErrorDialog(
          context, "Ops the application link is invalid or has expired!",
          cancellable: false);
      return;
    }

    var sub = FirebaseFirestore.instance
        .collection(EMPLOYEE_BASE)
        .where(OBJECT_ID, isEqualTo: formId)
        .limit(1)
        .snapshots()
        .listen((value) {
      if (value.size == 0) {
        showErrorDialog(
            context, "Ops the application link is invalid or has expired!",
            cancellable: false);
        return;
      }
      mainModel = BaseModel(doc: value.docs[0]);

      driversLicenceUrl = mainModel.getString(DRIVERS_LICENCE_URL);
      ssnCardUrl = mainModel.getString(SSN_CARD_URL);
      blsUrl = mainModel.getString(BLS_URL);
      tbUrl = mainModel.getString(TB_URL);
      stateLicenceUrl = mainModel.getString(STATE_LICENCE_URL);
      covidUrl = mainModel.getString(COVID_URL);
      residentUrl = mainModel.getString(RESIDENT_URL);

      driversLicenceExpiry = mainModel.getInt(DRIVERS_LICENCE_EXPIRY);
      ssnCardExpiry = mainModel.getInt(SSN_CARD_EXPIRY);
      blsExpiry = mainModel.getInt(BLS_EXPIRY);
      tbExpiry = mainModel.getInt(TB_EXPIRY);
      stateLicenceExpiry = mainModel.getInt(STATE_LICENCE_EXPIRY);
      covidExpiry = mainModel.getInt(COVID_EXPIRY);
      residentExpiry = mainModel.getInt(RESIDENT_EXPIRY);

      formsToFill = mainModel.getListModel(FORMS_TO_FILL);
      int p = formsToFill.indexWhere((e) => e.getString("path") == path);
      if (p != -1) {
        formModel = formsToFill[p];
      }
      setState(() {});
    });
    subs.add(sub);
  }

  bool hasSkill(List keys) {
    bool value = false;
    if (null == mainModel) return false;
    for (var key in keys) {
      if (mainModel
          .getList(SKILLS)
          .map((e) => e.toString().toUpperCase())
          .toList()
          .contains(key.toString().toUpperCase())) {
        value = true;
        break;
      }
    }
    return value;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: LayoutBuilder(builder: (c, box) {
              final size = box.maxWidth;
              bool isLargeScreen = box.maxWidth > 850;
              final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

              return Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                padding: EdgeInsets.only(
                  left: isLargeScreen ? size * 0.15 : 10,
                  right: isLargeScreen ? size * 0.15 : 10,
                  top: 120,
                  bottom: 20,
                ),
                decoration: BoxDecoration(
                    color: isLargeScreen ? scaffoldColor : white,
                    borderRadius: BorderRadius.circular(10)),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: isLargeScreen ? white : null,
                          borderRadius: BorderRadius.circular(10)),
                      padding: EdgeInsets.all(isLargeScreen ? 20 : 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.pushReplacementNamed(
                                        context, "/applicationForm??$formId");
                                  },
                                  style: TextButton.styleFrom(
                                    minimumSize: Size(40, 40),
                                    backgroundColor: white,
                                    primary: black,
                                    elevation: 4,
                                    // shape: CircleBorder()
                                  ),
                                  child: Icon(Icons.navigate_before)),
                              addSpaceWidth(10),
                              Text(
                                "Required Documents",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          addSpace(20),
                          uploadItem(
                              byteData: driversLicenceByte,
                              fileUrl: driversLicenceUrl,
                              expiry: driversLicenceExpiry,
                              title: "Driver’s License",
                              uploading: uploadItems.contains(0),
                              onChange: (p) {
                                driversLicenceExpiry = p;
                                setState(() {});
                              },
                              onUpload: () async {
                                if (driversLicenceExpiry == 0) {
                                  showError(
                                      context, "Enter expiry date of doc.");
                                  return;
                                }

                                uploadItems.add(0);
                                setState(() {});

                                driversLicenceUrl = await uploadByte(
                                    driversLicenceByte, driversLicenceExt);
                                uploadItems.remove(0);
                                setState(() {});

                                mainModel.put(
                                    DRIVERS_LICENCE_URL, driversLicenceUrl);
                                mainModel.put(
                                    DRIVERS_LICENCE_EXT, driversLicenceExt);
                                mainModel.put(DRIVERS_LICENCE_EXPIRY,
                                    driversLicenceExpiry);
                                mainModel.updateItems();
                                setState(() {});
                              },
                              onAdd: () async {
                                final result = await pickByte();
                                if (result == null) return {};
                                driversLicenceByte = result[0];
                                driversLicenceExt = result[1];
                                setState(() {});
                              }),
                          uploadItem(
                              hasExpiry: false,
                              byteData: ssnByte,
                              fileUrl: ssnCardUrl,
                              expiry: ssnCardExpiry,
                              title: "SSN Card/ Birth Certificate",
                              uploading: uploadItems.contains(1),
                              onChange: (p) {
                                ssnCardExpiry = p;
                                setState(() {});
                              },
                              onUpload: () async {
                                // if (ssnCardExpiry == 0) {
                                //   showError(
                                //       context, "Enter expiry date of doc.");
                                //   return;
                                // }

                                uploadItems.add(1);
                                setState(() {});

                                ssnCardUrl = await uploadByte(ssnByte, ssnExt);
                                uploadItems.remove(1);
                                setState(() {});

                                mainModel.put(SSN_CARD_URL, ssnCardUrl);
                                mainModel.put(SSN_CARD_EXPIRY, ssnCardExpiry);
                                mainModel.put(SSN_CARD_EXT, ssnExt);

                                mainModel.updateItems();
                                setState(() {});
                              },
                              onAdd: () async {
                                final result = await pickByte();
                                if (result == null) return {};
                                ssnByte = result[0];
                                ssnExt = result[1];
                                setState(() {});
                              }),
                          if (hasSkill([
                            "Certified Nursing Assistant (CNA)",
                            "Caregivers",
                            "MedTechs",
                            "Licensed Practical Nurse (LPN)",
                            "Registered Nurse (RN)",
                          ]))
                            uploadItem(
                                byteData: blsByte,
                                fileUrl: blsUrl,
                                expiry: blsExpiry,
                                title: "BLS Card (First Aide/CPR)",
                                uploading: uploadItems.contains(2),
                                onChange: (p) {
                                  blsExpiry = p;
                                  setState(() {});
                                },
                                onUpload: () async {
                                  if (blsExpiry == 0) {
                                    showError(
                                        context, "Enter expiry date of doc.");
                                    return;
                                  }

                                  uploadItems.add(1);
                                  setState(() {});

                                  blsUrl = await uploadByte(blsByte, blsExt);
                                  uploadItems.remove(0);
                                  setState(() {});

                                  mainModel.put(BLS_URL, blsUrl);
                                  mainModel.put(BLS_EXPIRY, blsExpiry);
                                  mainModel.put(BLS_EXT, blsExt);
                                  mainModel.updateItems();
                                  setState(() {});
                                },
                                onAdd: () async {
                                  final result = await pickByte();
                                  if (result == null) return {};
                                  blsByte = result[0];
                                  blsExt = result[1];
                                  setState(() {});
                                }),
                          if (hasSkill([
                            "Certified Nursing Assistant (CNA)",
                            "Caregivers",
                            "MedTechs",
                            "Licensed Practical Nurse (LPN)",
                            "Registered Nurse (RN)",
                          ]))
                            uploadItem(
                                byteData: tbTestByte,
                                fileUrl: tbUrl,
                                expiry: tbExpiry,
                                title: "TB Negative Skin Test Result",
                                uploading: uploadItems.contains(2),
                                onChange: (p) {
                                  tbExpiry = p;
                                  setState(() {});
                                },
                                onUpload: () async {
                                  if (tbExpiry == 0) {
                                    showError(
                                        context, "Enter expiry date of doc.");
                                    return;
                                  }

                                  uploadItems.add(1);
                                  setState(() {});

                                  tbUrl =
                                      await uploadByte(tbTestByte, tbTestExt);
                                  uploadItems.remove(0);
                                  setState(() {});

                                  mainModel.put(TB_URL, tbUrl);
                                  mainModel.put(TB_EXPIRY, tbExpiry);
                                  mainModel.put(TB_EXT, tbTestExt);
                                  mainModel.updateItems();
                                  setState(() {});
                                },
                                onAdd: () async {
                                  final result = await pickByte();
                                  if (result == null) return {};
                                  tbTestByte = result[0];
                                  tbTestExt = result[1];
                                  setState(() {});
                                }),
                          if (hasSkill([
                            "Certified Nursing Assistant (CNA)",
                            // "Caregivers",
                            "MedTechs",
                            "Licensed Practical Nurse (LPN)",
                            "Registered Nurse (RN)",
                          ]))
                            uploadItem(
                                byteData: stateLicenceByte,
                                fileUrl: stateLicenceUrl,
                                expiry: stateLicenceExpiry,
                                title: "Current State License (CNA/LPN/RN)",
                                uploading: uploadItems.contains(3),
                                onChange: (p) {
                                  stateLicenceExpiry = p;
                                  setState(() {});
                                },
                                onUpload: () async {
                                  if (stateLicenceExpiry == 0) {
                                    showError(
                                        context, "Enter expiry date of doc.");
                                    return;
                                  }

                                  uploadItems.add(3);
                                  setState(() {});

                                  stateLicenceUrl = await uploadByte(
                                      stateLicenceByte, stateLicenceExt);
                                  uploadItems.remove(3);
                                  setState(() {});

                                  mainModel.put(
                                      STATE_LICENCE_URL, stateLicenceUrl);
                                  mainModel.put(
                                      STATE_LICENCE_EXPIRY, stateLicenceExpiry);
                                  mainModel.put(
                                      STATE_LICENCE_EXT, stateLicenceExt);

                                  mainModel.updateItems();
                                  setState(() {});
                                },
                                onAdd: () async {
                                  final result = await pickByte();
                                  if (result == null) return {};
                                  stateLicenceByte = result[0];
                                  stateLicenceExt = result[1];
                                  setState(() {});
                                }),
                          uploadItem(
                              hasExpiry: false,
                              byteData: covidByte,
                              fileUrl: covidUrl,
                              expiry: covidExpiry,
                              title: "Covid Vaccination Card",
                              uploading: uploadItems.contains(4),
                              onChange: (p) {
                                covidExpiry = p;
                                setState(() {});
                              },
                              onUpload: () async {
                                // if (covidExpiry == 0) {
                                //   showError(
                                //       context, "Enter expiry date of doc.");
                                //   return;
                                // }

                                uploadItems.add(4);
                                setState(() {});

                                covidUrl =
                                    await uploadByte(covidByte, covidExt);
                                uploadItems.remove(4);
                                setState(() {});

                                mainModel.put(COVID_URL, covidUrl);
                                mainModel.put(COVID_EXPIRY, covidExpiry);
                                mainModel.put(COVID_EXT, covidExt);
                                mainModel.updateItems();
                                setState(() {});
                              },
                              onAdd: () async {
                                final result = await pickByte();
                                if (result == null) return {};
                                covidByte = result[0];
                                covidExt = result[1];
                                setState(() {});
                              }),
                          uploadItem(
                              byteData: residentByte,
                              fileUrl: residentUrl,
                              expiry: residentExpiry,
                              title:
                                  "Permanent Resident/ Current Work Authorization",
                              uploading: uploadItems.contains(5),
                              onChange: (p) {
                                residentExpiry = p;
                                setState(() {});
                              },
                              onUpload: () async {
                                if (residentExpiry == 0) {
                                  showError(
                                      context, "Enter expiry date of doc.");
                                  return;
                                }

                                uploadItems.add(5);
                                setState(() {});
                                residentUrl =
                                    await uploadByte(residentByte, residentExt);
                                uploadItems.remove(5);
                                setState(() {});

                                mainModel.put(RESIDENT_URL, residentUrl);
                                mainModel.put(RESIDENT_EXPIRY, residentExpiry);
                                mainModel.put(RESIDENT_EXT, residentExt);
                                mainModel.updateItems();
                                setState(() {});
                              },
                              onAdd: () async {
                                final result = await pickByte();
                                if (result == null) return {};
                                residentByte = result[0];
                                residentExt = result[1];
                                setState(() {});
                              }),
                          addSpace(50),
                        ],
                      ),
                    ),
                    addSpace(20),
                    InkWell(
                      onTap: () {
                        termsAccepted = !termsAccepted;
                        setState(() {});
                      },
                      child: Container(
                        child: Row(
                          children: [
                            checkBox(termsAccepted),
                            addSpaceWidth(20),
                            Expanded(
                                child: Text(
                              "By my signature below, I authorize Exceptional Medical Staffing Inc. (“EXM”) to contact "
                              "any current and prior employers, educational institutions, licensing boards and personal "
                              "references referenced in my resume or other materials provided by me to EXM. I authorize "
                              "my current and prior employers, educational institutions, licensing board and personal "
                              "references to supply any information concerning my background, character or qualifications "
                              "for employment with EXM. I release any current or prior employer, educational institution, "
                              "licensing board and individual who provides such information and I release EXM and any "
                              "related subsidiaries, partnerships or affiliated companies and their owners, employees and "
                              "agents from any and all liability and responsibility for damages or claims of any kind for "
                              "revealing or receiving information concerning my background, character or qualifications for "
                              "employment with EXM.",
                              style: TextStyle(fontSize: 12),
                            ))
                          ],
                        ),
                      ),
                    ),
                    addSpace(20),
                    Container(
                      height: 60,
                      child: TextButton(
                        onPressed: !termsAccepted
                            ? null
                            : () {
                                int status = STATUS_COMPLETED;

                                if (driversLicenceUrl.isEmpty &&
                                    ssnCardUrl.isEmpty &&
                                    // blsUrl.isEmpty &&
                                    // tbUrl.isEmpty &&
                                    // stateLicenceUrl.isEmpty &&
                                    covidUrl.isEmpty &&
                                    residentUrl.isEmpty &&
                                    //
                                    driversLicenceExpiry == 0 &&
                                    // ssnCardExpiry == 0 &&
                                    blsExpiry == 0 &&
                                    // tbExpiry == 0 &&
                                    // stateLicenceExpiry == 0 &&
                                    // covidExpiry == 0 &&
                                    residentExpiry == 0) {
                                  // status = STATUS_UNDONE;
                                }

                                if (driversLicenceUrl.isEmpty ||
                                    ssnCardUrl.isEmpty ||
                                    // blsUrl.isEmpty ||
                                    // tbUrl.isEmpty ||
                                    // stateLicenceUrl.isEmpty ||
                                    covidUrl.isEmpty ||
                                    residentUrl.isEmpty ||
                                    //
                                    driversLicenceExpiry == 0 ||
                                    // ssnCardExpiry == 0 ||
                                    blsExpiry == 0 ||
                                    // tbExpiry == 0 ||
                                    // stateLicenceExpiry == 0 ||
                                    // covidExpiry == 0 ||
                                    residentExpiry == 0) {
                                  // status = STATUS_IN_PROGRESS;
                                }

                                formModel.put(STATUS, status);

                                int p = formsToFill.indexWhere(
                                    (e) => e.getString("path") == path);
                                if (p != -1) {
                                  formsToFill[p] = formModel;
                                }

                                mainModel.put(FORMS_TO_FILL,
                                    formsToFill.map((e) => e.items).toList());
                                mainModel.updateItems();

                                showMessage(
                                  context,
                                  Icons.check,
                                  green,
                                  "Form Submitted!",
                                  "You have successful Uploaded all documents required for your application",
                                  // cancellable: false,
                                  delayInMilli: 700,
                                  clickYesText: "Go Back",
                                  cancellable: false,
                                  onClicked: (_) {
                                    // Navigator.pop(context, true);
                                    Navigator.pushReplacementNamed(
                                        context, "/applicationForm??$formId");
                                  },
                                );
                              },
                        child: Center(
                          child: Text(
                            "Save Information".toUpperCase(),
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            minimumSize: Size(300, 50)),
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
          appBarHeader(context)
        ],
      ),
    );
  }

  uploadItem({
    bool hasExpiry = true,
    @required Uint8List byteData,
    @required String fileUrl,
    @required int expiry,
    @required String title,
    @required bool uploading,
    @required onChange,
    @required onUpload,
    @required onAdd,
  }
      // String sub,
      ) {
    String string = "Document Uploaded";
    if (null == byteData && fileUrl.isEmpty) string = "No Document added";
    if (null != byteData && fileUrl.isEmpty) string = "Document added";

    return Container(
      margin: EdgeInsets.only(bottom: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(0),
            child: DottedBorder(
              color: black.withOpacity(.5),
              strokeWidth: 1,
              borderType: BorderType.RRect,
              radius: Radius.circular(10),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Container(
                    height: 90,
                    width: 60,
                    color: colorString(string),
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.file_present,
                          color: string == "No Document added"
                              ? black.withOpacity(.5)
                              : white,
                          size: 20,
                        ),
                        addSpace(10),
                        Text(
                          string,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 10,
                              color: string == "No Document added"
                                  ? black.withOpacity(.5)
                                  : white

                              // fontWeight: FontWeight.bold,
                              ),
                        )
                      ],
                    )),
              ),
            ),
          ),
          addSpaceWidth(10),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                if (hasExpiry)
                  Text(
                    "Expired Licences/Documents are not acceptable!",
                    style: TextStyle(
                        fontSize: 12,
                        color: black.withOpacity(.5),
                        fontWeight: FontWeight.bold),
                  ),
                if (hasExpiry) addSpace(10),
                if (hasExpiry)
                  clickField(
                      "Document Expiry",
                      expiry == 0
                          ? ""
                          : getSimpleDate(expiry, pattern: "EEE MMMM dd, yyyy"),
                      () async {
                    final DateTime picked = await showDatePicker(
                        builder: (ctx, child) {
                          return Theme(
                              data: Theme.of(context).copyWith(
                                colorScheme: ColorScheme.light(
                                  primary: appColor,
                                  onPrimary: black, // header text color
                                  // onSurface: Colors.green, // body text color
                                ),
                                buttonTheme: ButtonThemeData(
                                    textTheme: ButtonTextTheme.primary),
                              ),
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: child));
                        },
                        context: context,
                        initialDate: expiry == 0
                            ? DateTime.now()
                            : DateTime.fromMillisecondsSinceEpoch(expiry),
                        firstDate: DateTime.now(),
                        lastDate: DateTime(DateTime.now().year + 60));

                    if (picked != null &&
                        picked.millisecondsSinceEpoch != expiry)
                      onChange(picked.millisecondsSinceEpoch);
                  }),
                Container(
                  height: 50,
                  child: Row(
                    children: [
                      Container(
                        height: 50,
                        child: TextButton(
                          onPressed: () {
                            onAdd();
                          },
                          child: Text(
                            "Add Document",
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.bold),
                          ),
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.only(left: 10, right: 10),
                            primary: white,
                            backgroundColor: black,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                            // minimumSize: Size(160, 50)
                          ),
                        ),
                      ),
                      if (!uploading)
                        Container(
                          height: 50,
                          margin: EdgeInsets.only(left: 10),
                          child: TextButton(
                            onPressed: () {
                              onUpload();
                            },
                            child: Text(
                              "Upload Document",
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold),
                            ),
                            style: TextButton.styleFrom(
                              padding: EdgeInsets.only(left: 10, right: 10),
                              primary: white,
                              backgroundColor: string == "Document added"
                                  ? green_dark
                                  : black,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              // minimumSize: Size(160, 50)
                            ),
                          ),
                        ),
                      addSpaceWidth(10),
                      if (isAdmin)
                        Container(
                          height: 50,
                          margin: EdgeInsets.only(left: 10),
                          child: Link(
                            uri: Uri.parse(fileUrl),
                            target: LinkTarget.blank,
                            builder: (context, followLink) {
                              return TextButton(
                                onPressed: followLink,
                                child: Text(
                                  "Open Document",
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                ),
                                style: TextButton.styleFrom(
                                  padding: EdgeInsets.only(left: 10, right: 10),

                                  primary: white,
                                  backgroundColor: appColor,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  // minimumSize: Size(160, 50)
                                ),
                              );
                            },
                          ),
                        )
                    ],
                  ),
                )
              ],
            ),
          ),
          addSpaceWidth(10),
          Container(
            height: 20,
            width: 20,
            child: Stack(
              alignment: Alignment.center,
              children: [
                if (uploading)
                  CircularProgressIndicator(
                    strokeWidth: 2,
                  ),
                checkBox(fileUrl.isNotEmpty, checkColor: green, size: 18)
              ],
            ),
          )
        ],
      ),
    );
  }
}

uploadByte(Uint8List byte, String ext) async {
  String ref = getRandomId();

  // uploadFileTask(file)

  final metadata = SettableMetadata(
    contentType: ext == "pdf"
        ? "application/pdf"
        : ext == "png"
            ? "image/png"
            : "image/jpeg",
  );

  final uploadTask =
      await FirebaseStorage.instance.ref(ref).putData(byte, metadata);
  final _ = await uploadTask.ref.getDownloadURL();
  BaseModel sm = new BaseModel();
  sm.put(FILE_URL, _.toString());
  sm.put(REFERENCE, ref);
  sm.saveItem(REFERENCE_BASE, false);
  return _.toString();
}

Color colorString(String string) {
  if (string == "Document added") return orange0;
  if (string == "No Document added") return transparent;
  return green_dark;
}

Future<List> pickByte() async {
  final result = await FilePicker.platform.pickFiles(
      type: FileType.custom, allowedExtensions: ["pdf", "png", "jpg"]);
  if (result == null) return null;
  // return result.files.first.bytes;
  return [result.files.first.bytes, result.files.first.extension];
}
