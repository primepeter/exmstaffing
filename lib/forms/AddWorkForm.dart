import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EmailApi.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/forms/ShowClients.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shimmer/shimmer.dart';

import 'ShowEmployees.dart';

class Shifts {
  String startTime;
  String endTime;
  int shiftIndex = -1;
  String name;
  final neededController = TextEditingController();

  Shifts({Map items}) {
    if (null != items) {
      startTime = items["startTime"];
      endTime = items["endTime"];
      shiftIndex = items["shiftIndex"];
      // name = items["name"];
      neededController.text = items["needed"];
    }
  }

  bool get isValidated {
    bool validated = true;
    // if (startTime == 0) return false;
    // if (endTime == 0) return false;
    if (null == startTime || startTime.isEmpty) return false;
    if (null == endTime || endTime.isEmpty) return false;

    if (shiftIndex == -1) return false;
    // if (null == name || name.isEmpty) return false;
    if (neededController.text.isEmpty) return false;
    return validated;
  }

  Map get items {
    return {
      "startTime": startTime,
      "endTime": endTime,
      "shiftIndex": shiftIndex,
      "name": name,
      "needed": neededController.text,
    };
  }
}

class AssignInfo {
  int shiftIndex = -1;
  List employees = [];

  String startTime;
  String endTime;

  AssignInfo({Map items}) {
    if (null != items) {
      shiftIndex = items["shiftIndex"];
      employees = items["employees"] ?? [];
      startTime = items["startTime"];
      endTime = items["endTime"];
    }
  }

  bool get isValidated {
    bool validated = true;
    if (shiftIndex == -1) return false;
    if (null == employees || employees.isEmpty) return false;
    return validated;
  }

  Map get items {
    return {
      "employees": employees,
      "shiftIndex": shiftIndex,
      "startTime": startTime,
      "endTime": endTime,
    };
  }
}

class AddWorkForm extends StatefulWidget {
  const AddWorkForm(
      {Key key, this.bm, this.readOnly = false, this.assign = false})
      : super(key: key);
  final BaseModel bm;
  final bool readOnly;
  final bool assign;

  @override
  _AddWorkFormState createState() => _AddWorkFormState();
}

class _AddWorkFormState extends State<AddWorkForm> {
  final scrollController = ScrollController();
  final clientAddressController = TextEditingController();
  // final clientStateController = TextEditingController();
  // final employeesNeededController = TextEditingController();
  final clientNameController = TextEditingController();
  final clientTelController = TextEditingController();
  final reportToController = TextEditingController();

  final noteController = TextEditingController();

  String clientID = "";
  String clientName = "";
  String clientEmail = "";

  String state;
  String city;

  List selectedSkills = [];

  List shiftPeriod = [
    "First Shift",
    "Second Shift",
    "Third/Overnight Shift",
  ];

  List<Shifts> shiftsInfo = [];
  List<AssignInfo> assignInfo = [];

  List shiftAvailable = [];

  // String skillRequired;
  int startDate = 0;
  int endDate = 0;
  // String startTime;
  // String endTime;

  List affiliations = [];

  BaseModel model;
  bool readOnly = false;

  bool assign = false;

  List<BaseModel> assignedInfo = [];
  int employeesNeeded = 0;
  bool showNoteBtn = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    readOnly = widget.readOnly;
    assign = widget.assign;

    // shifts = List.generate(shiftAvailable.length, (index) {
    //   return Shifts()..name = shiftAvailable[index];
    // });

    noteController.addListener(() {
      String text = noteController.text;
      showNoteBtn = text.isNotEmpty;
      setState(() {});
    });

    if (null != widget.bm) {
      model = widget.bm;
      readOnly = widget.readOnly;

      shiftsInfo = model
          .getListModel(SHIFTS_INFO)
          .map((e) => Shifts(items: e.items))
          .toList();

      assignInfo = model
          .getListModel(ASSIGNED_INFO)
          .map((e) => AssignInfo(items: e.items))
          .toList();

      clientID = model.getString(CLIENT_ID);
      clientName = model.getString(CLIENT_NAME);
      clientEmail = model.getString(CLIENT_EMAIL);
      clientNameController.text = model.getString(NAME);
      clientAddressController.text = model.getString(ADDRESS);
      state = model.getString(STATE);
      city = model.getString(CITY);
      clientTelController.text = model.getString(TELEPHONE);
      selectedSkills = model.getList(SKILLS);
      shiftAvailable = model.getList(SHIFTS);
      reportToController.text = model.getString(REPORT_TO);

      employeesNeeded = model.getInt(EMPLOYEES_NEEDED);
      // employeesNeededController.text = employeesNeeded.toString();

      // startTime = model.getString(START_TIME);
      // endTime = model.getString(END_TIME);
      startDate = model.getInt(START_DATE);
      endDate = model.getInt(END_DATE);
      assignedInfo = model.getListModel(ASSIGNED_INFO);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: black.withOpacity(.8),
      child: LayoutBuilder(builder: (c, box) {
        bool isLargeScreen = box.maxWidth > 850;
        // final size = getScreenWidth(c);
        final size = box.maxWidth;
        final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

        return RawScrollbar(
          controller: scrollController,
          isAlwaysShown: true,
          thumbColor: Colors.white,
          radius: Radius.circular(20),
          thickness: 20,
          interactive: true,
          child: SingleChildScrollView(
            controller: scrollController,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Container(
                  decoration: BoxDecoration(
                      color: white, borderRadius: BorderRadius.circular(15)),
                  padding: EdgeInsets.all(30),
                  margin: EdgeInsets.only(
                      top: 20,
                      bottom: 20,
                      left: ((0.12) * size),
                      right: ((0.12) * size)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              "WorkOrder Information",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ),
                          if (!readOnly)
                            TextButton(
                              onPressed: () {
                                // String clientName = clientNameController.text;
                                String clientAddress =
                                    clientAddressController.text;
                                // String clientState = clientStateController.text;
                                String clientTel = clientTelController.text;
                                String reportTo = reportToController.text;
                                // String empNeeded = employeesNeededController.text;

                                if (clientName.isEmpty) {
                                  showError(context, "Enter Client Name!");
                                  return;
                                }
                                if (clientAddress.isEmpty) {
                                  showError(context, "Enter Client Address!");
                                  return;
                                }
                                if (null == state || state.isEmpty) {
                                  showError(context, "Enter Client State!");
                                  return;
                                }
                                if (null == city || city.isEmpty) {
                                  showError(context, "Enter Client City!");
                                  return;
                                }
                                // if (empNeeded.isEmpty) {
                                //   showError(
                                //       context, "Enter No of employees needed!");
                                //   return;
                                // }

                                if (startDate == 0) {
                                  showError(context, "Enter Start Date!");
                                  return;
                                }
                                if (endDate == 0) {
                                  showError(context, "Enter End Date!");
                                  return;
                                }

                                // if (null == startTime) {
                                //   showError(context, "Enter Start Time!");
                                //   return;
                                // }
                                //
                                // if (null == endTime) {
                                //   showError(context, "Enter End Time!");
                                //   return;
                                // }

                                if (selectedSkills.isEmpty) {
                                  showError(context, "Enter Skill set!");
                                  return;
                                }

                                // if (clientTel.isEmpty) {
                                //   showError(context, "Enter Client Tel!");
                                //   return;
                                // }

                                if (reportTo.isEmpty) {
                                  showError(context, "Enter Report To!");
                                  return;
                                }
                                if (shiftsInfo.isEmpty) {
                                  showError(context,
                                      "Add shifts period to work order!");
                                  return;
                                }
                                // if (shiftAvailable.isEmpty) {
                                //   showError(
                                //       context, "Choose work order shift period!");
                                //   return;
                                // }

                                String search = "${clientName.toLowerCase()}"
                                    " ${clientAddress.toLowerCase()} "
                                    "${state.toLowerCase()} ${city.toLowerCase()}";

                                showProgress(true, context,
                                    msg: "Please wait...");
                                String id = getRandomId();
                                if (null != widget.bm)
                                  id = widget.bm.getObjectId();

                                BaseModel model = BaseModel();

                                model.put(SHIFTS_INFO,
                                    shiftsInfo.map((e) => e.items).toList());
                                model.put(OBJECT_ID, id);
                                model.put(CLIENT_ID, clientID);
                                model.put(CLIENT_EMAIL, clientEmail);
                                model.put(CLIENT_NAME, clientName);
                                model.put(NAME, clientName);
                                model.put(ADDRESS, clientAddress);
                                model.put(STATE, state);
                                model.put(CITY, city);
                                model.put(TELEPHONE, clientTel);
                                model.put(SKILLS, selectedSkills);
                                model.put(SHIFTS, shiftAvailable);
                                model.put(REPORT_TO, reportTo);
                                // model.put(END_TIME, endTime);
                                // model.put(START_TIME, startTime);
                                model.put(START_DATE, startDate);

                                model.put(END_DATE, endDate);
                                model.put(STATUS, PENDING);
                                // model.put(EMPLOYEES_NEEDED, int.parse(empNeeded));
                                model.put(SEARCH, getSearchString(search));

                                model.saveItem(WORK_BASE, true, document: id,
                                    onComplete: (e) {
                                  showProgress(
                                    false,
                                    context,
                                  );
                                  if (null != e) {
                                    showErrorDialog(context, e);
                                    return;
                                  }
                                  appSettingsModel
                                    ..putInList(
                                      WORK_BASE_COUNT,
                                      id,
                                    )
                                    ..putInList(JOB_PENDING_LIST, id)
                                    ..putInList(JOB_COMPLETED_LIST, id,
                                        add: false)
                                    ..putInList(JOB_ASSIGNED_LIST, id,
                                        add: false)
                                    ..updateItems();

                                  String baseUrl =
                                      appSettingsModel.getString("baseUrl");
                                  String url = "${baseUrl}";

                                  FirebaseFirestore.instance
                                      .collection(USER_BASE)
                                      .where(PRIVILEGE_INDEX, isEqualTo: 1)
                                      .get()
                                      .then((value) async {
                                    final docs = value.docs
                                        .map((e) => BaseModel(doc: e))
                                        .toList();
                                    final items = docs
                                        .map((e) => {
                                              "email": e.getEmail(),
                                              "name": e.getString(FIRST_NAME)
                                            })
                                        .toList();

                                    await EmailApi.sendEmailAll(
                                      type: EmailApi.TYPE_ORDER,
                                      items: items,
                                      url: url,
                                      message:
                                          "Check, You have a new work order!",
                                    );
                                  });

                                  showMessage(
                                    context,
                                    Icons.check,
                                    green,

                                    null != widget.bm
                                        ? "WorkOrder Updated!"
                                        : "WorkOrder Added!",
                                    null != widget.bm
                                        ? "You have successful updated this WorkOrder"
                                        : "You have successful added this WorkOrder",
                                    // cancellable: false,
                                    delayInMilli: 700,
                                    clickYesText: "Nice!",
                                    cancellable: false,
                                    onClicked: (_) {
                                      Navigator.pop(context, model);
                                    },
                                  );
                                });
                              },
                              child: Center(
                                child: Text(
                                  null != widget.bm
                                      ? "Update Order"
                                      : "Add Order",
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              style: TextButton.styleFrom(
                                  primary: white,
                                  backgroundColor: green_dark,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  minimumSize: Size(100, 50)),
                            ),
                          addSpaceWidth(10),
                          Container(
                            // margin: EdgeInsets.only(bottom: 15),
                            child: TextButton(
                              onPressed: () {
                                Navigator.pop(context, readOnly ? model : null);
                              },
                              child: Icon(
                                Icons.clear,
                                size: 15,
                              ),
                              style: TextButton.styleFrom(
                                  padding: EdgeInsets.zero,
                                  primary: white,
                                  backgroundColor: red,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  minimumSize: Size(40, 40)),
                            ),
                          )
                        ],
                      ),
                      addSpace(20),
                      if (assign && readOnly)
                        Builder(
                          builder: (c) {
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "Assignees",
                                            style: TextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            "Assign work order to employees.",
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: black.withOpacity(.5),
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                    ),
                                    if (assignInfo.isNotEmpty &&
                                        model.getInt(STATUS) == PENDING)
                                      Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: TextButton(
                                          onPressed: () {
                                            bool validated = true;

                                            for (int p = 0;
                                                p < assignInfo.length;
                                                p++) {
                                              if (!assignInfo[p].isValidated) {
                                                validated = false;
                                                showError(context,
                                                    "Some fields on assign item ${p + 1} are empty!");

                                                break;
                                              }
                                            }

                                            if (!validated) return;

                                            yesNoDialog(context, "Just Save?",
                                                "Are you sure you want to save assignees added and continue later?",
                                                () async {
                                              model
                                                ..put(
                                                    ASSIGNED_INFO,
                                                    assignInfo
                                                        .map((e) => e.items)
                                                        .toList())
                                                ..updateItems();
                                            });
                                          },
                                          child: Text(
                                            "Save",
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          style: TextButton.styleFrom(
                                              padding: EdgeInsets.zero,
                                              primary: white,
                                              backgroundColor: black,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(8)),
                                              minimumSize: Size(100, 50)),
                                        ),
                                      ),
                                    if (assignInfo.isNotEmpty &&
                                        model.getInt(STATUS) != ASSIGNED)
                                      Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: TextButton(
                                          onPressed: () {
                                            bool validated = true;

                                            for (int p = 0;
                                                p < assignInfo.length;
                                                p++) {
                                              if (!assignInfo[p].isValidated) {
                                                validated = false;
                                                showError(context,
                                                    "Some fields on assign item ${p + 1} are empty!");

                                                break;
                                              }
                                            }

                                            if (!validated) return;

                                            if (assignInfo.length !=
                                                shiftsInfo.length) {
                                              showError(context,
                                                  "Assignees must match the work shifts available!");
                                              return;
                                            }

                                            if (assignInfo.isNotEmpty &&
                                                !assignInfo[
                                                        assignInfo.length - 1]
                                                    .isValidated) {
                                              showError(context,
                                                  "Some fields on the last assign item are empty!");
                                              return;
                                            }

                                            yesNoDialog(
                                                context,
                                                "Assign WorkOrder?",
                                                "Are you sure you want to assign this work order?",
                                                () async {
                                              try {
                                                String assignId =
                                                    model.getObjectId();

                                                showProgress(true, context,
                                                    msg: "Assigning");

                                                List firstShift = [];
                                                List secondShift = [];
                                                List thirdShift = [];

                                                String firstShiftTime;
                                                String secondShiftTime;
                                                String thirdShiftTime;

                                                // final checkIndex =
                                                // shiftsInfo.indexWhere(
                                                // (e) =>
                                                // e.shiftIndex ==
                                                // p);

                                                for (var d in assignInfo) {
                                                  final employees = d.employees;
                                                  print("employees ${d.items}");
                                                  for (var sub in employees) {
                                                    print("sub ${sub}");
                                                    String empId =
                                                        sub[OBJECT_ID];
                                                    final doc =
                                                        await FirebaseFirestore
                                                            .instance
                                                            .collection(
                                                                EMPLOYEE_BASE)
                                                            .doc(empId)
                                                            .get();
                                                    if (null == doc ||
                                                        !doc.exists) continue;
                                                    final base =
                                                        BaseModel(doc: doc);
                                                    // final shiftName =
                                                    //     shiftPeriod[
                                                    //         d.shiftIndex];
                                                    final empName =
                                                        "${base.getString(FIRST_NAME)} ${base.getString(LAST_NAME)}";

                                                    // if (d.shiftIndex == 0) {
                                                    //   firstShift.add(empName);
                                                    //   firstShiftTime =
                                                    //       "${shiftsInfo[0].startTime} - ${shiftsInfo[0].endTime}";
                                                    // }
                                                    // if (d.shiftIndex == 1) {
                                                    //   secondShift.add(empName);
                                                    //   secondShiftTime =
                                                    //       "${shiftsInfo[1].startTime} - ${shiftsInfo[1].endTime}";
                                                    // }
                                                    // if (d.shiftIndex == 2) {
                                                    //   thirdShift.add(empName);
                                                    //   thirdShiftTime =
                                                    //       "${shiftsInfo[2].startTime} - ${shiftsInfo[2].endTime}";
                                                    // }

                                                    if (d.shiftIndex == 0) {
                                                      firstShift.add(empName);
                                                      firstShiftTime =
                                                          "${d.startTime} - ${d.endTime}";
                                                    }
                                                    if (d.shiftIndex == 1) {
                                                      secondShift.add(empName);
                                                      secondShiftTime =
                                                          "${d.startTime} - ${d.endTime}";
                                                    }
                                                    if (d.shiftIndex == 2) {
                                                      thirdShift.add(empName);
                                                      thirdShiftTime =
                                                          "${d.startTime} - ${d.endTime}";
                                                    }

                                                    base
                                                      ..put(
                                                          ASSIGNED_ID, assignId)
                                                      ..updateItems();
                                                    String baseUrl =
                                                        appSettingsModel
                                                            .getString(
                                                                "baseUrl");

                                                    String id =
                                                        base.getObjectId();
                                                    String url =
                                                        "${baseUrl}clockIn??$id";
                                                    String data =
                                                        "Click the link to "
                                                        "complete your application process $url";

                                                    EmailApi.sendEmail(
                                                        type: EmailApi
                                                            .TYPE_CLOCKING,
                                                        url: url,
                                                        toName: base.getString(
                                                            FIRST_NAME),
                                                        toEmail:
                                                            base.getEmail(),
                                                        message: data);
                                                  }
                                                }

                                                print("finished that step!");

                                                // ASSIGNED_ID

                                                model
                                                  ..put(
                                                      ASSIGNED_INFO,
                                                      assignInfo
                                                          .map((e) => e.items)
                                                          .toList())
                                                  ..put(STATUS, ASSIGNED)
                                                  ..updateItems();

                                                String id = model.getObjectId();

                                                appSettingsModel
                                                  ..putInList(
                                                      JOB_ASSIGNED_LIST, id)
                                                  ..putInList(
                                                      JOB_COMPLETED_LIST, id,
                                                      add: false)
                                                  ..putInList(
                                                      JOB_PENDING_LIST, id,
                                                      add: false)
                                                  ..updateItems();

                                                await EmailApi.sendEmail(
                                                    type: EmailApi
                                                        .TYPE_ASSIGNED_ORDER,
                                                    toEmail: model.getString(
                                                        CLIENT_EMAIL),
                                                    toName: model
                                                        .getString(CLIENT_NAME),
                                                    firstShift: firstShift
                                                        .toString()
                                                        .replaceAll("[", "")
                                                        .replaceAll("]", ""),
                                                    secondShift: secondShift
                                                        .toString()
                                                        .replaceAll("[", "")
                                                        .replaceAll("]", ""),
                                                    thirdShift: thirdShift
                                                        .toString()
                                                        .replaceAll("[", "")
                                                        .replaceAll("]", ""),
                                                    firstShiftTime:
                                                        firstShiftTime,
                                                    secondShiftTime:
                                                        secondShiftTime,
                                                    thirdShiftTime:
                                                        thirdShiftTime,
                                                    orderDate: getSimpleDate(
                                                        startDate,
                                                        pattern:
                                                            "EEE MMMM dd, yyyy"));

                                                showMessage(
                                                  context,
                                                  Icons.check,
                                                  green,
                                                  "Order Assigned!",
                                                  "You have successful assigned this job.",
                                                  delayInMilli: 850,
                                                  clickYesText: "Nice!",
                                                  cancellable: false,
                                                  onClicked: (_) {
                                                    showProgress(
                                                        false, context);
                                                    setState(() {});
                                                  },
                                                );
                                              } catch (e) {
                                                showProgress(false, context);
                                                showErrorDialog(
                                                    context, e.toString());
                                              }
                                            });
                                          },
                                          child: Text(
                                            "Assign",
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          style: TextButton.styleFrom(
                                              padding: EdgeInsets.zero,
                                              primary: white,
                                              backgroundColor: black,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(8)),
                                              minimumSize: Size(100, 50)),
                                        ),
                                      ),
                                    if (assignInfo.isNotEmpty &&
                                        model.getInt(STATUS) == ASSIGNED)
                                      Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: TextButton(
                                          onPressed: () {
                                            yesNoDialog(
                                                context,
                                                "Order Completed?",
                                                "Are you sure you want to mark this job order as completed?\nEmployees assigned to this order would be released.",
                                                () async {
                                              try {
                                                showProgress(true, context,
                                                    msg: "Please wait");
                                                String assignId =
                                                    model.getObjectId();

                                                for (var d in assignInfo) {
                                                  final employees = d.employees;
                                                  for (var sub in employees) {
                                                    String empId =
                                                        sub[OBJECT_ID];
                                                    final doc =
                                                        await FirebaseFirestore
                                                            .instance
                                                            .collection(
                                                                EMPLOYEE_BASE)
                                                            .doc(empId)
                                                            .get();
                                                    if (null == doc ||
                                                        !doc.exists) continue;
                                                    final base =
                                                        BaseModel(doc: doc);
                                                    base
                                                      ..put(ASSIGNED_ID, "")
                                                      ..updateItems();

                                                    model
                                                      ..put(STATUS, PENDING)
                                                      ..put(
                                                          ASSIGNED_INFO,
                                                          assignInfo
                                                              .map((e) =>
                                                                  e.items)
                                                              .toList())
                                                      ..updateItems();
                                                  }
                                                }

                                                model
                                                  ..put(STATUS, COMPLETED)
                                                  ..updateItems();

                                                appSettingsModel
                                                  ..putInList(JOB_PENDING_LIST,
                                                      model.getObjectId(),
                                                      add: false)
                                                  ..putInList(JOB_ASSIGNED_LIST,
                                                      model.getObjectId(),
                                                      add: false)
                                                  ..putInList(
                                                    JOB_COMPLETED_LIST,
                                                    model.getObjectId(),
                                                  )
                                                  ..updateItems();

                                                showProgress(false, context);

                                                showMessage(
                                                  context,
                                                  Icons.check,
                                                  green,
                                                  "Order Completed!",
                                                  "You have marked this job order completed!.",
                                                  delayInMilli: 850,
                                                  clickYesText: "Nice!",
                                                  cancellable: false,
                                                  onClicked: (_) {
                                                    setState(() {});
                                                  },
                                                );
                                              } catch (e) {
                                                showProgress(false, context);
                                                showErrorDialog(
                                                    context, e.toString());
                                              }
                                            });
                                          },
                                          child: Text(
                                            "Completed",
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          style: TextButton.styleFrom(
                                              padding: EdgeInsets.zero,
                                              primary: white,
                                              backgroundColor: black,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(8)),
                                              minimumSize: Size(100, 50)),
                                        ),
                                      ),
                                    if (assignInfo.length !=
                                            shiftsInfo.length &&
                                        model.getInt(STATUS) == PENDING)
                                      Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: TextButton(
                                          onPressed: () {
                                            if (assignInfo.isNotEmpty &&
                                                !assignInfo[
                                                        assignInfo.length - 1]
                                                    .isValidated) {
                                              showError(context,
                                                  "Some fields on the current assign item are empty!");
                                              return;
                                            }

                                            assignInfo.add(AssignInfo());
                                            setState(() {});
                                          },
                                          child: Icon(
                                            Icons.add,
                                            size: 15,
                                          ),
                                          style: TextButton.styleFrom(
                                              padding: EdgeInsets.zero,
                                              primary: white,
                                              backgroundColor: black,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(8)),
                                              minimumSize: Size(40, 40)),
                                        ),
                                      ),
                                  ],
                                ),

                                // addLine(20, black.withOpacity(.03), 0, 20, 0, 20),
                                addSpace(20),
                                Container(
                                  margin: EdgeInsets.only(bottom: 20),
                                  child: Builder(builder: (ctx) {
                                    if (assignInfo.isEmpty)
                                      return Container(
                                        padding: EdgeInsets.all(0),
                                        child: DottedBorder(
                                          color: black.withOpacity(.5),
                                          strokeWidth: 1,
                                          borderType: BorderType.RRect,
                                          radius: Radius.circular(10),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            child: Container(
                                                height: 150,
                                                alignment: Alignment.center,
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Icon(
                                                      Icons.people,
                                                      color:
                                                          black.withOpacity(.5),
                                                      size: 30,
                                                    ),
                                                    addSpace(10),
                                                    Text(
                                                      "You have not assigned this work order to any employee yet.",
                                                      style: TextStyle(
                                                          fontSize: 14,
                                                          color: black
                                                              .withOpacity(.5)
                                                          // fontWeight: FontWeight.bold,
                                                          ),
                                                    )
                                                  ],
                                                )),
                                          ),
                                        ),
                                      );

                                    return Column(
                                      children: List.generate(assignInfo.length,
                                          (index) {
                                        final fm = assignInfo[index];

                                        return Container(
                                          margin: EdgeInsets.only(bottom: 10),
                                          padding: EdgeInsets.all(6),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              border: Border.all(
                                                  color:
                                                      black.withOpacity(.09))),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              Expanded(
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    addSpace(5),
                                                    groupFields(
                                                        "Choose shifts",
                                                        shiftPeriod,
                                                        fm.shiftIndex, (p) {
                                                      final shiftP = assignInfo
                                                          .indexWhere((e) =>
                                                              e.shiftIndex ==
                                                              p);

                                                      final checkIndex =
                                                          shiftsInfo.indexWhere(
                                                              (e) =>
                                                                  e.shiftIndex ==
                                                                  p);

                                                      if (checkIndex == -1) {
                                                        showError(context,
                                                            "This shift cannot be selected, because it's not inclusive in the number of shifts for this order!");
                                                        return;
                                                      }

                                                      print(
                                                          shiftsInfo[checkIndex]
                                                              .items);

                                                      if (assignInfo.length >
                                                              1 &&
                                                          shiftP != -1) {
                                                        showError(context,
                                                            "Assignee at $shiftP already has this shift selected!");
                                                        return;
                                                      }
                                                      // fm.shiftIndex = p;
                                                      fm.shiftIndex =
                                                          shiftsInfo[checkIndex]
                                                              .shiftIndex;

                                                      fm.startTime =
                                                          shiftsInfo[checkIndex]
                                                              .startTime;
                                                      fm.endTime =
                                                          shiftsInfo[checkIndex]
                                                              .endTime;
                                                      setState(() {});
                                                    }, width: 150),
                                                    addSpace(5),
                                                    Wrap(
                                                      children:
                                                          List.generate(
                                                              fm.employees
                                                                  .length,
                                                              (index) {
                                                        String name =
                                                            fm.employees[index]
                                                                [NAME];
                                                        String email =
                                                            fm.employees[index]
                                                                [EMAIL];
                                                        String id =
                                                            fm.employees[index]
                                                                [OBJECT_ID];

                                                        final employeeItem =
                                                            fm.employees[index];

                                                        Map clockInfo =
                                                            employeeItem[
                                                                    CLOCKING_INFO] ??
                                                                {};

                                                        // if (fm.employees
                                                        //     .isNotEmpty) {
                                                        //   clockInfo = fm.employees[
                                                        //               index][
                                                        //           CLOCKING_INFO] ??
                                                        //       {};
                                                        // }

                                                        int clockedIn = 0;
                                                        int clockedOut = 0;

                                                        if (null != clockInfo &&
                                                            clockInfo
                                                                .isNotEmpty) {
                                                          Map item = clockInfo[
                                                                  "${fm.shiftIndex}"] ??
                                                              {};

                                                          if (null != item &&
                                                              item.isNotEmpty) {
                                                            clockedIn =
                                                                item[CLOCKED_IN]
                                                                        [
                                                                        TIME] ??
                                                                    0;
                                                            clockedOut =
                                                                item[CLOCKED_OUT]
                                                                        [
                                                                        TIME] ??
                                                                    0;
                                                          }
                                                        }

                                                        return Container(
                                                          margin:
                                                              EdgeInsets.all(5),
                                                          child: Row(
                                                            mainAxisSize:
                                                                MainAxisSize
                                                                    .min,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Container(
                                                                height: 130,
                                                                width: 300,
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(5),
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .center,
                                                                  children: [
                                                                    Text(
                                                                      name,
                                                                      maxLines:
                                                                          1,
                                                                      overflow:
                                                                          TextOverflow
                                                                              .ellipsis,
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              16,
                                                                          color:
                                                                              black,
                                                                          fontWeight:
                                                                              FontWeight.bold),
                                                                    ),
                                                                    addSpace(5),
                                                                    Row(
                                                                      children:
                                                                          List.generate(
                                                                        2,
                                                                        (p) {
                                                                          Map clockInfo =
                                                                              (fm.employees[index][CLOCKING_INFO]) ?? {};

                                                                          String type = p == 0
                                                                              ? "ClockIn Info"
                                                                              : "ClockOut Info";

                                                                          int time =
                                                                              0;
                                                                          String
                                                                              placeName =
                                                                              "";
                                                                          String
                                                                              country =
                                                                              "";
                                                                          String
                                                                              city =
                                                                              "";

                                                                          if (null !=
                                                                              clockInfo) {
                                                                            Map item = clockInfo[p == 0
                                                                                ? CLOCKED_IN
                                                                                : CLOCKED_OUT];
                                                                            if (null !=
                                                                                item) {
                                                                              time = item[TIME] ?? 0;

                                                                              placeName = item[PLACE_NAME] ?? "";

                                                                              country = item[COUNTRY] ?? "";

                                                                              city = item[CITY] ?? "";
                                                                            }
                                                                          }

                                                                          return Expanded(
                                                                            child:
                                                                                Column(
                                                                              mainAxisSize: MainAxisSize.min,
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [
                                                                                Text(
                                                                                  type,
                                                                                  style: TextStyle(fontSize: 12, color: black, fontWeight: FontWeight.bold),
                                                                                ),
                                                                                addSpace(2),
                                                                                Text(
                                                                                  "Time: ${time == 0 ? "Yet to clock in" : TimeOfDay.fromDateTime(DateTime.fromMillisecondsSinceEpoch(time)).format(context)}",
                                                                                  maxLines: 1,
                                                                                  overflow: TextOverflow.ellipsis,
                                                                                  style: TextStyle(
                                                                                    fontSize: 11,
                                                                                    color: black,
                                                                                  ),
                                                                                ),
                                                                                Text(
                                                                                  "Country: $country",
                                                                                  maxLines: 1,
                                                                                  overflow: TextOverflow.ellipsis,
                                                                                  style: TextStyle(
                                                                                    fontSize: 11,
                                                                                    color: black,
                                                                                  ),
                                                                                ),
                                                                                Text(
                                                                                  "Place: $placeName",
                                                                                  maxLines: 1,
                                                                                  overflow: TextOverflow.ellipsis,
                                                                                  style: TextStyle(
                                                                                    fontSize: 11,
                                                                                    color: black,
                                                                                  ),
                                                                                ),
                                                                                Text(
                                                                                  "City $city",
                                                                                  maxLines: 1,
                                                                                  overflow: TextOverflow.ellipsis,
                                                                                  style: TextStyle(
                                                                                    fontSize: 11,
                                                                                    color: black,
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          );
                                                                        },
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                                alignment: Alignment
                                                                    .centerLeft,
                                                                decoration: BoxDecoration(
                                                                    color:
                                                                        white,
                                                                    boxShadow: [
                                                                      BoxShadow(
                                                                          color: black.withOpacity(
                                                                              .05),
                                                                          spreadRadius:
                                                                              2,
                                                                          blurRadius:
                                                                              5)
                                                                    ],
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            8)),
                                                              ),
                                                              addSpaceWidth(5),
                                                              Container(
                                                                // margin: EdgeInsets.only(bottom: 15),
                                                                child:
                                                                    TextButton(
                                                                  onPressed:
                                                                      () async {
                                                                    fm.employees
                                                                        .removeWhere((e) =>
                                                                            e[OBJECT_ID] ==
                                                                            id);

                                                                    final doc = await FirebaseFirestore
                                                                        .instance
                                                                        .collection(
                                                                            EMPLOYEE_BASE)
                                                                        .doc(id)
                                                                        .get();

                                                                    final base =
                                                                        BaseModel(
                                                                            doc:
                                                                                doc);
                                                                    base
                                                                      ..put(
                                                                          ASSIGNED_ID,
                                                                          "")
                                                                      ..updateItems();

                                                                    // assignInfo
                                                                    //     .removeAt(
                                                                    //         index);

                                                                    model
                                                                      ..put(
                                                                          STATUS,
                                                                          PENDING)
                                                                      ..put(
                                                                          ASSIGNED_INFO,
                                                                          assignInfo
                                                                              .map((e) => e.items)
                                                                              .toList())
                                                                      ..updateItems();
                                                                    setState(
                                                                        () {});
                                                                  },
                                                                  child: Icon(
                                                                    Icons.clear,
                                                                    size: 15,
                                                                  ),
                                                                  style: TextButton.styleFrom(
                                                                      padding:
                                                                          EdgeInsets
                                                                              .zero,
                                                                      primary:
                                                                          white,
                                                                      backgroundColor:
                                                                          red,
                                                                      shape: RoundedRectangleBorder(
                                                                          borderRadius: BorderRadius.circular(
                                                                              8)),
                                                                      minimumSize:
                                                                          Size(
                                                                              40,
                                                                              40)),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        );
                                                      })
                                                            ..insert(0, Builder(
                                                                builder: (c) {
                                                              final checkIndex =
                                                                  shiftsInfo.indexWhere((e) =>
                                                                      e.shiftIndex ==
                                                                      fm.shiftIndex);

                                                              // if (fm.shiftIndex == -1)
                                                              if (checkIndex ==
                                                                  -1)
                                                                return Container();

                                                              int needed = num.parse(
                                                                      shiftsInfo[
                                                                              checkIndex]
                                                                          .neededController
                                                                          .text)
                                                                  .toInt();
                                                              // int needed = num.parse(
                                                              //         shiftsInfo[
                                                              //                 fm.shiftIndex]
                                                              //             .neededController
                                                              //             .text)
                                                              //     .toInt();
                                                              int maxSelection =
                                                                  needed -
                                                                      fm.employees
                                                                          .length;

                                                              if (maxSelection
                                                                  .isNegative)
                                                                maxSelection =
                                                                    0;

                                                              if (maxSelection ==
                                                                  0)
                                                                return Container();

                                                              return TextButton(
                                                                onPressed: () {
                                                                  if (fm.shiftIndex ==
                                                                      -1) {
                                                                    showError(
                                                                        context,
                                                                        "Please first choose a shift!");
                                                                    return;
                                                                  }

                                                                  pushAndResult(
                                                                      context,
                                                                      ShowEmployees(
                                                                        model
                                                                            .getObjectId(),
                                                                        maxSelection:
                                                                            maxSelection,
                                                                      ),
                                                                      dialog:
                                                                          true,
                                                                      result:
                                                                          (List<BaseModel>
                                                                              _) {
                                                                    for (var bm
                                                                        in _) {
                                                                      String
                                                                          id =
                                                                          bm.getObjectId();
                                                                      int p = fm
                                                                          .employees
                                                                          .indexWhere((e) =>
                                                                              e[OBJECT_ID] ==
                                                                              id);

                                                                      int max =
                                                                          2;

                                                                      if (assignInfo
                                                                          .isNotEmpty) {
                                                                        int counter =
                                                                            0;

                                                                        for (var d
                                                                            in assignInfo) {
                                                                          int p = d.employees.indexWhere((e) =>
                                                                              e[OBJECT_ID] ==
                                                                              id);

                                                                          if (p !=
                                                                              -1)
                                                                            counter =
                                                                                counter + 1;
                                                                        }

                                                                        print(
                                                                            "counter $counter");
                                                                        if (counter >=
                                                                            max)
                                                                          continue;
                                                                      }

                                                                      Map item =
                                                                          {
                                                                        EMAIL: bm
                                                                            .getEmail(),
                                                                        NAME: bm
                                                                            .getString(FIRST_NAME),
                                                                        OBJECT_ID:
                                                                            id,
                                                                        COORDINATOR_INFO:
                                                                            {
                                                                          EMAIL:
                                                                              userModel.getEmail(),
                                                                          NAME:
                                                                              userModel.getString(FIRST_NAME),
                                                                          OBJECT_ID:
                                                                              userModel.getObjectId(),
                                                                        }
                                                                      };
                                                                      if (p ==
                                                                          -1) {
                                                                        fm.employees
                                                                            .add(item);
                                                                      } else {
                                                                        fm.employees[p] =
                                                                            item;
                                                                      }
                                                                    }
                                                                    setState(
                                                                        () {});
                                                                  });
                                                                },
                                                                child: Row(
                                                                  mainAxisSize:
                                                                      MainAxisSize
                                                                          .min,
                                                                  children: [
                                                                    Icon(
                                                                      Icons.add,
                                                                      size: 16,
                                                                    ),
                                                                    addSpaceWidth(
                                                                        4),
                                                                    Text(
                                                                      "Employee",
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              12,
                                                                          fontWeight:
                                                                              FontWeight.bold),
                                                                    ),
                                                                  ],
                                                                ),
                                                                style: TextButton.styleFrom(
                                                                    padding:
                                                                        EdgeInsets
                                                                            .zero,
                                                                    primary:
                                                                        white,
                                                                    backgroundColor:
                                                                        black,
                                                                    shape: RoundedRectangleBorder(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                8)),
                                                                    minimumSize:
                                                                        Size(
                                                                            120,
                                                                            50)),
                                                              );
                                                            })),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              addSpaceWidth(10),
                                              if (model.getInt(STATUS) ==
                                                  PENDING)
                                                Container(
                                                  // margin: EdgeInsets.only(bottom: 15),
                                                  child: TextButton(
                                                    onPressed: () {
                                                      assignInfo
                                                          .removeAt(index);
                                                      model
                                                        ..put(STATUS, PENDING)
                                                        ..put(
                                                            ASSIGNED_INFO,
                                                            assignInfo
                                                                .map((e) =>
                                                                    e.items)
                                                                .toList())
                                                        ..updateItems();
                                                      setState(() {});
                                                    },
                                                    child: Icon(
                                                      Icons.clear,
                                                      size: 15,
                                                    ),
                                                    style: TextButton.styleFrom(
                                                        padding:
                                                            EdgeInsets.zero,
                                                        primary: white,
                                                        backgroundColor: red,
                                                        shape:
                                                            RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            8)),
                                                        minimumSize:
                                                            Size(40, 40)),
                                                  ),
                                                )
                                            ],
                                          ),
                                        );
                                      }),
                                    );
                                  }),
                                ),
                                //addLine(20, black.withOpacity(.03), 0, 20, 0, 20),
                              ],
                            );
                          },
                        ),
                      Row(
                        children: [
                          Expanded(
                              child: clickField(
                                  "Client Name",
                                  clientName,
                                  readOnly
                                      ? null
                                      : () {
                                          pushAndResult(context, ShowClients(),
                                              dialog: true,
                                              result: (BaseModel bm) {
                                            clientID = bm.getObjectId();
                                            clientEmail = bm.getEmail();
                                            clientName =
                                                "${bm.getString(FIRST_NAME)} ${bm.getString(LAST_NAME)}";
                                            setState(() {});
                                          });
                                        })),
                          if (false)
                            Expanded(
                              child: inputField(
                                  "Client Name", clientNameController,
                                  onChanged: (s) {
                                setState(() {});
                              }, readOnly: readOnly),
                            ),
                          if (false)
                            Expanded(
                              child: Autocomplete<BaseModel>(
                                initialValue: TextEditingValue(),
                                optionsBuilder: (textEditingValue) async {
                                  String text = textEditingValue.text
                                      .trim()
                                      .toLowerCase();
                                  final shots = await FirebaseFirestore.instance
                                      .collection(WORK_BASE)
                                      .where(SEARCH, arrayContains: text.trim())
                                      .limit(15)
                                      .get();

                                  if (null == shots) return [];

                                  return shots.docs
                                      .map((e) => BaseModel(doc: e))
                                      .toList();
                                },
                                fieldViewBuilder: (context,
                                    textEditingController,
                                    focusNode,
                                    onFieldSubmitted) {
                                  return inputField(
                                      "Client Name", textEditingController,
                                      focusNode: focusNode, onChanged: (s) {
                                    setState(() {});
                                  }, readOnly: readOnly);
                                },
                                displayStringForOption: (bm) =>
                                    "${bm.getString(FIRST_NAME)} ${bm.getString(LAST_NAME)}",
                                optionsViewBuilder: (context,
                                    AutocompleteOnSelected<BaseModel>
                                        onSelected,
                                    Iterable<BaseModel> options) {
                                  return Align(
                                    alignment: Alignment.topLeft,
                                    child: Material(
                                      child: Container(
                                        width: 300,
                                        color: Colors.teal,
                                        child: ListView.builder(
                                          padding: EdgeInsets.all(10.0),
                                          itemCount: options.length,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            final BaseModel bm =
                                                options.elementAt(index);
                                            String name =
                                                "${bm.getString(FIRST_NAME)} ${bm.getString(LAST_NAME)}";

                                            return GestureDetector(
                                              onTap: () {
                                                clientNameController.text =
                                                    name;
                                                clientID = bm.getObjectId();
                                                onSelected(bm);
                                              },
                                              child: ListTile(
                                                title: Text(name,
                                                    style: const TextStyle(
                                                        color: Colors.white)),
                                              ),
                                            );
                                          },
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          addSpaceWidth(10),
                          Expanded(
                            child: inputField(
                              "Client Address",
                              clientAddressController,
                              readOnly: readOnly,
                              onChanged: (s) {
                                setState(() {});
                              },
                            ),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: clickField(
                                "State",
                                state,
                                readOnly
                                    ? null
                                    : () {
                                        showListDialog(
                                            context, usaStates.keys.toList(),
                                            (_) {
                                          state = usaStates.keys.toList()[_];
                                          city = null;
                                          setState(() {});
                                        }, returnIndex: true);
                                      }),
                          ),
                          addSpaceWidth(10),
                          Expanded(
                              child: clickField("City", city, () {
                            if (null == state || state.isEmpty) {
                              showError(context, "Select a state first");
                              return;
                            }
                            showListDialog(context, usaStates[state].toList(),
                                (_) {
                              city = usaStates[state].toList()[_];
                              setState(() {});
                            }, returnIndex: true);
                          })),

                          // Expanded(
                          //   child: inputField(
                          //       "Employees Needed", employeesNeededController,
                          //       readOnly: readOnly, onChanged: (s) {
                          //     setState(() {});
                          //   }, isNum: true),
                          // )
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: clickField(
                                "Start date",
                                startDate == 0
                                    ? ""
                                    : getSimpleDate(startDate,
                                        pattern: "EEE MMMM dd, yyyy"),
                                readOnly
                                    ? null
                                    : () async {
                                        // showTimePicker(context: context, initialTime: null);

                                        final picked = await showDatePicker(
                                            builder: (ctx, child) {
                                              return Theme(
                                                  data: Theme.of(context)
                                                      .copyWith(
                                                    colorScheme:
                                                        ColorScheme.light(
                                                      primary: appColor,
                                                      onPrimary: Colors
                                                          .black, // header text color
                                                      // onSurface: Colors.green, // body text color
                                                    ),
                                                    buttonTheme:
                                                        ButtonThemeData(
                                                            textTheme:
                                                                ButtonTextTheme
                                                                    .primary),
                                                  ),
                                                  child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      child: child));
                                            },
                                            context: context,
                                            initialDate: startDate == 0
                                                ? DateTime.now()
                                                : DateTime
                                                    .fromMillisecondsSinceEpoch(
                                                        startDate),
                                            firstDate: DateTime.now(),
                                            lastDate: DateTime(
                                                DateTime.now().year + 2));
                                        if (picked != null)
                                          startDate =
                                              picked.millisecondsSinceEpoch;
                                        setState(() {});
                                      }),
                          ),
                          addSpaceWidth(10),
                          Expanded(
                            child: clickField(
                                "End date",
                                endDate == 0
                                    ? ""
                                    : getSimpleDate(endDate,
                                        pattern: "EEE MMMM dd, yyyy"),
                                readOnly
                                    ? null
                                    : () async {
                                        final picked = await showDatePicker(
                                            builder: (ctx, child) {
                                              return Theme(
                                                  data: Theme.of(context)
                                                      .copyWith(
                                                    colorScheme:
                                                        ColorScheme.light(
                                                      primary: appColor,
                                                      onPrimary: Colors
                                                          .black, // header text color
                                                      // onSurface: Colors.green, // body text color
                                                    ),
                                                    buttonTheme:
                                                        ButtonThemeData(
                                                            textTheme:
                                                                ButtonTextTheme
                                                                    .primary),
                                                  ),
                                                  child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      child: child));
                                            },
                                            context: context,
                                            initialDate: endDate == 0
                                                ? DateTime.now()
                                                : DateTime
                                                    .fromMillisecondsSinceEpoch(
                                                        endDate),
                                            firstDate: DateTime.now(),
                                            lastDate: DateTime(
                                                DateTime.now().year + 2));
                                        if (picked != null)
                                          endDate =
                                              picked.millisecondsSinceEpoch;
                                        setState(() {});
                                      }),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          // Expanded(
                          //   child: inputField("Client Tel", clientTelController,
                          //       onChanged: (s) {
                          //         setState(() {});
                          //       }, isNum: true),
                          // ),
                          // addSpaceWidth(10),
                          Expanded(
                            child: inputField("Report To", reportToController,
                                onChanged: (s) {
                              setState(() {});
                            }),
                          ),
                          Expanded(
                            child: Container(),
                          )
                        ],
                      ),
                      addSpace(30),
                      Text(
                        "Skill Set",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      addSpace(5),
                      Text(
                        "Choose an skill set required for this work",
                        style: TextStyle(
                            fontSize: 13,
                            color: black.withOpacity(.5),
                            fontWeight: FontWeight.bold),
                      ),
                      addSpace(20),
                      Wrap(
                        children: List.generate(
                            appSettingsModel.getList("skills").length, (p) {
                          String title = appSettingsModel.getList("skills")[p];
                          bool active = selectedSkills.contains(title);
                          return GestureDetector(
                            onTap: () {
                              if (readOnly) return;
                              if (active)
                                selectedSkills.remove(title);
                              else
                                selectedSkills.add(title);
                              setState(() {});
                            },
                            child: Container(
                              height: 40,
                              width: 140,
                              margin: EdgeInsets.all(2),
                              child: Text(
                                title,
                                style: TextStyle(
                                    color: active ? white : black,
                                    fontWeight: active
                                        ? FontWeight.bold
                                        : FontWeight.normal),
                              ),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  color: active ? (black) : white,
                                  boxShadow: [
                                    BoxShadow(
                                        color: black.withOpacity(.05),
                                        spreadRadius: 2,
                                        blurRadius: 5)
                                  ],
                                  borderRadius: BorderRadius.circular(8)),
                            ),
                          );
                        }),
                      ),
                      addLine(20, black.withOpacity(.03), 0, 20, 0, 20),
                      Row(
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "WorkShifts",
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  "Add shifts for this work order.",
                                  style: TextStyle(
                                      fontSize: 12,
                                      color: black.withOpacity(.5),
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                          if (!readOnly)
                            if (shiftsInfo.length < 3)
                              TextButton(
                                onPressed: () {
                                  if (shiftsInfo.isNotEmpty &&
                                      !shiftsInfo[shiftsInfo.length - 1]
                                          .isValidated) {
                                    showError(context,
                                        "Some fields on the current shift item are empty!");
                                    return;
                                  }

                                  shiftsInfo.add(Shifts());
                                  setState(() {});
                                },
                                child: Text(
                                  "Add Shifts",
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                ),
                                style: TextButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                    primary: white,
                                    backgroundColor: black,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    minimumSize: Size(160, 50)),
                              )
                        ],
                      ),
                      addSpace(20),
                      // if (!readOnly)
                      Container(
                        margin: EdgeInsets.only(bottom: 20),
                        child: Builder(builder: (ctx) {
                          if (shiftsInfo.isEmpty)
                            return Container(
                              padding: EdgeInsets.all(0),
                              child: DottedBorder(
                                color: black.withOpacity(.5),
                                strokeWidth: 1,
                                borderType: BorderType.RRect,
                                radius: Radius.circular(10),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Container(
                                      height: 150,
                                      alignment: Alignment.center,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.timer_off,
                                            color: black.withOpacity(.5),
                                            size: 30,
                                          ),
                                          addSpace(10),
                                          Text(
                                            "You have not added any shifts yet",
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: black.withOpacity(.5)
                                                // fontWeight: FontWeight.bold,
                                                ),
                                          )
                                        ],
                                      )),
                                ),
                              ),
                            );

                          return Column(
                            children: List.generate(shiftsInfo.length, (index) {
                              final fm = shiftsInfo[index];

                              return Container(
                                margin: EdgeInsets.only(bottom: 10),
                                padding: EdgeInsets.all(4),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    border: Border.all(
                                        color: black.withOpacity(.09))),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              Expanded(
                                                child: clickField(
                                                    "Start time", fm.startTime,
                                                    () async {
                                                  if (readOnly) return;
                                                  final picked = await showTimePicker(
                                                      context: context,
                                                      initialTime: fm.startTime !=
                                                                  null &&
                                                              fm.startTime
                                                                  .isNotEmpty
                                                          ? stringToTimeOfDay(
                                                              fm.startTime)
                                                          : TimeOfDay.now());

                                                  if (picked != null)
                                                    fm.startTime =
                                                        picked.format(context);
                                                  setState(() {});
                                                }),
                                              ),
                                              addSpaceWidth(10),
                                              Expanded(
                                                child: clickField(
                                                    "End Time", fm.endTime,
                                                    () async {
                                                  if (readOnly) return;

                                                  final picked = await showTimePicker(
                                                      context: context,
                                                      initialTime: fm.endTime !=
                                                                  null &&
                                                              fm.endTime
                                                                  .isNotEmpty
                                                          ? stringToTimeOfDay(
                                                              fm.endTime)
                                                          : TimeOfDay.now());

                                                  if (picked != null)
                                                    fm.endTime =
                                                        picked.format(context);
                                                  setState(() {});
                                                }),
                                              ),
                                              addSpaceWidth(10),
                                              Expanded(
                                                child: inputField(
                                                    "Employees Needed",
                                                    fm.neededController,
                                                    readOnly: readOnly,
                                                    onChanged: (s) {
                                                  setState(() {});
                                                }),
                                              )
                                            ],
                                          ),
                                          addSpace(5),
                                          groupFields("Choose shifts",
                                              shiftPeriod, fm.shiftIndex, (p) {
                                            if (readOnly) return;

                                            final shiftP =
                                                shiftsInfo.indexWhere(
                                                    (e) => e.shiftIndex == p);

                                            if (shiftsInfo.length > 1 &&
                                                shiftP != -1) {
                                              showError(context,
                                                  "Shift at $shiftP already has this shift selected!");
                                              return;
                                            }
                                            fm.shiftIndex = p;
                                            setState(() {});
                                          }, width: 150),
                                          // inputField("Employees Needed",
                                          //     fm.neededController,
                                          //     onChanged: (s) {
                                          //   setState(() {});
                                          // })
                                        ],
                                      ),
                                    ),
                                    addSpaceWidth(10),
                                    if (!readOnly)
                                      Container(
                                        margin: EdgeInsets.only(bottom: 0),
                                        child: TextButton(
                                          onPressed: () {
                                            if (readOnly) return;

                                            shiftsInfo.removeAt(index);
                                            setState(() {});
                                          },
                                          child: Text(
                                            "Remove",
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          style: TextButton.styleFrom(
                                              padding: EdgeInsets.zero,
                                              primary: white,
                                              backgroundColor: red,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(8)),
                                              minimumSize: Size(100, 50)),
                                        ),
                                      )
                                  ],
                                ),
                              );
                            }),
                          );
                        }),
                      ),
                      addLine(20, black.withOpacity(.03), 0, 20, 0, 20),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      }),
    );
  }
}
