import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/EmailApi.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/main.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class ApplyJob extends StatefulWidget {
  const ApplyJob({Key key}) : super(key: key);

  @override
  _ApplyJobState createState() => _ApplyJobState();
}

class _ApplyJobState extends State<ApplyJob> {
  final firstNameController = TextEditingController();
  final middleNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final telController = TextEditingController();
  final emailController = TextEditingController();
  final addressController = TextEditingController();
  final zipController = TextEditingController();

  String selectedState;
  String selectedCity;
  String selectedSkill = "";

  int employmentIndex = -1;
  int vaccinationIndex = -1;
  int negativeIndex = -1;
  bool passVisible = false;
  bool termsAccepted = false;

  List days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];
  List months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  List availableDays = [];
  List availableMonths = [];
  List selectedShifts = [];
  List selectedSkills = [];

  List availableShifts = [
    "First Shift",
    "Second Shift",
    "Third/Overnight Shift",
  ];

  // List shiftAvailable = [];
  int startDate = 0;

  String get formId => routeDataHolder["/applyJob"];
  List<StreamSubscription> subs = [];

  @override
  initState() {
    super.initState();
    loadSettings();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadSettings() async {
    var settingsSub = FirebaseFirestore.instance
        .collection(APP_SETTINGS_BASE)
        .doc(APP_SETTINGS)
        .snapshots()
        .listen((shot) {
      if (shot != null) {
        appSettingsModel = BaseModel(doc: shot);
        List banned = appSettingsModel.getList(BANNED);
        setState(() {});
      }
    });
    subs.add(settingsSub);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: LayoutBuilder(builder: (c, box) {
              final size = box.maxWidth;
              print("size $size");
              bool isLargeScreen = box.maxWidth > 850;
              final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

              return Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                // padding: EdgeInsets.only(
                //     top: 130, bottom: 50, left: size * 0.2, right: size * 0.2),
                padding: EdgeInsets.only(
                  // left: isLargeScreen ? size * 0.25 : 20,
                  // right: isLargeScreen ? size * 0.25 : 20,

                  left: isLargeScreen ? size * 0.15 : 20,
                  right: isLargeScreen ? size * 0.15 : 20,

                  top: 120,
                  bottom: 20,
                ),
                decoration: BoxDecoration(
                    color: isLargeScreen ? scaffoldColor : white,
                    borderRadius: BorderRadius.circular(10)),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: isLargeScreen ? white : null,
                          borderRadius: BorderRadius.circular(10)),
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Basic Information",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          addSpace(20),
                          Row(
                            children: [
                              Expanded(
                                child: inputField(
                                    "First Name", firstNameController,
                                    onChanged: (s) {
                                  setState(() {});
                                }),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child: inputField(
                                    "Middle Name", middleNameController,
                                    onChanged: (s) {
                                  setState(() {});
                                }),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child:
                                    inputField("Last Name", lastNameController,
                                        onChanged: (s) {
                                  setState(() {});
                                }),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: inputField("Telephone", telController,
                                    isNum: true, onChanged: (s) {
                                  setState(() {});
                                }),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child:
                                    inputField("Email Address", emailController,
                                        onChanged: (s) {
                                  setState(() {});
                                }),
                              ),
                            ],
                          ),
                          inputField("Home Address", addressController,
                              onChanged: (s) {
                            setState(() {});
                          }, maxLines: 2),
                          Row(
                            children: [
                              Expanded(
                                child: clickField("State", selectedState, () {
                                  showListDialog(
                                      context, usaStates.keys.toList(), (_) {
                                    selectedState = usaStates.keys.toList()[_];
                                    setState(() {});
                                  }, returnIndex: true);
                                }),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child: clickField("City", selectedCity, () {
                                  if (null == selectedState) {
                                    showError(context, "Select a state first");
                                    return;
                                  }
                                  showListDialog(context,
                                      usaStates[selectedState].toList(), (_) {
                                    selectedCity =
                                        usaStates[selectedState].toList()[_];
                                    setState(() {});
                                  }, returnIndex: true);
                                }),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child: inputField("ZipCode", zipController,
                                    onChanged: (s) {
                                  setState(() {});
                                }),
                              ),
                            ],
                          ),
                          // addSpace(10),

                          Text(
                            "Choose your skill set",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                          addSpace(10),
                          Wrap(
                            children: List.generate(
                                appSettingsModel.getList("skills").length, (p) {
                              String title =
                                  appSettingsModel.getList("skills")[p];
                              // bool active = title == selectedSkill;
                              bool active = selectedSkills.contains(title);

                              return GestureDetector(
                                onTap: () {
                                  // selectedSkill = title;
                                  if (active) {
                                    selectedSkills.remove(title);
                                  } else {
                                    selectedSkills.add(title);
                                  }
                                  setState(() {});
                                },
                                child: Container(
                                  height: 40,
                                  width: 140,
                                  margin: EdgeInsets.all(2),
                                  child: Text(
                                    title,
                                    style: TextStyle(
                                        color: active ? white : black,
                                        fontWeight: active
                                            ? FontWeight.bold
                                            : FontWeight.normal),
                                  ),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: active ? (black) : white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: black.withOpacity(.05),
                                            spreadRadius: 2,
                                            blurRadius: 5)
                                      ],
                                      borderRadius: BorderRadius.circular(8)),
                                ),
                              );
                            }),
                          ),
                          addSpace(30),
                          Text(
                            "Are you seeking for employment?",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                          addSpace(20),
                          Wrap(
                            children: List.generate(2, (p) {
                              String title = p == 0 ? "Yes" : "No";
                              bool active = employmentIndex == p;
                              return GestureDetector(
                                onTap: () {
                                  employmentIndex = p;
                                  setState(() {});
                                },
                                child: Container(
                                  height: 40,
                                  width: 140,
                                  margin:
                                      EdgeInsets.only(right: p == 0 ? 10 : 0),
                                  child: Text(
                                    title,
                                    style: TextStyle(
                                        color: active ? white : black,
                                        fontWeight: active
                                            ? FontWeight.bold
                                            : FontWeight.normal),
                                  ),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: active ? (black) : white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: black.withOpacity(.05),
                                            spreadRadius: 2,
                                            blurRadius: 5)
                                      ],
                                      borderRadius: BorderRadius.circular(8)),
                                ),
                              );
                            }),
                          ),
                          if (employmentIndex == 0) ...[
                            addSpace(20),
                            Text(
                              "Choose days you would be available",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold),
                            ),
                            addSpace(10),
                            Wrap(
                              children: List.generate(days.length, (p) {
                                String title = days[p];
                                bool active = availableDays.contains(title);
                                return GestureDetector(
                                  onTap: () {
                                    if (active) {
                                      availableDays.remove(title);
                                    } else {
                                      availableDays.add(title);
                                    }
                                    setState(() {});
                                  },
                                  child: Container(
                                    height: 40,
                                    width: 140,
                                    margin: EdgeInsets.all(2),
                                    child: Text(
                                      title,
                                      style: TextStyle(
                                          color: active ? white : black,
                                          fontWeight: active
                                              ? FontWeight.bold
                                              : FontWeight.normal),
                                    ),
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: active ? (black) : white,
                                        boxShadow: [
                                          BoxShadow(
                                              color: black.withOpacity(.05),
                                              spreadRadius: 2,
                                              blurRadius: 5)
                                        ],
                                        borderRadius: BorderRadius.circular(8)),
                                  ),
                                );
                              }),
                            ),
                            addSpace(20),
                            Text(
                              "Choose months you would be available",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold),
                            ),
                            addSpace(10),
                            Wrap(
                              children: List.generate(months.length, (p) {
                                String title = months[p];
                                bool active = availableMonths.contains(title);
                                return GestureDetector(
                                  onTap: () {
                                    if (active) {
                                      availableMonths.remove(title);
                                    } else {
                                      availableMonths.add(title);
                                    }
                                    setState(() {});
                                  },
                                  child: Container(
                                    height: 40,
                                    width: 140,
                                    margin: EdgeInsets.all(2),
                                    child: Text(
                                      title,
                                      style: TextStyle(
                                          color: active ? white : black,
                                          fontWeight: active
                                              ? FontWeight.bold
                                              : FontWeight.normal),
                                    ),
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: active ? (black) : white,
                                        boxShadow: [
                                          BoxShadow(
                                              color: black.withOpacity(.05),
                                              spreadRadius: 2,
                                              blurRadius: 5)
                                        ],
                                        borderRadius: BorderRadius.circular(8)),
                                  ),
                                );
                              }),
                            ),
                            addSpace(20),
                            Text(
                              "What shifts are you seeking for?",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold),
                            ),
                            addSpace(10),
                            Wrap(
                              children:
                                  List.generate(availableShifts.length, (p) {
                                String title = availableShifts[p];
                                bool active = selectedShifts.contains(title);
                                return GestureDetector(
                                  onTap: () {
                                    if (active) {
                                      selectedShifts.remove(title);
                                    } else {
                                      selectedShifts.add(title);
                                    }

                                    setState(() {});
                                  },
                                  child: Container(
                                    height: 40,
                                    width: 160,
                                    margin: EdgeInsets.all(2),
                                    padding: EdgeInsets.all(2),
                                    child: Text(
                                      title,
                                      style: TextStyle(
                                          color: active ? white : black,
                                          fontWeight: active
                                              ? FontWeight.bold
                                              : FontWeight.normal),
                                    ),
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: active ? black : white,
                                        boxShadow: [
                                          BoxShadow(
                                              color: black.withOpacity(.05),
                                              spreadRadius: 2,
                                              blurRadius: 5)
                                        ],
                                        borderRadius: BorderRadius.circular(8)),
                                  ),
                                );
                              }),
                            ),
                            addSpace(20),
                            Text(
                              "Have you received your Covid-19 vaccination?",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold),
                            ),
                            addSpace(10),
                            Wrap(
                              children: List.generate(2, (p) {
                                String title = p == 0 ? "Yes" : "No";
                                bool active = vaccinationIndex == p;
                                return GestureDetector(
                                  onTap: () {
                                    vaccinationIndex = p;
                                    setState(() {});
                                  },
                                  child: Container(
                                    height: 40,
                                    width: 140,
                                    margin:
                                        EdgeInsets.only(right: p == 0 ? 10 : 0),
                                    padding: EdgeInsets.all(2),
                                    child: Text(
                                      title,
                                      style: TextStyle(
                                          color: active ? white : black,
                                          fontWeight: active
                                              ? FontWeight.bold
                                              : FontWeight.normal),
                                    ),
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: active ? (black) : white,
                                        boxShadow: [
                                          BoxShadow(
                                              color: black.withOpacity(.05),
                                              spreadRadius: 2,
                                              blurRadius: 5)
                                        ],
                                        borderRadius: BorderRadius.circular(8)),
                                  ),
                                );
                              }),
                            ),
                            addSpace(20),
                            clickField(
                                "When do you want to Start work?",
                                startDate == 0
                                    ? ""
                                    : getSimpleDate(startDate,
                                        pattern: "EEE MMMM dd, yyyy"),
                                () async {
                              final DateTime picked = await showDatePicker(
                                  builder: (ctx, child) {
                                    return Theme(
                                        data: Theme.of(context).copyWith(
                                          colorScheme: ColorScheme.light(
                                            primary: appColor,
                                            onPrimary: Colors
                                                .black, // header text color
                                            // onSurface: Colors.green, // body text color
                                          ),
                                          buttonTheme: ButtonThemeData(
                                              textTheme:
                                                  ButtonTextTheme.primary),
                                        ),
                                        child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            child: child));
                                  },
                                  context: context,
                                  initialDate: startDate == 0
                                      ? DateTime.now()
                                      : DateTime.fromMillisecondsSinceEpoch(
                                          startDate),
                                  firstDate: DateTime.now(),
                                  lastDate: DateTime(DateTime.now().year + 2));

                              if (picked != null &&
                                  picked.millisecondsSinceEpoch != startDate)
                                startDate = picked.millisecondsSinceEpoch;
                              setState(() {});
                            }),
                            addSpace(20),
                            Text(
                              "Do you have a recent negative Covid-19 Test Result?",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold),
                            ),
                            addSpace(10),
                            Wrap(
                              children: List.generate(2, (p) {
                                String title = p == 0 ? "Yes" : "No";
                                bool active = negativeIndex == p;
                                return GestureDetector(
                                  onTap: () {
                                    negativeIndex = p;
                                    setState(() {});
                                  },
                                  child: Container(
                                    height: 40,
                                    width: 140,
                                    margin:
                                        EdgeInsets.only(right: p == 0 ? 10 : 0),
                                    child: Text(
                                      title,
                                      style: TextStyle(
                                          color: active ? white : black,
                                          fontWeight: active
                                              ? FontWeight.bold
                                              : FontWeight.normal),
                                    ),
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: active ? (black) : white,
                                        boxShadow: [
                                          BoxShadow(
                                              color: black.withOpacity(.05),
                                              spreadRadius: 2,
                                              blurRadius: 5)
                                        ],
                                        borderRadius: BorderRadius.circular(8)),
                                  ),
                                );
                              }),
                            ),
                          ],
                          addSpace(50),
                        ],
                      ),
                    ),
                    addSpace(20),
                    Container(
                      height: 60,
                      child: TextButton(
                        onPressed: () {
                          String email = emailController.text;
                          String fName = firstNameController.text;
                          String mName = middleNameController.text;
                          String lName = lastNameController.text;
                          String tel = telController.text;
                          String address = addressController.text;
                          String zip = zipController.text;

                          if (fName.isEmpty) {
                            showError(context, "Enter First name!");
                            return;
                          }
                          if (mName.isEmpty) {
                            showError(context, "Enter Middle name!");
                            return;
                          }
                          if (lName.isEmpty) {
                            showError(context, "Enter Last name!");
                            return;
                          }
                          if (tel.isEmpty) {
                            showError(context, "Enter Telephone Number!");
                            return;
                          }
                          if (email.isEmpty) {
                            showError(context, "Enter Email address!");
                            return;
                          }

                          if (address.isEmpty) {
                            showError(context, "Enter Home Address!");
                            return;
                          }

                          if (null == selectedState) {
                            showError(context, "Choose Your State!");
                            return;
                          }

                          if (null == selectedCity) {
                            showError(context, "Choose Your City!");
                            return;
                          }

                          if (zip.isEmpty) {
                            showError(context, "Enter State ZipCode!");
                            return;
                          }

                          if (selectedSkills.isEmpty) {
                            showError(context, "Choose employee skill!");
                            return;
                          }

                          if (employmentIndex == -1) {
                            showError(
                                context, "Are you seeking for employment?");
                            return;
                          }

                          if (availableDays.isEmpty) {
                            showError(context, "Choose days of availability!");
                            return;
                          }

                          if (availableMonths.isEmpty) {
                            showError(
                                context, "Choose months of availability!");
                            return;
                          }

                          if (selectedShifts.isEmpty) {
                            showError(context,
                                "Choose shifts you'd be available to work!");
                            return;
                          }
                          if (vaccinationIndex == -1) {
                            showError(context,
                                "Have you received your Covid-19 vaccination?");
                            return;
                          }

                          if (startDate == 0) {
                            showError(context, "Choose a date to start work");
                            return;
                          }

                          if (negativeIndex == -1) {
                            showError(context,
                                "Have you received a recent negative Covid-19 test?");
                            return;
                          }

                          String search = "${fName.toLowerCase()}"
                              " ${lName.toLowerCase()} "
                              " ${address.toLowerCase()} "
                              "${selectedState.toLowerCase()} ";

                          List tags = [];
                          tags.addAll(selectedSkills);
                          tags.addAll(selectedShifts);
                          tags.add(selectedState);

                          showProgress(true, context, msg: "Please wait...");
                          String id = getRandomId();
                          BaseModel model = BaseModel();
                          model.put(OBJECT_ID, id);
                          model.put(JOB_ID, formId);
                          model.put(TELEPHONE, tel);
                          model.put(EMAIL, email);
                          model.put(FIRST_NAME, fName);
                          model.put(MIDDLE_NAME, mName);
                          model.put(LAST_NAME, lName);
                          model.put(ADDRESS, address);
                          model.put(STATE, selectedState);
                          model.put(CITY, selectedCity);
                          model.put(ZIPCODE, zip);

                          model.put(SHIFTS, selectedShifts);
                          model.put(SKILLS, selectedSkills);
                          model.put(DAYS, availableDays);
                          model.put(MONTHS, availableMonths);

                          model.put("tags", tags);

                          model.put(EMPLOYMENT_INDEX, employmentIndex);
                          model.put(NEGATIVE_INDEX, negativeIndex);
                          model.put(VACCINATION_INDEX, vaccinationIndex);
                          model.put(SHIFTS, selectedShifts);
                          model.put(SEARCH, getSearchString(search));
                          model.put(TYPE, TYPE_NEW_EMPLOYEES);
                          model.saveItem(EMPLOYEE_BASE, false, document: id,
                              onComplete: (e) {
                            showProgress(
                              false,
                              context,
                            );
                            if (null != e) {
                              showErrorDialog(context, e);
                              return;
                            }
                            appSettingsModel
                              ..putInList(
                                EMPLOYEE_BASE_COUNT,
                                id,
                              )
                              ..updateItems();

                            appSettingsModel
                              ..putInList(
                                EMPLOYEE_BASE_COUNT_P,
                                id,
                              )
                              ..updateItems();

                            //send email
                            String baseUrl =
                                appSettingsModel.getString("baseUrl");
                            String url = "${baseUrl}setter";

                            FirebaseFirestore.instance
                                .collection(USER_BASE)
                                .where(PRIVILEGE_INDEX, isEqualTo: 0)
                                .get()
                                .then((value) async {
                              final docs = value.docs
                                  .map((e) => BaseModel(doc: e))
                                  .toList();
                              final items = docs
                                  .map((e) => {
                                        "email": e.getEmail(),
                                        "name": e.getString(FIRST_NAME)
                                      })
                                  .toList();

                              String covid =
                                  "I have received my covid-19 vaccination";
                              if (vaccinationIndex == 1)
                                covid =
                                    "I have not received my covid-19 vaccination";

                              await EmailApi.sendEmailAll(
                                type: EmailApi.TYPE_EMPLOYEE_APPLY,
                                items: items,
                                url: url,
                                firstName: fName,
                                lastName: lName,
                                email: email,
                                state: selectedState,
                                city: selectedCity,
                                skillSet: selectedSkills
                                    .toString()
                                    .replaceAll("[", "")
                                    .replaceAll("]", ""),
                                tel: tel,
                                covid: covid,
                              );
                            });

                            showMessage(
                              context,
                              Icons.check,
                              green,
                              "Application Submitted!",
                              "You have successful submitted your job application",
                              // cancellable: false,
                              delayInMilli: 700,
                              clickYesText: "Go Back",
                              cancellable: false,
                              onClicked: (_) {
                                // Navigator.pop(context, true);
                                Navigator.pushReplacementNamed(
                                    context, "/jobListings");
                              },
                            );
                          });
                        },
                        child: Center(
                          child: Text(
                            "Apply for Job".toUpperCase(),
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            minimumSize: Size(300, 50)),
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
          appBarHeader(context)
        ],
      ),
    );
  }
}
