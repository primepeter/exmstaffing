import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/main.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:styled_text/styled_text.dart';

class CompanyPolicy extends StatefulWidget {
  const CompanyPolicy({Key key}) : super(key: key);

  @override
  _CompanyPolicyState createState() => _CompanyPolicyState();
}

class _CompanyPolicyState extends State<CompanyPolicy> {
  // String applicantName = "Steven Danny";
  int startDate = 0;

  bool termsAccepted = false;

  List<StreamSubscription> subs = [];
  String path = "companyPolicy";
  BaseModel mainModel;
  BaseModel formModel;
  String get formId => routeDataHolder["/companyPolicy"];
  String get name => mainModel.getString(FIRST_NAME);
  String get offerAmount => mainModel.getString(OFFER_AMOUNT);
  List<BaseModel> formsToFill = [];

  @override
  initState() {
    super.initState();
    loadItems();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadItems() async {
    if (null == formId || formId.isEmpty) {
      showErrorDialog(
          context, "Ops the application link is invalid or has expired!",
          cancellable: false);
      return;
    }

    var sub = FirebaseFirestore.instance
        .collection(EMPLOYEE_BASE)
        .where(OBJECT_ID, isEqualTo: formId)
        .limit(1)
        .snapshots()
        .listen((value) {
      if (value.size == 0) {
        showErrorDialog(
            context, "Ops the application link is invalid or has expired!",
            cancellable: false);
        return;
      }
      mainModel = BaseModel(doc: value.docs[0]);
      startDate = mainModel.getInt(AGREEMENT_DATE);
      if (startDate == 0) startDate = DateTime.now().millisecondsSinceEpoch;

      formsToFill = mainModel.getListModel(FORMS_TO_FILL);
      int p = formsToFill.indexWhere((e) => e.getString("path") == path);
      if (p != -1) {
        formModel = formsToFill[p];
        // startDate = formModel.getInt(AGREEMENT_DATE);
        // if (startDate == 0) startDate = DateTime.now().millisecondsSinceEpoch;
      }
      setState(() {});
    });
    subs.add(sub);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: LayoutBuilder(builder: (c, box) {
              final size = box.maxWidth;
              print("size $size");
              bool isLargeScreen = box.maxWidth > 850;
              final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

              return Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                padding: EdgeInsets.only(
                  // left: isLargeScreen ? size * 0.25 : 20,
                  // right: isLargeScreen ? size * 0.25 : 20,

                  left: isLargeScreen ? size * 0.15 : 20,
                  right: isLargeScreen ? size * 0.15 : 20,
                  top: 120,
                  bottom: 20,
                ),
                decoration: BoxDecoration(
                    color: isLargeScreen ? scaffoldColor : white,
                    borderRadius: BorderRadius.circular(10)),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: isLargeScreen ? white : null,
                          borderRadius: BorderRadius.circular(10)),
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              style: TextButton.styleFrom(
                                minimumSize: Size(40, 40),
                                backgroundColor: white,
                                primary: black,
                                elevation: 4,
                                // shape: CircleBorder()
                              ),
                              child: Icon(Icons.navigate_before)),
                          addSpace(10),
                          Text(
                            "EXCEPTIONAL MEDICAL STAFFING INC COMPANY POLICY",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          addSpace(20),
                          textData(),
                          addSpace(50),
                        ],
                      ),
                    ),
                    addSpace(20),
                    InkWell(
                      onTap: () {
                        termsAccepted = !termsAccepted;
                        setState(() {});
                      },
                      child: Container(
                        child: Row(
                          children: [
                            checkBox(termsAccepted),
                            addSpaceWidth(20),
                            Expanded(
                                child: Text(
                              "By clicking on SAVE INFORMATION below, you understand and accept"
                              " the terms of the Exceptional Medical Staffing Inc Company policy."
                              "We look forward to you joining our team and making "
                              "a positive contribution to the company.",
                              style: TextStyle(fontSize: 12),
                            ))
                          ],
                        ),
                      ),
                    ),
                    addSpace(20),
                    Container(
                      height: 60,
                      child: TextButton(
                        onPressed: !termsAccepted
                            ? null
                            : () {
                                mainModel.put(AGREEMENT_DATE,
                                    DateTime.now().millisecondsSinceEpoch);

                                formModel.put(STATUS, STATUS_COMPLETED);
                                int p = formsToFill.indexWhere(
                                    (e) => e.getString("path") == path);
                                if (p != -1) {
                                  // formModel.put(AGREEMENT_DATE,
                                  //     DateTime.now().millisecondsSinceEpoch);
                                  formsToFill[p] = formModel;
                                }

                                mainModel.put(FORMS_TO_FILL,
                                    formsToFill.map((e) => e.items).toList());

                                mainModel.updateItems();

                                showMessage(
                                  context,
                                  Icons.check,
                                  green,
                                  "Form Submitted!",
                                  "You have successful completed your Caregiver Agreement form",
                                  // cancellable: false,
                                  delayInMilli: 700,
                                  clickYesText: "Go Back",
                                  cancellable: false,
                                  onClicked: (_) {
                                    // Navigator.pop(context, true);
                                    Navigator.pushReplacementNamed(
                                        context, "/applicationForm??$formId");
                                  },
                                );
                              },
                        child: Center(
                          child: Text(
                            "Save Information".toUpperCase(),
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            minimumSize: Size(300, 50)),
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
          appBarHeader(context)
        ],
      ),
    );
  }

  StyledText textData() {
    return StyledText(
      text: """
Read Carefully all the requirements stated below:

• You will respond in a timely manner to pick up work shifts.
• When you accept a work shift you must make all effort to get to the work facility on-time and on the day of the shift without fail.
• If you must cancel or call out of a work shift you have previously accepted, you must do so at least 4 hours before the start of the shift.
• Failure to call-out at least 4 hours before the start of your shift will attract a \$30.00 (Thirty Dollars) fine deducted from your next paycheck.
• NO CALL, NO SHOW will attract a fine of \$100.00 (One Hundred Dollars) deducted from your next paycheck. NO CALL, NO SHOW is also grounds for termination.
• You must dress professionally to the work place all the time. Any color scrubs (top and pants) and covered shoes are acceptable. If you don’t wear scrubs, you are allowed to wear white shirt and black pants.
• When you are assigned to work at a facility, you must follow all the facility policy, procedures, and guidelines within and outside the building.
• You must complete every assigned work shift. You cannot abandon or leave the building without prior approval from the facility/client supervisor on duty. Abandonment will attract a fine of \$50.00 (Fifty Dollars) deducted from your next paycheck. Abandonment is also grounds for termination.
• You must conduct yourself in a professional manner at the work site. You are not permitted to use profane words, display an unruly attitude, get into fights, or cause any form of disruption to others.
• You must be polite, friendly, respectful, courteous, kind, and follow the instructions of the supervisor on duty.
• You agree to get paid two weeks in arrears based on the company’s pay calendar.
• You must have a Timesheet with you anytime you go in for a work shift.
• You must fill out your timesheet according to company guidelines.
• You must get your timesheet signed and dated by the floor supervisor at the end of your assigned shifts.
• You must turn in your signed timesheet by sending it to the email address: TIMESHEETS@EXMSTAFFING.COM every Monday following the end of each work week by 2.00pm Eastern Standard Time.
• I understand that failure to turn in my signed weekly timesheet in a timely manner will result in a delay of my paycheck.
• I understand that due to the No-Compete Policy of the company, I cannot accept any offer of employment from any client of Exceptional Medical Staffing Inc. until after 24 months of my employment with Exceptional Medical Staffing Inc, except the client of Exceptional Medical Staffing Inc has paid Exceptional Medical Staffing Inc. a BUY-OUT FEE, which is equivalent to the two months full time salary of the said employee of Exceptional Medical Staffing Inc. (The Buy-Out Fee is calculated based on Bill the Rate).
• You must provide the best possible care to the residents and report any form of resident abuse to the Supervisor on duty or the Exceptional Medical Staffing Inc. Manager.
• You will refer other experienced and reliable candidates to Exceptional Medical Staffing Inc. for employment consideration.
• You will utilize all software tools provided to you by Exceptional Medical Staffing Inc.

I have read and I understand all the above stated Exceptional Medical Staffing Inc. Policy Requirements. I agree to comply to all the stated requirements.

    """,
      // textAlign: TextAlign.left,
      style: TextStyle(fontSize: 18),
    );
  }
}
