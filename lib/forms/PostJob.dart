import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/main.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class PostJob extends StatefulWidget {
  const PostJob({Key key}) : super(key: key);

  @override
  _PostJobState createState() => _PostJobState();
}

class _PostJobState extends State<PostJob> {
  final titleController = TextEditingController();
  final zipController = TextEditingController();
  final descriptionController = TextEditingController();
  final requirementController = TextEditingController();
  final salaryController = MoneyMaskedTextController(leftSymbol: "\$");
  String selectedState;
  String selectedCity;
  String selectedSkill = "";

  int employmentIndex = -1;
  int vaccinationIndex = -1;
  int negativeIndex = -1;
  bool passVisible = false;
  bool termsAccepted = false;

  List days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];
  List daysAvailable = [];

  List shiftPeriod = [
    "First Shift",
    "Second Shift",
    "Third/Overnight Shift",
  ];

  List shiftAvailable = [];
  int startDate = 0;

  List<StreamSubscription> subs = [];

  String path = "postJob";
  BaseModel mainModel;
  BaseModel formModel;
  String get formId => routeDataHolder["/postJob"];

  String objectId = getRandomId();
  BaseModel model = BaseModel();

  @override
  initState() {
    super.initState();
    loadItems();
    loadSettings();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadSettings() async {
    var settingsSub = FirebaseFirestore.instance
        .collection(APP_SETTINGS_BASE)
        .doc(APP_SETTINGS)
        .snapshots()
        .listen((shot) {
      if (shot != null) {
        appSettingsModel = BaseModel(doc: shot);
        List banned = appSettingsModel.getList(BANNED);
        setState(() {});
      }
    });
    subs.add(settingsSub);
  }

  loadItems() async {
    if (null == formId || formId.isEmpty) {
      return;
    }

    var sub = FirebaseFirestore.instance
        .collection(JOB_BASE)
        .where(OBJECT_ID, isEqualTo: formId)
        .limit(1)
        .snapshots()
        .listen((value) {
      if (value.size == 0) {
        // showErrorDialog(
        //     context, "Ops the application link is invalid or has expired!",
        //     cancellable: false);
        return;
      }
      model = BaseModel(doc: value.docs[0]);
      objectId = model.getObjectId();
      titleController.text = model.getString(TITLE);
      selectedSkill = model.getString(SKILLS);
      startDate = model.getInt("jobDate");
      descriptionController.text = model.getString("jobDescription");
      requirementController.text = model.getString("jobRequirement");
      salaryController.updateValue(model.getDouble("jobSalary"));
      selectedState = model.getString(STATE);
      selectedCity = model.getString(CITY);

      // model.put(TITLE, title);
      // model.put(SKILLS, selectedSkill);
      // model.put("jobDate", startDate);
      // model.put("jobDescription", jobDesc);
      // model.put("jobRequirement", jobReq);
      // model.put("jobSalary", salary);
      // model.put(STATE, selectedState);
      // model.put(CITY, selectedCity);

      setState(() {});
    });
    subs.add(sub);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: LayoutBuilder(builder: (c, box) {
              final size = box.maxWidth;
              print("size $size");
              bool isLargeScreen = box.maxWidth > 850;
              final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

              return Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                // padding: EdgeInsets.only(
                //     top: 130, bottom: 50, left: size * 0.2, right: size * 0.2),
                padding: EdgeInsets.only(
                  left: isLargeScreen ? size * 0.25 : 20,
                  right: isLargeScreen ? size * 0.25 : 20,
                  top: 120,
                  bottom: 20,
                ),
                decoration: BoxDecoration(
                    color: isLargeScreen ? scaffoldColor : white,
                    borderRadius: BorderRadius.circular(10)),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: isLargeScreen ? white : null,
                          borderRadius: BorderRadius.circular(10)),
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Basic Information",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          addSpace(20),
                          Text(
                            "Skill required for job",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                          addSpace(10),
                          Wrap(
                            children: List.generate(
                                appSettingsModel.getList("skills").length, (p) {
                              String title =
                                  appSettingsModel.getList("skills")[p];
                              bool active = title == selectedSkill;
                              return GestureDetector(
                                onTap: () {
                                  selectedSkill = title;
                                  setState(() {});
                                },
                                child: Container(
                                  height: 40,
                                  width: 140,
                                  margin: EdgeInsets.all(2),
                                  child: Text(
                                    title,
                                    style: TextStyle(
                                        color: active ? white : black,
                                        fontWeight: active
                                            ? FontWeight.bold
                                            : FontWeight.normal),
                                  ),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: active ? (black) : white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: black.withOpacity(.05),
                                            spreadRadius: 2,
                                            blurRadius: 5)
                                      ],
                                      borderRadius: BorderRadius.circular(8)),
                                ),
                              );
                            }),
                          ),
                          addSpace(20),
                          inputField("Job Title", titleController,
                              onChanged: (s) {
                            setState(() {});
                          }, maxLines: 4),
                          Row(
                            children: [
                              Expanded(
                                  child: clickField(
                                      "Date of Job?",
                                      startDate == 0
                                          ? ""
                                          : getSimpleDate(startDate,
                                              pattern: "EEE MMMM dd, yyyy"),
                                      () async {
                                final DateTime picked = await showDatePicker(
                                    builder: (ctx, child) {
                                      return Theme(
                                          data: Theme.of(context).copyWith(
                                            colorScheme: ColorScheme.light(
                                              primary: appColor,
                                              onPrimary: Colors
                                                  .black, // header text color
                                              // onSurface: Colors.green, // body text color
                                            ),
                                            buttonTheme: ButtonThemeData(
                                                textTheme:
                                                    ButtonTextTheme.primary),
                                          ),
                                          child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              child: child));
                                    },
                                    context: context,
                                    initialDate: startDate == 0
                                        ? DateTime.now()
                                        : DateTime.fromMillisecondsSinceEpoch(
                                            startDate),
                                    firstDate: DateTime.now(),
                                    lastDate:
                                        DateTime(DateTime.now().year + 2));

                                if (picked != null &&
                                    picked.millisecondsSinceEpoch != startDate)
                                  startDate = picked.millisecondsSinceEpoch;
                                setState(() {});
                              })),
                              addSpaceWidth(10),
                              Expanded(
                                  child: inputField(
                                "Expected Salary",
                                salaryController,
                                isNum: true,
                                onChanged: (s) {
                                  setState(() {});
                                },
                              ))
                            ],
                          ),
                          inputField("Job Description", descriptionController,
                              onChanged: (s) {
                            setState(() {});
                          }, maxLines: 4),
                          inputField("Job Requirement", requirementController,
                              onChanged: (s) {
                            setState(() {});
                          }, maxLines: 4),
                          Row(children: [
                            Expanded(
                                child: clickField("State", selectedState, () {
                              showListDialog(context, usaStates.keys.toList(),
                                  (_) {
                                selectedState = usaStates.keys.toList()[_];
                                setState(() {});
                              }, returnIndex: true);
                            })),
                            addSpaceWidth(10),
                            Expanded(
                                child: clickField("City", selectedCity, () {
                              if (null == selectedState) {
                                showError(context, "Select a state first");
                                return;
                              }
                              showListDialog(
                                  context, usaStates[selectedState].toList(),
                                  (_) {
                                selectedCity =
                                    usaStates[selectedState].toList()[_];
                                setState(() {});
                              }, returnIndex: true);
                            }))
                          ]),
                          addSpace(50),
                        ],
                      ),
                    ),
                    addSpace(20),
                    Container(
                      height: 60,
                      child: TextButton(
                        onPressed: () {
                          String title = titleController.text;
                          String jobDesc = descriptionController.text;
                          String jobReq = requirementController.text;
                          double salary = salaryController.numberValue;

                          if (null == selectedSkill) {
                            showError(context, "Choose Job Title!");
                            return;
                          }

                          if (startDate == 0) {
                            showError(context, "Choose a date to start work");
                            return;
                          }

                          if (title.isEmpty) {
                            showError(context, "Enter Job Title!");
                            return;
                          }

                          if (jobDesc.isEmpty) {
                            showError(context, "Enter Job Description!");
                            return;
                          }
                          if (jobReq.isEmpty) {
                            showError(context, "Enter Job Requirement!");
                            return;
                          }

                          if (null == selectedState) {
                            showError(context, "Choose Your State!");
                            return;
                          }

                          if (null == selectedCity) {
                            showError(context, "Choose Your City!");
                            return;
                          }

                          if (salary == 0) {
                            showError(context, "Enter Expected Salary!");
                            return;
                          }

                          showProgress(true, context, msg: "Please wait...");
                          model.put(OBJECT_ID, objectId);
                          model.put(TITLE, title);
                          model.put(SKILLS, selectedSkill);
                          model.put("jobDate", startDate);
                          model.put("jobDescription", jobDesc);
                          model.put("jobRequirement", jobReq);
                          model.put("jobSalary", salary);
                          model.put(STATE, selectedState);
                          model.put(CITY, selectedCity);
                          model.saveItem(JOB_BASE, true, document: objectId,
                              onComplete: (e) {
                            showProgress(
                              false,
                              context,
                            );
                            if (null != e) {
                              showErrorDialog(context, e);
                              return;
                            }

                            showMessage(
                              context,
                              Icons.check,
                              green,
                              "Job Submitted!",
                              "You have successful posted this job opening",
                              // cancellable: false,
                              delayInMilli: 700,
                              clickYesText: "Go Back",
                              cancellable: false,
                              onClicked: (_) {
                                Navigator.pushReplacementNamed(
                                    context, "/jobListings");
                                //Navigator.pop(context, true);
                              },
                            );
                          });
                        },
                        child: Center(
                          child: Text(
                            "Post Job".toUpperCase(),
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            minimumSize: Size(300, 50)),
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
          appBarHeader(context)
        ],
      ),
    );
  }
}
