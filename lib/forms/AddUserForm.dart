import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EmailApi.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class AddUserForm extends StatefulWidget {
  const AddUserForm({Key key, this.bm, this.readOnly = false})
      : super(key: key);
  final BaseModel bm;
  final bool readOnly;

  @override
  _AddUserFormState createState() => _AddUserFormState();
}

class _AddUserFormState extends State<AddUserForm> {
  final scrollController = ScrollController();
  final messageController = TextEditingController();
  final firstNameController = TextEditingController();
  final middleNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  int privilegeIndex = -1;
  List privileges = appSettingsModel.getList("privileges");

  BaseModel model;
  bool readOnly = false;

  List types = ["Active", "InActive"];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (null != widget.bm) {
      model = widget.bm;
      readOnly = widget.readOnly;
      firstNameController.text = model.getString(FIRST_NAME);
      middleNameController.text = model.getString(MIDDLE_NAME);
      lastNameController.text = model.getString(LAST_NAME);
      emailController.text = model.getString(EMAIL);
      passwordController.text = model.getString(PASSWORD);
      privilegeIndex = model.getInt(PRIVILEGE_INDEX);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: black.withOpacity(.8),
      child: LayoutBuilder(builder: (c, box) {
        bool isLargeScreen = box.maxWidth > 850;
        // final size = getScreenWidth(c);
        final size = box.maxWidth;
        final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

        return RawScrollbar(
          controller: scrollController,
          isAlwaysShown: true,
          thumbColor: Colors.white,
          radius: Radius.circular(20),
          thickness: 20,
          interactive: true,
          child: SingleChildScrollView(
            controller: scrollController,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Container(
                  decoration: BoxDecoration(
                      color: white, borderRadius: BorderRadius.circular(15)),
                  padding: EdgeInsets.all(30),
                  margin: EdgeInsets.only(
                      top: 20,
                      bottom: 20,
                      left: ((0.12) * size),
                      right: ((0.12) * size)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              "Users Information",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            // margin: EdgeInsets.only(bottom: 15),
                            child: TextButton(
                              onPressed: () {
                                Navigator.pop(context, readOnly ? model : null);
                              },
                              child: Icon(
                                Icons.clear,
                                size: 15,
                              ),
                              style: TextButton.styleFrom(
                                  padding: EdgeInsets.zero,
                                  primary: white,
                                  backgroundColor: red,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  minimumSize: Size(40, 40)),
                            ),
                          ),
                          addSpaceWidth(10),
                          if (!readOnly)
                            TextButton(
                              onPressed: () {
                                String email = emailController.text;
                                String fName = firstNameController.text;
                                String mName = middleNameController.text;
                                String lName = lastNameController.text;
                                String password = passwordController.text;

                                if (email.isEmpty) {
                                  showError(context, "Enter Email address!");
                                  return;
                                }
                                if (fName.isEmpty) {
                                  showError(context, "Enter First name!");
                                  return;
                                }

                                // if (mName.isEmpty) {
                                //   showError(context, "Enter Middle name!");
                                //   return;
                                // }
                                if (lName.isEmpty) {
                                  showError(context, "Enter Last name!");
                                  return;
                                }

                                if (password.isEmpty) {
                                  showError(context, "Enter user password!");
                                  return;
                                }

                                if (privilegeIndex == -1) {
                                  showError(context, "Add user privilege!");
                                  return;
                                }

                                String search = "${email.toLowerCase()}"
                                    " ${fName.toLowerCase()} "
                                    "${mName.toLowerCase()} "
                                    "${lName.toLowerCase()}";

                                showProgress(true, context,
                                    msg: "Please wait...");
                                String id = getRandomId();
                                if (null != widget.bm)
                                  id = widget.bm.getObjectId();
                                BaseModel model = BaseModel();
                                model.put(OBJECT_ID, id);
                                model.put(EMAIL, email);
                                model.put(FIRST_NAME, fName);
                                model.put(MIDDLE_NAME, mName);
                                model.put(LAST_NAME, lName);
                                model.put(PASSWORD, password);
                                model.put(PRIVILEGE_INDEX, privilegeIndex);
                                model.put(SEARCH, getSearchString(search));
                                model.saveItem(USER_BASE, true, document: id,
                                    onComplete: (e) {
                                  showProgress(
                                    false,
                                    context,
                                  );
                                  if (null != e) {
                                    showErrorDialog(context, e);
                                    return;
                                  }

                                  appSettingsModel
                                    ..putInList(USER_BASE_COUNT, id)
                                    ..putInList(COORDINATORS_LIST, id,
                                        add: privilegeIndex == 1)
                                    ..updateItems();

                                  String baseUrl =
                                      appSettingsModel.getString("baseUrl");
                                  String url = "${baseUrl}setter";

                                  EmailApi.sendEmail(
                                      type: EmailApi.TYPE_WELCOME,
                                      toName: firstNameController.text,
                                      toEmail: emailController.text,
                                      url: url,
                                      message:
                                          "Your credentials: Email: $email Password: $password");

                                  showMessage(
                                    context,
                                    Icons.check,
                                    green,
                                    null != widget.bm
                                        ? "User Updated!"
                                        : "User Added!",
                                    null != widget.bm
                                        ? "You have successful updated this User Account"
                                        : "You have successful added this User",
                                    // cancellable: false,
                                    delayInMilli: 700,
                                    clickYesText: "Nice!",
                                    cancellable: false,
                                    onClicked: (_) {
                                      Navigator.pop(context, model);
                                    },
                                  );
                                });
                              },
                              child: Center(
                                child: Text(
                                  null != widget.bm
                                      ? "Update User"
                                      : "Add User",
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              style: TextButton.styleFrom(
                                  primary: white,
                                  backgroundColor: green_dark,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  minimumSize: Size(100, 50)),
                            ),
                        ],
                      ),
                      addSpace(20),

                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Expanded(
                            child: inputField(
                              "Send Instructions",
                              messageController,
                              maxLines: 5,
                              onChanged: (s) {
                                setState(() {});
                              },
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 10, bottom: 20),
                            child: TextButton(
                              onPressed: () {
                                if (messageController.text.isEmpty) return;
                                String text = messageController.text;

                                String firstName = model.getString(FIRST_NAME);
                                String lastName = model.getString(LAST_NAME);
                                String name = "$lastName $firstName";

                                final bm = BaseModel();
                                bm.put(MESSAGE, text);
                                bm.put(PARTIES, [model.getObjectId()]);
                                bm.put("sentTo", name);
                                bm.saveItem(INSTRUCTION_BASE, true);
                                messageController.clear();

                                EmailApi.sendEmail(
                                    type: EmailApi.TYPE_INSTRUCTIONS,
                                    toName: firstNameController.text,
                                    toEmail: emailController.text,
                                    message: text);
                              },
                              onLongPress: () {
                                if (messageController.text.isEmpty) return;
                                String text = messageController.text;
                                final bm = BaseModel();
                                bm.put(MESSAGE, text);
                                bm.put(PARTIES, ["All"]);
                                bm.put("sentTo", "General Instruction");
                                bm.saveItem(INSTRUCTION_BASE, true);
                                messageController.clear();

                                EmailApi.sendEmail(
                                    type: EmailApi.TYPE_INSTRUCTIONS,
                                    toName: firstNameController.text,
                                    toEmail: emailController.text,
                                    message: text);
                              },
                              child: Text(
                                "Send",
                                style: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.bold),
                              ),
                              style: TextButton.styleFrom(
                                  padding: EdgeInsets.zero,
                                  primary: white,
                                  backgroundColor: black.withOpacity(
                                      messageController.text.isEmpty ? 0.5 : 1),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  minimumSize: Size(100, 50)),
                            ),
                          )
                        ],
                      ),

                      Row(
                        children: [
                          Expanded(
                            child: inputField("Email Address", emailController,
                                onChanged: (s) {
                              setState(() {});
                            }, readOnly: readOnly),
                          ),
                          addSpaceWidth(10),
                          Expanded(
                            child: inputField("First Name", firstNameController,
                                onChanged: (s) {
                              setState(() {});
                            }, readOnly: readOnly),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: inputField("Last Name", lastNameController,
                                onChanged: (s) {
                              setState(() {});
                            }, readOnly: readOnly),
                          ),
                          addSpaceWidth(10),
                          Expanded(
                            child: inputField("Password", passwordController,
                                onChanged: (s) {
                              setState(() {});
                            }, readOnly: readOnly),
                          )
                        ],
                      ),

                      addSpace(30),
                      Text(
                        "Account Privilege",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      addSpace(5),
                      //if (!readOnly)
                      Text(
                        "Choose an access privilege for this user",
                        style: TextStyle(
                            fontSize: 13,
                            color: black.withOpacity(.5),
                            fontWeight: FontWeight.bold),
                      ),
                      addSpace(20),
                      Wrap(
                        children: List.generate(
                            appSettingsModel.getList("privileges").length, (p) {
                          String title =
                              appSettingsModel.getList("privileges")[p];
                          bool active = privilegeIndex == p;
                          return GestureDetector(
                            onTap: () {
                              if (readOnly) return;

                              privilegeIndex = p;
                              setState(() {});
                            },
                            child: Container(
                              height: 40,
                              width: 140,
                              margin: EdgeInsets.only(right: 10),
                              child: Text(
                                title,
                                style: TextStyle(
                                    color: active ? white : black,
                                    fontWeight: active
                                        ? FontWeight.bold
                                        : FontWeight.normal),
                              ),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  color: active ? black : white,
                                  boxShadow: [
                                    BoxShadow(
                                        color: black.withOpacity(.05),
                                        spreadRadius: 2,
                                        blurRadius: 5)
                                  ],
                                  borderRadius: BorderRadius.circular(8)),
                            ),
                          );
                        }),
                      ),
                      addSpace(20),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      }),
    );
  }
}
