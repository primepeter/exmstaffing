import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/main.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class TrainingProgram extends StatefulWidget {
  const TrainingProgram({Key key}) : super(key: key);

  @override
  _TrainingProgramState createState() => _TrainingProgramState();
}

class _TrainingProgramState extends State<TrainingProgram> {
  bool termsAccepted = false;

  List<StreamSubscription> subs = [];

  String path = "trainingProgram";
  BaseModel mainModel;
  BaseModel formModel;
  String get formId => routeDataHolder["/trainingProgram"];
  String get name => mainModel.getString(NAME);
  List<BaseModel> formsToFill = [];

  YoutubePlayerController _controller = YoutubePlayerController(
    initialVideoId: 'lORX9sm_JOM',
    params: YoutubePlayerParams(
        // playlist: ['nPt8bK2gbaU', 'gQDByCdjUXw'],
        // startAt: Duration(seconds: 30),
        showControls: true,
        showFullscreenButton: true,
        autoPlay: false),
  );

  @override
  initState() {
    super.initState();
    loadItems();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadItems() async {
    if (null == formId || formId.isEmpty) {
      showErrorDialog(
          context, "Ops the application link is invalid or has expired!",
          cancellable: false);
      return;
    }

    var sub = FirebaseFirestore.instance
        .collection(EMPLOYEE_BASE)
        .where(OBJECT_ID, isEqualTo: formId)
        .limit(1)
        .snapshots()
        .listen((value) {
      if (value.size == 0) {
        showErrorDialog(
            context, "Ops the application link is invalid or has expired!",
            cancellable: false);
        return;
      }
      mainModel = BaseModel(doc: value.docs[0]);
      formsToFill = mainModel.getListModel(FORMS_TO_FILL);
      int p = formsToFill.indexWhere((e) => e.getString("path") == path);
      if (p != -1) {
        formModel = formsToFill[p];
      }
      setState(() {});
    });
    subs.add(sub);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: LayoutBuilder(builder: (c, box) {
              final size = box.maxWidth;
              print("size $size");
              bool isLargeScreen = box.maxWidth > 850;
              final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

              return Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                padding: EdgeInsets.only(
                  // left: isLargeScreen ? size * 0.25 : 20,
                  // right: isLargeScreen ? size * 0.25 : 20,

                  left: isLargeScreen ? size * 0.15 : 20,
                  right: isLargeScreen ? size * 0.15 : 20,
                  top: 120,
                  bottom: 20,
                ),
                decoration: BoxDecoration(
                    color: isLargeScreen ? scaffoldColor : white,
                    borderRadius: BorderRadius.circular(10)),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: isLargeScreen ? white : null,
                          borderRadius: BorderRadius.circular(10)),
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  style: TextButton.styleFrom(
                                    minimumSize: Size(40, 40),
                                    backgroundColor: white,
                                    primary: black,
                                    elevation: 4,
                                    // shape: CircleBorder()
                                  ),
                                  child: Icon(Icons.navigate_before)),
                              addSpaceWidth(5),
                              Expanded(
                                child: Text(
                                  "Alzheimer's and Dementia Care On-going Self-Training Program for Caregivers",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                          addSpace(20),
                          Text.rich(
                            TextSpan(children: [
                              TextSpan(
                                text: "\n• Caregiver Training: Introduction"
                                    "\n\n• Caregiver Training: Agitation and Anxiety | UCLA Alzheimer's and Dementia Care Program "
                                    "\n\n• Caregiver Training: Refusal to Bathe | UCLA Alzheimer's and Dementia Care "
                                    "\n\n• Caregiver Training: Repetitive Questions | UCLA Alzheimer's and Dementia Care Program "
                                    "\n\n• Caregiver Training: Depression/Apathy | UCLA Alzheimer's and Dementia Care Program "
                                    "\n\n• Caregiver Training: Sundowning | UCLA Alzheimer's and Dementia Care Program "
                                    "\n\n• Caregiver Training: Hallucinations | UCLA Alzheimer's and Dementia Care "
                                    "\n\n• Caregiver Training: Wandering | UCLA Alzheimer's and Dementia Care Program "
                                    "\n\n• Caregiver Training: Home Safety | UCLA Alzheimer's and Dementia Care Program "
                                    "\n\n• Caregiver Training: Repetitive Phone Calls | UCLA Alzheimer's and Dementia Care Program "
                                    "\n\n• Caregiver Training: Aggressive Language/Behavior | UCLA Alzheimer's and Dementia Care Program "
                                    "\n\n• Caregiver Training: Refusal to Take Medication | UCLA Alzheimer's and Dementia Care Program "
                                    "\n\n• Caregiver Training: Sleep Disturbances | UCLA Alzheimer's and Dementia Care Program "
                                    "\n\n• Caregiver Training: Sexually Inappropriate Behaviors | UCLA Alzheimer's and Dementia Care Program "
                                    "\n\n• Caregiver Training: Repetitive Behaviors | UCLA Alzheimer's and Dementia Care Program",
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.w600),
                              ),
                            ]),
                            style: TextStyle(
                              fontSize: 14,
                            ),
                          ),
                          addLine(20, black.withOpacity(.03), 0, 20, 0, 20),
                          Text(
                            "YouTube Video",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          addSpace(20),
                          YoutubePlayerIFrame(
                            controller: _controller,
                            aspectRatio: 16 / 9,
                          ),
                          addSpace(50),
                        ],
                      ),
                    ),
                    addSpace(20),
                    InkWell(
                      onTap: () {
                        termsAccepted = !termsAccepted;
                        setState(() {});
                      },
                      child: Container(
                        child: Row(
                          children: [
                            checkBox(termsAccepted),
                            addSpaceWidth(20),
                            Expanded(
                                child: Text(
                              "By clicking on  SAVE INFORMATION below, I Agree to commit "
                              "to on-going self training by watching the explanatory "
                              "videos on how to care for ALZHEIMER’S AND DEMENTIA "
                              "RESIDENTS as part of my work development with  "
                              "EXCEPTIONAL MEDICAL STAFFING",
                              style: TextStyle(fontSize: 12),
                            ))
                          ],
                        ),
                      ),
                    ),
                    addSpace(20),
                    Container(
                      height: 60,
                      child: TextButton(
                        onPressed: !termsAccepted
                            ? null
                            : () {
                                formModel.put(STATUS, STATUS_COMPLETED);
                                int p = formsToFill.indexWhere(
                                    (e) => e.getString("path") == path);
                                if (p != -1) {
                                  formsToFill[p] = formModel;
                                }

                                mainModel.put(FORMS_TO_FILL,
                                    formsToFill.map((e) => e.items).toList());
                                mainModel.updateItems();
                                // Navigator.pop(context, true);
                                Navigator.pushReplacementNamed(
                                    context, "/applicationForm??$formId");

                                return;

                                showMessage(
                                  context,
                                  Icons.check,
                                  green,
                                  "Form Submitted!",
                                  "You have successful completed your Training Program Agreement form",
                                  // cancellable: false,
                                  delayInMilli: 700,
                                  clickYesText: "Go Back",
                                  cancellable: false,
                                  onClicked: (_) {
                                    Navigator.pushReplacementNamed(
                                        context, "/applicationForm??$formId");
                                    // Navigator.pushNamed(context, "/$path??$formId");
                                  },
                                );
                              },
                        child: Center(
                          child: Text(
                            "Save Information & Exit".toUpperCase(),
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            minimumSize: Size(300, 50)),
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
          appBarHeader(context)
        ],
      ),
    );
  }

  educationItem(
    String title,
    TextEditingController nameController,
    TextEditingController subjectController,
    int graduateIndex,
    int yearsCompletedIndex,
    onGradChanged,
    onYearsChanged,
  ) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        addSpace(20),
        Text(
          title,
          style: TextStyle(
              fontSize: 16,
              color: black.withOpacity(.5),
              fontWeight: FontWeight.bold),
        ),
        addSpace(10),
        Row(
          children: [
            Expanded(
                child: inputField("Name & School Location", nameController,
                    onChanged: (s) {
              setState(() {});
            })),
            addSpaceWidth(10),
            Expanded(
              child: inputField("Subject Studied", subjectController,
                  onChanged: (s) {
                setState(() {});
              }),
            ),
          ],
        ),
        yesNoField("Did you graduate?", graduateIndex, (p) {
          onGradChanged(p);
        }),
        addSpace(20),
        Text(
          "Years Completed?",
          style: TextStyle(
              fontSize: 14,
              color: black.withOpacity(.5),
              fontWeight: FontWeight.bold),
        ),
        addSpace(10),
        Wrap(
          children: List.generate(4, (p) {
            String title = "${p + 1} Years";
            bool active = p == yearsCompletedIndex;
            return GestureDetector(
              onTap: () {
                onYearsChanged(p);
              },
              child: Container(
                height: 40,
                width: 160,
                margin: EdgeInsets.all(2),
                child: Text(
                  title,
                  style: TextStyle(
                      color: active ? white : black,
                      fontWeight: active ? FontWeight.bold : FontWeight.normal),
                ),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: active ? (black) : white,
                    boxShadow: [
                      BoxShadow(
                          color: black.withOpacity(.05),
                          spreadRadius: 2,
                          blurRadius: 5)
                    ],
                    borderRadius: BorderRadius.circular(8)),
              ),
            );
          }),
        ),
        addSpace(20),
      ],
    );
  }
}
