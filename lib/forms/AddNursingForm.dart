import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EmailApi.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shimmer/shimmer.dart';

import 'ShowEmployees.dart';

class NoteInfo {
  int savedAt = DateTime.now().millisecondsSinceEpoch;
  final noteController = TextEditingController();

  NoteInfo({Map items}) {
    if (null != items) {
      savedAt = items["savedAt"];
      noteController.text = items["note"];
    }
  }

  bool get isValidated {
    bool validated = true;
    if (savedAt == 0) return false;
    if (noteController.text.isEmpty) return false;
    return validated;
  }

  Map get items {
    return {
      "note": noteController.text,
      "savedAt": savedAt,
    };
  }
}

class AddNursingForm extends StatefulWidget {
  const AddNursingForm(
      {Key key, this.bm, this.readOnly = false, this.assign = false})
      : super(key: key);
  final BaseModel bm;
  final bool readOnly;
  final bool assign;

  @override
  _AddNursingFormState createState() => _AddNursingFormState();
}

class _AddNursingFormState extends State<AddNursingForm> {
  final scrollController = ScrollController();
  final addressController = TextEditingController();
  final nameController = TextEditingController();
  final telController = TextEditingController();
  final noteController = TextEditingController();

  String state;
  String city;

  List selectedSkills = [];

  List shiftPeriod = [
    "First Shift",
    "Second Shift",
    "Third/Overnight Shift",
  ];

  List<NoteInfo> notesInfo = [];

  List shiftAvailable = [];

  // String skillRequired;
  int startDate = 0;
  int endDate = 0;
  // String startTime;
  // String endTime;

  List affiliations = [];

  BaseModel model;
  bool readOnly = false;

  bool assign = false;

  List<BaseModel> assignedInfo = [];
  int employeesNeeded = 0;
  bool showNoteBtn = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    readOnly = widget.readOnly;
    assign = widget.assign;

    if (null != widget.bm) {
      model = widget.bm;
      readOnly = widget.readOnly;

      notesInfo = model
          .getListModel(NOTES_INFO)
          .map((e) => NoteInfo(items: e.items))
          .toList();

      nameController.text = model.getString(NAME);
      addressController.text = model.getString(ADDRESS);
      state = model.getString(STATE);
      city = model.getString(CITY);
      telController.text = model.getString(TELEPHONE);
      selectedSkills = model.getList(SKILLS);
      shiftAvailable = model.getList(SHIFTS);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: black.withOpacity(.8),
      child: LayoutBuilder(builder: (c, box) {
        bool isLargeScreen = box.maxWidth > 850;
        // final size = getScreenWidth(c);
        final size = box.maxWidth;
        final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

        return RawScrollbar(
          controller: scrollController,
          isAlwaysShown: true,
          thumbColor: Colors.white,
          radius: Radius.circular(20),
          thickness: 20,
          interactive: true,
          child: SingleChildScrollView(
            controller: scrollController,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Container(
                  decoration: BoxDecoration(
                      color: white, borderRadius: BorderRadius.circular(15)),
                  padding: EdgeInsets.all(30),
                  margin: EdgeInsets.only(
                      top: 20,
                      bottom: 20,
                      left: ((0.12) * size),
                      right: ((0.12) * size)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              "Nursing Home Information",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ),
                          if (!readOnly)
                            TextButton(
                              onPressed: () {
                                String name = nameController.text;
                                String address = addressController.text;
                                String tel = telController.text;

                                if (name.isEmpty) {
                                  showError(
                                      context, "Enter Name of Nursing Home!");
                                  return;
                                }
                                if (address.isEmpty) {
                                  showError(context,
                                      "Enter Address of Nursing Home!");
                                  return;
                                }
                                if (tel.isEmpty) {
                                  showError(
                                      context, "Enter Tel of Nursing Home!");
                                  return;
                                }
                                if (null == state || state.isEmpty) {
                                  showError(
                                      context, "Enter State of Nursing Home!");
                                  return;
                                }
                                if (null == city || city.isEmpty) {
                                  showError(
                                      context, "Enter City of Nursing Home!");
                                  return;
                                }

                                String search = "${name.toLowerCase()}"
                                    " ${address.toLowerCase()} "
                                    "${state.toLowerCase()} ${city.toLowerCase()}";

                                showProgress(true, context,
                                    msg: "Please wait...");
                                String id = getRandomId();
                                if (null != widget.bm)
                                  id = widget.bm.getObjectId();

                                BaseModel model = BaseModel();

                                model.put(NOTES_INFO,
                                    notesInfo.map((e) => e.items).toList());
                                model.put(OBJECT_ID, id);
                                model.put(NAME, name);
                                model.put(ADDRESS, address);
                                model.put(STATE, state);
                                model.put(CITY, city);
                                model.put(TELEPHONE, tel);
                                model.put(STATUS, PENDING);
                                model.put(SEARCH, getSearchString(search));

                                model.saveItem(NURSING_BASE, true, document: id,
                                    onComplete: (e) {
                                  showProgress(
                                    false,
                                    context,
                                  );
                                  if (null != e) {
                                    showErrorDialog(context, e);
                                    return;
                                  }
                                  appSettingsModel
                                    ..putInList(
                                      NURSING_BASE_COUNT,
                                      id,
                                    )
                                    ..updateItems();

                                  // FirebaseFirestore.instance
                                  //     .collection(USER_BASE)
                                  //     .where(PRIVILEGE_INDEX, isEqualTo: 1)
                                  //     .get()
                                  //     .then((value) {
                                  //   for (var doc in value.docs) {
                                  //     final bm = BaseModel(doc: doc);
                                  //     EmailApi.sendEmail(
                                  //         isAdmin: true,
                                  //         toName: bm.getString(FIRST_NAME),
                                  //         toEmail: bm.getEmail(),
                                  //         message:
                                  //             "A new work order has been created!");
                                  //   }
                                  // });

                                  showMessage(
                                    context,
                                    Icons.check,
                                    green,

                                    null != widget.bm
                                        ? "Nursing Home Updated!"
                                        : "Nursing Home Added!",
                                    null != widget.bm
                                        ? "You have successful updated this Nursing Home"
                                        : "You have successful added this Nursing Home",
                                    // cancellable: false,
                                    delayInMilli: 700,
                                    clickYesText: "Nice!",
                                    cancellable: false,
                                    onClicked: (_) {
                                      Navigator.pop(context, model);
                                    },
                                  );
                                });
                              },
                              child: Center(
                                child: Text(
                                  null != widget.bm
                                      ? "Update Nursing Home"
                                      : "Add Nursing Home",
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              style: TextButton.styleFrom(
                                  primary: white,
                                  backgroundColor: green_dark,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  minimumSize: Size(160, 50)),
                            ),
                          addSpaceWidth(10),
                          Container(
                            // margin: EdgeInsets.only(bottom: 15),
                            child: TextButton(
                              onPressed: () {
                                Navigator.pop(context, readOnly ? model : null);
                              },
                              child: Icon(
                                Icons.clear,
                                size: 15,
                              ),
                              style: TextButton.styleFrom(
                                  padding: EdgeInsets.zero,
                                  primary: white,
                                  backgroundColor: red,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  minimumSize: Size(40, 40)),
                            ),
                          )
                        ],
                      ),
                      addSpace(20),
                      Row(
                        children: [
                          Expanded(
                            child: inputField("Name", nameController,
                                onChanged: (s) {
                              setState(() {});
                            }, readOnly: readOnly),
                          ),
                          addSpaceWidth(10),
                          Expanded(
                            child: inputField(
                              "Tel",
                              telController,
                              isNum: true,
                              readOnly: readOnly,
                              onChanged: (s) {
                                setState(() {});
                              },
                            ),
                          )
                        ],
                      ),
                      inputField(
                        "Address",
                        addressController,
                        readOnly: readOnly,
                        maxLines: 3,
                        onChanged: (s) {
                          setState(() {});
                        },
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: clickField(
                                "State",
                                state,
                                readOnly
                                    ? null
                                    : () {
                                        showListDialog(
                                            context, usaStates.keys.toList(),
                                            (_) {
                                          state = usaStates.keys.toList()[_];
                                          city = null;
                                          setState(() {});
                                        }, returnIndex: true);
                                      }),
                          ),
                          addSpaceWidth(10),
                          Expanded(
                              child: clickField("City", city, () {
                            if (null == state || state.isEmpty) {
                              showError(context, "Select a state first");
                              return;
                            }
                            showListDialog(context, usaStates[state].toList(),
                                (_) {
                              city = usaStates[state].toList()[_];
                              setState(() {});
                            }, returnIndex: true);
                          })),
                        ],
                      ),
                      if (readOnly) ...[
                        addLine(20, black.withOpacity(.03), 0, 20, 0, 20),
                        Row(
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Notes",
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "Add notes to this nursing home.",
                                    style: TextStyle(
                                        fontSize: 12,
                                        color: black.withOpacity(.5),
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ),
                            if (notesInfo.length < 3)
                              TextButton(
                                onPressed: () {
                                  if (notesInfo.isNotEmpty &&
                                      !notesInfo[notesInfo.length - 1]
                                          .isValidated) {
                                    showError(context,
                                        "Some fields on the current note item are empty!");
                                    return;
                                  }

                                  notesInfo.add(NoteInfo());
                                  setState(() {});
                                },
                                child: Text(
                                  "Add Note",
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                ),
                                style: TextButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                    primary: white,
                                    backgroundColor: black,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    minimumSize: Size(160, 50)),
                              )
                          ],
                        ),
                        addSpace(20),
                        Container(
                          margin: EdgeInsets.only(bottom: 20),
                          child: Builder(builder: (ctx) {
                            if (notesInfo.isEmpty)
                              return Container(
                                padding: EdgeInsets.all(0),
                                child: DottedBorder(
                                  color: black.withOpacity(.5),
                                  strokeWidth: 1,
                                  borderType: BorderType.RRect,
                                  radius: Radius.circular(10),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Container(
                                        height: 150,
                                        alignment: Alignment.center,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Icon(
                                              Icons.note,
                                              color: black.withOpacity(.5),
                                              size: 30,
                                            ),
                                            addSpace(10),
                                            Text(
                                              "You have not added any notes yet",
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  color: black.withOpacity(.5)
                                                  // fontWeight: FontWeight.bold,
                                                  ),
                                            )
                                          ],
                                        )),
                                  ),
                                ),
                              );

                            return Column(
                              children:
                                  List.generate(notesInfo.length, (index) {
                                final fm = notesInfo[index];

                                return Container(
                                  margin: EdgeInsets.only(bottom: 10),
                                  padding: EdgeInsets.all(4),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      border: Border.all(
                                          color: black.withOpacity(.09))),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            clickField(
                                                "Time of note",
                                                getSimpleDate(fm.savedAt),
                                                () async {}),
                                            addSpace(10),
                                            inputField(
                                                "Write Note", fm.noteController,
                                                //readOnly: readOnly,
                                                maxLines: 4, onChanged: (s) {
                                              setState(() {});
                                            }),
                                          ],
                                        ),
                                      ),
                                      addSpaceWidth(10),
                                      Container(
                                        margin: EdgeInsets.only(bottom: 15),
                                        child: TextButton(
                                          onPressed: () {
                                            if (!notesInfo[index].isValidated) {
                                              showError(context,
                                                  "Some fields are empty!");
                                              return;
                                            }
                                            model.put(
                                                NOTES_INFO,
                                                notesInfo
                                                    .map((e) => e.items)
                                                    .toList());
                                            model.updateItems();
                                            setState(() {});
                                          },
                                          child: Text(
                                            "Save",
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          style: TextButton.styleFrom(
                                              padding: EdgeInsets.zero,
                                              primary: white,
                                              backgroundColor: green,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(8)),
                                              minimumSize: Size(100, 40)),
                                        ),
                                      ),
                                      addSpaceWidth(10),
                                      Container(
                                        margin: EdgeInsets.only(bottom: 15),
                                        child: TextButton(
                                          onPressed: () {
                                            //   if (readOnly) return;
                                            notesInfo.removeAt(index);
                                            setState(() {});
                                          },
                                          child: Icon(Icons.clear),
                                          style: TextButton.styleFrom(
                                              padding: EdgeInsets.zero,
                                              primary: white,
                                              backgroundColor: red,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(8)),
                                              minimumSize: Size(40, 40)),
                                        ),
                                      )
                                    ],
                                  ),
                                );
                              }),
                            );
                          }),
                        ),
                      ],
                      addLine(20, black.withOpacity(.03), 0, 20, 0, 20),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      }),
    );
  }
}
