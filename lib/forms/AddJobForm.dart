import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EmailApi.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shimmer/shimmer.dart';

import 'ShowEmployees.dart';

class AddJobForm extends StatefulWidget {
  const AddJobForm(
      {Key key, this.bm, this.readOnly = false, this.assign = false})
      : super(key: key);
  final BaseModel bm;
  final bool readOnly;
  final bool assign;

  @override
  _AddJobFormState createState() => _AddJobFormState();
}

class _AddJobFormState extends State<AddJobForm> {
  final titleController = TextEditingController();
  final zipController = TextEditingController();
  final descriptionController = TextEditingController();
  final requirementController = TextEditingController();
  final salaryController = TextEditingController();
  // final salaryController = MoneyMaskedTextController(leftSymbol: "\$");
  String selectedState;
  String selectedCity;
  String selectedSkill = "";

  int employmentIndex = -1;
  int vaccinationIndex = -1;
  int negativeIndex = -1;
  bool passVisible = false;
  bool termsAccepted = false;

  List days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];
  List daysAvailable = [];

  List shiftPeriod = [
    "First Shift",
    "Second Shift",
    "Third/Overnight Shift",
  ];

  List shiftAvailable = [];
  int startDate = 0;

  // List<StreamSubscription> subs = [];

  String path = "postJob";
  BaseModel mainModel;
  BaseModel formModel;
  // String get formId => routeDataHolder["/postJob"];

  String objectId = getRandomId();
  BaseModel model = BaseModel();

  @override
  initState() {
    super.initState();
    if (widget.bm != null) {
      model = widget.bm;
      objectId = model.getObjectId();

      titleController.text = model.getString(TITLE);
      selectedSkill = model.getString(SKILLS);
      startDate = model.getInt("jobDate");
      descriptionController.text = model.getString("jobDescription");
      requirementController.text = model.getString("jobRequirement");
      salaryController.text = model.getString("jobSalary");
      // salaryController.updateValue(model.getDouble("jobSalary"));
      selectedState = model.getString(STATE);
      selectedCity = model.getString(CITY);

      setState(() {});
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    // for (var s in subs) s?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: black.withOpacity(.8),
      child: LayoutBuilder(builder: (c, box) {
        bool isLargeScreen = box.maxWidth > 850;
        // final size = getScreenWidth(c);
        final size = box.maxWidth;
        final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

        return SingleChildScrollView(
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: Container(
                decoration: BoxDecoration(
                    color: white, borderRadius: BorderRadius.circular(15)),
                padding: EdgeInsets.all(30),
                margin: EdgeInsets.only(
                    top: 20,
                    bottom: 20,
                    left: ((0.12) * size),
                    right: ((0.12) * size)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            "Post Job",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            String title = titleController.text;
                            String jobDesc = descriptionController.text;
                            String jobReq = requirementController.text;
                            String salary = salaryController.text;
                            // double salary = salaryController.numberValue;

                            if (null == selectedSkill) {
                              showError(context, "Choose Job Title!");
                              return;
                            }

                            if (startDate == 0) {
                              showError(context, "Choose a date to start work");
                              return;
                            }

                            if (title.isEmpty) {
                              showError(context, "Enter Job Title!");
                              return;
                            }

                            if (jobDesc.isEmpty) {
                              showError(context, "Enter Job Description!");
                              return;
                            }
                            if (jobReq.isEmpty) {
                              showError(context, "Enter Job Requirement!");
                              return;
                            }

                            if (null == selectedState) {
                              showError(context, "Choose Your State!");
                              return;
                            }

                            if (null == selectedCity) {
                              showError(context, "Choose Your City!");
                              return;
                            }

                            if (salary.isEmpty) {
                              showError(
                                  context, "Enter Expected Pay Per Hour!");
                              return;
                            }

                            // if (salary == 0) {
                            //   showError(
                            //       context, "Enter Expected Pay Per Hour!");
                            //   return;
                            // }

                            showProgress(true, context, msg: "Please wait...");
                            model.put(OBJECT_ID, objectId);
                            model.put(TITLE, title);
                            model.put(SKILLS, selectedSkill);
                            model.put("jobDate", startDate);
                            model.put("jobDescription", jobDesc);
                            model.put("jobRequirement", jobReq);
                            model.put("jobSalary", salary);
                            model.put(STATE, selectedState);
                            model.put(CITY, selectedCity);
                            model.saveItem(JOB_BASE, true, document: objectId,
                                onComplete: (e) {
                              showProgress(
                                false,
                                context,
                              );
                              if (null != e) {
                                showErrorDialog(context, e);
                                return;
                              }

                              appSettingsModel
                                ..putInList(
                                  JOB_BASE_COUNT,
                                  objectId,
                                )
                                ..updateItems();

                              showMessage(
                                context,
                                Icons.check,
                                green,
                                "Job Submitted!",
                                "You have successful posted this job opening",
                                // cancellable: false,
                                delayInMilli: 700,
                                clickYesText: "Go Back",
                                cancellable: false,
                                onClicked: (_) {
                                  Navigator.pop(context, model);
                                },
                              );
                            });
                          },
                          child: Center(
                            child: Text(
                              null != widget.bm ? "Update Job" : "Add Job",
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold),
                            ),
                          ),
                          style: TextButton.styleFrom(
                              primary: white,
                              backgroundColor: black,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              minimumSize: Size(100, 50)),
                        ),
                        addSpaceWidth(10),
                        Container(
                          // margin: EdgeInsets.only(bottom: 15),
                          child: TextButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.clear,
                              size: 15,
                            ),
                            style: TextButton.styleFrom(
                                padding: EdgeInsets.zero,
                                primary: white,
                                backgroundColor: red,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                minimumSize: Size(40, 40)),
                          ),
                        )
                      ],
                    ),
                    Text(
                      "Skill required for job",
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                    addSpace(10),
                    Wrap(
                      children: List.generate(
                          appSettingsModel.getList("skills").length, (p) {
                        String title = appSettingsModel.getList("skills")[p];
                        bool active = title == selectedSkill;
                        return GestureDetector(
                          onTap: () {
                            selectedSkill = title;
                            setState(() {});
                          },
                          child: Container(
                            height: 40,
                            width: 140,
                            margin: EdgeInsets.all(2),
                            child: Text(
                              title,
                              style: TextStyle(
                                  color: active ? white : black,
                                  fontWeight: active
                                      ? FontWeight.bold
                                      : FontWeight.normal),
                            ),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: active ? (black) : white,
                                boxShadow: [
                                  BoxShadow(
                                      color: black.withOpacity(.05),
                                      spreadRadius: 2,
                                      blurRadius: 5)
                                ],
                                borderRadius: BorderRadius.circular(8)),
                          ),
                        );
                      }),
                    ),
                    addSpace(20),
                    inputField("Job Title", titleController, onChanged: (s) {
                      setState(() {});
                    }, maxLines: 4),
                    Row(
                      children: [
                        Expanded(
                            child: clickField(
                                "Date of Job?",
                                startDate == 0
                                    ? ""
                                    : getSimpleDate(startDate,
                                        pattern: "EEE MMMM dd, yyyy"),
                                () async {
                          final DateTime picked = await showDatePicker(
                              builder: (ctx, child) {
                                return Theme(
                                    data: Theme.of(context).copyWith(
                                      colorScheme: ColorScheme.light(
                                        primary: appColor,
                                        onPrimary:
                                            Colors.black, // header text color
                                        // onSurface: Colors.green, // body text color
                                      ),
                                      buttonTheme: ButtonThemeData(
                                          textTheme: ButtonTextTheme.primary),
                                    ),
                                    child: ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: child));
                              },
                              context: context,
                              initialDate: startDate == 0
                                  ? DateTime.now()
                                  : DateTime.fromMillisecondsSinceEpoch(
                                      startDate),
                              firstDate: DateTime.now(),
                              lastDate: DateTime(DateTime.now().year + 2));

                          if (picked != null &&
                              picked.millisecondsSinceEpoch != startDate)
                            startDate = picked.millisecondsSinceEpoch;
                          setState(() {});
                        })),
                        addSpaceWidth(10),
                        Expanded(
                            child: Row(
                          children: [
                            Container(
                              height: 65,
                              padding: EdgeInsets.all(5),
                              alignment: Alignment.center,
                              child: Image.asset(
                                "assets/icons/dollar.png",
                                height: 30,
                                width: 30,
                                color: black.withOpacity(.5),
                              ),
                            ),
                            addSpaceWidth(5),
                            Expanded(
                              child: inputField(
                                "Expected Pay Per Hour",
                                salaryController,
                                // isNum: true,
                                onChanged: (s) {
                                  setState(() {});
                                },
                              ),
                            ),
                          ],
                        ))
                      ],
                    ),
                    inputField("Job Description", descriptionController,
                        onChanged: (s) {
                      setState(() {});
                    }, maxLines: 4),
                    inputField("Job Requirement", requirementController,
                        onChanged: (s) {
                      setState(() {});
                    }, maxLines: 4),
                    Row(children: [
                      Expanded(
                          child: clickField("State", selectedState, () {
                        showListDialog(context, usaStates.keys.toList(), (_) {
                          selectedState = usaStates.keys.toList()[_];
                          setState(() {});
                        }, returnIndex: true);
                      })),
                      addSpaceWidth(10),
                      Expanded(
                          child: clickField("City", selectedCity, () {
                        if (null == selectedState) {
                          showError(context, "Select a state first");
                          return;
                        }
                        showListDialog(
                            context, usaStates[selectedState].toList(), (_) {
                          selectedCity = usaStates[selectedState].toList()[_];
                          setState(() {});
                        }, returnIndex: true);
                      }))
                    ]),
                    addSpace(50),
                    addLine(20, black.withOpacity(.03), 0, 20, 0, 20),
                  ],
                ),
              ),
            ),
          ),
        );
      }),
    );
  }
}
