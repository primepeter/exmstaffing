import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/main.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:styled_text/styled_text.dart';

class InfectionControlTraining extends StatefulWidget {
  const InfectionControlTraining({Key key}) : super(key: key);

  @override
  _InfectionControlTrainingState createState() =>
      _InfectionControlTrainingState();
}

class _InfectionControlTrainingState extends State<InfectionControlTraining> {
  // String applicantName = "Steven Danny";
  int startDate = 0;

  bool termsAccepted = false;

  List<StreamSubscription> subs = [];
  String path = "infectionControl";
  BaseModel mainModel;
  BaseModel formModel;

  String get formId => routeDataHolder["/infectionControl"];

  String get name => mainModel.getString(FIRST_NAME);

  String get offerAmount => mainModel.getString(OFFER_AMOUNT);
  List<BaseModel> formsToFill = [];

  @override
  initState() {
    super.initState();
    loadItems();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadItems() async {
    if (null == formId || formId.isEmpty) {
      showErrorDialog(
          context, "Ops the application link is invalid or has expired!",
          cancellable: false);
      return;
    }

    var sub = FirebaseFirestore.instance
        .collection(EMPLOYEE_BASE)
        .where(OBJECT_ID, isEqualTo: formId)
        .limit(1)
        .snapshots()
        .listen((value) {
      if (value.size == 0) {
        showErrorDialog(
            context, "Ops the application link is invalid or has expired!",
            cancellable: false);
        return;
      }
      mainModel = BaseModel(doc: value.docs[0]);
      startDate = mainModel.getInt(AGREEMENT_DATE);
      if (startDate == 0) startDate = DateTime.now().millisecondsSinceEpoch;

      formsToFill = mainModel.getListModel(FORMS_TO_FILL);
      int p = formsToFill.indexWhere((e) => e.getString("path") == path);
      if (p != -1) {
        formModel = formsToFill[p];
        // startDate = formModel.getInt(AGREEMENT_DATE);
        // if (startDate == 0) startDate = DateTime.now().millisecondsSinceEpoch;
      }
      setState(() {});
    });
    subs.add(sub);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: LayoutBuilder(builder: (c, box) {
              final size = box.maxWidth;
              print("size $size");
              bool isLargeScreen = box.maxWidth > 850;
              final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

              return Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                padding: EdgeInsets.only(
                  // left: isLargeScreen ? size * 0.25 : 20,
                  // right: isLargeScreen ? size * 0.25 : 20,

                  left: isLargeScreen ? size * 0.15 : 20,
                  right: isLargeScreen ? size * 0.15 : 20,
                  top: 120,
                  bottom: 20,
                ),
                decoration: BoxDecoration(
                    color: isLargeScreen ? scaffoldColor : white,
                    borderRadius: BorderRadius.circular(10)),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: isLargeScreen ? white : null,
                          borderRadius: BorderRadius.circular(10)),
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              style: TextButton.styleFrom(
                                minimumSize: Size(40, 40),
                                backgroundColor: white,
                                primary: black,
                                elevation: 4,
                                // shape: CircleBorder()
                              ),
                              child: Icon(Icons.navigate_before)),
                          addSpace(10),
                          Text(
                            "Infection Control Training",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          addSpace(20),
                          textData(),
                          addSpace(50),
                        ],
                      ),
                    ),
                    addSpace(20),
                    InkWell(
                      onTap: () {
                        termsAccepted = !termsAccepted;
                        setState(() {});
                      },
                      child: Container(
                        child: Row(
                          children: [
                            checkBox(termsAccepted),
                            addSpaceWidth(20),
                            Expanded(
                                child: Text(
                              "By clicking on SAVE INFORMATION below, you have read and understand the Exceptional Medical Staffing Infection Control and Covid-19 Training, and you agree to abide by all the requirements.",
                              style: TextStyle(fontSize: 12),
                            ))
                          ],
                        ),
                      ),
                    ),
                    addSpace(20),
                    Container(
                      height: 60,
                      child: TextButton(
                        onPressed: !termsAccepted
                            ? null
                            : () {
                                mainModel.put(AGREEMENT_DATE,
                                    DateTime.now().millisecondsSinceEpoch);

                                formModel.put(STATUS, STATUS_COMPLETED);
                                int p = formsToFill.indexWhere(
                                    (e) => e.getString("path") == path);
                                if (p != -1) {
                                  // formModel.put(AGREEMENT_DATE,
                                  //     DateTime.now().millisecondsSinceEpoch);
                                  formsToFill[p] = formModel;
                                }

                                mainModel.put(FORMS_TO_FILL,
                                    formsToFill.map((e) => e.items).toList());

                                mainModel.updateItems();

                                showMessage(
                                  context,
                                  Icons.check,
                                  green,
                                  "Form Submitted!",
                                  "You have successful completed your Caregiver Agreement form",
                                  // cancellable: false,
                                  delayInMilli: 700,
                                  clickYesText: "Go Back",
                                  cancellable: false,
                                  onClicked: (_) {
                                    // Navigator.pop(context, true);
                                    Navigator.pushReplacementNamed(
                                        context, "/applicationForm??$formId");
                                  },
                                );
                              },
                        child: Center(
                          child: Text(
                            "Save Information".toUpperCase(),
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            minimumSize: Size(300, 50)),
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
          appBarHeader(context)
        ],
      ),
    );
  }

  StyledText textData() {
    return StyledText(
      text: """
What is Infection Control?

Infection control refers to procedures that help stop the spread of infection, or germs, to residents, patients, clients, employees, visitors, and family members. Infection control is important in any care setting, whether the home or a facility such as a hospital, nursing home or assisted living facility.

We can all help control infections by doing special things in these areas:

❖ The environment. We can all help to keep the client living areas clean and sanitary. Clean rooms spread less germs.
❖ Our work practices. All healthcare workers must make infection control a part of everything we do. We must use standard precautions. We must wash our hands and we must do several other things while we work in order to stop the spread of germs from one person to another. We must make infection control a part of everything we do.
❖ Our own state of health. Healthcare workers who come to work with a cold or flu can spread it to their clients. We must get enough rest, a good diet andtake care of ourselves so that we can stay well and work without harming the ones we care for. We should also call the office when we have a bad cold, flu or another illness that our clients can catch from us.
❖ Our clients’ state of health. Many older people and those with a history of breathing problems get the pneumonia and flu vaccines to protect them against these common illnesses.

Why Are Infections A Big Problem in Senior Care Facilities?

They are a problem because:

❖ We do not know when we are spreading germs. They are very, very tiny and cannot be seen.
❖ Elderly people with multiple illnesses are at great risk of getting an infection. For one thing, they may have a weak immune system that makes them less able to fight off an infection. They may also have a disease like emphysema or diabetes that makes them more prone to infection.
❖ Infections cause deaths, hospitalizations and they cost a lot of money. 
  
Infections Are Caused by Germs

Germs are very tiny. We cannot see them with our eyes as we work. They are found all over and in our own bodies. Some germs are good for us. These good germs keep us healthy. For example, we have good germs in our intestines that help us digest food and prevent other infections. Many other germs can cause great harm and illnesses. Harmful germs that can make people ill are called "pathogens". They cause infections and diseases. They can cause wound infections, colds, pneumonia, AIDS/HIV and other diseases. Many of these diseases can cause serious harm and even death. These illnesses can be passed from one person to another without the person knowing that they are spreading it because these germs are very tiny and small. We cannot see them.
Germs will grow and multiply when they are fed and given a good place to live and grow. Senior Care Aides can stop the growth of germs by taking away the things that feed germs and help them to grow. Germs like moisture and darkness. They grow very well when they are wet and kept in the dark.
Germs also like the food we eat. Leftover food is more than enough food to grow germs. We can help in our battle against germs by keeping clients and their environment clean, dry and bright with light.

How Infections Spread

Infections can spread from person to person when germs: 
1) Are able to leave the body,
2) Have a means of transportation, and
3) Can enter another body.

Germs are everywhere. They are in the air, on our body, in our body, on our clothes, on and in food, in liquids, in human waste, on tabletops, bed sheets, flowers and everywhere else.
Senior Care Aides can do many things to prevent the spread of germs. We must keep foods safe. We must make sure that clients’ rooms are clean and without dust. Dust carries germs through the air.
Senior Care Aides cannot prevent germs from leaving someone's body. Germs will leave a person's body when they cough, sneeze, move their bowels and when they have a draining wound. We have no control over a sneeze and a cough, but we do have control over the tissues that someone is using when they sneeze or cough. Tissues are a way for germs to move from one person to another.
Tissues and our hands are vehicles for germs, just like a car is a vehicle for us to move from one place to another. We cannot get to a faraway store, work or church unless we have a car or another mode of transportation to get us from our house to where we want to go.
Germs cannot move from one place to another unless they, too, have a means of transportation. If we take this away from them, they cannot move from one person to another. Tissues, hands and all other items that have, or may have, body fluids canmove germs from one person to another. We can stop the spread of infection when we take the germs’ transportation away.
We can break the cycle of infection and stop infections from moving from one person to another when we:

❖ Wash our hands properly before and after EVERY client contact.
❖ Wash our hands properly before and after EACH task we do.
❖ Handle all items that have, or may have, germs in the proper way, and 
❖ Do other simple things like keeping dirty bed sheets away from our clothing. EVERYONE must control infection.

How Can Infections Be Stopped?

Infections can be prevented and stopped when we:

❖ Follow infection control and standard precautions procedures. 
❖ Handle hazardous waste properly.
❖ Keep ourselves healthy.
❖ Keep our clients healthy.
❖ Wash our hands properly.

What are Standard Precautions (Universal Precautions)?

All healthcare workers must use standard precautions when they handle and throw out items that may have blood or another body fluid. All body fluids are able to transport invisible infections so we must use standard precautions whenever we handle any substance that may have an infection. The basic idea of standard precautions is that we
treat all clients as if they had an infection even when we are pretty sure that they do not.
Some examples of body fluids are: blood, feces, wound drainage, secretions from the nose, saliva, sputum, tears, urine, vomit, breast milk, fluids taken from the abdomen, lungs, spinal area etc. Use standard precautions for all clients and all body substances.

Infection Control Procedures

Hand Washing

Simple hand washing is the single most effective thing that we can do to prevent the spread of germs. The success of hand washing has been proven over and over again. Yes, hand washing is the MOST important thing that you can do to prevent the spread of infection from one person to another.
Healthcare worker hands carry most infections. It is our hands that carry germs from one person to another. We must wash our hands, keep our nails short and clean and avoid wearing a ring, other than a small and clean wedding band. Germs like to hide under moist and dark nails and rings. Rings and nails transport germs.
You use soap, water, lots of rubbing and a little time to wash your hands.
Never hurry through the process. Do not ever skip a step. Do not forget to wash your hands.
Remember, infections from your hands can kill your clients.

You MUST wash your hands:

❖ As soon as you arrive at a Facility.
❖ Before and after you touch a client/resident to help with dressing, bathing, etc.
❖ Before and after you put gloves on.
❖ Before and after you eat or handle food.
❖ After you cough or sneeze.
❖ After you blow your nose.
❖ After handling garbage or trash.
❖ When you are leaving work.

The correct hand washing procedure is simple and takes less than 20
seconds to complete:

1. Turn on the water.
2. Wet your hands up to your wrists.
3. Apply a good amount of soap to your hands and wrists while the water remains running.
4. Rub your hands together to work up suds.
5. Rub the front and back of your hands, rub between your fingers, rub around the edges of your nails, clean under your nails, rub your wrist up to about 8 inches above your hand. This rubbing should continue for at least 15 seconds. Hold your hands down lower than your elbows, but do NOT touch any part of the sink. Put a little more water on your hands if the soap dries out while you are rubbing.
6. Rinse your hands well under the running water without touching the sink and while keeping your fingers lower than your wrist
7. Take a clean towel or a paper towel and dry your hands 8. Turn the water off with the towel, not your clean hands

The Use of Gloves

You MUST:

❖ Wear gloves whenever you may touch anybody fluid.
❖ Remove your gloves and throw them away after each use in the proper manner.
❖ Never use gloves more than once. They must be thrown out after every use. They cannot be re-used. Wash your hands immediately after taking off your gloves.
❖ Never walk around the house with gloves that have touched a client or a body fluid.

Equipment and Supplies; Environmental Control 

You MUST:

❖ Carefully handle all dirty client/resident’s care equipment so that it does not touch your clothing or anotherperson.
❖ Routinely clean all visibly dirty items such as bedside commodes, toilets etc.
❖ Make sure that all wheelchairs, beds, and walkers are routinely kept clean.

Handling Soiled Linens and Clothing

You MUST:

❖ Keep dirty bed linens and all other soiled objects away from your body and clothing.
❖ Place dirty linen in a laundry container or directly into the washing machine.

Special Waste Handling

Everyone must also throw away all contaminated articles such as incontinence undergarments as if they were infected with germs in order to prevent infections from spreading. Place them in a plastic bag, then tie off the bag before throwing in the general trash.

Keeping Yourself Healthy

There are many things that you can do to stay healthy. When you keep yourself healthy you can easily fight off many infections. Your clients/residents cannot get an illness or infection from you when you are healthy.
❖ Eat a good diet.
❖ Get plenty of rest.
❖ Exercise.
❖ Manage your stress.
❖ Get the flu shot every year.
❖ Stay away from other sick people in your home, as much as possible.

You may need to stay home if you have been exposed to an illness like measles and if you have a cold with a fever or heavy mucous, the flu or another infection. Call the office right away if you have any of these problems.

Keeping Your Clients/Residents Healthy

Senior Care Aides can work together to keep their clients/residents as healthy as possible. We can help them to:

• Get a good diet.
• Get plenty of fluids. 
• Get enough sleep and rest.
• Manage their stress.
• Get their flu, pneumonia or hepatitis B shots.
• Stay away from infections and other sick people, as much as possible.

Blood/Body Fluids Exposure

Senior Care Aides face a small risk of occupational exposure to blood or body fluids in Senior Care Facilities. Such exposure could make you seriously ill from certain infectious diseases, including AIDS and hepatitis. To minimize the risk of such exposure, Exceptional Medical Staffing has an Exposure Plan which is described in this section. The Exposure Plan is reviewed every year and updated with new procedures if necessary.
“Body fluids” refers to vomit, saliva, semen, stool, urine, mucus from the lungs or nose and any other secretion or excretion produced by the body. “Occupational exposure” means contact with blood or body fluids through breaks in the skin or contact with mucus membranes in the eyes, nose and mouth. Circumstances that might lead to occupational exposure of a Senior Care worker include contact with laundry or other materials that might be contaminated with blood or body fluids, and accidental penetration of the skin by contaminated sharp objects such as misplaced injection needles or used razor blades.
Exceptional Medical Staffing’s’ Exposure Plan requires Senior Care Aides to follow these simple measures for their protection:

❖ Be observant and note any evidence of possible contamination. Examples include visible evidence of stool or urine on bed linens, frequent coughing up of secretions by clients and the presence of injection needles in the resident’s room. Treat all such materials as if they are potentially dangerous.
❖ Wear gloves at all times while doing laundry, making beds or changing bed linens. In addition, wear gloves whenever there is the possibility of contacting blood or body fluids. Exceptional Medical Staffing makes appropriate protective gloves available to all Senior Care employees, but it is your responsibility to be sure that you have a supply at all times. Do not remove gloves with your teeth.
❖ Use hot water and detergent for laundering soiled bed linens or clothing.
❖ Wash your hands with warm soap and water after removing gloves and after any direct contact with a client’sbody.
❖ Do not reach into trash containers.
❖ If you encounter a spillage of body fluids or blood, first cover your hands with protective gloves, then wipe up the spill with paper towels or disposable cloth; place the paper towels/cloth in a plastic bag and seal it, then place that bag in a second bag and discard in a trash container; if there is the possibility of bare skin contact with a contaminated hard surface such as tile, Formica, porcelain, granite, metal or stone, clean the area with a dilute bleach solution (1 part bleach in 20 parts water), but do this only after obtaining consent from the client/resident; leave the bleach solution in contact with the area for 15 minutes, then wash off and dry; remove your gloves and wash your hands with hot water and soap.
❖ Inform your Exceptional Medical Staffing manager immediately of any contact you may have experienced with contaminated blood or body fluids. This is necessary so that records can be kept and so that appropriate medical measures can be taken to protect you if necessary.

Covid-19: What It is and How To Prevent It:

WHAT IS CORONAVIRUS?

Coronavirus disease also called (COVID-19) is a contagious respiratory and vascular blood vessel disease. It is caused by becoming infected with severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2), which is a specific type of coronavirus.

SYMPTOMS:

The common symptoms include fever, cough, fatigue, shortness of breath or breathing difficulties, and loss of smell and taste.
While most people who contact the disease have mild symptoms, some people develop acute respiratory distress syndrome (ARDS) possibly caused by cytokine storm, multi-organ failure, septic shock, and blood clots. Longer-term damage to organs (in particular, the lungs and heart) has been observed, and there is concern about a significant number of patients who have recovered from the acute phase of the disease but continue to experience a range of effects—including severe fatigue, memory loss and other cognitive issues, low grade fever, muscle weakness, breathlessness, and other symptoms—for months afterwards.

INCUBATION PERIOD:

The incubation period, which is the time between becoming infected with the virus and showing symptoms, may range from one to fourteen days.

HOW IT SPREADS:

COVID-19 spreads most often when people are physically close. It spreads very easily and sustainably through the air, primarily via small droplets or aerosols, as an infected person breathes, coughs, sneezes, sings, or speaks. It may also be transmitted via contaminated surfaces, although this has not been conclusively demonstrated. Airborne transmission from aerosol formation is suspected to be the main mode of transmission. It can spread from an infected person for up to two days before they display symptoms, and from people who are asymptomatic. People remain infectious for seven to twelve days in moderate cases, and up to two weeks in severe cases.

HOW TO PREVENT COVID-19:

The recommended measures to prevent COVID-19 infection include frequent social distancing (Six feet), quarantine, covering coughs and sneezes, hand washing, and keeping unwashed hands away from the face.

USE OF FACE MASK:

The use of face masks or cloth face coverings has been recommended by health officials in public settings to minimize the risk of transmissions, with some authorities requiring their use in certain settings, such as on public transport and in shops.

TREATMENT:
Several Covid-19 Vaccines with FDA Approval now exist in approved Drug Pharmacies and Departmental Store Pharmacies. Booster Shots are now also available.

I have read and understand the Exceptional Medical Staffing Infection Control and COVID-19 Training, and I commit myself to abiding by it.
  """,
      // textAlign: TextAlign.left,
      style: TextStyle(fontSize: 18),
    );
  }
}
