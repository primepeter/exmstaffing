import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class KeysForm extends StatefulWidget {
  final List keys;
  const KeysForm(this.keys);

  @override
  _KeysFormState createState() => _KeysFormState();
}

class _KeysFormState extends State<KeysForm> {
  List keys = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    keys = widget.keys;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: black.withOpacity(.8),
      child: LayoutBuilder(builder: (c, box) {
        bool isLargeScreen = box.maxWidth > 850;
        // final size = getScreenWidth(c);
        final size = box.maxWidth;
        final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

        return SingleChildScrollView(
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: Container(
                decoration: BoxDecoration(
                    color: white, borderRadius: BorderRadius.circular(15)),
                padding: EdgeInsets.all(30),
                margin: EdgeInsets.only(
                    top: 20,
                    bottom: 20,
                    left: ((0.12) * size),
                    right: ((0.12) * size)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            "Visible Information",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Cancel",
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.bold),
                          ),
                          style: TextButton.styleFrom(
                              padding: EdgeInsets.zero,
                              primary: white,
                              backgroundColor: red,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              minimumSize: Size(100, 50)),
                        ),
                        addSpaceWidth(10),
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context, keys);
                          },
                          child: Center(
                            child: Text(
                              "Apply",
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold),
                            ),
                          ),
                          style: TextButton.styleFrom(
                              primary: white,
                              backgroundColor: green_dark,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              minimumSize: Size(100, 50)),
                        ),
                      ],
                    ),
                    addSpace(10),
                    Column(
                      children: List.generate(keys.length, (p) {
                        String title = keys[p]["title"];
                        bool visible = keys[p]["visible"] ?? false;
                        return InkWell(
                          onTap: () {
                            keys[p]["visible"] = !visible;
                            setState(() {});
                          },
                          child: Container(
                            padding: EdgeInsets.all(5),
                            margin: EdgeInsets.only(bottom: 5),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    title,
                                    style: TextStyle(
                                        fontSize: 14,
                                        color: black.withOpacity(.5),
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                addSpaceWidth(5),
                                checkBox(visible, checkColor: black)
                              ],
                            ),
                          ),
                        );
                      }),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      }),
    );
  }
}
