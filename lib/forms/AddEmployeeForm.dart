import 'dart:convert';

import 'package:Loopin/EmailApi.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shimmer/shimmer.dart';

import 'ShowEmployees.dart';

class AddEmployeeForm extends StatefulWidget {
  const AddEmployeeForm(
      {Key key, this.bm, this.readOnly = false, this.assign = false})
      : super(key: key);
  final BaseModel bm;
  final bool readOnly;
  final bool assign;

  @override
  _AddEmployeeFormState createState() => _AddEmployeeFormState();
}

class _AddEmployeeFormState extends State<AddEmployeeForm> {
  final scrollController = ScrollController();

  final fNameController = TextEditingController();
  final mNameController = TextEditingController();
  final lNameController = TextEditingController();
  final employeeEmailController = TextEditingController();
  final employeeTelController = TextEditingController();
  final employeeAddressController = TextEditingController();
  final clientAddressController = TextEditingController();
  final noteController = TextEditingController();
  List status = ["Available", "Busy", "Off-Station"];
  List days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];
  List months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  List availableSkills = appSettingsModel.getList("skills");

  String selectedState;
  String selectedCity;

  List selectedShifts = [];
  List selectedSkills = [];
  List availableDays = [];
  List availableMonths = [];

  List availableShifts = [
    "First Shift",
    "Second Shift",
    "Third/Overnight Shift",
  ];

  // BaseModel model = BaseModel();
  BaseModel model;
  bool readOnly = false;
  bool assign = false;

  List<BaseModel> assignedInfo = [];
  int employeesNeeded = 0;
  bool showNoteBtn = false;

  List<Map> get generalForms => [
        {"title": "Required Documents", "path": "docUploads"},
        {"title": "Employee Profile", "path": "empProfile"},
        {"title": "Direct Deposit Employee Form", "path": "depositApp"},
        {"title": "Blood-Borne Pathogens", "path": "bbpForm"},
        {"title": "Employment Application", "path": "employmentApp"},
        {"title": "Form 1-9", "path": "eligibilityForm"},
        {"title": "New Hire Information", "path": "hireApp"},
        {"title": "Accutrace Consent", "path": "consentForm"},
      ];

  // List formsKeys = [];
  String formKey = "";

  Map<String, List<Map>> get formsFormatted => {
        "General Forms: (This applies to all employees)": generalForms,
        "Certified Nursing Assistant (CNA)": generalForms
          ..addAll([
            {
              "title": "Certified Nursing Assistant (CNA) Offer Agreement",
              "path": "cnaAgreement",
              "showAmount": true,
              "type": EmailApi.TYPE_AGREEMENT_CNA
            },
            {"title": "Training Program Form", "path": "trainingProgram"},
            {"title": "CNA Skill Checklist", "path": "cnaSkillCheck"},
            {"title": "Company Policy", "path": "companyPolicy"},
            {"title": "Infection Control Training", "path": "infectionControl"},
          ]),
        "Caregivers": generalForms
          ..addAll([
            {
              "title": "Caregiver Offer Agreement",
              "path": "careAgreement",
              "showAmount": true,
              "type": EmailApi.TYPE_AGREEMENT_CARE
            },
            {"title": "Training Program Form", "path": "trainingProgram"},
            {"title": "Company Policy", "path": "companyPolicy"},
            {"title": "Infection Control Training", "path": "infectionControl"},
          ]),
        "MedTechs": generalForms
          ..addAll([
            {
              "title": "MedTechs Offer Agreement",
              "path": "medAgreement",
              "showAmount": true,
              "type": EmailApi.TYPE_AGREEMENT_MED
            },
            {"title": "Training Program Form", "path": "trainingProgram"},
            {"title": "Company Policy", "path": "companyPolicy"},
            {"title": "Infection Control Training", "path": "infectionControl"},
          ]),
        "Licensed Practical Nurse (LPN)": generalForms
          ..addAll([
            {
              "title": "LPN Offer Agreement",
              "path": "lpnAgreement",
              "showAmount": true,
              "type": EmailApi.TYPE_AGREEMENT_LPN
            },
            {"title": "Training Program Form", "path": "trainingProgram"},
            {"title": "Company Policy", "path": "companyPolicy"},
            {"title": "Infection Control Training", "path": "infectionControl"},
          ]),
        "Registered Nurse (RN)": generalForms
          ..addAll([
            {
              "title": "Registered Nurse Offer Agreement",
              "path": "rnAgreement",
              "showAmount": true,
              "type": EmailApi.TYPE_AGREEMENT_RN
            },
            {"title": "Training Program Form", "path": "trainingProgram"},
            {"title": "Company Policy", "path": "companyPolicy"},
            {"title": "Infection Control Training", "path": "infectionControl"},
          ]),
        "Receptionist": generalForms
          ..addAll([
            {
              "title": "Receptionist Offer Agreement",
              "path": "receptionAgreement",
              "showAmount": true,
              "type": EmailApi.TYPE_AGREEMENT_RECEPTION
            },
          ]),
        "Cooks": generalForms
          ..addAll([
            {
              "title": "Cooks Offer Agreement",
              "path": "cookAgreement",
              "showAmount": true,
              "type": EmailApi.TYPE_AGREEMENT_COOK
            },
            {"title": "Company Policy", "path": "companyPolicy"},
            {"title": "Infection Control Training", "path": "infectionControl"},
          ]),
        "Dining Assistant": generalForms
          ..addAll([
            {
              "title": "Dining Assistant Offer Agreement",
              "path": "dinningAgreement",
              "showAmount": true,
              "type": EmailApi.TYPE_AGREEMENT_DINNING
            },
            {"title": "Training Program Form", "path": "trainingProgram"},
            {"title": "Company Policy", "path": "companyPolicy"},
            {"title": "Infection Control Training", "path": "infectionControl"},
          ]),
        "Others": generalForms
          ..addAll([
            {
              "title": "Offer Agreement",
              "path": "otherAgreement",
              "showAmount": true,
              "type": EmailApi.TYPE_AGREEMENT_OTHER
            },
            {"title": "Offer letter", "path": "trainingProgram"},
          ]),
      };

  bool hasSkill(List keys) {
    bool value = false;
    if (null == model) return false;
    for (var key in keys) {
      if (model
          .getList(SKILLS)
          .map((e) => e.toString().toUpperCase())
          .toList()
          .contains(key.toString().toUpperCase())) {
        value = true;
        break;
      }
    }
    return value;
  }

  List formsToFill = [];

  List amountItems = [];
  final amountController = TextEditingController();

  List types = ["Active", "InActive"];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    readOnly = widget.readOnly;
    assign = widget.assign;
    noteController.addListener(() {
      String text = noteController.text;
      showNoteBtn = text.isNotEmpty;
      setState(() {});
    });

    if (null != widget.bm) {
      model = widget.bm;
      fNameController.text = model.getString(FIRST_NAME);
      mNameController.text = model.getString(MIDDLE_NAME);
      lNameController.text = model.getString(LAST_NAME);
      employeeEmailController.text = model.getString(EMAIL);
      employeeTelController.text = model.getString(TELEPHONE);
      employeeAddressController.text = model.getString(ADDRESS);
      noteController.text = model.getString("note");
      selectedShifts = model.getList(SHIFTS);
      selectedSkills = model.getList(SKILLS);
      availableDays = model.getList(DAYS);
      availableMonths = model.getList(MONTHS);
      selectedState = model.getString(STATE);
      selectedCity = model.getString(CITY);
      assignedInfo = model.getListModel(ASSIGNED_INFO);
      employeesNeeded = model.getInt(EMPLOYEES_NEEDED);
      formsToFill = model.getList(FORMS_TO_FILL);
      formKey = model.getString(FORM_KEY);
    }
  }

  Color get boxColor {
    int status = model.getInt(STATUS);
    if (status == PENDING) return red;
    if (status == APPROVED) return green;
    return blue0;
  }

  String get title {
    int status = model.getInt(STATUS);
    if (status == PENDING) return "Pending Approval!";
    if (status == APPROVED) return "Employee Approved!";
    return "Email Sent";
  }

  String get message {
    int status = model.getInt(STATUS);
    if (status == PENDING)
      return "Email application forms to this applicant to complete application process.";
    if (status == APPROVED) return "This applicant has been Approved!";
    return "You have have emailed applications to this applicant to complete application process.";
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: black.withOpacity(0),
      child: Stack(
        children: [
          GestureDetector(
            onTap: () {
              Navigator.pop(context, readOnly ? model : null);
            },
            // child: Container(
            //   color: red,
            // ),
          ),
          LayoutBuilder(builder: (c, box) {
            bool isLargeScreen = box.maxWidth > 850;
            // final size = getScreenWidth(c);
            final size = box.maxWidth;
            final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

            return RawScrollbar(
              controller: scrollController,
              isAlwaysShown: true,
              thumbColor: Colors.white,
              radius: Radius.circular(20),
              thickness: 20,
              interactive: true,
              child: SingleChildScrollView(
                controller: scrollController,
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8),
                    child: Container(
                      decoration: BoxDecoration(
                          color: white,
                          borderRadius: BorderRadius.circular(15)),
                      padding: EdgeInsets.all(30),
                      margin: EdgeInsets.only(
                          top: 20,
                          bottom: 20,
                          left: ((0.12) * size),
                          right: ((0.12) * size)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: Text(
                                  "Employee Information",
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              if (!readOnly)
                                Container(
                                  margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                  child: TextButton(
                                    onPressed: () {
                                      String fName = fNameController.text;
                                      String mName = mNameController.text;
                                      String lName = lNameController.text;
                                      String email =
                                          employeeEmailController.text;
                                      String tel = employeeTelController.text;
                                      String address =
                                          employeeAddressController.text;
                                      String note = noteController.text;

                                      if (fName.isEmpty) {
                                        showError(context,
                                            "Enter Employee First Name!");
                                        return;
                                      }

                                      if (lName.isEmpty) {
                                        showError(context,
                                            "Enter Employee Last Name!");
                                        return;
                                      }
                                      if (email.isEmpty) {
                                        showError(
                                            context, "Enter Employee Email!");
                                        return;
                                      }
                                      if (tel.isEmpty) {
                                        showError(
                                            context, "Enter Employee Tel!");
                                        return;
                                      }
                                      if (address.isEmpty) {
                                        showError(
                                            context, "Enter Employee address!");
                                        return;
                                      }

                                      if (null == selectedState) {
                                        showError(
                                            context, "Choose Your State!");
                                        return;
                                      }

                                      if (null == selectedCity) {
                                        showError(context, "Choose Your City!");
                                        return;
                                      }

                                      // if (zip.isEmpty) {
                                      //   showError(context, "Enter State ZipCode!");
                                      //   return;
                                      // }

                                      if (selectedShifts.isEmpty) {
                                        showError(
                                            context, "Choose employee shift!");
                                        return;
                                      }

                                      if (selectedSkills.isEmpty) {
                                        showError(
                                            context, "Choose employee skill!");
                                        return;
                                      }

                                      if (availableDays.isEmpty) {
                                        showError(context,
                                            "Choose days of availability!");
                                        return;
                                      }

                                      if (availableMonths.isEmpty) {
                                        showError(context,
                                            "Choose months of availability!");
                                        return;
                                      }

                                      String search = "${fName.toLowerCase()}"
                                          " ${lName.toLowerCase()} "
                                          " ${address.toLowerCase()} "
                                          "${selectedState.toLowerCase()} ";

                                      List tags = [];
                                      tags.addAll(selectedSkills);
                                      tags.addAll(selectedShifts);
                                      tags.add(selectedState);

                                      showProgress(true, context,
                                          msg: "Please wait...");
                                      String id = getRandomId();
                                      if (null != widget.bm)
                                        id = widget.bm.getObjectId();

                                      BaseModel model = BaseModel();
                                      model.put(OBJECT_ID, id);
                                      model.put(FIRST_NAME, fName);
                                      model.put(MIDDLE_NAME, mName);
                                      model.put(LAST_NAME, lName);
                                      model.put(EMAIL, email);
                                      model.put(TELEPHONE, tel);
                                      model.put(ADDRESS, address);
                                      model.put(SHIFTS, selectedShifts);
                                      model.put(SKILLS, selectedSkills);
                                      model.put(DAYS, availableDays);
                                      model.put(MONTHS, availableMonths);

                                      model.put(STATE, selectedState);
                                      model.put(CITY, selectedCity);
                                      if (null == widget.bm) {
                                        model.put(STATUS, APPROVED);
                                        model.put(TYPE, TYPE_OLD_EMPLOYEES);
                                      }
                                      model.put("tags", tags);
                                      // model.put("note", note);
                                      // model.put(ZIPCODE, zip);
                                      model.put(
                                          SEARCH, getSearchString(search));

                                      model.saveItem(EMPLOYEE_BASE, false,
                                          document: id, onComplete: (e) {
                                        showProgress(
                                          false,
                                          context,
                                        );
                                        if (null != e) {
                                          showErrorDialog(context, e);
                                          return;
                                        }
                                        appSettingsModel
                                          ..putInList(
                                            EMPLOYEE_BASE_COUNT,
                                            id,
                                          )
                                          ..updateItems();

                                        showMessage(
                                          context,
                                          Icons.check,
                                          green,

                                          null != widget.bm
                                              ? "Employee Updated!"
                                              : "Employee Added!",
                                          null != widget.bm
                                              ? "You have successful updated this Employee Account"
                                              : "You have successful added this Employee",

                                          // "Employee Added!",
                                          // "You have successful added this Employee",
                                          // cancellable: false,
                                          delayInMilli: 700,
                                          clickYesText: "Nice!",
                                          cancellable: false,
                                          onClicked: (_) {
                                            Navigator.pop(context, model);
                                          },
                                        );
                                      });
                                    },
                                    child: Center(
                                      child: Text(
                                        null != widget.bm
                                            ? "Update Employee"
                                            : "Add Employee",
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    style: TextButton.styleFrom(
                                        primary: white,
                                        backgroundColor: green_dark,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        minimumSize: Size(120, 50)),
                                  ),
                                ),
                              addSpaceWidth(10),
                              if (null != model)
                                Row(
                                  children: List.generate(types.length, (p) {
                                    String title = types[p];
                                    bool active = model.getInt(STATUS) ==
                                        (p == 0 ? APPROVED : PENDING);
                                    return InkWell(
                                      onTap: () {
                                        if (!active)
                                          model
                                            ..put(STATUS,
                                                p == 0 ? APPROVED : PENDING)
                                            // ..put(EMPLOYEE_STATUS, p)
                                            ..updateItems();
                                        setState(() {});
                                      },
                                      child: Container(
                                        width: 100,
                                        height: 40,
                                        alignment: Alignment.center,
                                        margin: EdgeInsets.fromLTRB(2, 0, 2, 0),
                                        padding: EdgeInsets.all(8),
                                        decoration: BoxDecoration(
                                            color: active ? blue : white,
                                            border: Border.all(
                                                color: black.withOpacity(.1)),
                                            borderRadius:
                                                BorderRadius.circular(8)),
                                        child: Text(
                                          title,
                                          style: TextStyle(
                                              color: active ? white : black,
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    );
                                  }),
                                ),
                              if (!userModel.isCoordinator &&
                                  formsToFill.isNotEmpty)
                                Container(
                                  margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                  child: TextButton(
                                    onPressed: () {
                                      Navigator.pushReplacementNamed(context,
                                          "/applicationForm??${model.getObjectId()}??admin??true");
                                    },
                                    child: Center(
                                      child: Text(
                                        "View Forms",
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    style: TextButton.styleFrom(
                                        primary: white,
                                        backgroundColor: black,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        minimumSize: Size(120, 50)),
                                  ),
                                ),
                              addSpaceWidth(10),
                              Container(
                                // margin: EdgeInsets.only(bottom: 15),
                                child: TextButton(
                                  onPressed: () {
                                    Navigator.pop(
                                        context, readOnly ? model : null);
                                  },
                                  child: Icon(
                                    Icons.clear,
                                    size: 15,
                                  ),
                                  style: TextButton.styleFrom(
                                      padding: EdgeInsets.zero,
                                      primary: white,
                                      backgroundColor: red,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      minimumSize: Size(40, 40)),
                                ),
                              )
                            ],
                          ),
                          if (null != model)
                            infoBox(boxColor, title, message,
                                margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                                clickText:
                                    //model.getInt(STATUS) == APPROVED ? null :
                                    model.getInt(STATUS) == SENT
                                        ? "Resend Forms"
                                        : "Send Email", onClick: () {
                              String amount = amountController.text;

                              if (model.getInt(STATUS) == SENT) {
                                formsToFill = model.getList(FORMS_TO_FILL);
                                // model
                                //   ..put(STATUS, PENDING)
                                //   ..updateItems();
                                setState(() {});
                                return;
                              }

                              if (formsToFill.isEmpty) {
                                showError(context,
                                    "Choose a form to email to applicant!");
                                return;
                              }

                              if (amountItems.isNotEmpty && amount.isEmpty) {
                                showError(context, "Enter Offer amount!");
                                return;
                              }
                              yesNoDialog(context, "Email Forms?",
                                  "Once email is sent to this applicant the status of the application gets updated with respect to the progress of forms completed.",
                                  () async {
                                model
                                  ..put(STATUS, SENT)
                                  // ..put(FORMS_UNLOCKED, true)
                                  ..put(OFFER_AMOUNT, amount)
                                  ..put(FORMS_SENT,
                                      DateTime.now().millisecondsSinceEpoch)
                                  ..put(
                                      FORMS_EXPIRY,
                                      DateTime.now()
                                          .add(Duration(days: 3))
                                          .millisecondsSinceEpoch)
                                  ..put(FORMS_TO_FILL, formsToFill)
                                  ..put(FORM_KEY, formKey)
                                  ..updateItems();
                                setState(() {});
                                String baseUrl =
                                    appSettingsModel.getString("baseUrl");

                                String id = model.getObjectId();
                                String url = "${baseUrl}applicationForm??$id";
                                String data = "Click the link to "
                                    "complete your application process $url";

                                EmailApi.sendEmail(
                                    type: EmailApi.TYPE_EMPLOYEE,
                                    url: url,
                                    toName: fNameController.text,
                                    toEmail: employeeEmailController.text,
                                    message: data);
                              });
                            }),

                          // formsToSend

                          if (null != model) ...[
                            addSpace(10),
                            Text(
                              "Choose forms to email",
                              style: TextStyle(
                                  fontSize: 13,
                                  color: black.withOpacity(.5),
                                  fontWeight: FontWeight.bold),
                            ),
                            addSpace(10),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              // alignment: WrapAlignment.start,
                              children:
                                  // List.generate(formsFormatted.keys.length, (p) {
                                  List.generate(
                                      formsFormatted.keys
                                          .where((e) => hasSkill([e]))
                                          .toList()
                                          .length, (p) {
                                // final key = formsFormatted.keys.toList()[p];
                                final key = formsFormatted.keys
                                    .where((e) => hasSkill([e]))
                                    .toList()[p];

                                // final formsToSend = formsToSend
                                //     .where((e) => !e["showAmount"] == true)
                                //     .toList();

                                final formsToSend = [];

                                // // final formsToSend = formsFormatted[key];
                                // final formsToSend =
                                //     List.from(formsFormatted[key]);

                                final forms = formsFormatted[key];
                                for (var item in forms) {
                                  if (item['showAmount'] == true) continue;
                                  formsToSend.add(item);
                                }

                                bool active = formKey == key;
                                // bool active = false;

                                return GestureDetector(
                                  onTap: () {
                                    if (active) {
                                      formKey = "";
                                      formsToFill.clear();
                                    } else {
                                      formKey = key;
                                      formsToFill = formsToSend;
                                    }
                                    setState(() {});
                                  },
                                  child: Container(
                                    height: active ? null : 60,
                                    margin: EdgeInsets.only(bottom: 8),
                                    padding: EdgeInsets.all(6),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        addSpace(5),
                                        Text(
                                          key,
                                          style: TextStyle(
                                              fontSize: 18,
                                              color: active ? white : black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        addSpace(10),
                                        if (active)
                                          Wrap(
                                            children: List.generate(
                                                formsToSend.length, (p) {
                                              String title =
                                                  formsToSend[p]["title"];
                                              bool active = false;

                                              return Container(
                                                height: 50,
                                                width: 180,
                                                margin: EdgeInsets.all(2),
                                                padding: EdgeInsets.all(2),
                                                child: Text(
                                                  title,
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      color: active
                                                          ? white
                                                          : black,
                                                      fontWeight: active
                                                          ? FontWeight.bold
                                                          : FontWeight.normal),
                                                ),
                                                alignment: Alignment.center,
                                                decoration: BoxDecoration(
                                                    color:
                                                        active ? black : white,
                                                    boxShadow: [
                                                      BoxShadow(
                                                          color: black
                                                              .withOpacity(.05),
                                                          spreadRadius: 2,
                                                          blurRadius: 5)
                                                    ],
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8)),
                                              );
                                            }),
                                          ),
                                        addSpace(5),
                                      ],
                                    ),
                                    alignment: Alignment.centerLeft,
                                    decoration: BoxDecoration(
                                        color: active ? black : white,
                                        boxShadow: [
                                          BoxShadow(
                                              color: black.withOpacity(.05),
                                              spreadRadius: 2,
                                              blurRadius: 5)
                                        ],
                                        borderRadius: BorderRadius.circular(8)),
                                  ),
                                );
                              }),
                            ),
                            addSpace(10),
                            // if (amountItems.isNotEmpty)
                          ],

                          if (null != model)
                            Builder(
                              builder: (c) {
                                bool pending =
                                    model.getInt(OFFER_STATUS) == PENDING;
                                String msg =
                                    "You have emailed the Offer/Agreement letter to this employee";
                                if (pending)
                                  msg =
                                      "You are yet to send an email of the Offer/Agreement letter to this employee";

                                final key = formsFormatted.keys
                                    .where((e) => hasSkill([e]))
                                    .toList()
                                    .first;
                                final formsToSend = formsFormatted[key];

                                final fetch = formsToSend
                                    .where((e) => e["showAmount"] == true)
                                    .toList();
                                int type = fetch.first["type"];

                                return Container(
                                  padding: EdgeInsets.all(10),
                                  margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                  decoration: BoxDecoration(
                                      color: pending ? red : green,
                                      boxShadow: [
                                        BoxShadow(
                                            color: black.withOpacity(.05),
                                            spreadRadius: 2,
                                            blurRadius: 5)
                                      ],
                                      borderRadius: BorderRadius.circular(8)),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        msg,
                                        style: textStyle(true, 16, white),
                                      ),
                                      addSpace(10),
                                      // if (pending)
                                      inputField(
                                          "Offer Amount", amountController,
                                          isNum: true, onChanged: (s) {
                                        setState(() {});
                                      }),
                                      addSpace(10),
                                      // if (pending)
                                      Container(
                                        margin: EdgeInsets.only(top: 10),
                                        height: 30,
                                        child: RaisedButton(
                                          onPressed: () {
                                            String amount =
                                                amountController.text;
                                            if (amount.isEmpty) {
                                              showError(context,
                                                  "Enter Offer Amount");
                                              return;
                                            }

                                            EmailApi.sendEmail(
                                                type: type,
                                                toName: model.getFullName(),
                                                toEmail: model.getEmail(),
                                                amount: amount);
                                            model
                                              ..put(OFFER_STATUS, SENT)
                                              ..updateItems();
                                            amountController.clear();
                                            setState(() {});

                                            showMessage(
                                              context,
                                              Icons.check,
                                              green,
                                              "Email Sent!",
                                              "You have successful sent offer email to this Employee",
                                              delayInMilli: 700,
                                              clickYesText: "Nice!",
                                              cancellable: false,
                                              onClicked: (_) {
                                                setState(() {});
                                              },
                                            );
                                          },
                                          child: Text(
                                            pending
                                                ? "Send Offer"
                                                : "Resend Offer",
                                            style: textStyle(true, 14, black),
                                          ),
                                          color: white,
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                        ),
                                      )
                                    ],
                                  ),
                                );
                              },
                            ),

                          addSpace(20),
                          Row(
                            children: [
                              Expanded(
                                child: inputField("First Name", fNameController,
                                    onChanged: (s) {
                                  setState(() {});
                                }, readOnly: readOnly),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child: inputField("Last Name", lNameController,
                                    onChanged: (s) {
                                  setState(() {});
                                }, readOnly: readOnly),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child:
                                    inputField("Email", employeeEmailController,
                                        onChanged: (s) {
                                  setState(() {});
                                }, readOnly: readOnly),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: inputField(
                                  "Employee Tel No",
                                  employeeTelController,
                                  readOnly: readOnly,
                                  onChanged: (s) {
                                    setState(() {});
                                  },
                                  isNum: true,
                                ),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child: inputField("Employees Address",
                                    employeeAddressController,
                                    readOnly: readOnly, onChanged: (s) {
                                  setState(() {});
                                }),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: clickField(
                                    "State",
                                    selectedState,
                                    readOnly
                                        ? null
                                        : () {
                                            showListDialog(context,
                                                usaStates.keys.toList(), (_) {
                                              selectedState =
                                                  usaStates.keys.toList()[_];
                                              setState(() {});
                                            }, returnIndex: true);
                                          }),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child: clickField(
                                    "City",
                                    selectedCity,
                                    readOnly
                                        ? null
                                        : () {
                                            if (null == selectedState) {
                                              showError(context,
                                                  "Select a state first");
                                              return;
                                            }
                                            showListDialog(
                                                context,
                                                usaStates[selectedState]
                                                    .toList(), (_) {
                                              selectedCity =
                                                  usaStates[selectedState]
                                                      .toList()[_];
                                              setState(() {});
                                            }, returnIndex: true);
                                          }),
                              )
                              //
                            ],
                          ),
                          addSpace(30),
                          Text(
                            "Work Shift",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          addSpace(5),
                          Text(
                            "Choose shift periods",
                            style: TextStyle(
                                fontSize: 13,
                                color: black.withOpacity(.5),
                                fontWeight: FontWeight.bold),
                          ),
                          addSpace(10),
                          Wrap(
                            children:
                                List.generate(availableShifts.length, (p) {
                              String title = availableShifts[p];
                              bool active = selectedShifts.contains(title);
                              // bool active = p == currentSkill;
                              return GestureDetector(
                                onTap: () {
                                  if (readOnly) return;
                                  if (active) {
                                    selectedShifts.remove(title);
                                  } else {
                                    selectedShifts.add(title);
                                  }
                                  setState(() {});
                                },
                                child: Container(
                                  height: 40,
                                  width: 150,
                                  margin: EdgeInsets.all(2),
                                  child: Text(
                                    title,
                                    style: TextStyle(
                                        color: active ? white : black,
                                        fontWeight: active
                                            ? FontWeight.bold
                                            : FontWeight.normal),
                                  ),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: active ? black : white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: black.withOpacity(.05),
                                            spreadRadius: 2,
                                            blurRadius: 5)
                                      ],
                                      borderRadius: BorderRadius.circular(8)),
                                ),
                              );
                            }),
                          ),
                          addSpace(20),
                          Text(
                            "Skill Set",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          addSpace(5),
                          Text(
                            "Choose skill set required for this work",
                            style: TextStyle(
                                fontSize: 13,
                                color: black.withOpacity(.5),
                                fontWeight: FontWeight.bold),
                          ),
                          addSpace(10),
                          Wrap(
                            children:
                                List.generate(availableSkills.length, (p) {
                              String title = availableSkills[p];
                              bool active = selectedSkills.contains(title);
                              // bool active = p == currentSkill;
                              return GestureDetector(
                                onTap: () {
                                  if (readOnly) return;
                                  if (active) {
                                    selectedSkills.remove(title);
                                  } else {
                                    selectedSkills.add(title);
                                  }
                                  setState(() {});
                                },
                                child: Container(
                                  height: 40,
                                  width: 190,
                                  margin: EdgeInsets.all(2),
                                  child: Text(
                                    title,
                                    style: TextStyle(
                                        color: active ? white : black,
                                        fontSize: 12,
                                        fontWeight: active
                                            ? FontWeight.bold
                                            : FontWeight.normal),
                                  ),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: active ? black : white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: black.withOpacity(.05),
                                            spreadRadius: 2,
                                            blurRadius: 5)
                                      ],
                                      borderRadius: BorderRadius.circular(8)),
                                ),
                              );
                            }),
                          ),
                          addSpace(20),
                          Text(
                            "Days Present",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          addSpace(5),
                          Text(
                            "Choose days employee would be available",
                            style: TextStyle(
                                fontSize: 13,
                                color: black.withOpacity(.5),
                                fontWeight: FontWeight.bold),
                          ),
                          addSpace(10),
                          Wrap(
                            children: List.generate(days.length, (p) {
                              String title = days[p];
                              bool active = availableDays.contains(title);
                              return GestureDetector(
                                onTap: () {
                                  if (readOnly) return;
                                  if (active) {
                                    availableDays.remove(title);
                                  } else {
                                    availableDays.add(title);
                                  }
                                  setState(() {});
                                },
                                child: Container(
                                  height: 40,
                                  width: 140,
                                  margin: EdgeInsets.all(2),
                                  child: Text(
                                    title,
                                    style: TextStyle(
                                        color: active ? white : black,
                                        fontWeight: active
                                            ? FontWeight.bold
                                            : FontWeight.normal),
                                  ),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: active ? black : white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: black.withOpacity(.05),
                                            spreadRadius: 2,
                                            blurRadius: 5)
                                      ],
                                      borderRadius: BorderRadius.circular(8)),
                                ),
                              );
                            }),
                          ),
                          addSpace(20),
                          Text(
                            "Months Present",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          addSpace(5),
                          Text(
                            "Choose months employee would be available",
                            style: TextStyle(
                                fontSize: 13,
                                color: black.withOpacity(.5),
                                fontWeight: FontWeight.bold),
                          ),
                          addSpace(10),
                          Wrap(
                            children: List.generate(months.length, (p) {
                              String title = months[p];
                              bool active = availableMonths.contains(title);
                              return GestureDetector(
                                onTap: () {
                                  if (readOnly) return;
                                  if (active) {
                                    availableMonths.remove(title);
                                  } else {
                                    availableMonths.add(title);
                                  }

                                  setState(() {});
                                },
                                child: Container(
                                  height: 40,
                                  width: 140,
                                  margin: EdgeInsets.all(2),
                                  child: Text(
                                    title,
                                    style: TextStyle(
                                        color: active ? white : black,
                                        fontWeight: active
                                            ? FontWeight.bold
                                            : FontWeight.normal),
                                  ),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: active ? black : white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: black.withOpacity(.05),
                                            spreadRadius: 2,
                                            blurRadius: 5)
                                      ],
                                      borderRadius: BorderRadius.circular(8)),
                                ),
                              );
                            }),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            );
          }),
        ],
      ),
    );
  }
}
