import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/main.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class Tax {
  int taxType = -1;
  int fillingType = -1;

  final exemptionNoController = TextEditingController();
  final childrenNoController = TextEditingController();
  final amountController = TextEditingController();

  Tax({Map items}) {
    if (null != items) {
      taxType = items["taxType"];
      fillingType = items["fillingType"];
      exemptionNoController.text = items["exemptionNo"];
      childrenNoController.text = items["childrenNo"];
      amountController.text = items["amount"];
    }
  }

  bool get isValidated {
    bool validated = true;

    if (taxType == -1) return false;
    if (fillingType == -1) return false;
    if (exemptionNoController.text.isEmpty) return false;
    if (childrenNoController.text.isEmpty) return false;
    if (amountController.text.isEmpty) return false;
    return validated;
  }

  Map get items {
    return {
      "taxType": taxType,
      "fillingType": taxType,
      "exemptionNo": exemptionNoController.text,
      "childrenNo": childrenNoController.text,
      "amount": amountController.text,
    };
  }
}

class EmployeeProfile extends StatefulWidget {
  const EmployeeProfile({Key key}) : super(key: key);

  @override
  _EmployeeProfileState createState() => _EmployeeProfileState();
}

class _EmployeeProfileState extends State<EmployeeProfile> {
  // Tax taxInfo = Tax();

  List<Tax> taxInfo = [];

  final ssNameController = TextEditingController();
  final emailController = TextEditingController();
  final telController = TextEditingController();
  final ssnController = TextEditingController();
  final tCardController = TextEditingController();
  final positionController = TextEditingController();
  final departmentController = TextEditingController();
  final rateController = TextEditingController();
  final addressController = TextEditingController();
  final zipController = TextEditingController();

  int hireDate = DateTime.now().millisecondsSinceEpoch;
  int dateOfBirth = 0;

  int employmentIndex = -1;
  int genderIndex = -1;
  int paymentIndex = -1;
  int conditionIndex = -1;

  String selectedState;
  String selectedCity;

  bool termsAccepted = false;

  List payTypes = [
    "Hourly",
    "Salaried Excempt",
    "Salaried Non-Excempt",
    "Commission",
    "Piecework",
  ];

  List employmentTypes = [
    "Full-Time",
    "Part-Time",
    "Seasonal",
  ];

  List conditionTypes = [
    "New Hire",
    "Active Employee Change",
    "Rehire",
  ];

  List genderTypes = [
    "Male",
    "Female",
  ];

  String path = "empProfile";
  BaseModel mainModel;
  BaseModel formModel;
  String get formId => routeDataHolder["/empProfile"];
  String get name => mainModel.getString(NAME);
  List<BaseModel> formsToFill = [];
  List<StreamSubscription> subs = [];

  @override
  initState() {
    super.initState();
    // loadSettings();
    loadItems();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadSettings() async {
    var settingsSub = FirebaseFirestore.instance
        .collection(APP_SETTINGS_BASE)
        .doc(APP_SETTINGS)
        .snapshots()
        .listen((shot) {
      if (shot != null) {
        appSettingsModel = BaseModel(doc: shot);
        List banned = appSettingsModel.getList(BANNED);
      }
    });
    subs.add(settingsSub);
  }

  loadItems() async {
    if (null == formId || formId.isEmpty) {
      showErrorDialog(
          context, "Ops the application link is invalid or has expired!",
          cancellable: false);
      return;
    }

    var sub = FirebaseFirestore.instance
        .collection(EMPLOYEE_BASE)
        .where(OBJECT_ID, isEqualTo: formId)
        .limit(1)
        .snapshots()
        .listen((value) {
      if (value.size == 0) {
        showErrorDialog(
            context, "Ops the application link is invalid or has expired!",
            cancellable: false);
        return;
      }
      mainModel = BaseModel(doc: value.docs[0]);

      taxInfo = mainModel
          .getListModel(TAX_INFO)
          .map((e) => Tax(items: e.items))
          .toList();

      conditionIndex = mainModel.getInt(CONDITION, true);
      ssNameController.text = mainModel.getString(NAME);
      emailController.text = mainModel.getString(EMAIL);
      telController.text = mainModel.getString(TELEPHONE);
      ssnController.text = mainModel.getString(SOCIAL_SECURITY_NUMBER);
      tCardController.text = mainModel.getString(TIME_CARD);
      positionController.text = mainModel.getString(POSITION);
      departmentController.text = mainModel.getString(DEPARTMENT);
      addressController.text = mainModel.getString(ADDRESS);
      // hireDate = mainModel.getInt(HIRE_DATE);
      dateOfBirth = mainModel.getInt(DATE_OF_BIRTH);
      selectedState = mainModel.getString(STATE);
      selectedCity = mainModel.getString(CITY);
      zipController.text = mainModel.getString(ZIPCODE);
      genderIndex = mainModel.getInt(GENDER, true);
      employmentIndex = mainModel.getInt(EMPLOYMENT_TYPE, true);
      rateController.text = mainModel.getString(EMPLOYMENT_PAY_RATE);
      paymentIndex = mainModel.getInt(PAYMENT_TYPE, true);

      formsToFill = mainModel.getListModel(FORMS_TO_FILL);
      int p = formsToFill.indexWhere((e) => e.getString("path") == path);
      if (p != -1) {
        formModel = formsToFill[p];

        // conditionIndex = formModel.getInt(CONDITION, true);
        // ssNameController.text = formModel.getString(NAME);
        // emailController.text = formModel.getString(EMAIL);
        // telController.text = formModel.getString(TELEPHONE);
        // ssnController.text = formModel.getString(SOCIAL_SECURITY_NUMBER);
        // tCardController.text = formModel.getString(TIME_CARD);
        // positionController.text = formModel.getString(POSITION);
        // departmentController.text = formModel.getString(DEPARTMENT);
        // hireDate = formModel.getInt(HIRE_DATE);
        // dateOfBirth = formModel.getInt(DATE_OF_BIRTH);
        // selectedState = formModel.getString(STATE);
        // selectedCity = formModel.getString(CITY);
        // zipController.text = formModel.getString(ZIPCODE);
        // genderIndex = formModel.getInt(GENDER, true);
        // employmentIndex = formModel.getInt(EMPLOYMENT_TYPE, true);
        // rateController.text = formModel.getString(EMPLOYMENT_PAY_RATE);
        // paymentIndex = formModel.getInt(PAYMENT_TYPE, true);

      }
      setState(() {});
    });
    subs.add(sub);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: LayoutBuilder(builder: (c, box) {
              final size = box.maxWidth;
              print("size $size");
              bool isLargeScreen = box.maxWidth > 850;
              final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

              return Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                // padding: EdgeInsets.only(
                //     top: 130, bottom: 50, left: size * 0.2, right: size * 0.2),
                padding: EdgeInsets.only(
                  left: isLargeScreen ? size * 0.15 : 20,
                  right: isLargeScreen ? size * 0.15 : 20,
                  top: 120,
                  bottom: 20,
                ),
                decoration: BoxDecoration(
                    color: isLargeScreen ? scaffoldColor : white,
                    borderRadius: BorderRadius.circular(10)),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: isLargeScreen ? white : null,
                          borderRadius: BorderRadius.circular(10)),
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.pushReplacementNamed(
                                        context, "/applicationForm??$formId");
                                  },
                                  style: TextButton.styleFrom(
                                    minimumSize: Size(40, 40),
                                    backgroundColor: white,
                                    primary: black,
                                    elevation: 4,
                                    // shape: CircleBorder()
                                  ),
                                  child: Icon(Icons.navigate_before)),
                              addSpaceWidth(10),
                              Text(
                                "Employee Information",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),

                          addSpace(20),
                          Text(
                            "Condition",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                          addSpace(10),
                          Wrap(
                            children: List.generate(conditionTypes.length, (p) {
                              String title = conditionTypes[p];
                              bool active = conditionIndex == p;
                              return GestureDetector(
                                onTap: () {
                                  conditionIndex = p;
                                  setState(() {});
                                },
                                child: Container(
                                  height: 40,
                                  width: 180,
                                  margin: EdgeInsets.only(right: 10, bottom: 5),
                                  child: Text(
                                    title,
                                    style: TextStyle(
                                        color: active ? white : black,
                                        fontWeight: active
                                            ? FontWeight.bold
                                            : FontWeight.normal),
                                  ),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: active ? black : white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: black.withOpacity(.05),
                                            spreadRadius: 2,
                                            blurRadius: 5)
                                      ],
                                      borderRadius: BorderRadius.circular(8)),
                                ),
                              );
                            }),
                          ),
                          addSpace(20),
                          Row(
                            children: [
                              Expanded(
                                  child: inputField(
                                      "Name on Social Security Card",
                                      ssNameController, onChanged: (s) {
                                setState(() {});
                              })),
                              addSpaceWidth(10),
                              Expanded(
                                  child: inputField(
                                      "Email Address", emailController,
                                      onChanged: (s) {
                                setState(() {});
                              })),
                              addSpaceWidth(10),
                              Expanded(
                                  child: inputField("Tel No", telController,
                                      isNum: true, onChanged: (s) {
                                setState(() {});
                              })),
                            ],
                          ),
                          Row(
                            children: [
                              Expanded(
                                  child: inputField(
                                      "Social Security Number", ssnController,
                                      onChanged: (s) {
                                setState(() {});
                              })),
                              addSpaceWidth(10),
                              Expanded(
                                  child: inputField(
                                      "Who Referred You", tCardController,
                                      onChanged: (s) {
                                setState(() {});
                              })),
                              addSpaceWidth(10),
                              Expanded(
                                  child:
                                      inputField("Position", positionController,
                                          onChanged: (s) {
                                setState(() {});
                              })),
                            ],
                          ),
                          Row(
                            children: [
                              // Expanded(
                              //   child: inputField(
                              //     "Department",
                              //     departmentController,
                              //     onChanged: (s) {
                              //       setState(() {});
                              //     },
                              //   ),
                              // ),
                              // addSpaceWidth(10),
                              Expanded(
                                child: clickField(
                                    "Today's Date",
                                    hireDate == 0
                                        ? ""
                                        : getSimpleDate(hireDate,
                                            pattern: "EEE MMMM dd, yyyy"),
                                    () async {}),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child: clickField(
                                    "Date of Birth",
                                    dateOfBirth == 0
                                        ? ""
                                        : getSimpleDate(dateOfBirth,
                                            pattern: "EEE MMMM dd, yyyy"),
                                    () async {
                                  final DateTime picked = await showDatePicker(
                                      builder: (ctx, child) {
                                        return Theme(
                                            data: Theme.of(context).copyWith(
                                              colorScheme: ColorScheme.light(
                                                primary: appColor,
                                                onPrimary: Colors
                                                    .black, // header text color
                                                // onSurface: Colors.green, // body text color
                                              ),
                                              buttonTheme: ButtonThemeData(
                                                  textTheme:
                                                      ButtonTextTheme.primary),
                                            ),
                                            child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                child: child));
                                      },
                                      context: context,
                                      initialDate: dateOfBirth == 0
                                          ? DateTime(DateTime.now().year - 18)
                                          : DateTime.fromMillisecondsSinceEpoch(
                                              dateOfBirth),
                                      firstDate: DateTime(1960),
                                      lastDate:
                                          DateTime(DateTime.now().year - 18));

                                  if (picked != null &&
                                      picked.millisecondsSinceEpoch !=
                                          dateOfBirth)
                                    dateOfBirth = picked.millisecondsSinceEpoch;
                                  setState(() {});
                                }),
                              ),
                            ],
                          ),
                          inputField(
                            "Address",
                            addressController,
                            maxLines: 4,
                            onChanged: (s) {
                              setState(() {});
                            },
                          ),
                          Row(
                            children: [
                              Expanded(
                                  child: clickField("State", selectedState, () {
                                showListDialog(context, usaStates.keys.toList(),
                                    (_) {
                                  selectedState = usaStates.keys.toList()[_];
                                  setState(() {});
                                }, returnIndex: true);
                              })),
                              addSpaceWidth(10),
                              Expanded(
                                  child: clickField("City", selectedCity, () {
                                if (null == selectedState) {
                                  showError(context, "Select a state first");
                                  return;
                                }
                                showListDialog(
                                    context, usaStates[selectedState].toList(),
                                    (_) {
                                  selectedCity =
                                      usaStates[selectedState].toList()[_];
                                  setState(() {});
                                }, returnIndex: true);
                              })),
                              addSpaceWidth(10),
                              Expanded(
                                  child: inputField("ZipCode", zipController,
                                      onChanged: (s) {
                                setState(() {});
                              })),
                            ],
                          ),
                          addSpace(20),
                          Text(
                            "Employment Type",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                          addSpace(10),
                          Wrap(
                            children:
                                List.generate(employmentTypes.length, (p) {
                              String title = employmentTypes[p];
                              bool active = employmentIndex == p;
                              return GestureDetector(
                                onTap: () {
                                  employmentIndex = p;
                                  setState(() {});
                                },
                                child: Container(
                                  height: 40,
                                  width: 150,
                                  margin: EdgeInsets.only(right: 10),
                                  padding: EdgeInsets.all(2),
                                  child: Text(
                                    title,
                                    style: TextStyle(
                                        color: active ? white : black,
                                        fontWeight: active
                                            ? FontWeight.bold
                                            : FontWeight.normal),
                                  ),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: active ? black : white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: black.withOpacity(.05),
                                            spreadRadius: 2,
                                            blurRadius: 5)
                                      ],
                                      borderRadius: BorderRadius.circular(8)),
                                ),
                              );
                            }),
                          ),
                          addSpace(20),
                          Text(
                            "Gender",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                          addSpace(10),
                          Wrap(
                            children: List.generate(genderTypes.length, (p) {
                              String title = genderTypes[p];
                              bool active = genderIndex == p;
                              return GestureDetector(
                                onTap: () {
                                  genderIndex = p;
                                  setState(() {});
                                },
                                child: Container(
                                  height: 40,
                                  width: 160,
                                  padding: EdgeInsets.all(2),
                                  margin: EdgeInsets.only(right: 10),
                                  child: Text(
                                    title,
                                    style: TextStyle(
                                        color: active ? white : black,
                                        fontWeight: active
                                            ? FontWeight.bold
                                            : FontWeight.normal),
                                  ),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: active ? black : white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: black.withOpacity(.05),
                                            spreadRadius: 2,
                                            blurRadius: 5)
                                      ],
                                      borderRadius: BorderRadius.circular(8)),
                                ),
                              );
                            }),
                          ),
                          addSpace(20),

                          Row(
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Tax Information",
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      "Add your tax information",
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: black.withOpacity(.5),
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                              if (taxInfo.length < 2)
                                TextButton(
                                  onPressed: () {
                                    if (taxInfo.isNotEmpty &&
                                        !taxInfo[taxInfo.length - 1]
                                            .isValidated) {
                                      showError(context,
                                          "Some fields on the current tax item are empty!");
                                      return;
                                    }
                                    taxInfo.add(Tax());
                                    setState(() {});
                                  },
                                  child: Text(
                                    "Add Tax",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  style: TextButton.styleFrom(
                                      padding: EdgeInsets.zero,
                                      primary: white,
                                      backgroundColor: black,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      minimumSize: Size(160, 50)),
                                )
                            ],
                          ),
                          addSpace(20),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            child: Builder(builder: (ctx) {
                              if (taxInfo.isEmpty)
                                return Container(
                                  padding: EdgeInsets.all(0),
                                  child: DottedBorder(
                                    color: black.withOpacity(.5),
                                    strokeWidth: 1,
                                    borderType: BorderType.RRect,
                                    radius: Radius.circular(10),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Container(
                                          height: 150,
                                          alignment: Alignment.center,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Icon(
                                                Icons.approval,
                                                color: black.withOpacity(.5),
                                                size: 30,
                                              ),
                                              addSpace(10),
                                              Text(
                                                "You have not added any tax information yet",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: black.withOpacity(.5)
                                                    // fontWeight: FontWeight.bold,
                                                    ),
                                              )
                                            ],
                                          )),
                                    ),
                                  ),
                                );

                              return Column(
                                children:
                                    List.generate(taxInfo.length, (index) {
                                  final bm = taxInfo[index];

                                  return Container(
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Tax Withholding",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              Wrap(
                                                children: List.generate(2, (p) {
                                                  String title = p == 0
                                                      ? "Federal"
                                                      : "State";
                                                  bool active = bm.taxType == p;
                                                  return GestureDetector(
                                                    onTap: () {
                                                      if (index != 0 &&
                                                          bm.taxType == p) {
                                                        showError(context,
                                                            "$title already selected!");
                                                        return;
                                                      }

                                                      bm.taxType = p;
                                                      setState(() {});
                                                    },
                                                    child: Container(
                                                      height: 40,
                                                      width: 150,
                                                      margin: EdgeInsets.all(2),
                                                      child: Text(
                                                        title,
                                                        style: TextStyle(
                                                            color: active
                                                                ? white
                                                                : black,
                                                            fontWeight: active
                                                                ? FontWeight
                                                                    .bold
                                                                : FontWeight
                                                                    .normal),
                                                      ),
                                                      alignment:
                                                          Alignment.center,
                                                      decoration: BoxDecoration(
                                                          color: active
                                                              ? (green)
                                                              : white,
                                                          boxShadow: [
                                                            BoxShadow(
                                                                color: black
                                                                    .withOpacity(
                                                                        .05),
                                                                spreadRadius: 2,
                                                                blurRadius: 5)
                                                          ],
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(8)),
                                                    ),
                                                  );
                                                }),
                                              ),
                                              addSpace(10),
                                              Text(
                                                "Filing Status",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              Wrap(
                                                children: List.generate(3, (p) {
                                                  String title = "Single";
                                                  if (p == 1) title = "Married";
                                                  if (p == 2)
                                                    title = "Head of Household";

                                                  bool active =
                                                      bm.fillingType == p;
                                                  return GestureDetector(
                                                    onTap: () {
                                                      bm.fillingType = p;
                                                      setState(() {});
                                                    },
                                                    child: Container(
                                                      height: 40,
                                                      width: 150,
                                                      margin: EdgeInsets.all(2),
                                                      child: Text(
                                                        title,
                                                        style: TextStyle(
                                                            color: active
                                                                ? white
                                                                : black,
                                                            fontWeight: active
                                                                ? FontWeight
                                                                    .bold
                                                                : FontWeight
                                                                    .normal),
                                                      ),
                                                      alignment:
                                                          Alignment.center,
                                                      decoration: BoxDecoration(
                                                          color: active
                                                              ? (green)
                                                              : white,
                                                          boxShadow: [
                                                            BoxShadow(
                                                                color: black
                                                                    .withOpacity(
                                                                        .05),
                                                                spreadRadius: 2,
                                                                blurRadius: 5)
                                                          ],
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(8)),
                                                    ),
                                                  );
                                                }),
                                              ),
                                              addSpace(10),
                                              Row(
                                                children: [
                                                  Expanded(
                                                      child: inputField(
                                                          "Number of Exemptions",
                                                          bm
                                                              .exemptionNoController,
                                                          isNum: true,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                      child: inputField(
                                                          "Number Qualifying Children",
                                                          bm
                                                              .childrenNoController,
                                                          isNum: true,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                      child: inputField(
                                                          "Amount to withhold (\$)",
                                                          bm.amountController,
                                                          isNum: true,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                }),
                              );
                            }),
                          ),

                          // inputField("Pay Rate", rateController,
                          //     onChanged: (s) {
                          //   setState(() {});
                          // }),
                          // addSpace(0),
                          // Text(
                          //   "Payment Type",
                          //   style: TextStyle(
                          //       fontSize: 14, fontWeight: FontWeight.bold),
                          // ),
                          // addSpace(10),
                          // Wrap(
                          //   children: List.generate(payTypes.length, (p) {
                          //     String title = payTypes[p];
                          //     bool active = paymentIndex == p;
                          //     return GestureDetector(
                          //       onTap: () {
                          //         paymentIndex = p;
                          //         setState(() {});
                          //       },
                          //       child: Container(
                          //         height: 40,
                          //         width: 160,
                          //         margin: EdgeInsets.all(2),
                          //         padding: EdgeInsets.all(2),
                          //         child: Text(
                          //           title,
                          //           style: TextStyle(
                          //               color: active ? white : black,
                          //               fontWeight: active
                          //                   ? FontWeight.bold
                          //                   : FontWeight.normal),
                          //         ),
                          //         alignment: Alignment.center,
                          //         decoration: BoxDecoration(
                          //             color: active ? (black) : white,
                          //             boxShadow: [
                          //               BoxShadow(
                          //                   color: black.withOpacity(.05),
                          //                   spreadRadius: 2,
                          //                   blurRadius: 5)
                          //             ],
                          //             borderRadius: BorderRadius.circular(8)),
                          //       ),
                          //     );
                          //   }),
                          // ),
                          addSpace(50),
                        ],
                      ),
                    ),
                    addSpace(20),
                    Container(
                      height: 60,
                      child: TextButton(
                        onPressed: () {
                          String ssName = ssNameController.text;
                          String email = emailController.text;
                          String tel = telController.text;
                          String ssn = ssnController.text;
                          String tCard = tCardController.text;
                          String position = positionController.text;
                          // String dept = departmentController.text;
                          String rate = rateController.text;

                          String address = addressController.text;
                          String zip = zipController.text;

                          int status = STATUS_COMPLETED;

                          if (
                              // conditionIndex == -1 &&
                              ssName.isEmpty &&
                                  email.isEmpty &&
                                  tel.isEmpty &&
                                  ssn.isEmpty &&
                                  tCard.isEmpty &&
                                  position.isEmpty &&
                                  // dept.isEmpty &&
                                  hireDate == 0 &&
                                  dateOfBirth == 0 &&
                                  address.isEmpty &&
                                  null == selectedState &&
                                  null == selectedCity &&
                                  zip.isEmpty &&
                                  employmentIndex == -1 &&
                                  genderIndex == -1
                              //&& !taxInfo.isValidated
                              // rate.isEmpty &&
                              // paymentIndex == -1
                              ) {
                            status = STATUS_UNDONE;
                          }

                          if (
                              // conditionIndex == -1 ||
                              ssName.isEmpty ||
                                  email.isEmpty ||
                                  tel.isEmpty ||
                                  ssn.isEmpty ||
                                  tCard.isEmpty ||
                                  position.isEmpty ||
                                  // dept.isEmpty ||
                                  hireDate == 0 ||
                                  dateOfBirth == 0 ||
                                  address.isEmpty ||
                                  null == selectedState ||
                                  null == selectedCity ||
                                  zip.isEmpty ||
                                  employmentIndex == -1 ||
                                  genderIndex == -1
                              // || !taxInfo.isValidated
                              // rate.isEmpty && paymentIndex == -1
                              ) {
                            status = STATUS_IN_PROGRESS;
                          }

                          mainModel.put(CONDITION, conditionIndex);
                          mainModel.put(EMAIL, email);
                          mainModel.put(NAME, ssName);
                          mainModel.put(TELEPHONE, tel);
                          mainModel.put(SOCIAL_SECURITY_NUMBER, ssn);
                          mainModel.put(TIME_CARD, tCard);
                          mainModel.put(POSITION, position);
                          // mainModel.put(DEPARTMENT, dept);
                          mainModel.put(HIRE_DATE, hireDate);
                          mainModel.put(DATE_OF_BIRTH, dateOfBirth);
                          mainModel.put(ADDRESS, address);
                          mainModel.put(STATE, selectedState);
                          mainModel.put(CITY, selectedCity);
                          mainModel.put(ZIPCODE, zip);
                          mainModel.put(GENDER, genderIndex);
                          mainModel.put(EMPLOYMENT_TYPE, employmentIndex);
                          mainModel.put(EMPLOYMENT_PAY_RATE, rate);
                          mainModel.put(PAYMENT_TYPE, paymentIndex);
                          mainModel.put(
                              TAX_INFO, taxInfo.map((e) => e.items).toList());

                          // formModel.put(CONDITION, conditionIndex);
                          // formModel.put(EMAIL, email);
                          // formModel.put(NAME, ssName);
                          // formModel.put(TELEPHONE, tel);
                          // formModel.put(SOCIAL_SECURITY_NUMBER, ssn);
                          // formModel.put(TIME_CARD, tCard);
                          // formModel.put(POSITION, position);
                          // formModel.put(DEPARTMENT, dept);
                          // formModel.put(HIRE_DATE, hireDate);
                          // formModel.put(DATE_OF_BIRTH, dateOfBirth);
                          // formModel.put(ADDRESS, address);
                          // formModel.put(STATE, selectedState);
                          // formModel.put(CITY, selectedCity);
                          // formModel.put(ZIPCODE, zip);
                          // formModel.put(GENDER, genderIndex);
                          // formModel.put(EMPLOYMENT_TYPE, employmentIndex);
                          // formModel.put(EMPLOYMENT_PAY_RATE, rate);
                          // formModel.put(PAYMENT_TYPE, paymentIndex);
                          formModel.put(STATUS, status);

                          int p = formsToFill
                              .indexWhere((e) => e.getString("path") == path);
                          if (p != -1) {
                            formsToFill[p] = formModel;
                          }

                          mainModel.put(FORMS_TO_FILL,
                              formsToFill.map((e) => e.items).toList());
                          mainModel.updateItems();

                          showMessage(
                            context,
                            Icons.check,
                            green,
                            "Form Submitted!",
                            "You have successful completed your employee application form",
                            // cancellable: false,
                            delayInMilli: 700,
                            clickYesText: "Go Back",
                            cancellable: false,
                            onClicked: (_) {
                              // Navigator.pop(context, true);
                              Navigator.pushReplacementNamed(
                                  context, "/applicationForm??$formId");
                            },
                          );
                        },
                        child: Center(
                          child: Text(
                            "Save Information".toUpperCase(),
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            minimumSize: Size(300, 50)),
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
          appBarHeader(context)
        ],
      ),
    );
  }
}
