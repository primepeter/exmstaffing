import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EmailApi.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class AddSkillsForm extends StatefulWidget {
  @override
  _AddSkillsFormState createState() => _AddSkillsFormState();
}

class _AddSkillsFormState extends State<AddSkillsForm> {
  final skillController = TextEditingController();
  int currentSkill = -1;
  List skills = appSettingsModel.getList(SKILLS);

  List<StreamSubscription> subs = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // loadSettings();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadSettings() async {
    var settingsSub = FirebaseFirestore.instance
        .collection(APP_SETTINGS_BASE)
        .doc(APP_SETTINGS)
        .snapshots()
        .listen((shot) {
      if (shot != null) {
        appSettingsModel = BaseModel(doc: shot);
        setState(() {});
      }
    });
    subs.add(settingsSub);
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: black.withOpacity(.8),
      child: LayoutBuilder(builder: (c, box) {
        bool isLargeScreen = box.maxWidth > 850;
        // final size = getScreenWidth(c);
        final size = box.maxWidth;
        final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

        return SingleChildScrollView(
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: Container(
                decoration: BoxDecoration(
                    color: white, borderRadius: BorderRadius.circular(15)),
                padding: EdgeInsets.all(30),
                margin: EdgeInsets.only(
                    top: 20,
                    bottom: 20,
                    left: ((0.12) * size),
                    right: ((0.12) * size)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            "Skills Settings",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Close",
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.bold),
                          ),
                          style: TextButton.styleFrom(
                              padding: EdgeInsets.zero,
                              primary: white,
                              backgroundColor: red,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              minimumSize: Size(100, 50)),
                        ),
                        addSpaceWidth(10),
                        TextButton(
                          onPressed: () {
                            String skill = skillController.text;

                            if (skill.isEmpty) {
                              showError(context, "Nothing to save or update!");
                              return;
                            }

                            if (currentSkill == -1) {
                              skills.add(skill);
                            } else {
                              skills[currentSkill] = skill;
                            }

                            appSettingsModel
                              ..put(SKILLS, skills)
                              ..updateItems();

                            skillController.clear();
                            currentSkill = -1;
                            setState(() {});
                          },
                          child: Center(
                            child: Text(
                              currentSkill == -1
                                  ? "Save Skill"
                                  : "Update Skill",
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold),
                            ),
                          ),
                          style: TextButton.styleFrom(
                              primary: white,
                              backgroundColor: green_dark,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              minimumSize: Size(100, 50)),
                        ),
                      ],
                    ),
                    addSpace(20),

                    inputField(
                      "Skill Title",
                      skillController,
                      onChanged: (s) {
                        setState(() {});
                      },
                    ),

                    addSpace(30),
                    Text(
                      "All Skills",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    addSpace(5),
                    //if (!readOnly)
                    Text(
                      "Choose a skill to update",
                      style: TextStyle(
                          fontSize: 13,
                          color: black.withOpacity(.5),
                          fontWeight: FontWeight.bold),
                    ),
                    addSpace(20),
                    Wrap(
                      children: List.generate(skills.length, (p) {
                        String title = skills[p];
                        bool active = currentSkill == p;
                        return Container(
                          margin: EdgeInsets.all(5),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  currentSkill = p;
                                  skillController.text = title;
                                  setState(() {});
                                },
                                child: Container(
                                  height: 40,
                                  width: 200,
                                  padding: EdgeInsets.all(5),
                                  child: Text(
                                    title,
                                    style: TextStyle(
                                        color: active ? white : black,
                                        fontWeight: active
                                            ? FontWeight.bold
                                            : FontWeight.normal),
                                  ),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: active ? black : white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: black.withOpacity(.05),
                                            spreadRadius: 2,
                                            blurRadius: 5)
                                      ],
                                      borderRadius: BorderRadius.circular(8)),
                                ),
                              ),
                              addSpaceWidth(10),
                              Container(
                                // margin: EdgeInsets.only(bottom: 15),
                                child: TextButton(
                                  onPressed: () {
                                    skills.remove(title);
                                    appSettingsModel
                                      ..putInList(SKILLS, title, add: false)
                                      ..updateItems();
                                    setState(() {});
                                  },
                                  child: Text(
                                    "Remove",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  style: TextButton.styleFrom(
                                      padding: EdgeInsets.zero,
                                      primary: white,
                                      backgroundColor: red,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      minimumSize: Size(90, 40)),
                                ),
                              )
                            ],
                          ),
                        );
                      }),
                    ),
                    addSpace(20),
                  ],
                ),
              ),
            ),
          ),
        );
      }),
    );
  }
}
