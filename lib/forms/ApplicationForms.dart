import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/HomePage.dart';
// import 'package:Loopin/HomePage.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/main.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ApplicationForms extends StatefulWidget {
  const ApplicationForms({Key key}) : super(key: key);

  @override
  _ApplicationFormsState createState() => _ApplicationFormsState();
}

class _ApplicationFormsState extends State<ApplicationForms>
    with WidgetsBindingObserver {
  List<BaseModel> itemList = [];
  bool isBusy = true;
  bool noData = true;

  BaseModel model;
  String get formId => routeDataHolder["/applicationForm"];
  String get name => model.getString(FIRST_NAME);
  List<BaseModel> formsToFill = [];
  List<StreamSubscription> subs = [];

  int formExpiry = 0;
  int formSent = 0;
  bool formsSecured = false;
  // bool formsUnLocked = false;
  bool formsUnLocked = isAdmin;
  bool formsExpired = false;
  String formsPassword = "";

  final passwordController = TextEditingController();
  final passwordController2 = TextEditingController();
  bool passVisible = false;
  bool passVisible2 = false;

  String errorMsg = "";

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    loadItems();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    // if (state == AppLifecycleState.detached) {
    //   model
    //     ..put(FORMS_UNLOCKED, false)
    //     ..updateItems();
    // }
  }

  loadItems() async {
    print("formId $formId");
    print("formId $routeDataHolder");
    if (null == formId || formId.isEmpty) {
      errorMsg = "Ops the application link is invalid or has expired!";
      isBusy = false;
      setState(() {});
      return;
    }

    var sub = FirebaseFirestore.instance
        .collection(EMPLOYEE_BASE)
        .where(OBJECT_ID, isEqualTo: formId)
        .limit(1)
        .snapshots()
        .listen((value) async {
      // String deviceId = await PlatformDeviceId.getDeviceId;

      if (value.size == 0) {
        errorMsg = "Ops the application link is invalid or has expired!";
        isBusy = false;
        setState(() {});
        return;
      }

      model = BaseModel(doc: value.docs[0]);
      String devId = model.getString(DEVICE_ID);
      formsSecured = model.getBoolean(FORMS_SECURED);
      // formsUnLocked = model.getBoolean(FORMS_UNLOCKED);
      formsPassword = model.getString(FORMS_PASSWORD);
      formExpiry = model.getInt(FORMS_EXPIRY);
      formSent = model.getInt(FORMS_SENT);
      int time = model.getTime();

      final dt = DateTime.fromMillisecondsSinceEpoch(formSent);
      int inDays = DateTime.now().difference(dt).inDays;

      if (isAdmin) {
        formsExpired = false;
        formsSecured = true;
      } else {
        formsExpired = inDays > 3;
      }

      formsToFill = model.getListModel(FORMS_TO_FILL);
      isBusy = false;
      if (formsToFill.isEmpty) formsExpired = true;
      setState(() {});
      // checkExpiry();
      print("app form isAdmin $isAdmin formsUnLocked $formsUnLocked");
      if (!isAdmin) checkLogin();
    });
    subs.add(sub);
  }

  checkExpiry() async {
    formSent = model.getInt(FORMS_SENT);
    final dt = DateTime.fromMillisecondsSinceEpoch(formSent);
    int inDays = DateTime.now().difference(dt).inDays;
    formsExpired = inDays > 3;
    setState(() {});
  }

  checkLogin() async {
    print("checkLogin");
    final pref = await SharedPreferences.getInstance();
    final dFormId = pref.getString("formId");
    final formPass = pref.getString("formPass");
    final time = pref.getInt("time");
    final dt = DateTime.fromMillisecondsSinceEpoch(time);
    int inHours = DateTime.now().difference(dt).inHours;
    // int inDays = DateTime.now().difference(dt).inMinutes;
    // formsUnLocked = (dFormId == formId) && (inDays < 3);
    formsUnLocked = (dFormId == formId) && (inHours < 1);
    if (isAdmin) formsUnLocked = (inHours < 1);
    setState(() {});
  }

  logLogin() async {
    final pref = await SharedPreferences.getInstance();
    pref.setString("formId", formId);
    pref.setString("formPass", formsPassword);
    pref.setInt("time", DateTime.now().millisecondsSinceEpoch);
    formsUnLocked = !formsUnLocked;
    setState(() {});
  }

  String getStatusMsg(int status) {
    if (status == STATUS_UNDONE)
      return "You are yet to start filling this form.";
    if (status == STATUS_IN_PROGRESS)
      return "You are yet to complete this form.";
    return "You have completed this form.";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          LayoutBuilder(builder: (c, box) {
            final size = box.maxWidth;
            bool isLargeScreen = box.maxWidth > 850;
            final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

            return Container(
              margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
              // padding: EdgeInsets.only(
              //     top: 130, bottom: 50, left: size * 0.2, right: size * 0.2),
              padding: EdgeInsets.only(
                left: isLargeScreen ? size * 0.25 : 20,
                right: isLargeScreen ? size * 0.25 : 20,
                top: 120,
                bottom: 20,
              ),
              decoration: BoxDecoration(
                  color: isLargeScreen ? scaffoldColor : white,
                  borderRadius: BorderRadius.circular(10)),
              alignment: Alignment.center,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        border: Border(
                            left: BorderSide(
                                color: Colors.deepOrange, width: 4))),
                    padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
                    child: Text(
                      "Application Forms",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  addSpace(20),
                  Expanded(
                    child: Builder(
                      builder: (c) {
                        if (isBusy)
                          return Center(
                              child: SizedBox(
                                  height: 20,
                                  width: 20,
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation(black),
                                  )));

                        if (formsExpired || errorMsg.isNotEmpty)
                          return Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.access_time,
                                  size: 50,
                                ),
                                addSpace(10),
                                Text(
                                    errorMsg.isNotEmpty
                                        ? errorMsg
                                        : "Sorry you no longer access this application because it has expired.",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: black.withOpacity(.5),
                                      fontSize: 14,
                                      // fontWeight: FontWeight.bold
                                    )),
                              ],
                            ),
                          );

                        if (!formsSecured || !formsUnLocked)
                          return Center(
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  border: Border.all(
                                      color: black.withOpacity(.05))),
                              padding: EdgeInsets.all(10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.lock_outline,
                                    size: 50,
                                  ),
                                  addSpace(10),
                                  Text(
                                      !formsSecured
                                          ? "You are required to create a One-Time password to secure your account."
                                          : "You are required to enter your One-Time password to have access to this page.",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        color: black.withOpacity(.5),
                                        fontSize: 14,
                                        // fontWeight: FontWeight.bold
                                      )),
                                  addSpace(10),
                                  inputField(
                                      "Enter Password", passwordController,
                                      isPass: true,
                                      passwordVisible: passVisible,
                                      onChanged: (s) {
                                    setState(() {});
                                  }, onVisiChanged: () {
                                    passVisible = !passVisible;
                                    setState(() {});
                                  }),
                                  if (!formsSecured)
                                    inputField(
                                        "Confirm Password", passwordController2,
                                        isPass: true,
                                        passwordVisible: passVisible2,
                                        onChanged: (s) {
                                      setState(() {});
                                    }, onVisiChanged: () {
                                      passVisible2 = !passVisible2;
                                      setState(() {});
                                    }),
                                  Container(
                                    // height: 60,
                                    alignment: Alignment.center,
                                    margin:
                                        EdgeInsets.only(left: 0, bottom: 15),
                                    child: TextButton(
                                      onPressed: () async {
                                        String pass = passwordController.text;
                                        String pass2 = passwordController2.text;
                                        // String deviceId =
                                        //     await PlatformDeviceId.getDeviceId;

                                        if (pass.isEmpty) {
                                          showError(
                                              context, "Enter Your Password!");
                                          return;
                                        }

                                        if (!formsSecured && pass2.isEmpty) {
                                          showError(context,
                                              "Confirm Your Password!");
                                          return;
                                        }

                                        if (!formsSecured && pass != pass2) {
                                          showError(context,
                                              "Passwords don't match!");
                                          return;
                                        }

                                        if (formsSecured &&
                                            !formsUnLocked &&
                                            pass != formsPassword) {
                                          showError(context,
                                              "Invalid Form Password!");
                                          return;
                                        }

                                        if (!formsSecured) {
                                          model.put(FORMS_SECURED, true);
                                          // model.put(DEVICE_ID, deviceId);
                                          model.put(FORMS_PASSWORD, pass);
                                          model.updateItems();
                                        }
                                        // formsUnLocked = !formsUnLocked;
                                        // model.put(
                                        //     FORMS_UNLOCKED, formsUnLocked);
                                        // model.updateItems();
                                        // setState(() {});

                                        logLogin();
                                      },
                                      child: Center(
                                        child: Text(
                                          (!formsSecured
                                              ? "CREATE PASSWORD"
                                              : "VERIFY PASSWORD"),
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.normal),
                                        ),
                                      ),
                                      style: TextButton.styleFrom(
                                          primary: white,
                                          backgroundColor: black,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          minimumSize: Size(120, 60)),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );

                        return SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: EdgeInsets.only(
                                    left: 10, top: 5, bottom: 5),
                                margin: EdgeInsets.only(bottom: 20),
                                child: Text.rich(
                                  TextSpan(children: [
                                    TextSpan(
                                      text: "Dear ",
                                    ),
                                    TextSpan(
                                      text: "$name, ",
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextSpan(
                                      text:
                                          "\nkindly fill the forms below to complete your application process.",
                                    )
                                  ]),
                                  style: TextStyle(
                                    fontSize: 14,
                                    // fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children:
                                    List.generate(formsToFill.length, (p) {
                                  final bm = formsToFill[p];
                                  String title = bm.getString("title");
                                  String path = bm.getString("path");
                                  int status = bm.getInt(STATUS);
                                  String msg = getStatusMsg(status);

                                  return Container(
                                    padding: EdgeInsets.all(10),
                                    margin: EdgeInsets.only(bottom: 15, top: 0),
                                    decoration: BoxDecoration(
                                        color: white,
                                        border: Border.all(
                                            color: black.withOpacity(.01)),
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    alignment: Alignment.centerLeft,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        // if (formsUnLocked)
                                        //   Container(
                                        //     child: Icon(Icons.lock_outline),
                                        //     margin: EdgeInsets.only(right: 5),
                                        //   ),
                                        checkBox(status == STATUS_COMPLETED,
                                            checkColor: green, size: 10),
                                        addSpaceWidth(5),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                children: [
                                                  Text(
                                                    title,
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                        color: black,
                                                        fontSize: 20,
                                                        fontWeight:
                                                            FontWeight.w900),
                                                  ),
                                                  Spacer(),
                                                  if (isAdmin ||
                                                      model.getInt(STATUS) !=
                                                          APPROVED)
                                                    TextButton(
                                                      onPressed: () {
                                                        if (!formsUnLocked)
                                                          return;

                                                        // Navigator
                                                        //     .pushReplacementNamed(
                                                        //         context,
                                                        //         "/$path??$formId");

                                                        Navigator.pushNamed(
                                                            context,
                                                            "/$path??$formId");
                                                      },
                                                      child: Text(
                                                        status == STATUS_UNDONE
                                                            ? "Start"
                                                            : "Continue",
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                      style: TextButton.styleFrom(
                                                          padding:
                                                              EdgeInsets.zero,
                                                          primary: white,
                                                          backgroundColor:
                                                              green_dark,
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10)),
                                                          minimumSize:
                                                              Size(100, 40)),
                                                    )
                                                ],
                                              ),
                                              // addSpace(2),
                                              Text(
                                                msg,
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                    color: black, fontSize: 12),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                }),
                              ),
                              if (model.getInt(STATUS) != APPROVED)
                                Container(
                                  height: 60,
                                  margin: EdgeInsets.only(top: 20),
                                  child: TextButton(
                                    onPressed: () {
                                      final items = formsToFill
                                          .where((e) =>
                                              e.getInt(STATUS) !=
                                              STATUS_COMPLETED)
                                          .toList();

                                      if (items.isNotEmpty) {
                                        showError(context,
                                            "Some forms are yet to be filled!");
                                        return;
                                      }

                                      yesNoDialog(
                                          context,
                                          "Submit Applications?",
                                          "Are you sure you want to submit this forms for review?",
                                          () async {
                                        model
                                          ..put(
                                              AGREEMENT_DATE,
                                              DateTime.now()
                                                  .millisecondsSinceEpoch)
                                          ..put(STATUS, APPROVED)
                                          ..updateItems();
                                        showMessage(
                                          context,
                                          Icons.check,
                                          green,
                                          "Application Submitted!",
                                          "You have successful completed your application forms",
                                          // cancellable: false,
                                          delayInMilli: 700,
                                          clickYesText: "Go Back",
                                          cancellable: false,
                                          onClicked: (_) {},
                                        );
                                      });
                                    },
                                    child: Center(
                                      child: Text(
                                        "Submit Application".toUpperCase(),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    style: TextButton.styleFrom(
                                        primary: white,
                                        backgroundColor: appColor,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        minimumSize: Size(300, 50)),
                                  ),
                                )
                            ],
                          ),
                        );
                      },
                    ),
                  )
                ],
              ),
            );
          }),
          appBarHeader(context)
        ],
      ),
    );
  }
}
