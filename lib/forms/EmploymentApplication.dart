import 'dart:async';

import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/EdgeAlert.dart';
import 'package:Loopin/HomePage.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/api/Lucro.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/main.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Education {
  final nameController = TextEditingController();
  final locationController = TextEditingController();
  final subjectController = TextEditingController();

  int graduateIndex = -1;
  int yearsCompletedIndex = -1;
  int levelIndex = -1;

  Education({Map items}) {
    if (null != items) {
      nameController.text = items["schoolName"];
      locationController.text = items["schoolLocation"];
      subjectController.text = items["subjectStudied"];
      graduateIndex = items["graduateIndex"];
      yearsCompletedIndex = items["yearsCompletedIndex"];
      levelIndex = items["levelIndex"];
    }
  }

  Map get items {
    return {
      "schoolName": nameController.text,
      "schoolLocation": locationController.text,
      "subjectStudied": subjectController.text,
      "graduateIndex": graduateIndex,
      "yearsCompletedIndex": yearsCompletedIndex,
      "levelIndex": levelIndex,
    };
  }

  bool get isValidated {
    bool validated = true;
    if (nameController.text.isEmpty) return false;
    if (locationController.text.isEmpty) return false;
    if (subjectController.text.isEmpty) return false;
    if (graduateIndex == -1) return false;
    if (yearsCompletedIndex == -1) return false;
    if (levelIndex == -1) return false;
    return validated;
  }
}

class References {
  final nameController = TextEditingController();
  final phoneController = TextEditingController();
  final businessController = TextEditingController();
  final yearsController = TextEditingController();

  References({Map items}) {
    if (null != items) {
      nameController.text = items["employerName"];
      phoneController.text = items["telephone"];
      businessController.text = items["businessName"];
      yearsController.text = items["yearsKnown"];
    }
  }

  Map get items {
    return {
      "employerName": nameController.text,
      "telephone": phoneController.text,
      "businessName": businessController.text,
      "yearsKnown": yearsController.text,
    };
  }

  bool get isValidated {
    bool validated = true;
    if (nameController.text.isEmpty) return false;
    if (phoneController.text.isEmpty) return false;
    if (businessController.text.isEmpty) return false;
    if (yearsController.text.isEmpty) return false;
    return validated;
  }
}

class FormerEmployers {
  int fromDate = 0;
  int toDate = 0;

  final nameController = TextEditingController();
  final phoneController = TextEditingController();
  String state;
  String city;

  final salaryController = TextEditingController();
  final positionController = TextEditingController();
  final reasonController = TextEditingController();

  FormerEmployers({Map items}) {
    if (null != items) {
      nameController.text = items["employerName"];
      phoneController.text = items["telephone"];
      reasonController.text = items["reason"];
      positionController.text = items["position"];
      salaryController.text = items["salary"];
      fromDate = items["fromDate"];
      toDate = items["toDate"];
      state = items["state"];
      city = items["city"];
    }
  }

  Map get items {
    return {
      "employerName": nameController.text,
      "telephone": phoneController.text,
      "fromDate": fromDate,
      "toDate": toDate,
      "state": state,
      "city": city,
      "salary": salaryController.text,
      "position": positionController.text,
      "reason": reasonController.text,
    };
  }

  bool get isValidated {
    bool validated = true;
    if (nameController.text.isEmpty) return false;
    if (phoneController.text.isEmpty) return false;
    if (salaryController.text.isEmpty) return false;
    if (positionController.text.isEmpty) return false;
    if (reasonController.text.isEmpty) return false;
    if (null == state || state.isEmpty) return false;
    if (null == city || city.isEmpty) return false;
    if (fromDate == 0) return false;
    if (toDate == 0) return false;
    return validated;
  }
}

class EmploymentApplication extends StatefulWidget {
  const EmploymentApplication({Key key}) : super(key: key);

  @override
  _EmploymentApplicationState createState() => _EmploymentApplicationState();
}

class _EmploymentApplicationState extends State<EmploymentApplication> {
  final presentAddressController = TextEditingController();
  final presentZipController = TextEditingController();
  final permanentZipController = TextEditingController();

  final phoneController = TextEditingController();
  final nameController = TextEditingController();
  final permanentAddressController = TextEditingController();
  final ssnController = TextEditingController();
  final referredByController = TextEditingController();
  final salaryDesiredController = TextEditingController();
  final presentEmployerController = TextEditingController();
  final appliedWhereController = TextEditingController();

  String permanentState;
  String permanentCity;

  String presentState;
  String presentCity;

  String selectedSkill = "";

  int employmentIndex = -1;
  int vaccinationIndex = -1;
  int negativeIndex = -1;
  bool passVisible = false;
  bool termsAccepted = false;

  List shiftAvailable = [];
  int hireDate = 0;
  int dateOfBirth = 0;
  int appliedWhen = 0;

  String selectedGender = "";

  // int employedIndex = -1;
  int appliedIndex = -1;

  List<StreamSubscription> subs = [];

  int startDate = 0;
  int employmentStartDate = 0;

  int weekendIndex = -1;
  int overTimeIndex = -1;
  int nightIndex = -1;
  final limitationController = TextEditingController();
  int workFullIndex = -1;
  final partTimeController = TextEditingController();
  int terminatedIndex = -1;
  final terminatedController = TextEditingController();

  List educationLevels = [
    "Grammar School",
    "High School",
    "College School",
    "Trade,Business or Correspondence School",
  ];

  List<FormerEmployers> employersInfo = [];
  List<References> referencesInfo = [];
  List<Education> educationInfo = [];

  String path = "employmentApp";
  BaseModel mainModel;
  BaseModel formModel;
  String get formId => routeDataHolder["/employmentApp"];
  String get name => mainModel.getString(NAME);
  List<BaseModel> formsToFill = [];

  @override
  initState() {
    super.initState();
    loadSettings();
    loadItems();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadSettings() async {
    var settingsSub = FirebaseFirestore.instance
        .collection(APP_SETTINGS_BASE)
        .doc(APP_SETTINGS)
        .snapshots()
        .listen((shot) {
      if (shot != null) {
        appSettingsModel = BaseModel(doc: shot);
        List banned = appSettingsModel.getList(BANNED);
        setState(() {});
      }
    });
    subs.add(settingsSub);
  }

  loadItems() async {
    if (null == formId || formId.isEmpty) {
      showErrorDialog(
          context, "Ops the application link is invalid or has expired!",
          cancellable: false);
      return;
    }

    var sub = FirebaseFirestore.instance
        .collection(EMPLOYEE_BASE)
        .where(OBJECT_ID, isEqualTo: formId)
        .limit(1)
        .snapshots()
        .listen((value) {
      if (value.size == 0) {
        showErrorDialog(
            context, "Ops the application link is invalid or has expired!",
            cancellable: false);
        return;
      }
      mainModel = BaseModel(doc: value.docs[0]);
      employersInfo = mainModel
          .getListModel(EMPLOYERS_INFO)
          .map((e) => FormerEmployers(items: e.items))
          .toList();
      referencesInfo = mainModel
          .getListModel(REFERENCES_INFO)
          .map((e) => References(items: e.items))
          .toList();

      educationInfo = mainModel
          .getListModel(EDUCATION_INFO)
          .map((e) => Education(items: e.items))
          .toList();

      phoneController.text = mainModel.getString(TELEPHONE);
      nameController.text = mainModel.getString(NAME);
      startDate = mainModel.getInt(START_DATE);
      presentAddressController.text = mainModel.getString(PRESENT_ADDRESS);
      presentState = mainModel.getString(PRESENT_STATE);
      presentCity = mainModel.getString(PRESENT_CITY);
      presentZipController.text = mainModel.getString(PRESENT_ZIP);

      permanentAddressController.text = mainModel.getString(PERMANENT_ADDRESS);
      permanentState = mainModel.getString(PERMANENT_STATE);
      permanentCity = mainModel.getString(PERMANENT_CITY);
      permanentZipController.text = mainModel.getString(PERMANENT_ZIP);

      ssnController.text = mainModel.getString(SOCIAL_SECURITY_NUMBER);
      referredByController.text = mainModel.getString(REFERRED_BY);

      selectedSkill = mainModel.getString(POSITION);
      salaryDesiredController.text = mainModel.getString(SALARY_DESIRED);
      employmentStartDate = mainModel.getInt(EMPLOYMENT_START_DATE);
      employmentIndex = mainModel.getInt(EMPLOYMENT_INDEX, true);
      presentEmployerController.text = mainModel.getString(PRESENT_EMPLOYER);
      appliedWhen = mainModel.getInt(APPLIED_WHEN);
      appliedWhereController.text = mainModel.getString(APPLIED_WHERE);
      appliedIndex = mainModel.getInt(APPLIED_INDEX, true);

      overTimeIndex = mainModel.getInt(OVERTIME_INDEX, true);
      nightIndex = mainModel.getInt(NIGHT_INDEX, true);
      weekendIndex = mainModel.getInt(WEEKEND_INDEX, true);
      workFullIndex = mainModel.getInt(FULL_TIME_INDEX, true);
      terminatedIndex = mainModel.getInt(TERMINATED_INDEX, true);

      partTimeController.text = mainModel.getString(PART_TIME_REASON);
      terminatedController.text = mainModel.getString(TERMINATED_TIME_REASON);
      limitationController.text = mainModel.getString(LIMITATION_REASON);

      formsToFill = mainModel.getListModel(FORMS_TO_FILL);
      int p = formsToFill.indexWhere((e) => e.getString("path") == path);
      if (p != -1) {
        formModel = formsToFill[p];

        // employersInfo = formModel
        //     .getListModel(EMPLOYERS_INFO)
        //     .map((e) => FormerEmployers(items: e.items))
        //     .toList();
        // referencesInfo = formModel
        //     .getListModel(REFERENCES_INFO)
        //     .map((e) => References(items: e.items))
        //     .toList();
        //
        // educationInfo = formModel
        //     .getListModel(EDUCATION_INFO)
        //     .map((e) => Education(items: e.items))
        //     .toList();
        //
        // phoneController.text = formModel.getString(TELEPHONE);
        // nameController.text = formModel.getString(NAME);
        // startDate = formModel.getInt(START_DATE);
        // presentAddressController.text = formModel.getString(PRESENT_ADDRESS);
        // presentState = formModel.getString(PRESENT_STATE);
        // presentCity = formModel.getString(PRESENT_CITY);
        // presentZipController.text = formModel.getString(PRESENT_ZIP);
        //
        // permanentAddressController.text =
        //     formModel.getString(PERMANENT_ADDRESS);
        // permanentState = formModel.getString(PERMANENT_STATE);
        // permanentCity = formModel.getString(PERMANENT_CITY);
        // permanentZipController.text = formModel.getString(PERMANENT_ZIP);
        //
        // ssnController.text = formModel.getString(SOCIAL_SECURITY_NUMBER);
        // referredByController.text = formModel.getString(REFERRED_BY);
        //
        // selectedSkill = formModel.getString(POSITION);
        // salaryDesiredController.text = formModel.getString(SALARY_DESIRED);
        // employmentStartDate = formModel.getInt(EMPLOYMENT_START_DATE);
        // employmentIndex = formModel.getInt(EMPLOYMENT_INDEX, true);
        // presentEmployerController.text = formModel.getString(PRESENT_EMPLOYER);
        // appliedWhen = formModel.getInt(APPLIED_WHEN);
        // appliedWhereController.text = formModel.getString(APPLIED_WHERE);
        // appliedIndex = formModel.getInt(APPLIED_INDEX, true);
        //
        // overTimeIndex = formModel.getInt(OVERTIME_INDEX, true);
        // nightIndex = formModel.getInt(NIGHT_INDEX, true);
        // weekendIndex = formModel.getInt(WEEKEND_INDEX, true);
        // workFullIndex = formModel.getInt(FULL_TIME_INDEX, true);
        // terminatedIndex = formModel.getInt(TERMINATED_INDEX, true);
        //
        // partTimeController.text = formModel.getString(PART_TIME_REASON);
        // terminatedController.text = formModel.getString(TERMINATED_TIME_REASON);
        // limitationController.text = formModel.getString(LIMITATION_REASON);
      }
      setState(() {});
    });
    subs.add(sub);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // color: black.withOpacity(.5),
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: LayoutBuilder(builder: (c, box) {
              final size = box.maxWidth;
              bool isLargeScreen = box.maxWidth > 850;
              final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

              return Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                padding: EdgeInsets.only(
                  left: isLargeScreen ? size * 0.15 : 20,
                  right: isLargeScreen ? size * 0.15 : 20,
                  top: 120,
                  bottom: 20,
                ),
                decoration: BoxDecoration(
                    color: isLargeScreen ? scaffoldColor : white,
                    borderRadius: BorderRadius.circular(10)),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: isLargeScreen ? white : null,
                          borderRadius: BorderRadius.circular(10)),
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Employment Application",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "We are an equal opportunity employer, dedicated "
                            "to a policy of nondiscrimination in employment on any"
                            "basis including race, color, age, sex, religion,"
                            " national origin, or disability.",
                            style: TextStyle(
                                fontSize: 13, fontWeight: FontWeight.normal),
                          ),
                          addSpace(20),
                          Text(
                            "Personal Information",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          addSpace(20),
                          Row(
                            children: [
                              Expanded(
                                child: inputField(
                                  "Phone",
                                  phoneController,
                                  isNum: true,
                                  onChanged: (s) {
                                    setState(() {});
                                  },
                                ),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child: clickField(
                                    "When do you want to Start work?",
                                    startDate == 0
                                        ? ""
                                        : getSimpleDate(startDate,
                                            pattern: "EEE MMMM dd, yyyy"),
                                    () async {
                                  final DateTime picked = await showDatePicker(
                                      builder: (ctx, child) {
                                        return Theme(
                                            data: Theme.of(context).copyWith(
                                              colorScheme: ColorScheme.light(
                                                primary: appColor,
                                                onPrimary: Colors
                                                    .black, // header text color
                                                // onSurface: Colors.green, // body text color
                                              ),
                                              buttonTheme: ButtonThemeData(
                                                  textTheme:
                                                      ButtonTextTheme.primary),
                                            ),
                                            child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                child: child));
                                      },
                                      context: context,
                                      initialDate: startDate == 0
                                          ? DateTime.now()
                                          : DateTime.fromMillisecondsSinceEpoch(
                                              startDate),
                                      firstDate: DateTime.now(),
                                      lastDate:
                                          DateTime(DateTime.now().year + 2));

                                  if (picked != null &&
                                      picked.millisecondsSinceEpoch !=
                                          startDate)
                                    startDate = picked.millisecondsSinceEpoch;
                                  setState(() {});
                                }),
                              )
                            ],
                          ),
                          inputField("Name", nameController, onChanged: (s) {
                            setState(() {});
                          }),
                          inputField(
                              "Present Address", presentAddressController,
                              maxLines: 4, onChanged: (s) {
                            setState(() {});
                          }),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: clickField("State", presentState, () {
                                  showListDialog(
                                      context, usaStates.keys.toList(), (_) {
                                    presentState = usaStates.keys.toList()[_];
                                    presentCity = null;
                                    setState(() {});
                                  }, returnIndex: true);
                                }),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                  child: clickField("City", presentCity, () {
                                if (null == presentState ||
                                    presentState.isEmpty) {
                                  showError(context, "Select a state first");
                                  return;
                                }
                                showListDialog(
                                    context, usaStates[presentState].toList(),
                                    (_) {
                                  presentCity =
                                      usaStates[presentState].toList()[_];
                                  setState(() {});
                                }, returnIndex: true);
                              })),
                              addSpaceWidth(10),
                              Expanded(
                                  child: inputField(
                                      "ZipCode", presentZipController,
                                      onChanged: (s) {
                                setState(() {});
                              })),
                            ],
                          ),
                          Row(
                            children: [
                              Expanded(
                                  child: inputField(
                                      "Social Security No.", ssnController,
                                      isNum: true, onChanged: (s) {
                                setState(() {});
                              })),
                              addSpaceWidth(10),
                              Expanded(
                                  child: inputField(
                                      "Referred By", referredByController,
                                      onChanged: (s) {
                                setState(() {});
                              })),
                            ],
                          ),
                          addLine(20, black.withOpacity(.03), 0, 20, 0, 20),
                          Text(
                            "Employment Desired",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          addSpace(20),
                          groupFields(
                              "Position Desired?",
                              appSettingsModel.getList("skills"),
                              selectedSkill, (p) {
                            selectedSkill = p;
                            setState(() {});
                          }),
                          Row(
                            children: [
                              Expanded(
                                child: clickField(
                                    "Date you can start",
                                    employmentStartDate == 0
                                        ? ""
                                        : getSimpleDate(employmentStartDate,
                                            pattern: "EEE MMMM dd, yyyy"),
                                    () async {
                                  final DateTime picked = await showDatePicker(
                                      builder: (ctx, child) {
                                        return Theme(
                                            data: Theme.of(context).copyWith(
                                              colorScheme: ColorScheme.light(
                                                primary: appColor,
                                                onPrimary: Colors
                                                    .black, // header text color
                                                // onSurface: Colors.green, // body text color
                                              ),
                                              buttonTheme: ButtonThemeData(
                                                  textTheme:
                                                      ButtonTextTheme.primary),
                                            ),
                                            child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                child: child));
                                      },
                                      context: context,
                                      initialDate: employmentStartDate == 0
                                          ? DateTime.now()
                                          : DateTime.fromMillisecondsSinceEpoch(
                                              employmentStartDate),
                                      firstDate: DateTime.now(),
                                      lastDate:
                                          DateTime(DateTime.now().year + 2));

                                  if (picked != null &&
                                      picked.millisecondsSinceEpoch !=
                                          employmentStartDate)
                                    employmentStartDate =
                                        picked.millisecondsSinceEpoch;
                                  setState(() {});
                                }),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                  child: inputField("Hourly Rate Desired",
                                      salaryDesiredController, isNum: true,
                                      onChanged: (s) {
                                setState(() {});
                              })),
                            ],
                          ),
                          addSpace(10),
                          yesNoField("Are you Employed Now?", employmentIndex,
                              (p) {
                            employmentIndex = p;
                            setState(() {});
                          }),
                          if (employmentIndex == 0)
                            inputField(
                                "Present Employer", presentEmployerController,
                                onChanged: (s) {
                              setState(() {});
                            }),
                          yesNoField(
                              "Have You Ever Applied To This Company Before",
                              appliedIndex, (p) {
                            appliedIndex = p;
                            setState(() {});
                          }),
                          if (appliedIndex == 0)
                            Row(
                              children: [
                                Expanded(
                                  child: inputField(
                                      "Applied Where", appliedWhereController,
                                      onChanged: (s) {
                                    setState(() {});
                                  }),
                                ),
                                addSpaceWidth(10),
                                Expanded(
                                  child: clickField(
                                      "Applied When?",
                                      appliedWhen == 0
                                          ? ""
                                          : getSimpleDate(appliedWhen,
                                              pattern: "EEE MMMM dd, yyyy"),
                                      () async {
                                    final DateTime picked =
                                        await showDatePicker(
                                      builder: (ctx, child) {
                                        return Theme(
                                            data: Theme.of(context).copyWith(
                                              colorScheme: ColorScheme.light(
                                                primary: appColor,
                                                onPrimary:
                                                    black, // header text color
                                                // onSurface: Colors.green, // body text color
                                              ),
                                              buttonTheme: ButtonThemeData(
                                                  textTheme:
                                                      ButtonTextTheme.primary),
                                            ),
                                            child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                child: child));
                                      },
                                      context: context,
                                      initialDate: appliedWhen == 0
                                          ? DateTime(DateTime.now().year - 1)
                                          : DateTime.fromMillisecondsSinceEpoch(
                                              appliedWhen),
                                      firstDate: DateTime(1960),
                                      lastDate: DateTime.now(),
                                    );

                                    if (picked != null &&
                                        picked.millisecondsSinceEpoch !=
                                            appliedWhen)
                                      appliedWhen =
                                          picked.millisecondsSinceEpoch;
                                    setState(() {});
                                  }),
                                )
                              ],
                            ),
                          addLine(20, black.withOpacity(.03), 0, 20, 0, 20),
                          Row(
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Education",
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      "List your educational qualifications.",
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: black.withOpacity(.5),
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                              if (educationInfo.length < 4)
                                TextButton(
                                  onPressed: () {
                                    if (educationInfo.isNotEmpty &&
                                        !educationInfo[educationInfo.length - 1]
                                            .isValidated) {
                                      showError(context,
                                          "Some fields on the current education item are empty!");
                                      return;
                                    }

                                    educationInfo.add(Education());
                                    setState(() {});
                                  },
                                  child: Text(
                                    "Add Education",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  style: TextButton.styleFrom(
                                      padding: EdgeInsets.zero,
                                      primary: white,
                                      backgroundColor: black,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      minimumSize: Size(160, 50)),
                                )
                            ],
                          ),
                          addSpace(20),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            child: Builder(builder: (ctx) {
                              if (educationInfo.isEmpty)
                                return Container(
                                  padding: EdgeInsets.all(0),
                                  child: DottedBorder(
                                    color: black.withOpacity(.5),
                                    strokeWidth: 1,
                                    borderType: BorderType.RRect,
                                    radius: Radius.circular(10),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Container(
                                          height: 150,
                                          alignment: Alignment.center,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Icon(
                                                Icons.school,
                                                color: black.withOpacity(.5),
                                                size: 30,
                                              ),
                                              addSpace(10),
                                              Text(
                                                "You have not added any educational qualifications yet.",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: black.withOpacity(.5)
                                                    // fontWeight: FontWeight.bold,
                                                    ),
                                              )
                                            ],
                                          )),
                                    ),
                                  ),
                                );

                              return Column(
                                children: List.generate(educationInfo.length,
                                    (index) {
                                  final ed = educationInfo[index];

                                  return Container(
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              addSpace(20),
                                              groupFields(
                                                  "Education Level",
                                                  educationLevels,
                                                  ed.levelIndex, (p) {
                                                ed.levelIndex = p;
                                                setState(() {});
                                              }),
                                              addSpace(10),
                                              Row(
                                                children: [
                                                  Expanded(
                                                      child: inputField(
                                                          "School Name",
                                                          ed.nameController,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                      child: inputField(
                                                          "School Location",
                                                          ed.locationController,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: inputField(
                                                        "Subject Studied",
                                                        ed.subjectController,
                                                        onChanged: (s) {
                                                      setState(() {});
                                                    }),
                                                  ),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                    child: yesNoField(
                                                        "Graduate?",
                                                        ed.graduateIndex, (p) {
                                                      ed.graduateIndex = p;
                                                      setState(() {});
                                                    }),
                                                  ),
                                                ],
                                              ),
                                              addSpace(20),
                                              groupFields(
                                                  "Years Completed?",
                                                  [
                                                    "1 Years",
                                                    "2 years",
                                                    "3 years",
                                                    "4 years",
                                                    "5 years"
                                                  ],
                                                  ed.yearsCompletedIndex, (p) {
                                                ed.yearsCompletedIndex = p;
                                                setState(() {});
                                              }),
                                              addSpace(20),
                                            ],
                                          ),
                                        ),
                                        addSpaceWidth(10),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 15),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              TextButton(
                                                onPressed: () {
                                                  educationInfo.removeAt(index);
                                                  setState(() {});
                                                },
                                                child: Text(
                                                  "Remove",
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                style: TextButton.styleFrom(
                                                    padding: EdgeInsets.zero,
                                                    primary: white,
                                                    backgroundColor: red,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        8)),
                                                    minimumSize: Size(140, 50)),
                                              ),
                                              addSpace(10),
                                              TextButton(
                                                onPressed: () {
                                                  if (educationInfo
                                                          .isNotEmpty &&
                                                      !educationInfo[
                                                              educationInfo
                                                                      .length -
                                                                  1]
                                                          .isValidated) {
                                                    showError(context,
                                                        "Some fields on the current education item are empty!");
                                                    return;
                                                  }

                                                  educationInfo
                                                      .add(Education());
                                                  setState(() {});
                                                },
                                                child: Text(
                                                  "Add Another",
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                style: TextButton.styleFrom(
                                                    padding: EdgeInsets.zero,
                                                    primary: white,
                                                    backgroundColor: black,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        8)),
                                                    minimumSize: Size(140, 50)),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  );
                                }),
                              );
                            }),
                          ),
                          addLine(20, black.withOpacity(.03), 0, 20, 0, 20),
                          Row(
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Former Employers",
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      "List last four employers, starting.with last one first.",
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: black.withOpacity(.5),
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                              if (employersInfo.length < 4)
                                TextButton(
                                  onPressed: () {
                                    if (employersInfo.isNotEmpty &&
                                        !employersInfo[employersInfo.length - 1]
                                            .isValidated) {
                                      showError(context,
                                          "Some fields on the current employee item are empty!");
                                      return;
                                    }

                                    employersInfo.add(FormerEmployers());
                                    setState(() {});
                                  },
                                  child: Text(
                                    "Add Former Employers",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  style: TextButton.styleFrom(
                                      padding: EdgeInsets.zero,
                                      primary: white,
                                      backgroundColor: black,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      minimumSize: Size(160, 50)),
                                )
                            ],
                          ),
                          addSpace(20),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            child: Builder(builder: (ctx) {
                              if (employersInfo.isEmpty)
                                return Container(
                                  padding: EdgeInsets.all(0),
                                  child: DottedBorder(
                                    color: black.withOpacity(.5),
                                    strokeWidth: 1,
                                    borderType: BorderType.RRect,
                                    radius: Radius.circular(10),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Container(
                                          height: 150,
                                          alignment: Alignment.center,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Icon(
                                                Icons.people,
                                                color: black.withOpacity(.5),
                                                size: 30,
                                              ),
                                              addSpace(10),
                                              Text(
                                                "You have not added former employers yet",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: black.withOpacity(.5)
                                                    // fontWeight: FontWeight.bold,
                                                    ),
                                              )
                                            ],
                                          )),
                                    ),
                                  ),
                                );

                              return Column(
                                children: List.generate(employersInfo.length,
                                    (index) {
                                  final fm = employersInfo[index];

                                  return Container(
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: clickField(
                                                        "Start date",
                                                        fm.fromDate == 0
                                                            ? ""
                                                            : getSimpleDate(
                                                                fm.fromDate,
                                                                pattern:
                                                                    "EEE MMMM dd, yyyy"),
                                                        () async {
                                                      final picked =
                                                          await showDatePicker(
                                                        builder: (ctx, child) {
                                                          return Theme(
                                                              data: Theme.of(
                                                                      context)
                                                                  .copyWith(
                                                                colorScheme:
                                                                    ColorScheme
                                                                        .light(
                                                                  primary:
                                                                      appColor,
                                                                  onPrimary:
                                                                      black, // header text color
                                                                  // onSurface: Colors.green, // body text color
                                                                ),
                                                                buttonTheme: ButtonThemeData(
                                                                    textTheme:
                                                                        ButtonTextTheme
                                                                            .primary),
                                                              ),
                                                              child: ClipRRect(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              10),
                                                                  child:
                                                                      child));
                                                        },
                                                        context: context,
                                                        initialDate: fm
                                                                    .fromDate ==
                                                                0
                                                            ? DateTime.now()
                                                            : DateTime
                                                                .fromMillisecondsSinceEpoch(
                                                                    fm.fromDate),
                                                        firstDate:
                                                            DateTime(1960),
                                                        lastDate:
                                                            DateTime.now(),
                                                      );
                                                      if (picked != null)
                                                        fm.fromDate = picked
                                                            .millisecondsSinceEpoch;
                                                      setState(() {});
                                                    }),
                                                  ),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                    child: clickField(
                                                        "End date",
                                                        fm.toDate == 0
                                                            ? ""
                                                            : getSimpleDate(
                                                                fm.toDate,
                                                                pattern:
                                                                    "EEE MMMM dd, yyyy"),
                                                        () async {
                                                      final picked =
                                                          await showDatePicker(
                                                        builder: (ctx, child) {
                                                          return Theme(
                                                              data: Theme.of(
                                                                      context)
                                                                  .copyWith(
                                                                colorScheme:
                                                                    ColorScheme
                                                                        .light(
                                                                  primary:
                                                                      appColor,
                                                                  onPrimary:
                                                                      black, // header text color
                                                                  // onSurface: Colors.green, // body text color
                                                                ),
                                                                buttonTheme: ButtonThemeData(
                                                                    textTheme:
                                                                        ButtonTextTheme
                                                                            .primary),
                                                              ),
                                                              child: ClipRRect(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              10),
                                                                  child:
                                                                      child));
                                                        },
                                                        context: context,
                                                        initialDate: fm
                                                                    .toDate ==
                                                                0
                                                            ? DateTime.now()
                                                            : DateTime
                                                                .fromMillisecondsSinceEpoch(
                                                                    fm.toDate),
                                                        firstDate:
                                                            DateTime(1960),
                                                        lastDate:
                                                            DateTime.now(),
                                                      );
                                                      if (picked != null)
                                                        fm.toDate = picked
                                                            .millisecondsSinceEpoch;
                                                      setState(() {});
                                                    }),
                                                  )
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                      child: inputField(
                                                          "Employer Name",
                                                          fm.nameController,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                      child: inputField(
                                                          "Employer Tel",
                                                          fm.phoneController,
                                                          isNum: true,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                ],
                                              ),
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Expanded(
                                                    child: clickField(
                                                        "State", fm.state, () {
                                                      showListDialog(
                                                          context,
                                                          usaStates.keys
                                                              .toList(), (_) {
                                                        fm.state = usaStates
                                                            .keys
                                                            .toList()[_];
                                                        setState(() {});
                                                      }, returnIndex: true);
                                                    }),
                                                  ),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                      child: clickField(
                                                          "City", fm.city, () {
                                                    if (null == fm.state) {
                                                      showError(context,
                                                          "Select a state first");
                                                      return;
                                                    }
                                                    showListDialog(
                                                        context,
                                                        usaStates[fm.state]
                                                            .toList(), (_) {
                                                      fm.city =
                                                          usaStates[fm.state]
                                                              .toList()[_];
                                                      setState(() {});
                                                    }, returnIndex: true);
                                                  })),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                      child: inputField(
                                                          "Hourly Rate",
                                                          fm.salaryController,
                                                          isNum: true,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                ],
                                              ),
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Expanded(
                                                      child: inputField(
                                                          "Position",
                                                          fm.positionController,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                      child: inputField(
                                                          "Reason for leaving",
                                                          fm.reasonController,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        addSpaceWidth(10),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 15),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              TextButton(
                                                onPressed: () {
                                                  employersInfo.removeAt(index);
                                                  setState(() {});
                                                },
                                                child: Text(
                                                  "Remove",
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                style: TextButton.styleFrom(
                                                    padding: EdgeInsets.zero,
                                                    primary: white,
                                                    backgroundColor: red,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        8)),
                                                    minimumSize: Size(140, 50)),
                                              ),
                                              addSpace(10),
                                              TextButton(
                                                onPressed: () {
                                                  if (employersInfo
                                                          .isNotEmpty &&
                                                      !employersInfo[
                                                              employersInfo
                                                                      .length -
                                                                  1]
                                                          .isValidated) {
                                                    showError(context,
                                                        "Some fields on the current employee item are empty!");
                                                    return;
                                                  }

                                                  employersInfo
                                                      .add(FormerEmployers());
                                                  setState(() {});
                                                },
                                                child: Text(
                                                  "Add Another",
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                style: TextButton.styleFrom(
                                                    padding: EdgeInsets.zero,
                                                    primary: white,
                                                    backgroundColor: black,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        8)),
                                                    minimumSize: Size(140, 50)),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  );
                                }),
                              );
                            }),
                          ),
                          addLine(20, black.withOpacity(.03), 0, 20, 0, 20),
                          Row(
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "References",
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      "Give the names of three professional"
                                      " references whom you have known"
                                      " at least one year.",
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: black.withOpacity(.5),
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                              if (referencesInfo.length < 3)
                                TextButton(
                                  onPressed: () {
                                    if (referencesInfo.isNotEmpty &&
                                        !referencesInfo[
                                                referencesInfo.length - 1]
                                            .isValidated) {
                                      showError(context,
                                          "Some fields on the current references item are empty!");
                                      return;
                                    }

                                    referencesInfo.add(References());
                                    setState(() {});
                                  },
                                  child: Text(
                                    "Add References",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  style: TextButton.styleFrom(
                                      padding: EdgeInsets.zero,
                                      primary: white,
                                      backgroundColor: black,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      minimumSize: Size(160, 50)),
                                )
                            ],
                          ),
                          addSpace(20),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            child: Builder(builder: (ctx) {
                              if (referencesInfo.isEmpty)
                                return Container(
                                  padding: EdgeInsets.all(0),
                                  child: DottedBorder(
                                    color: black.withOpacity(.5),
                                    strokeWidth: 1,
                                    borderType: BorderType.RRect,
                                    radius: Radius.circular(10),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Container(
                                          height: 150,
                                          alignment: Alignment.center,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Icon(
                                                Icons.people,
                                                color: black.withOpacity(.5),
                                                size: 40,
                                              ),
                                              addSpace(10),
                                              Text(
                                                "You have not added references yet",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: black.withOpacity(.5)
                                                    // fontWeight: FontWeight.bold,
                                                    ),
                                              )
                                            ],
                                          )),
                                    ),
                                  ),
                                );

                              return Column(
                                children: List.generate(referencesInfo.length,
                                    (index) {
                                  final ref = referencesInfo[index];

                                  return Container(
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                children: [
                                                  Expanded(
                                                      child: inputField(
                                                          "Employer Name",
                                                          ref.nameController,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                      child: inputField(
                                                          "Employer Tel",
                                                          ref.phoneController,
                                                          isNum: true,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                      child: inputField(
                                                          "Business Name",
                                                          ref.businessController,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                  addSpaceWidth(10),
                                                  Expanded(
                                                      child: inputField(
                                                          "Years Known",
                                                          ref.yearsController,
                                                          isNum: true,
                                                          onChanged: (s) {
                                                    setState(() {});
                                                  })),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        addSpaceWidth(10),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 15),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              TextButton(
                                                onPressed: () {
                                                  referencesInfo
                                                      .removeAt(index);
                                                  setState(() {});
                                                },
                                                child: Text(
                                                  "Remove",
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                style: TextButton.styleFrom(
                                                    padding: EdgeInsets.zero,
                                                    primary: white,
                                                    backgroundColor: red,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        8)),
                                                    minimumSize: Size(140, 50)),
                                              ),
                                              addSpace(10),
                                              TextButton(
                                                onPressed: () {
                                                  if (referencesInfo
                                                          .isNotEmpty &&
                                                      !referencesInfo[
                                                              referencesInfo
                                                                      .length -
                                                                  1]
                                                          .isValidated) {
                                                    showError(context,
                                                        "Some fields on the current references item are empty!");
                                                    return;
                                                  }

                                                  referencesInfo
                                                      .add(References());
                                                  setState(() {});
                                                },
                                                child: Text(
                                                  "Add Another",
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                style: TextButton.styleFrom(
                                                    padding: EdgeInsets.zero,
                                                    primary: white,
                                                    backgroundColor: black,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        8)),
                                                    minimumSize: Size(140, 50)),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  );
                                }),
                              );
                            }),
                          ),
                          addLine(20, black.withOpacity(.03), 0, 20, 0, 20),
                          Text(
                            "Other",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          addSpace(20),
                          Row(
                            children: [
                              Expanded(
                                child: yesNoField(
                                    "Work Weekends?", weekendIndex, (p) {
                                  weekendIndex = p;
                                  setState(() {});
                                }),
                              ),
                              addSpaceWidth(10),
                              Expanded(
                                child: yesNoField("Over Time?", overTimeIndex,
                                    (p) {
                                  overTimeIndex = p;
                                  setState(() {});
                                }),
                              ),
                            ],
                          ),
                          yesNoField("Night?", nightIndex, (p) {
                            nightIndex = p;
                            setState(() {});
                          }),
                          inputField(
                              "State any limitations on your working days or hours",
                              limitationController,
                              maxLines: 4, onChanged: (s) {
                            setState(() {});
                          }),
                          yesNoField("Work full time?", workFullIndex, (p) {
                            workFullIndex = p;
                            setState(() {});
                          }),
                          if (workFullIndex == 1)
                            inputField(
                                "If part time, days/hours?", partTimeController,
                                onChanged: (s) {
                              setState(() {});
                            }),
                          yesNoField(
                              "Have you ever been terminated or asked to resign from a job?",
                              terminatedIndex, (p) {
                            terminatedIndex = p;
                            setState(() {});
                          }),
                          if (terminatedIndex == 0)
                            inputField("If yes, what jobs and why?",
                                terminatedController, onChanged: (s) {
                              setState(() {});
                            }),
                          addSpace(50),
                        ],
                      ),
                    ),
                    addSpace(20),
                    InkWell(
                      onTap: () {
                        termsAccepted = !termsAccepted;
                        setState(() {});
                      },
                      child: Container(
                        child: Row(
                          children: [
                            checkBox(termsAccepted),
                            addSpaceWidth(20),
                            Expanded(
                                child: Text(
                              footerTermsText,
                              style: TextStyle(fontSize: 12),
                            ))
                          ],
                        ),
                      ),
                    ),
                    addSpace(20),
                    Container(
                      height: 60,
                      child: TextButton(
                        onPressed: !termsAccepted
                            ? null
                            : () {
                                String phone = phoneController.text;
                                String name = nameController.text;

                                String presentAddress =
                                    presentAddressController.text;
                                String presentZip = presentZipController.text;

                                String permanentAddress =
                                    permanentAddressController.text;

                                String permanentZip =
                                    permanentZipController.text;
                                String ssN = ssnController.text;
                                String referredBy = referredByController.text;

                                String salary = salaryDesiredController.text;

                                String presentEmployer =
                                    presentEmployerController.text;
                                String appliedWhere =
                                    appliedWhereController.text;
                                // String appliedWhen = appliedWhenController.text;
                                String partTime = partTimeController.text;
                                String terminated = terminatedController.text;
                                String limitation = limitationController.text;

                                int status = STATUS_COMPLETED;

                                if (phone.isEmpty &&
                                    startDate == 0 &&
                                    name.isEmpty &&
                                    presentAddress.isEmpty &&
                                    presentState.isEmpty &&
                                    presentCity.isEmpty &&
                                    presentZip.isEmpty &&
                                    // permanentAddress.isEmpty &&
                                    // permanentState.isEmpty &&
                                    // permanentCity.isEmpty &&
                                    // permanentZip.isEmpty &&
                                    ssN.isEmpty &&
                                    referredBy.isEmpty &&
                                    //

                                    selectedSkill.isEmpty &&
                                    employmentStartDate == 0 &&
                                    salary.isEmpty &&
                                    employmentIndex == -1 &&
                                    (employmentIndex == 0 &&
                                        presentEmployer.isEmpty) &&
                                    appliedIndex == -1 &&
                                    (appliedIndex == 0 &&
                                        (appliedWhere.isEmpty ||
                                            appliedWhen == 0)) &&
                                    //
                                    educationInfo.isEmpty &&
                                    employersInfo.isEmpty &&
                                    referencesInfo.isEmpty &&
                                    weekendIndex == -1 &&
                                    overTimeIndex == -1 &&
                                    nightIndex == -1 &&
                                    workFullIndex == -1 &&
                                    (workFullIndex == 1 && partTime.isEmpty) &&
                                    terminatedIndex == -1 &&
                                    (terminatedIndex == 0 &&
                                        terminated.isEmpty) &&
                                    limitation.isEmpty) {
                                  status = STATUS_UNDONE;
                                }

                                if (phone.isEmpty ||
                                    startDate == 0 ||
                                    name.isEmpty ||
                                    presentAddress.isEmpty ||
                                    presentState.isEmpty ||
                                    presentCity.isEmpty ||
                                    presentZip.isEmpty ||
                                    // permanentAddress.isEmpty ||
                                    // permanentState.isEmpty ||
                                    // permanentCity.isEmpty ||
                                    // permanentZip.isEmpty ||
                                    ssN.isEmpty ||
                                    referredBy.isEmpty ||
                                    //
                                    selectedSkill.isEmpty ||
                                    employmentStartDate == 0 ||
                                    salary.isEmpty ||
                                    employmentIndex == -1 ||
                                    (employmentIndex == 0 &&
                                        presentEmployer.isEmpty) ||
                                    appliedIndex == -1 ||
                                    (appliedIndex == 0 &&
                                        (appliedWhere.isEmpty ||
                                            appliedWhen == 0)) ||

                                    //
                                    educationInfo.isEmpty ||
                                    employersInfo.isEmpty ||
                                    referencesInfo.isEmpty ||
                                    weekendIndex == -1 ||
                                    overTimeIndex == -1 ||
                                    nightIndex == -1 ||
                                    workFullIndex == -1 ||
                                    (workFullIndex == 1 && partTime.isEmpty) ||
                                    terminatedIndex == -1 ||
                                    (terminatedIndex == 0 &&
                                        terminated.isEmpty) ||
                                    limitation.isEmpty) {
                                  status = STATUS_IN_PROGRESS;
                                }

                                print("status $status");

                                mainModel.put(TELEPHONE, phone);
                                mainModel.put(NAME, name);
                                mainModel.put(START_DATE, startDate);
                                mainModel.put(PRESENT_ADDRESS, presentAddress);
                                mainModel.put(PRESENT_STATE, presentState);
                                mainModel.put(PRESENT_CITY, presentCity);
                                mainModel.put(PRESENT_ZIP, presentZip);

                                mainModel.put(
                                    PERMANENT_ADDRESS, permanentAddress);
                                mainModel.put(PERMANENT_STATE, permanentState);
                                mainModel.put(PERMANENT_CITY, permanentCity);
                                mainModel.put(PERMANENT_ZIP, permanentZip);

                                mainModel.put(SOCIAL_SECURITY_NUMBER, ssN);
                                mainModel.put(REFERRED_BY, referredBy);

                                mainModel.put(POSITION, selectedSkill);
                                mainModel.put(
                                    EMPLOYMENT_START_DATE, employmentStartDate);
                                mainModel.put(
                                    EMPLOYMENT_INDEX, employmentIndex);
                                mainModel.put(
                                    PRESENT_EMPLOYER, presentEmployer);
                                mainModel.put(SALARY_DESIRED, salary);
                                mainModel.put(APPLIED_WHEN, appliedWhen);
                                mainModel.put(APPLIED_WHERE, appliedWhere);
                                mainModel.put(APPLIED_INDEX, appliedIndex);

                                mainModel.put(OVERTIME_INDEX, overTimeIndex);
                                mainModel.put(NIGHT_INDEX, nightIndex);
                                mainModel.put(WEEKEND_INDEX, weekendIndex);
                                mainModel.put(FULL_TIME_INDEX, workFullIndex);
                                mainModel.put(
                                    TERMINATED_INDEX, terminatedIndex);
                                mainModel.put(PART_TIME_REASON, partTime);
                                mainModel.put(
                                    TERMINATED_TIME_REASON, terminated);
                                mainModel.put(LIMITATION_REASON, limitation);

                                mainModel.put(EDUCATION_INFO,
                                    educationInfo.map((e) => e.items).toList());
                                mainModel.put(
                                    REFERENCES_INFO,
                                    referencesInfo
                                        .map((e) => e.items)
                                        .toList());
                                mainModel.put(EMPLOYERS_INFO,
                                    employersInfo.map((e) => e.items).toList());

                                // if (
                                //     // selectedSkill.isEmpty ||
                                //     //     employmentStartDate == 0 ||
                                //     //     salary.isEmpty ||
                                //     employmentIndex == -1 ||
                                //         (employmentIndex == 0 &&
                                //             presentEmployer.isEmpty)) {
                                //   showError(context, "Its here");
                                // }
                                // return;

                                // formModel.put(TELEPHONE, phone);
                                // formModel.put(NAME, name);
                                // formModel.put(START_DATE, startDate);
                                // formModel.put(PRESENT_ADDRESS, presentAddress);
                                // formModel.put(PRESENT_STATE, presentState);
                                // formModel.put(PRESENT_CITY, presentCity);
                                // formModel.put(PRESENT_ZIP, presentZip);
                                //
                                // formModel.put(
                                //     PERMANENT_ADDRESS, permanentAddress);
                                // formModel.put(PERMANENT_STATE, permanentState);
                                // formModel.put(PERMANENT_CITY, permanentCity);
                                // formModel.put(PERMANENT_ZIP, permanentZip);
                                //
                                // formModel.put(SOCIAL_SECURITY_NUMBER, ssN);
                                // formModel.put(REFERRED_BY, referredBy);
                                //
                                // formModel.put(POSITION, selectedSkill);
                                // formModel.put(
                                //     EMPLOYMENT_START_DATE, employmentStartDate);
                                // formModel.put(
                                //     EMPLOYMENT_INDEX, employmentIndex);
                                // formModel.put(
                                //     PRESENT_EMPLOYER, presentEmployer);
                                // formModel.put(SALARY_DESIRED, salary);
                                // formModel.put(APPLIED_WHEN, appliedWhen);
                                // formModel.put(APPLIED_WHERE, appliedWhere);
                                // formModel.put(APPLIED_INDEX, appliedIndex);
                                //
                                // formModel.put(OVERTIME_INDEX, overTimeIndex);
                                // formModel.put(NIGHT_INDEX, nightIndex);
                                // formModel.put(WEEKEND_INDEX, weekendIndex);
                                // formModel.put(FULL_TIME_INDEX, workFullIndex);
                                // formModel.put(
                                //     TERMINATED_INDEX, terminatedIndex);
                                // formModel.put(PART_TIME_REASON, partTime);
                                // formModel.put(
                                //     TERMINATED_TIME_REASON, terminated);
                                // formModel.put(LIMITATION_REASON, limitation);
                                //
                                // formModel.put(EDUCATION_INFO,
                                //     educationInfo.map((e) => e.items).toList());
                                // formModel.put(
                                //     REFERENCES_INFO,
                                //     referencesInfo
                                //         .map((e) => e.items)
                                //         .toList());
                                // formModel.put(EMPLOYERS_INFO,
                                //     employersInfo.map((e) => e.items).toList());

                                formModel.put(STATUS, status);

                                int p = formsToFill.indexWhere(
                                    (e) => e.getString("path") == path);
                                if (p != -1) {
                                  formsToFill[p] = formModel;
                                }

                                mainModel.put(FORMS_TO_FILL,
                                    formsToFill.map((e) => e.items).toList());
                                mainModel.updateItems();

                                showMessage(
                                  context,
                                  Icons.check,
                                  green,
                                  "Form Submitted!",
                                  "You have successful completed your employment application form",
                                  // cancellable: false,
                                  delayInMilli: 700,
                                  clickYesText: "Go Back",
                                  cancellable: false,
                                  onClicked: (_) {
                                    // Navigator.pop(context, true);
                                    Navigator.pushReplacementNamed(
                                        context, "/applicationForm??$formId");
                                  },
                                );
                              },
                        child: Center(
                          child: Text(
                            "Save Information".toUpperCase(),
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        style: TextButton.styleFrom(
                            primary: white,
                            backgroundColor: appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            minimumSize: Size(300, 50)),
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
          appBarHeader(context)
        ],
      ),
    );
  }
}
