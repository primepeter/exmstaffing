import 'package:dotted_border/dotted_border.dart';
import 'package:Loopin/AppEngine.dart';
import 'package:Loopin/Usa.json.dart';
import 'package:Loopin/assets.dart';
import 'package:Loopin/basemodel.dart';
import 'package:Loopin/widgets/inputField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shimmer/shimmer.dart';

import 'AddEmployeeForm.dart';

class ShowClients extends StatefulWidget {
  @override
  _ShowClientsState createState() => _ShowClientsState();
}

class _ShowClientsState extends State<ShowClients> {
  List<BaseModel> itemList = [];
  List<BaseModel> mainItemList = [];
  List<BaseModel> selections = [];
  bool isBusy = true;
  bool isFiltering = false;

  final searchController = TextEditingController();
  bool showCancel = false;
  bool searching = false;
  FocusNode focusNode = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    handleSearching();
    // loadItems();
  }

  handleSearching() async {
    searchController.addListener(() async {
      List<DocumentSnapshot> docList = [];
      String text = searchController.text.trim().toLowerCase();
      setState(() {});
      if (text.isEmpty) {
        itemList = mainItemList;
        showCancel = false;
        searching = false;
        if (mounted) setState(() {});
        return;
      }
      print("is searching");
      itemList.clear();
      showCancel = true;
      searching = true;
      if (mounted) setState(() {});
      final shots = await FirebaseFirestore.instance
          .collection(USER_BASE)
          .where(PRIVILEGE_INDEX, isEqualTo: 2)
          .where(SEARCH, arrayContains: text.trim())
          .limit(30)
          .get();
      for (var doc in shots.docs) {
        final bm = BaseModel(doc: doc);
        int p = itemList.indexWhere((e) => e.getObjectId() == bm.getObjectId());
        if (p != -1) {
          itemList[p] = bm;
        } else {
          itemList.add(bm);
        }
      }
      searching = false;
      if (mounted) setState(() {});
    });
  }

  loadItems() async {
    QuerySnapshot shots = await FirebaseFirestore.instance
        .collection(EMPLOYEE_BASE)
        .limit(20)
        .get();

    for (var doc in shots.docs) {
      final bm = BaseModel(doc: doc);
      int p = itemList.indexWhere((e) => e.getObjectId() == bm.getObjectId());
      if (p == -1) {
        itemList.add(bm);
      } else {
        itemList[p] = bm;
      }
    }
    mainItemList = itemList;
    isBusy = false;
    setState(() {});
  }

  List keys = [
    // {"title": "Action", "key": "action", "visible": true},
    {"title": "First Name", "key": FIRST_NAME, "visible": true},
    {"title": "Last Name", "key": LAST_NAME, "visible": true},
    {"title": "Email", "key": EMAIL, "visible": false},
    {"title": "Tel No", "key": TELEPHONE, "visible": true},
    // {"title": "Address", "key": ADDRESS, "visible": false},
    // {"title": "State", "key": STATE, "visible": true},
    // {"title": "Status", "key": STATUS, "visible": true},
    // {"title": "Assigned", "key": ASSIGNED_ID, "visible": true},
    // {"title": "SkillSet", "key": SKILLS, "visible": true, "isList": true},
  ];

  List get visibleKeys => keys.where((e) => e["visible"] ?? false).toList();

  List<DataColumn2> headerColumns() {
    return List.generate(visibleKeys.length, (p) {
      String title = visibleKeys[p]["title"];
      bool action = title == "Action";
      return DataColumn2(
        label: Text(
          title,
          style: TextStyle(
              fontSize: 12,
              color: black.withOpacity(.5),
              fontWeight: FontWeight.bold),
        ),
        size: action ? ColumnSize.S : ColumnSize.L,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: black.withOpacity(.8),
      child: LayoutBuilder(builder: (c, box) {
        bool isLargeScreen = box.maxWidth > 850;
        // final size = getScreenWidth(c);
        final size = box.maxWidth;
        final scaffoldColor = Theme.of(context).scaffoldBackgroundColor;

        return Stack(
          children: [
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
            ),
            SingleChildScrollView(
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Container(
                    decoration: BoxDecoration(
                        color: white, borderRadius: BorderRadius.circular(15)),
                    padding: EdgeInsets.all(30),
                    // padding: EdgeInsets.only(left:30,right: 30,bottom: 30),
                    margin: EdgeInsets.only(
                        top: 20,
                        bottom: 20,
                        left: ((0.12) * size),
                        right: ((0.12) * size)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                "Clients of ExmStaffing",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ),
                            addSpaceWidth(10),
                            Container(
                              // margin: EdgeInsets.only(bottom: 15),
                              child: TextButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Icon(
                                  Icons.clear,
                                  size: 15,
                                ),
                                style: TextButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                    primary: white,
                                    backgroundColor: red,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    minimumSize: Size(40, 40)),
                              ),
                            )
                          ],
                        ),
                        addSpace(20),
                        Row(
                          children: [
                            Expanded(
                                child: Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: black.withOpacity(.09),
                                  border:
                                      Border.all(color: black.withOpacity(.04)),
                                  borderRadius: BorderRadius.circular(8)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  addSpaceWidth(10),
                                  Icon(
                                    Icons.search,
                                    color: black.withOpacity(.5),
                                    size: 18,
                                  ),
                                  addSpaceWidth(5),
                                  Expanded(
                                    child: TextField(
                                      controller: searchController,
                                      focusNode: focusNode,
                                      style: TextStyle(
                                          color: black.withOpacity(.5),
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold),
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        isCollapsed: true,
                                        //  contentPadding: EdgeInsets.zero,

                                        hintText:
                                            "Search by Client name or email",
                                        hintStyle: TextStyle(
                                            color: black.withOpacity(.5),
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  if (showCancel)
                                    GestureDetector(
                                      onTap: () {
                                        searchController.clear();
                                        itemList = mainItemList;
                                        showCancel = false;
                                        searching = true;

                                        loadItems();
                                        setState(() {});
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            left: 10, right: 10),
                                        child: Icon(
                                          Icons.close,
                                          color: black.withOpacity(.5),
                                          size: 18,
                                        ),
                                      ),
                                    ),
                                ],
                              ),
                            )),
                          ],
                        ),
                        ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: AnimatedContainer(
                            duration: Duration(milliseconds: 400),
                            child: LinearProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(orange03),
                            ),
                            height: searching ? 2 : 0,
                            margin: EdgeInsets.only(
                              top: searching ? 5 : 0,
                              bottom: searching ? 5 : 0,
                            ),
                          ),
                        ),
                        DataTable2(
                            columnSpacing: 12,
                            horizontalMargin: 12,
                            //minWidth: visibleKeys.length > 5 ? 1200 : 800,
                            showCheckboxColumn: true,
                            columns: headerColumns(),
                            rows: List<DataRow>.generate(itemList.length, (p) {
                              final bm = itemList[p];
                              return DataRow(
                                  cells: List.generate(visibleKeys.length,
                                      (index) {
                                String key = visibleKeys[index]["key"];
                                int p = selections.indexWhere(
                                    (e) => e.getObjectId() == bm.getObjectId());
                                String value = (bm.getString(key));

                                return DataCell(Builder(
                                  builder: (c) {
                                    return Text(
                                      value.isEmpty ? "------" : value,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold),
                                    );
                                  },
                                ), onTap: () {
                                  Navigator.pop(context, bm);
                                });
                              }));
                            })),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
      }),
    );
  }
}
