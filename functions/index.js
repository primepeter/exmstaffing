const functions = require("firebase-functions");
const axios = require("axios");
// The Firebase Admin SDK to access Firestore.
const admin = require("firebase-admin");
admin.initializeApp();
// const db = admin.firestore();


exports.corsRouter = functions.https.onRequest((request, res) => {
  // response.send("Hello from Firebase corsRouter starting!");
  const payload = request.body.payload;
  const reqHeader = request.body.reqHeader;
  const baseURL = request.body.baseURL;
  const reqPath = request.body.reqPath;

  //  const instance = axios.create({
  //    baseURL: "https://api.sendinblue.com/v3",
  //    timeout: 1000,
  //    headers: {
  //      "api-key": "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",
  //      "content-type": "application/json",
  //      "accept": "application/json",
  //    },
  //  });

  //  const data = new TextEncoder().encode(
  //      JSON.stringify({
  //        "to": [
  //          {
  //            "email": "ammaugost@gmail.com",
  //            "name": "John Doe",
  //          },
  //          {
  //            "email": "peter.okachie@gmail.com",
  //            "name": "Peter Okachie",
  //          },
  //        ],
  //        "templateId": 6,
  //        "params": {
  //          "name": "John",
  //          "surname": "Doe",
  //        },
  //      })
  //  );

  const instance = axios.create({
    baseURL: baseURL,
    timeout: 1000,
    headers: reqHeader,
  });

  // axios#post(url[, data[, config]])
  //  instance.post("/smtp/email", data)
  instance.post(reqPath, payload)
      .then(function(response) {
        console.log(response);
        res.send("Successful");
      })
      .catch(function(error) {
        console.log(error);
        //        res.send(error);
        res.status(400).send(error);
      });
});


exports.helloWorld = functions.https.onRequest((request, response) => {
  functions.logger.info("Hello logs!", {structuredData: true});
  response.send("Hello from Firebase!");
});


exports.triggerEmail = functions.firestore.document("/emailBase/{documentId}")
    .onCreate((snap, context) => {
      const payload = snap.data().payload;
      const reqHeader = snap.data().reqHeader;
      const baseURL = snap.data().baseURL;
      const reqPath = snap.data().reqPath;

      console.log(payload);

      const data = new TextEncoder().encode(
          JSON.stringify(payload)
      );

      const instance = axios.create({
        baseURL: baseURL,
        timeout: 1000,
        headers: reqHeader,
      });

      return instance.post(reqPath, data)
          .then(function(response) {
            console.log(response);
            return snap.ref.set({"status": 1}, {merge: true});
          })
          .catch(function(error) {
            console.log(error);
            return snap.ref.set({"status": 2, "error": JSON.stringify(error)}, {merge: true});
          });

      //      return snap.ref.set({uppercase}, {merge: true});
      // [END makeUppercaseBody]
    });

// for auto fixing issues with js code...
// npm install -g eslint
// eslint --fix .
// npm install express --save
